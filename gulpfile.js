// Include Plugins
var gulp = require('gulp');
var autoprefix = require('gulp-autoprefixer') ;
var bower = require('gulp-bower');
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var mainBowerFiles = require('main-bower-files');
var gulpfilter = require('gulp-filter');
var minifyCSS = require('gulp-minify-css');


//Configurations
var config = {
     sassPath: './resources/sass',
     bowerDir: './public/bower_resources' 
}

//Connect Path to Bower Resources and Run Bower's Install Command
gulp.task('bower', function() { 
    return bower()
         .pipe(gulp.dest(config.bowerDir)) 
});

// Concatenate & Minify JS via Bower
gulp.task('bower-libs', function() {

    var jsFilter = gulpfilter('*.js');
    var cssFilter = gulpfilter('**.css');

    return gulp.src(mainBowerFiles())

     // Grab vendor js files from bower_components, minify and push in public/js/lib
    .pipe(jsFilter)
    .pipe(uglify())
    .pipe(rename({
        suffix: ".min"
    }))
    .pipe(gulp.dest('public/js/lib'))
    .pipe(concat('all.min.js'))
    .pipe(gulp.dest('public/js/compiled'))
    .pipe(jsFilter.restore())

     // Grab vendor css files from bower_components, minify and push in public/css/lib
    .pipe(cssFilter)
    .pipe(autoprefix('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
    .pipe(minifyCSS())
    .pipe(rename({
        suffix: ".min"
    }))
    .pipe(gulp.dest('public/css/lib'))
    .pipe(concat('all.min.css'))
    .pipe(gulp.dest('public/css/compiled'))
    .pipe(cssFilter.restore());

});


// Concatenate & Minify Local JS/CSS
gulp.task('local-libs', function() {

    var jsFilter = gulpfilter('*.js');
    var cssFilter = gulpfilter('*.css');

    // grab vendor js files from bower_components, minify and push in /public
    return gulp.src('public/js/*')
    .pipe(jsFilter)
    .pipe(uglify())
    .pipe(rename({
         suffix: ".min"
     }))
    .pipe(gulp.dest('public/js/lib'))
    .pipe(concat('all-local.min.js'))
    .pipe(gulp.dest('public/js/compiled'))
    .pipe(jsFilter.restore())

    .pipe(gulp.src('public/css/*'))
    .pipe(cssFilter)
    .pipe(minifyCSS())
    .pipe(rename({
         suffix: ".min"
     }))
    .pipe(gulp.dest('public/css/lib'))
    .pipe(concat('all-local.min.css'))
    .pipe(gulp.dest('public/css/compiled'));
});


// Compile Our Sass
gulp.task('sass', function() {
    return gulp.src('public/scss/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('public/css'));
});

// Lint Task - Check all js files for errors
gulp.task('lint', function() {
    return gulp.src('public/js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('js/*.js', ['lint', 'scripts']);
    //gulp.watch('scss/*.scss', ['sass']);
});

// Default Task - Just run "gulp" and the following tasks will run
gulp.task('default', ['bower','local-libs','bower-libs']);