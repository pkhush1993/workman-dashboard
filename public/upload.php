<?php
$ds          = DIRECTORY_SEPARATOR;  //1

$storeFolder = 'avatars';   //2
 
if (!empty($_FILES)) {
     
    $tempFile = $_FILES['file']['tmp_name'];          //3             
      
    $targetPath = dirname( __FILE__ ) . $ds. $storeFolder . $ds;  //4

    $file_name = str_replace(' ', '-', uniqid().'-avatars-'.$_FILES['file']['name']);
     
    $targetFile =  $targetPath.$file_name;  //5
 
    move_uploaded_file($tempFile,$targetFile); //6

    echo '/avatars/'.$file_name;
     
} else {
	echo 0;
}
?>