jQuery.noConflict();
(function( $ ) {
	$(function() {

		$.site({debug: false});
		$.site('disable debug');

		// Fades in the Page Content on Load and Deactivates Dimmer/Loader =======================================
		$("#page-content").animate({ opacity : "1.0" },600,function(){
			$('.active.inverted.dimmer').removeClass('active');
		})


		//Semantic UI Defaults =======================================

		// Semantic UI Dropdowns
		$('.ui.dropdown').dropdown({fullTextSearch:true});

		// Semantic UI Checkboxes
		$('.ui.checkbox').checkbox();

		// Semantic UI Tabs
		$('.menu .item').tab({
			history: true,
		});

		// Semantic UI Accordion
		$('.ui.accordion').accordion({exclusive: true});

		// Semantic UI Message
		$('.message .close').on('click', function() {
		  	$(this).closest('.message').fadeOut();
		});

		// Semantic UI Sticky
		$('.ui.sticky').sticky({
			context: '#context'
		});


		// ------------------------------------------------------------

		// Print Button
		$('.print-button').click(function(event) {
			window.print();
		});

		// Dynamic Date Picker
		$('body').on('focus',"input.date", function(){
		    $(this).datepicker();
		});​

		// Dynamic Date Picker
		$('body').on('focus',"input.datetime", function(){
		    $(this).datetimepicker({
				timeFormat: "hh:mm tt"
			});
		});​

		$( "#slider" ).slider();

		//Input Masks
		$('input.date').mask('00/00/0000');
		// $('input.phone').mask('(000) 000-0000');
		$('input.phone').mask('(000) 000-0000 ext 00000');
		$('input.money').maskMoney();


		// Modal Pop Function =========================================
		$.fn.pop = function(modalClass,closable,detachable) {

			//Default Closable to True
			closable = (typeof closable === "undefined") ? true : closable;

			detachable = (typeof detachable === "undefined") ? true : detachable;

			$(this).click(function(event) {
				event.preventDefault();
				// Check to See if Both Detach and Cloasable Options are Set
				if (detachable === false && closable === false){
					$(modalClass).modal({closable  : false,detachable  : false,autofocus : false, observeChanges:true}).modal('show').modal('refresh');
				}
				else if (closable === false) {
					$(modalClass).modal({closable  : false,autofocus : false, observeChanges:true}).modal('show').modal('refresh');
				}
				else if (detachable === false){
					$(modalClass).modal({ detachable:false, observeChanges:true }).modal('show').modal('refresh');
				}

				else{
					$(modalClass).modal({autofocus : false, observeChanges:true}).modal('show').modal('refresh');
				}
			});

		};

		// ------------------------------------------------------------


		// REUSABLE REPEATER FIELD	FUNCTION
		// ------------------------------------------------------------
		var maxInputs = 20; //maximum input boxes allowed
		var fieldCount = 1; //Track the field amounts
		var fieldClass = '.field'; //Track the field amounts
		var increment = true; //Option to Increment Input Names

			$.fn.repeater = function(fieldClass,increment) {
				this.each(function() {

				    var fieldWrapper = $(this);

				    fieldWrapper.find(".add-field").click(function(e) {
				    	if(fieldCount <= maxInputs) //max input boxes allowed
				    	{

				        	var clonedObject = $(fieldClass+':first-of-type', fieldWrapper).clone(true);
				        	clonedObject.removeClass('hidden');
				        	clonedObject.appendTo(fieldWrapper).find('input').removeAttr('disabled').focus().val('');
				        	fieldCount++;//text box added increment
							clonedObject.find('.bid_comment').addClass('dropdown search');
							clonedObject.find('.bid_comment').dropdown({fullTextSearch:true});
							if(increment == true) {
								clonedObject.find('input').each(function () {
			    					var originalName = $(this).attr('name');
			    					$(this).attr('name', originalName + "_" + fieldCount );
			    					// Handle Radio Inputs
			    					if ( $(this).attr('type') == 'radio' ) {
			    						var originalId = $(this).attr('id');
			    						$(this).attr('id', originalId + "_" + fieldCount );
			    					};
								});
							}

				    	}
				    });
				    $(fieldClass + ' .remove-field', fieldWrapper).click(function() {
				    	// Check that it is not the last Field
				        if (fieldWrapper.find(fieldClass).length > 1)
				        {
				        	// Check If Removing Repeater Fields Is Available By Checking for the Hidden input[name=removed_ids]
				        	if (fieldWrapper.find('input[name=removed_ids]').length) {

				        		// Store the Field's ID in a variable
								removeId = $(this).closest(fieldClass).find('input').data('id');

								// Store the Hidden remove_ids Input's Value
								removedIDs = fieldWrapper.find('input[name=removed_ids]').val();

								// If ID's Exists Append to the Val
								if (removedIDs) {
									fieldWrapper.find('input[name=removed_ids]').val(removedIDs+','+removeId);
								}else{
									fieldWrapper.find('input[name=removed_ids]').val(removeId);
								}

				        	};

				        	// Remove DOM Element
				        	$(this).closest(fieldClass).remove();
				        }
				        fieldCount--
				    });
				});
			return this;
			};
		// ------------------------------------------------------------

		// -- Custom Validation functions
		//Semantic UI Custom Form Validation Rule For Pay Periods

		// Validate In Date before Out Date
		$.fn.form.settings.rules.timeEntry = function(value) {
			var dateIn = moment($(this).closest('.time-range').find('.date-in').val(), "MM/DD/YYYY HH:mma");
			var dateOut = moment($(this).closest('.time-range').find('.date-out').val(), "MM/DD/YYYY HH:mma");
			if (dateIn > dateOut){
				return false;
			}
			else{
				return true;
			}
		}

		// Validate date is within range
		$.fn.form.settings.rules.timeRange = function(value, range) {
			value = moment(value, "MM/DD/YYYY HH:mma");
			range = range.split(",");
			range[0] = moment(range[0], "MM/DD/YYYY");
			range[1] = moment(range[1], "MM/DD/YYYY").add(1, "days");
			if(value < range[0]){
				return false;
			}
			if(value >= range[1]){
				return false;
			}
			return true;
		}

		// Validate Value is 0 or a positive number
		$.fn.form.settings.rules.totalHoursValid = function(value){
			var v = Number(value);
			if(isNaN(v)){
				return false;
			}
			if(v < 0){
				return false;
			}
			return true;
		}



	});
})(jQuery);
