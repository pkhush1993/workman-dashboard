(function( $ ) {
    $(function () {
        var TimesheetView = function(view, form, minDate, maxDate, jobsEndpoint) {
            this.DATE_TIME_FORMAT = "MM/DD/YYYY HH:mma";
            this.maxInputs = 100;
            this.increment = true;
            this.view = view;
            this.form = form;
            this.repeater = $(".time-entry-repeater", this.view);
            this.lastRowIndex = this.getRows().length + 1;
            this.removedIds = $("input[name=removed_ids]", this.form);
            this.minDate = minDate;
            this.maxDate = new Date(moment(maxDate).subtract(1,"minutes"));
            this.rangeStart = moment(this.minDate).add(1, "days");
            this.rangeEnd =  moment(this.maxDate).subtract(1, "days");
            this.jobsEndpoint = jobsEndpoint;
            this.updateTimesheetButton = $(".update-timesheet,.save-timesheet",this.form);


            console.log("minDate", this.minDate);
            console.log("rangeStart",new Date(this.rangeStart));
            console.log("maxDate", this.maxDate);
            console.log("rangeEnd",new Date(this.rangeEnd));

            console.log("removedIds",this.removedIds);

            if(this.updateTimesheetButton.length == 0){
                var allFields = $("input, select, .add-field, .remove-field", this.form);
                allFields.attr("disabled","disabled").addClass("disabled");
                console.log("disabled",allFields);
                return;
            }

            this.jobSearchConfig = {
                apiSettings: {
                    url: this.jobsEndpoint + '/' + '{query}',
                },
                maxResults: 50,
                onSelect: function (result) {
                    $(this).closest('.time-entry').find('input.job').val(result.job_id);
                    $(this).closest(".field").removeClass("error");
                },
                onSearchQuery:function(query){

                },
                onResults:function(results){
                    if(results.results.length != 1){
                        $(this).closest(".field").addClass("error");
                        $(this).closest('.time-entry').find('input.job').val(0);
                    }if(results.results.length == 1){
                        var result = results.results[0];
                        var currValue = $(this).closest(".field").find(".prompt").val();
                        if(result.title == currValue) {
                            $(this).closest('.time-entry').find('input.job').val(result.job_id);
                            $(this).closest(".field").removeClass("error");
                        }else{
                            $(this).closest(".field").addClass("error");
                            $(this).closest('.time-entry').find('input.job').val(0);
                        }
                    }
                }
            }

            //this.form.on("submit", $.proxy(function(){
            //    console.log("custom validation",this.form.form);
            //    //this.form.form('add errors', [{
            //    //    "date_in[1]":"error text"
            //    //
            //    //}]);
            //    this.form.form('add error', 'date_in[1]', 'error text');
            //    //$(this).form('remove prompt', 'date_in[1]');
            //    return false;
            //},this));

            //console.log("serch", $('.ui.job-input-ajax.search', this.view))
            $('.ui.job-input-ajax.search', this.view).search(this.jobSearchConfig);

            $('.task').keyup($.proxy(function (e) {
                var task = $(e.currentTarget);
                var row = task.closest('.time-entry');
                var lastRow = row.parent().find(".time-entry").last();
                var isLast = row[0] === lastRow[0];
                var code = e.keyCode || e.which;
                //console.log("code", isLast)
                // Code 9  is the Tab Key
                if (isLast && code == '9') {
                    row.find('.add-field').trigger('click');
                }
            }, this));

            //TODO: THis only calculated on the first focus click
            // Calculate Total Time Per Entry on the Fly
            $('body').on('change keyup', '.time-range input,.break', function () {
                // Set Variables
                var timeEntry = $(this).closest('.time-entry');
                var dateIn = timeEntry.find('.date-in').val();
                var dateOut = timeEntry.find('.date-out').val();
                var breakTime = (timeEntry.find('.break').val() / 60).toFixed(2);

                // Format and Set Date/Time In and Date/Time Out
                var dateStart = moment(dateIn, "MM/DD/YYYY HH:mma");
                var dateEnd = moment(dateOut, "MM/DD/YYYY HH:mma");

                // get total seconds between the times
                var delta = Math.abs(dateStart - dateEnd) / 1000;
                // calculate hours
                var hours = (delta / 3600);

                if(dateStart > dateEnd){
                    hours = hours;
                }

                if (dateIn && dateOut) {
                    timeEntry.find('.total').val((hours - breakTime).toFixed(2)).css('border-color', '#5BBD72');
                }

            });

            this.view.on("click", ".add-field", $.proxy(function (e) {
                this.addRow();
            }, this));

            this.view.on("click", ".remove-field", $.proxy(function (e) {
                var el = $(e.currentTarget);
                var row = el.closest(".time-entry");
                //console.log("row",row[0])
                console.log("REMOVE", row.index())
                this.removeRowAt(row.index()-1); //ignore hidden template row AND removedIds field
            }, this));

            // Dynamic Date Picker
            //this.view.on('blur','input.datetime-entry', $.proxy(function(evt){
            //    var el = $(evt.currentTarget);
            //    el.datetimepicker("destroy");
            //},this));

            this.view.on('focus','input.datetime-entry', $.proxy(function(evt){
                var el = $(evt.currentTarget);
                //console.log("el.datetimepicker",el.datetimepicker("getDate"))
                //if(el.datetimepicker("getDate")){
                //    el.datetimepicker("destroy");
                //    console.log("DESTROY")
                //}
                var itsDateOut = el.hasClass("date-out");
                var dateIn = el.closest(".time-range").find(".date-in");
                var dateInSet = dateIn.val().length > 0;
                var dateInValue = moment(dateIn.val(),"MM/DD/YYYY hh:mma");

                var dateOut = el.closest(".time-range").find(".date-out");
                var dateOutValue = moment(dateOut.val(),"MM/DD/YYYY hh:mma");

                var val = moment(el.val(),"MM/DD/YYYY hh:mma");
                //console.log("el",el[0], dateOut[0], dateOut.val())

                el.css('border', '#00CDF1 1px solid');

                var dtConfig = {
                    constrainInput:true,
                    timeFormat: "h:mmtt",
                    dateFormat:"mm/d/yy",
                    onClose:function(){
                        var currentDate = moment($(this).datetimepicker("getDate"));

                        el.val(currentDate.format("MM/DD/YYYY hh:mma"));
                        $(this).datetimepicker("destroy");
                        $("#ui-datepicker-div").remove();
                    }
                }

                dtConfig["minDate"] = new Date(this.minDate);
                dtConfig["maxDate"] = new Date(this.maxDate);
                //console.log("dtConfig",dtConfig.maxDate)



                //console.log("el",el[0], dateOut[0], dateOut.val())

                if(itsDateOut && dateInValue.isValid()) {
                    //console.log("minDate", this.minDate);
                    //console.log("rangeStart",this.rangeStart);
                    //console.log("dateInValue", new Date(dateInValue));
                    if(new Date(dateInValue) > this.rangeStart){
                        //console.log("USER DATE IN")
                        dtConfig["minDate"] = new Date(dateInValue);
                    }else{
                        //console.log("USER RANGE START")
                        dtConfig["minDate"] = new Date(this.rangeStart);
                    }
                    //dtConfig["minDate"] = new Date(dateInValue);
                }else if (itsDateOut) {
                    dtConfig["minDate"] = new Date(this.rangeStart);
                }

                if(!itsDateOut && dateOutValue.isValid()){
                    //console.log("maxDate", this.maxDate);
                    //console.log("rangeEnd",this.rangeEnd);
                    //console.log("dateOutValue", new Date(dateOutValue));
                    if(new Date(dateOutValue) < this.rangeEnd){
                        console.log("USER DATE Out")
                        dtConfig["maxDate"] = new Date(dateOutValue);;
                    }else{
                        console.log("USER RANGE END")
                        dtConfig["maxDate"] = new Date(this.rangeEnd);
                    }
                    //dtConfig["maxDate"] = new Date(dateOutValue);
                }else if(!itsDateOut){
                    console.log("USER RANGE END")
                    dtConfig["maxDate"] = new Date(this.rangeEnd);

                }
                //console.log("dtConfig",dtConfig.maxDate)

                el.datetimepicker(dtConfig);
                //console.log(el.datetimepicker("option", "minDate"), el.datetimepicker("option", "maxDate"));

                el.datetimepicker("refresh");

                if(itsDateOut) {
                    if (val.isValid()) {
                        if(dateInValue <= val) {
                            el.datetimepicker("set date", el.val());
                        }else{
                            el.datetimepicker("setDate", new Date(dateInValue));
                        }
                    } else if (dateInValue.isValid()) {
                        el.datetimepicker("setDate", new Date(dateInValue));
                    } else {
                        //console.log("4")
                        el.datetimepicker("setDate", new Date());
                    }
                }

                if(!itsDateOut){
                    if(el.val()){
                        el.datetimepicker("set date",el.val())
                    }else{
                        el.datetimepicker("setDate",new Date());
                    }
                }


            },this));

            this.resetValidation();
        }
        //
        TimesheetView.prototype.initRow = function(row) {
            var currentIndex = this.lastRowIndex;
            row.removeClass('hidden');
            row.appendTo(this.repeater).find('input').removeAttr('disabled').val('');
            row.appendTo(this.repeater).find('select').removeAttr('disabled').val('');
            var dateIn = row.find('.date-in');
            var breakField = row.find('.break');
            var jobSearch = row.find('.ui.job-input-ajax-repeater.search');

            breakField.val(0);

            row.find('.date-out').addClass('datetime-entry').attr('data-validate', 'date-out');
            row.find('.total').attr('data-validate', 'totals');
            row.find('.ui.job-input-ajax-repeater.search').search(this.jobSearchConfig);

            if(this.increment == true) {
                row.find('input').each(function () {
                    var originalName = $(this).attr('name');
                    $(this).attr('name', originalName + "["+currentIndex+"]" );
                });
                row.find('select').each(function () {
                    var originalName = $(this).attr('name');
                    $(this).attr('name', originalName + "["+currentIndex+"]" );
                });
            }
            this.lastRowIndex++;
        }

        TimesheetView.prototype.getRows = function(){
            return $(".time-entry",this.view).not(".hidden");
        }

        TimesheetView.prototype.getRowAt = function(index){
            //console.log("GET ROW AT", index);
            return $($(".time-entry",this.view).not(".hidden")[index]);
        }

        TimesheetView.prototype.resetValidation = function(){
            var rows = this.getRows();
            var validation = {};

            //console.log("MOMENT",this.minDate,moment(this.minDate).format("MM/DD/YYYY"))

            rows.each($.proxy(function(index, el){
                el = $(el);
                var dateIn = el.find(".date-in",el);
                var dateOut = el.find(".date-out",el);
                var breakField = el.find(".break",el);
                var total = el.find(".total",el);
                var job = el.find(".job",el);
                var task = el.find(".task",el);

                validation["total_"+index] = {
                    identifier  : total.attr("name"),
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Total Hours is required'
                        },
                        {
                            type   : 'totalHoursValid',
                            prompt : 'Total Hours must be a valid number, >= 0'
                        }
                    ]
                }
                validation["dateIn_"+index] = {
                    identifier  : dateIn.attr("name"),
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Date In '+index+' is required'
                        },
                        {
                            type   : 'timeEntry',
                            prompt : 'Date In Must be Before End Date'
                        },
                        {
                            type   : "timeRange['"+moment(this.minDate).format("MM/DD/YYYY")+"','"+moment(this.rangeEnd).format("MM/DD/YYYY")+"']",
                            prompt : 'Date In must be in current period'
                        },
                        {
                            type:"timesheetRowsOverlap["+index+"]",
                            prompt : 'Date range in row ' +(index+1)+' overlaps with another row'
                        }
                    ]
                }
                validation["dateOut_"+index] = {
                    identifier  : dateOut.attr("name"),
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Date Out '+index+' is required'
                        },
                        {
                            type: 'timeEntry',
                            prompt: 'Date Out Must be After End Date'
                        },
                        {
                            type   : "timeRange['"+moment(this.rangeStart).format("MM/DD/YYYY")+"','"+moment(this.maxDate).format("MM/DD/YYYY")+"']",
                            prompt : 'Date Out must be in current period'
                        },
                    ]
                }
                validation["job_"+index] = {
                    identifier  : job.attr("name"),
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Job is required'
                        },
                        {
                            type:"integer[1..99999999]",
                            prompt:'Job must refer to a valid job'
                        }
                    ]
                }
                validation["task_"+index] = {
                    identifier  : task.attr("name"),
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Task is required'
                        },
                    ]
                }
                validation["break_"+index] = {
                    identifier  : breakField.attr("name"),
                    rules: [
                        {
                            type:"integer[0..99999999]",
                            prompt:'Break must be a positive number'
                        }
                    ]
                }


            },this));



            var settings = {
                rules: {
                    timesheetRowsOverlap: $.proxy(function (value, rowIndex) {
                        var isValid = true;
                        var rows = $(".time-entry",this.form).not(".hidden");
                        var row = $(rows[Number(rowIndex)]);

                        var dateIn = moment(row.find(".date-in").val(),this.DATE_TIME_FORMAT);
                        var dateOut = moment(row.find(".date-out").val(),this.DATE_TIME_FORMAT);
                        //console.log("Custom",dateIn,dateOut);
                        var isZero = dateIn-dateOut == 0;

                        if(!dateIn.isValid() || !dateOut.isValid() || isZero){
                            return true;
                        }


                        rows.each($.proxy(function(index, el){
                            if(rowIndex == index){
                                return;
                            }
                            el = $(el);
                            var elDateIn = moment(el.find(".date-in").val(),this.DATE_TIME_FORMAT);
                            var elDateOut = moment(el.find(".date-out").val(),this.DATE_TIME_FORMAT);
                            var elIsZero = elDateIn-elDateOut == 0;

                            if (elIsZero)
                                return;

                            if(!dateIn.isValid() || !dateOut.isValid()){
                                return;
                            }

                            var overlaps = (dateIn > elDateIn && dateIn < elDateOut || dateOut > elDateIn && dateOut < elDateOut);

                            if(!overlaps){
                                //console.log(dateIn- elDateIn, dateOut-elDateOut);
                                if(dateIn - elDateIn == 0 && dateOut - elDateOut == 0){
                                    overlaps = true;
                                }
                            }
                            if(overlaps)
                                isValid = false;

                            //console.log(overlaps, dateIn.format(this.DATE_TIME_FORMAT) + " - " + dateOut.format(this.DATE_TIME_FORMAT),elDateIn.format(this.DATE_TIME_FORMAT) + " - " + elDateOut.format(this.DATE_TIME_FORMAT));

                        },this));


                        return isValid;
                    },this)
                }
            };

            this.form.form(validation,settings);
        }

        TimesheetView.prototype.newRow = function(){
            return $(".time-entry.hidden", this.view).clone(true);
        }

        TimesheetView.prototype.addRow = function(){
            var fieldCount = this.getRows().length;
            if(fieldCount <= this.maxInputs) //max input boxes allowed
            {
                var clonedObject = this.newRow();
                //console.log("TimesheetView",TimesheetView)
                this.initRow(clonedObject);
                this.resetValidation();
            }
        }


        TimesheetView.prototype.removeRowAt = function(index){

            var row = this.getRowAt(index);
            console.log("row",row);

            removeId = row.find("input").data("id");
            var ids = this.removedIds.val();
            if(this.removedIds.length != 0) {
                ids = (ids == "") ? ids = [] : ids = ids.split(",");
                ids.push(removeId);
                this.removedIds.val(ids.join(","));
                console.log("ids",this.removedIds.val(), removeId)

            }
            row.remove();
            if(this.getRows().length == 0){
                this.addRow();
            }else{
                this.resetValidation();
            }

        }

        window.TimesheetView = TimesheetView;
    });
})(jQuery);
