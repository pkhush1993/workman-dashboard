<?php

namespace App\Services;

use GitElephant\Repository;

class GitHelper extends Repository
{
    /**
     * @var array
     */
    protected $data = [];

    /**
     * @var string
     */
    protected $defaultRelease = 'alpha';

    /**
     * @var int
     */
    protected $shaSize = 8;

    /**
     * @var array
     */
    protected $releases = [
        'master'    =>    'release',
        'staging'   =>     'beta',
        'hotfix'    =>     'edge',
        'feature'   =>     'alpha'
    ];


    /**
     * GitHelper constructor.
     */
    public function __construct()
    {
        parent::__construct(base_path());
    }

    /**
     * Get the human readable version number of the current codebase
     *
     * @return string
     */
    public function getVersion()
    {
        if (empty($this->data)) {
            $this->setupRepositoryVariables();
        }

        return vsprintf('%s-%s (%s)', $this->data);
    }

    /**
     * Setup the data used to create the version number
     */
    protected function setupRepositoryVariables()
    {
        $this->data = [
            'version'  => $this->getLastTag() ? $this->getLastTag()->getName() : '',
            'release'  => $this->getRelease(),
            'sha'      => $this->getSha(),
        ];
    }

    /**
     * Get commit sha
     *
     * @return string
     */
    protected function getSha()
    {
        return str_limit($this->getCommit()->getSha(), $this->shaSize, '');
    }

    /**
     * Get the current branch revision based on branch mapping
     *
     * @return string
     */
    protected function getRelease()
    {
        return;
        $branch =  $this->getMainBranch()->getName();
        $release = array_where($this->releases, function($key) use($branch) {
            return str_contains($branch, $key);
        });
        return is_array($release) ? current($release) : $this->defaultRelease;
    }


}