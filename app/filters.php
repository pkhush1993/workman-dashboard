<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});


/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/
Route::filter('auth', function()
{
	if ( !Sentinel::check() ) {
		// Since this filter only catches internal attempts at accessing a restricted
		// page, we can assume that users will always be redirected from within
		// the Laravel build. This just adds the convenience of not being redirected
		// to the fucking dashboard after every login. Simply put, we encode a referer
		// and let the AuthController@postLogin use it to redirect the user. The first
		// parameter is absolutely useless and acts as a decoy. Why? Because fucking
		// pterodactyls, that's why. But seriously, this adds a sense of implied visual
		// security. While it's still pointless, it visually implies a secure session.
		$returnUrl = URL::to('auth/login', array(urlencode(base64_encode(URL::current())), base64_encode(URL::current())));
		return Redirect::to($returnUrl);
	}
});
Route::filter('postauth', function()
{
	if ( Sentinel::check() ) {
		return Redirect::route('dashboard');
	}
});

Route::filter('auth.basic', function()
{
	return Auth::basic();
});


/* ==== Executive Restricted Views - make sure user is has the correct Role ====*/
Route::filter('executive', function()
{
	if ( !Sentinel::inRole('admin') AND !Sentinel::inRole('executive') ){
		 return Redirect::back()->withErrors(Lang::get('authorization.no_permission'));
	}
});


/* ==== Foreman Restricted Views - make sure user is has the correct Role ====*/
Route::filter('foreman', function()
{
	if ( !Sentinel::inRole('admin') AND !Sentinel::inRole('executive') AND !Sentinel::inRole('foreman') ){
		 return Redirect::back()->withErrors(Lang::get('authorization.no_permission'));
	}
});

/* ==== Project Manager Restricted Views - make sure user is has the correct Role ====*/
Route::filter('project_manager', function()
{
	if ( !Sentinel::inRole('project_manager') AND !Sentinel::inRole('admin') AND !Sentinel::inRole('executive') ){
		 return Redirect::back()->withErrors(Lang::get('authorization.no_permission'));
	}
});

/* ==== Estimator Restricted Views - make sure user is has the correct Role ====*/
Route::filter('estimator', function()
{
	if ( !Sentinel::inRole('admin') AND !Sentinel::inRole('executive') AND !Sentinel::inRole('estimator') ){
		 return Redirect::back()->withErrors(Lang::get('authorization.no_permission'));
	}
});

/* ==== Accounting Restricted Views - make sure user is has the correct Role ====*/
Route::filter('accounting', function()
{
	if ( !Sentinel::inRole('admin') AND !Sentinel::inRole('executive') AND !Sentinel::inRole('accounting') ){
		 return Redirect::back()->withErrors(Lang::get('authorization.no_permission'));
	}
});

/* ==== Accounting Restricted Views - make sure user is has the correct Role ====*/
Route::filter('restrict_time_entry', function()
{
	if ( Sentinel::inRole('time_entry') ){
		 return Redirect::back()->withErrors(Lang::get('authorization.no_permission'));
	}
});


/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() !== Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});
