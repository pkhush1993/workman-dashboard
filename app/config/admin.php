<?php
return array(
 	/*
	|--------------------------------------------------------------------------
	| Allow Frontend Registration
	|--------------------------------------------------------------------------
	|
	| Since this application framework is agnostic to the final product, in regards	
	| to shipability, we may need to disable user registration when the app is
	| primarily admin driven. By setting to false, users won't be able to navigate
	| to or submit a registration request. Admins can still manage users in the
	| backend panel.
	*/
	'allow_registration'		=>	true,
	'require_activation'		=>	true,

 	/*
	|--------------------------------------------------------------------------
	| Set Path for Default Avatar Location
	|--------------------------------------------------------------------------
	|
	| This is used with both Inviting and Creating New Administrator
	| 
	*/

	'default_avatar'		=>	'/img/default_avatar.jpg',


);