@extends('layouts.default')

@section('content')


    <div id="search">
        <h1>Search Timesheets</h1>

        <h3 class="header-5"><i class="file text icon"></i> Text Reports</h3>

        {{ Form::open(array('class' => 'ui form run-report','route' => 'run.search-time')) }}
        <div class="five fields">
            <div class="field report-type">
                {{ Form::label('report_type', 'Report Type') }}
                {{ Form::select('report_type', array(
                    'time_entries'                      => 'Time Entries',

                ), null , array('class'=>'ui dropdown','autocomplete' =>'off')) }}
            </div>
            <div class="field start_date">
                {{ Form::label('start_date', 'Start Date') }}
                {{ Form::text('start_date',NULL, array('placeholder'=>'Date Start','class'=>'date')) }}
            </div>
            <div class="field end_date">
                {{ Form::label('end_date', 'End Date') }}
                {{ Form::text('end_date',NULL, array('placeholder'=>'Date End','class'=>'date')) }}
            </div>
            <div class="field job_id ui search">
                {{ Form::label('job_id', 'Job Number / Name') }}
                {{ Form::hidden('job_id',null, array('placeholder'=>'Job Id', 'id'=>'job_id')) }}

                <div class="ui search job-input-ajax">
                    {{ Form::text('',$default = NULL, array('placeholder'=>'Search for Job...','class'=>'prompt')) }}
                    <div class="results"></div>
                </div>
            </div>

            <div class="field user_id ui search">
                {{ Form::label('user_id', 'Employee') }}
                {{ Form::hidden('user_id',null, array('placeholder'=>'User Id', 'id'=>'user_id')) }}

                <div class="ui search user-input-ajax">
                    {{ Form::text('',$default = NULL, array('placeholder'=>'Search for Employee...','class'=>'prompt')) }}
                    <div class="results"></div>
                    </di>
                </div>
            </div>

        </div>

        <div>
            {{ Form::submit('RUN REPORT',array('class' => 'ui submit green button')); }}
            {{ Form::close() }}
        </div>

                <script>
                    jQuery.noConflict();
                    (function ($) {

                        $(document).ready(function () {

                            var jobSearch = $('.ui.job-input-ajax.search');
                            var userSearch = $('.ui.user-input-ajax.search');

                            var jobPrompt = $(".prompt", jobSearch);
                            var userPrompt = $(".prompt", userSearch);

                            console.log("jobPrompt",jobPrompt)

                            var job_id = $('#job_id');
                            var user_id = $('#user_id');

                            jobPrompt.on("change", function(evt){
                                console.log("jobPrompt change", jobPrompt.val() );
                                if(jobPrompt.val() == ''){
                                    job_id.val("all");
                                }
                            });
                            userPrompt.on("change", function(evt){
                                console.log("jobPrompt change", jobPrompt.val() );
                                if(userPrompt.val() == ''){
                                    user_id.val("all");
                                }
                            });


                            // NEW
                            var jobsEndpoint = '{{route("jobs.input.ajax")}}';
                            var userEndpoint = '{{route("users.input.ajax")}}';

                            var jobSearchConfig = {
                                apiSettings: {
                                    url: jobsEndpoint + '/' + '{query}',
                                },
                                maxResults: 50,
                                onSelect: function (result) {
                                    var job_id = $('#job_id');
                                    job_id.val(result.job_id)
                                    $(this).closest(".field").removeClass("error");
                                },
                                onSearchQuery: function (query) {

                                },
                                onResults: function (results) {

                                }
                            }

                            var userSearchConfig = {
                                apiSettings: {
                                    url: userEndpoint + '/' + '{query}',
                                },
                                maxResults: 50,
                                onSelect: function (result) {
                                    var user_id = $('#user_id');
                                    user_id.val(result.user_id)
                                    $(this).closest(".field").removeClass("error");
                                },
                                onSearchQuery: function (query) {

                                },
                                onResults: function (results) {

                                }
                            }

                            $('.ui.job-input-ajax.search', this.view).search(jobSearchConfig);
                            $('.ui.user-input-ajax.search', this.view).search(userSearchConfig);

                            // END NEW


                            $('form.run-report').on('submit', function (event) {

                                //event.preventDefault();

                                var job = $('#job_id');
                                var user = $('#user_id');

                                if (job.val() == '') {
                                    job.val('all')
                                }
                                if (user.val() == '') {
                                    user.val('all')
                                }

                                return true;
                            });

                            $(function () {

                                $('body').on('change', 'select[name=report_type]', function (e) {
                                    var reportType = $(this).val();


                                });

        //$('select[name=report_type]').trigger('change');

                                //Semantic UI Custom Form Validation Rule For Pay Periods
                                $.fn.form.settings.rules.dateRange = function (value) {
                                    var startDate = new Date($('input[name=start_date]').val());
                                    var endDate = new Date($('input[name=end_date]').val());
                                    if (startDate > endDate) {
                                        return false;
                                    }
                                    else {
                                        return true;
                                    }
                                }

        //Semantic UI Form Validation =========================
                                $('.ui.form').form(
                                        {
                                            startDate: {
                                                identifier: 'start_date',
                                                rules: [
                                                    {
                                                        type: 'empty',
                                                        prompt: 'Start Date is required'
                                                    },
                                                    {
                                                        type: 'dateRange',
                                                        prompt: 'Start Date Must be Before End Date'
                                                    },
                                                ]
                                            },
                                            endDate: {
                                                identifier: 'end_date',
                                                rules: [
                                                    {
                                                        type: 'empty',
                                                        prompt: 'End Date is required'
                                                    },
                                                    {
                                                        type: 'dateRange',
                                                        prompt: 'End Date Must be After Start Date'
                                                    },
                                                ]
                                            },
                                        },
                                        {
                                            inline: true,
                                            on: 'submit',
                                        }
                                );

                            });

                            var results = $('#page-content .ui.search .results');
                            var result = $('#page-content .ui.search .results .result');

                            $('body').on('click', '#page-content, .job-number-name .result, .employee .result', function (event) {

                                var hidden;

                                if ($(event.target).is('.job-number-name .result *', '.job-number-name .result') || $(event.target).is('.employee .result *', '.employee .result')) {
                                    hidden = $(this).find('.title').data('id');

                                    var input_impreg = $(this).find('.title').text();
                                    $(this).parent().parent().find('input[type="text"]').val(input_impreg);
                                    $(this).parent().parent().find('input[type="hidden"]').val(hidden);
                                }

        // remove window and unpopulate results if user clicks result or outside of results box
                                results.removeClass('transition visible');
        //remove previous nodes if populated
                                if (!results.empty()) {
                                    results.fadeOut(200).empty();
                                }

                            });

                            var searchRequest = null;

                            $(function () {

                                var minlength = 3;

                            });

                        });

                    })(jQuery);
                </script>

@stop