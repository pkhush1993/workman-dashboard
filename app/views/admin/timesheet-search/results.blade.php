@extends('layouts.default')

@section('content')



    <div id="view-timesheet">

        {{ Form::open( array('class' => 'ui form edit-timesheets-form','route' => 'update.time-sheets') ) }}
        <?php

        $period_start = $start_date;
        $period_end = $end_date;

        if (!isset($job_id) || $job_id == '%') {
            $job_id = 'all';
        }

        // if empty query all
        if (!isset($user_id) || $user_id == '%') {
            $user_id = 'all';
        }

        ?>
        <h1>
            Review / Adjust Time Entries
            <small style="font-weight: normal;font-size: 18px;">
                (Range {{ date('m/d/Y',strtotime($period_start)).' - '.date('m/d/Y',strtotime($period_end)) }})
            </small>
            {{-- If Todays Date is Equal to or less than the Grace Period GIve Option to Edit --}}



            {{ Form::submit('SAVE TIMESHEET',array('class' => 'ui submit right floated green icon button save-timesheet')); }}

            <a target="_blank"
               href="{{route('export.timesheet.search-results') }}<?php echo '/' . $start_date . '/' . $end_date . '/' . $job_id . '/' . $user_id . '/' ?>"
               data-start="<?php echo $start_date;?>" data-end="<?php echo $end_date;?>"
               data-job_id="<?php echo $job_id;?>" data-user_id="<?php echo $user_id;?>"
               class="ui submit right floated red icon button export-timesheet">Export to CSV</a>

        </h1>
        <?php if($result_count > 500) { ?>

        <small style="color: #ff695e;">Your parameters resulted in more than 500 results. Please narrow parameters to
            limit results to 500 entries.
        </small>

        <?php } ?>

                <!-- Line Items -->
        <table class="ui compact small celled striped table">
            <thead>
            <tr>
                <th class="two wide">Name</th>
                <th class="one wide">Time In</th>
                <th class="one wide">Time Out</th>
                <th class="one wide">Break
                    <small>(Mins.)</small>
                </th>
                <th class="one wide">Total Hours</th>
                <th class="one wide">Job</th>
                <th class="one wide">Task Category</th>
            </tr>
            </thead>
            <tbody class="time-entry-repeater">

            {{-- Repeat 5 Times --}}
            @if ( $data)
                <?php

                $time_query_count = 1; ?>
                @foreach ($data as $time_query)


                    <?php

                    // limit results to 500
                    if($time_query_count < 501) {

                    //format data
                    $job_id = $time_query->job_id;
                    $job_number = $time_query->job_number;
                    $job_name = $time_query->name;
                    $task = $time_query->task;

                    $time_sheet_id = $time_query->id;

                    $date_in = $time_query->date_in;
                    $date_out = $time_query->date_out;
                    $break = $time_query->break;
                    $total = $time_query->total;

                    //$user_nice_name = $time_query->CONCAT(users.first_name,' ',users.last_name);
                    $first_name = $time_query->first_name;
                    $last_name = $time_query->last_name;
                    $user_nice_name = $first_name . ' ' . $last_name;
                    $user_id = $time_query->user_id;

                    ?>

                    <tr class="time-entry">
                        {{-- {{ Form::hidden('user_id['.$time_query_count.']',Sentinel::getUser()->id) }} --}}
                        {{ Form::hidden('time_sheet_id[]', $time_sheet_id,array('data-id' => $time_sheet_id)) }}
                        {{ Form::hidden('job_id[]', $job_id,array('data-id' => $job_id, 'class'=>'job_id_capture')) }}
                        {{ Form::hidden('user_id[]', $user_id,array('data-id' => $user_id)) }}
                        <td class="name">

                            <div class="required field">
                                {{ Form::text(null ,$user_nice_name, array('placeholder'=>'Name','readonly'=>'readonly','class'=>'pretty_name')) }}
                            </div>

                        </td>
                        <td class="time-range">
                            <div class="required fields">

                                <div class="field">

                                    {{--{{ Form::label('date-in', 'Date In') }}--}}

                                    <?php
                                    if(Sentinel::inRole('admin'))
                                    {
                                    ?>
                                    {{ Form::text('date_in[]',date('m/d/Y g:ia',strtotime($date_in)), array('placeholder'=>'Date In','class'=>'datetime-entry date-in','style' => 'padding:8px;width:155px')) }}
                                    <?php
                                    }
                                    else {
                                    ?>
                                    {{ Form::text('date_in[]',date('m/d/Y g:ia',strtotime($date_in)), array('readonly' => 'readonly', 'placeholder'=>'Date In','class'=>'datetime-entry date-in','style' => 'padding:8px;width:155px')) }}
                                    <?php
                                    }
                                    ?>

                                </div>
                            </div>
                        </td>
                        <td class="time-range">
                            {{--<div class="field">--}}
                            {{--<b style="top: 25px;position: relative;">- TO - </b>--}}
                            {{--</div>--}}
                            <div class="required fields">

                                <div class="field">
                                    {{--{{ Form::label('date-out', 'Date Out') }}--}}

                                    <?php
                                    if(Sentinel::inRole('admin'))
                                    {
                                    ?>
                                    {{ Form::text('date_out[]',date('m/d/Y g:ia',strtotime($date_out)), array('placeholder'=>'Date Out','class'=>'datetime-entry date-out','style' => 'padding:8px;width:155px')) }}
                                    <?php
                                    }
                                    else {
                                    ?>

                                    {{ Form::text('date_out[]',date('m/d/Y g:ia',strtotime($date_out)), array('readonly' => 'readonly','placeholder'=>'Date Out','class'=>'datetime-entry date-out','style' => 'padding:8px;width:155px')) }}
                                    <?php
                                    }
                                    ?>

                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="required field">
                                <?php
                                if(Sentinel::inRole('admin'))
                                {
                                ?>
                                {{ Form::text('break[]',$break, array('placeholder'=>'Break','class'=>'break')) }}
                                <?php
                                }
                                else {
                                ?>

                                {{ Form::text('break[]',$break, array('readonly' => 'readonly', 'placeholder'=>'Break','class'=>'break')) }}
                                <?php
                                }
                                ?>
                            </div>
                        </td>
                        <td>
                            <div class="field">
                                {{ Form::text('total[]',$total, array('placeholder'=>'Total','readonly'=>'readonly','class'=>'total')) }}
                            </div>
                        </td>
                        <td>
                            <div class="field">
                                <div class="ui search job-input-ajax">
                                    {{ Form::text('',$default = $job_name, array('placeholder'=>'Search for Job...','class'=>'prompt job')) }}
                                    <div class="results"></div>
                                </div>
                                {{-- {{ Form::select('job_id['.$i.']', $jobs_select, $default = '' , array('class'=>'ui dropdown jobs','autocomplete' =>'off','data-validate'=>'job','required'=>'required')) }} --}}
                            </div>
                        </td>

                        <td>
                            <div class="field">
                                {{ Form::text('task[]',$task, array('placeholder'=>'Enter Task')) }}
                            </div>
                        </td>


                    </tr>

                    <?php

                    // increment count for 500 limit
                    $time_query_count++;

                    // end entry count 500
                    }

                    ?>
                @endforeach
            @else

            @endif


            </tbody>
            {{-- Only Show Hours if the current Users has privledges --}}
            @if (Sentinel::inRole('!admin') OR Sentinel::inRole('!executive') OR Sentinel::inRole('!project_manager') )

            @endif
        </table>

        {{ Form::close() }}
    </div>

    <script>
        jQuery.noConflict();
        (function ($) {
            $(function () {

                // Auto Add a new Time Entry on Tab for last field
                $('.task-input').keyup(function (e) {
                    var code = e.keyCode || e.which;
                    // Code 9  is the Tab Key
                    if (code == '9') {
                        $(this).closest('.time-entry').find('.add-field').trigger('click');
                    }
                });

                // TIME ENTRY REPEATER FIELD FUNCTION
                // ------------------------------------------------------------
                var maxInputs = 100; //maximum input boxes allowed
                var fieldCount = {{ (isset($time_entry_count)) ? $time_entry_count : 1 }}; //Track the field amounts
                var fieldClass = '.field'; //Track the field amounts
                var increment = true; //Option to Increment Input Names


                //Semantic UI AJAX Search =========================
                $('.ui.job-input-ajax.search')
                        .search({
                            apiSettings: {
                                url: '{{route("jobs.input.ajax")}}/' + '{query}',
                            },
                            maxResults: 50,
                            onSelect(result)
                {
                    $(this).closest('.time-entry').find('input.job').val(result.job_id);
                    $(this).closest('.time-entry').find('.job_id_capture').val(result.job_id);
                    //console.log(result);
                }
            })
            ;
            // ------------------------------------------------------------


            // Calculate Total Time Per Entry on the Fly
            $('body').on('change keyup', '.time-range input,.break', function () {

                // Set Variables
                var timeEntry = $(this).closest('.time-entry');
                var dateIn = timeEntry.find('.date-in').val();
                var dateOut = timeEntry.find('.date-out').val();
                var breakTime = (timeEntry.find('.break').val() / 60).toFixed(2);

                // Format and Set Date/Time In and Date/Time Out
                var dateStart = moment(dateIn, "MM/DD/YYYY HH:mma");
                var dateEnd = moment(dateOut, "MM/DD/YYYY HH:mma");

                // get total seconds between the times
                var delta = Math.abs(dateStart - dateEnd) / 1000;
                // calculate hours
                var hours = (delta / 3600);

                if (dateIn && dateOut) {
                    timeEntry.find('.total').val((hours - breakTime).toFixed(2)).css('border-color', '#5BBD72');
                }

            });


            //Semantic UI Custom Form Validation Rule For Pay Periods
            $.fn.form.settings.rules.timeEntry = function (value) {
                var dateIn = moment($(this).closest('.time-range').find('.date-in').val(), "MM/DD/YYYY HH:mma");
                var dateOut = moment($(this).closest('.time-range').find('.date-out').val(), "MM/DD/YYYY HH:mma");
                if (dateIn > dateOut) {
                    return false;
                }
                else {
                    return true;
                }
            }


            $('.ui.form.update-timesheets').form({
                total: {
                    identifier: 'totals',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'Total Hours is required'
                        },
                    ]
                },
                dateIn: {
                    identifier: 'date-in',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'Date Out is required'
                        },
                        {
                            type: 'timeEntry',
                            prompt: 'Date In Must be Before End Date'
                        },
                    ]
                },
                dateOut: {
                    identifier: 'date-out',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'Date In is required'
                        },
                        {
                            type: 'timeEntry',
                            prompt: 'Date In Must be Before End Date'
                        },
                    ]
                },
                job: {
                    identifier: 'job_id',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'Job is required'
                        },
                    ]
                }
            });


            //          //Submit Timesheet for Approval
            // $('body').on('click', '.update-timesheet', function(){

            //              event.preventDefault();

            // 	// Submit Form
            // 	$('.updatetimesheet-form').submit();

            //          });


            // Dynamic Date Picker
            $('body').on('focus', 'input.datetime-entry', function () {
                if (!$(this).attr('readonly')) {


                    $(this).css('border', '#00CDF1 1px solid');
                    $(this).datetimepicker({
                        timeFormat: "hh:mm tt",
                        //minDate: new Date("{{ $date_in }}"),
                        //maxDate: new Date("{{ $date_out }}"),
                    });
                }
            });

        });
        })
        (jQuery);
    </script>

@stop