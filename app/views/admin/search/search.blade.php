@extends('layouts.default')

@section('content')


	<div id="search">
		<h1><i class="search icon"></i>Search</h1>
		{{ Form::open(array('class' => 'ui form activity-form')) }}
			<div class="four fields">
				<div class="field">
					{{ Form::label('Type', 'Type') }}
					{{ Form::text('search_type', $value=NULL, array('placeholder' => 'Enter Bid Comment')) }}
				</div>
				<div class="field">
					{{ Form::label('bid_comment', 'New Bid Comment') }}
					{{ Form::text('bid_comment', $value=NULL, array('placeholder' => 'Enter Bid Comment')) }}
					{{ Form::text('date_start',NULL, array('placeholder'=>'Period Date Start','class'=>'pay_period date','data-format'=>'MMMM dd yyyy')) }}
				</div>
			</div>
			{{ Form::submit('UPDATE BID COMMENTS',array('class' => 'ui submit green button')); }}
		{{ Form::close() }}
	</div>

	<script>
		jQuery.noConflict();
		(function( $ ) {
			$(function() {



			});
		})(jQuery);	
	</script>

@stop