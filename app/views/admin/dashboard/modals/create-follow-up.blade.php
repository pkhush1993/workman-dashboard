<!-- Job Defaults Modal -->
<div class="ui modal create-follow-up">
	<h2 class="ui header">
	  <i class="comments icon"></i>
	  <div class="content">
			Create New Follow Up
			<div class="sub header">Add a new Follow Up to a Job and Attach.</div>
	  </div>
	</h2>
	<div class="content">
        {{ Form::open(array('class' => 'ui form create-job-note','route' => ['job.create.note'])) }}
        	{{ Form::hidden('follow-up',1) }}
            <h3 class="ui dividing header" style="margin-top:0">Create New Job Note/Activity</h3>
            <div class="two fields">
                <div class="field">
                    {{ Form::label('activity', 'Activity') }}
                    {{ Form::select('activity', is_array($activities_select) ? $activities_select : [], $default = '' , array('class'=>'ui dropdown activity-drop','autocomplete' =>'off')) }}
                </div>
                <div class="field">
                    {{ Form::label('type', 'Type') }}
                    {{ Form::select('type', array(
                        ''       => 'Select Status',
                        'accounting' => 'Accounting',
                        'estimating' => 'Estimating',
                        'production' => 'Production',
                    ), $default = '' , array('class'=>'ui dropdown','autocomplete' =>'off')) }}
                </div>
            </div>
            <div class="two fields">
				<div class="field">
	                {{ Form::label('job_id', 'Job') }}
					<div class="ui search job-ajax">
					    <input name="job_id" class="prompt jobs" type="text" placeholder="Search for Job...">
					    <div class="results"></div>
					</div>
	                {{-- {{ Form::select('job_id', $jobs_select, $default = '' , array('class'=>'ui dropdown jobs','autocomplete' =>'off')) }} --}}
	            </div>
                <div class="field">
                    {{ Form::label('contact', 'Contact') }}
                    {{ Form::select('contact', is_array($contacts) ? $contacts : [], $default = '' , array('class'=>'ui dropdown','autocomplete' =>'off')) }}
                </div>
            </div>
            <div class="two fields">
                <div class="field assigned">
                    {{ Form::label('assigned', 'Assigned') }}
                    {{ Form::select('assigned', is_array($users) ? $users : [], $default = '' , array('class'=>'ui dropdown','autocomplete' =>'off')) }}
                </div>
                <div class="field deadline">
                    {{ Form::label('Deadline') }}
                    {{ Form::text('deadline',date('m/d/Y'), array('placeholder'=>'Choose a Deadline','class'=>'date')) }}
                </div>
            </div>
            <div class="field">
                {{ Form::label('note', 'Note') }}
                {{ Form::textarea('note',$value = null, array('placeholder'=>'Enter Note')) }}
            </div>
            {{ Form::submit('CREATE JOB NOTE',array('class' => 'ui submit green button')); }}
        {{ Form::close() }}
	</div>
</div>

<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {

			//Semantic UI AJAX Search =========================
			$('.ui.job-ajax.search')
			  .search({
				apiSettings: {
					url: '{{route("jobs.input.ajax")}}/' + '{query}',
				},
			  })
			;

            //Semantic UI Form Validation For Job Notes =========================
            $('.ui.form.create-job-note').form(
                {
                    activity: {
                        identifier  : 'activity',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Activity is required'
                            }
                        ]
                    },
                    type: {
                        identifier  : 'type',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Type is required'
                            }
                        ]
                    },
                    note: {
                        identifier  : 'note',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Note is required'
                            }
                        ]
                    },
                    job: {
                        identifier  : 'job_id',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Job is required'
                            }
                        ]
                    },
                    contact: {
                        identifier  : 'contact',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Contact is required'
                            }
                        ]
                    },
                    assigned: {
                        identifier  : 'assigned',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Contact is required'
                            }
                        ]
                    }
                },
				{
				    inline : true,
				    on     : 'blur'
				}
            );

        });
    })(jQuery);
</script>
