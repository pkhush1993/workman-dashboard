<h2 style="margin-top:0">Activity Stream<i class="activity-info blue circle info icon" data-content="Showing 7 days of history"></i></h2>

<table class="ui striped compact celled activity-stream-table table">
	<thead>
		<tr>
			<th style="width: 250px;font-size: 12px;"></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
		</tr>
		<tr>
			<th>Timestamp</th>
			<th>Job</th>
			<th>Activity</th>
			<th>User</th>
			<th>Type</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($activities as $activity)
			@if ( $activity['jobData'] )
			<tr>
				<td class="date-filter">{{ date('m/d/Y @ g:i a',strtotime($activity['updated_at'])) }}</td>
				<td><a href="{{ route('view.job', $activity['jobData']['id']) }}">{{ '#'.$activity['jobData']['job_number'].' - '.$activity['jobData']['name'] }}</a></td>
				<td>{{ $activity['activity'] }}</td>
				<td>{{ $activity['created_profile_data']['first_name'] .' '. $activity['created_profile_data']['last_name']  }}</td>
				<td>{{ ucwords($activity['type']) }}</td>
			</tr>
			@endif
		@endforeach
	</tbody>
</table>

<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {

            $(document).ready(function(){
                $('.activity-stream-table').dataTable( {
                    "info": false,
                    "lengthChange": false,
					"order": [[ 0, "desc" ]],
                    "dom": '<"table-search"<f> > t <"F"ip>'
                } )
		  		.columnFilter({ sPlaceHolder: "head:before",
					aoColumns: [ { type: "date-range" },{ type: "text" },{ type: "text" },{ type: "text" },{ type: "text" }  ]
				});

				// Activity Info Info Tooltip
				$('.activity-info')
				  .popup({
				    inline: true,
				});

            });

        });
    })(jQuery);
</script>
