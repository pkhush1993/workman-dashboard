<h2>Outstanding Jobs</h2>

<table class="ui striped celled table outstanding-jobs">
	<thead>
		<tr>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
		</tr>	
		<tr>
			<th>Status</th>
			<th>Job</th>
			<th>Account</th>
			<th>Amount</th>
			<th>Days</th>
		</tr>
	</thead>
	<tbody>
		@if ($outstanding_jobs->count())
			@foreach ($outstanding_jobs as $outstanding_job)
				@if ($outstanding_job->completion_priority == "High") <tr class="error"> @else <tr> @endif
					<td>{{ ucwords($outstanding_job->status) }}</td>
					<td><a href="{{ route('view.job', $outstanding_job->id) }}">{{ '#'.$outstanding_job->job_number.' - '.$outstanding_job->name }}</a></td>
					<td>{{ $outstanding_job->account()->name }}</td>
					<td>
						@if ($outstanding_job->getBid())
							<span style="color:#DC6868">$ {{ $outstanding_job->getBid()->getAttribute('subtotal') }}</span>
						@else
  							<span style="color:#D95C5C">Bid Not Yet Created</span>
						@endif
					</td>
					<td>{{$outstanding_job->getDaysSinceJobCompletion($outstanding_job->completed_date)}}</td>
				</tr>			
			@endforeach
		@else
			<tr>
				<td colspan="6">
			        <div class="ui large green message">
						<div class="header">There Are Currently No Outstanding Jobs <i class="smile icon"></i></div>
			        </div>					
				</td>
			</tr>
		@endif	
	
	</tbody>
</table>



{{-- Only Sort Table if there is Data to Sort --}}
@if ($outstanding_jobs->count())
	<script>
	    jQuery.noConflict();
	    (function( $ ) {
	        $(function() {

	            $(document).ready(function(){
	                $('.outstanding-jobs').dataTable( {
	                    "info": false,
	                    "lengthChange": false,
	                    "dom": '<"table-search"<f> > t <"F"ip>'                  
	                } )
			  		.columnFilter({ sPlaceHolder: "head:before",
						aoColumns: [ { type: "text" },{ type: "text" },{ type: "text" },{ type: "text" },{ type: "text" }  ]
					});	                
	            });

	        });
	    })(jQuery);
	</script>
@endif	
