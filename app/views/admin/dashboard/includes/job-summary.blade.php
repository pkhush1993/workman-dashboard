<h2>Job Status Summary</h2>

<table class="ui striped celled table">
	<thead>
		<tr>
			<th>Status</th>
			<th>Job Count</th>
			<th>Dollars</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Request</td>
			<td> {{ $request_totals['job_count'] }}</td>
			<td>$ {{ number_format($request_totals['grand_total'],2) }}</td>
		</tr>
		<tr>
			<td>Outstanding</td>
			<td>{{ $outstanding_totals['job_count'] }}</td>
			<td>$ {{ number_format($outstanding_totals['grand_total'],2) }}</td>
		</tr>
		<tr>
			<td>Contracted</td>
			<td>{{ $contracted_totals['job_count'] }}</td>
			<td>$ {{ number_format($contracted_totals['grand_total'],2) }}</td>
		</tr>
		<tr>
			<td>Proceed</td>
			<td>{{ $proceed_totals['job_count'] }}</td>
			<td>$ {{ number_format($proceed_totals['grand_total'],2) }}</td>
		</tr>
		<tr>
			<td>Scheduled</td>
			<td>{{ $scheduled_totals['job_count'] }}</td>
			<td>$ {{ number_format($scheduled_totals['grand_total'],2) }}</td>
		</tr>
		<tr>
			<td>Completed</td>
			<td>{{ $completed_totals['job_count'] }}</td>
			<td>$ {{ number_format($completed_totals['grand_total'],2) }}</td>
		</tr>
		<tr>
			<td>Invoicing</td>
			<td>{{ $invoicing_totals['job_count'] }}</td>
			<td>$ {{ number_format($invoicing_totals['grand_total'],2) }}</td>
		</tr>
		<tr>
			<td>Paid</td>
			<td>{{ $paid_totals['job_count'] }}</td>
			<td>$ {{ number_format($paid_totals['grand_total'],2) }}</td>
		</tr>				
	</tbody>
</table>









