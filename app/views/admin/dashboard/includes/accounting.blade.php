<h3>Accounting Pipeline</h3>

<table class="ui striped celled table">
	<thead>
		<tr>
			<th>Status</th>
			<th>Job</th>
			<th>Account</th>
			<th>Amount</th>
			<th>Priority</th>
			<th>Deadline</th>
		</tr>
	</thead>
	<tbody>
		@if ($jobs->count())
			@foreach ($jobs as $job)
				@if ($job->completion_priority == "High") <tr class="error"> @else <tr> @endif
					<td>{{ ucwords($job->status) }}</td>
					<td><a href="{{ route('view.job', $job->id) }}">{{ '#'.$job->job_number.' - '.$job->name }}</a></td>
					<td>{{ $job->account()->name }}</td>
					<td>
						@if ($job->getBid())
							<span style="color:#5BBD72">$ {{ $job->getBid()->grand_total }}</span>
						@else
  							<span style="color:#D95C5C">Bid Not Yet Created</span>
						@endif
					</td>
					<td>
						@if ($job->completion_priority)
							{{ $job->completion_priority }}
						@else
  							None
						@endif
					</td>
					<td>{{ date('m/d/Y',strtotime($job->completed_date)) }}</td>
				</tr>			
			@endforeach
		@else
			<tr>
				<td colspan="6">
			        <div class="ui large blue message">
						<div class="header">There Are Currently No Jobs in the Status of Contracted, Outstanding, or Proceed Assigned to this User</div>
			        </div>					
				</td>
			</tr>
		@endif	
	
	</tbody>
</table>
