<h3>Accounting Pipeline</h3>

<table class="ui striped celled table accounting-pipeline">
	<thead>
		<tr>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
		</tr>	
		<tr>
			<th>Status</th>
			<th>Job</th>
			<th>Account</th>
			<th>Amount</th>
			<th>Days</th>
		</tr>
	</thead>
	<tbody>
		@if ($jobs->count())
			@foreach ($jobs as $job)
				@if ($job->completion_priority == "High") <tr class="error"> @else <tr> @endif
					<td>{{ ucwords($job->status) }}</td>
					<td><a href="{{ route('view.job', $job->id) }}">{{ '#'.$job->job_number.' - '.$job->name }}</a></td>
					<td>{{ $job->account()->name }}</td>
					<td>
						@if ($job->getBid())
							<span style="color:#5BBD72">$ {{ $job->getBid()->getAttribute('subtotal') }}</span>
						@else
  							<span style="color:#D95C5C">Bid Not Yet Created</span>
						@endif
					</td>
					<td>
						{{$job->getDaysSinceJobCompletion($job->completed_date)}}
					</td>
				</tr>			
			@endforeach
		@else
			<tr>
				<td colspan="6">
			        <div class="ui large blue message">
						<div class="header">There Are Currently No Jobs in the Status of Completed or Invoicing Assigned to this User</div>
			        </div>					
				</td>
			</tr>
		@endif	
	
	</tbody>
</table>


{{-- Only Sort Table if there is Data to Sort --}}
@if ($jobs->count())
	<script>
	    jQuery.noConflict();
	    (function( $ ) {
	        $(function() {

	            $(document).ready(function(){
	                $('.accounting-pipeline').dataTable( {
	                    "info": false,
	                    "lengthChange": false,
	                    "dom": '<"table-search"<f> > t <"F"ip>'                  
	                } )
			  		.columnFilter({ sPlaceHolder: "head:before",
						aoColumns: [ { type: "text" },{ type: "text" },{ type: "text" },{ type: "text" },{ type: "text" }  ]
					});	                
	            });

	        });
	    })(jQuery);
	</script>
@endif	
