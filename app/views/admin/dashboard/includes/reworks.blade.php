<h2>Job Rework</h2>

<table class="ui striped celled table reworks">
	<thead>
		<tr>
			<th></th>
			<th></th>
			<th style="width: 200px;font-size: 12px;"></th>
		</tr>	
		<tr>
			<th>Job</th>
			<th>Description</th>
			<th>Deadline</th>
		</tr>
	</thead>
	<tbody>
		{{-- Only Show Reworks for the Correct Project Manager if specified--}}
		@if ($user_id != 'all')
			@foreach ($reworks as $rework)
				@if (($rework->job()->project_manager == $user_id) || ($rework->job()->foreman == $user_id))
					<tr {{ ( date('Y-m-d',strtotime($rework->deadline) ) <= date('Y-m-d') ) ? 'class="error"' : '' }}>
						<td><a href="{{ route('view.job', $rework->job()->id) }}">{{ '#'.$rework->job()->job_number.' - '.$rework->job()->name }}</a></td>
						<td> {{ $rework->description }} </td>
						<td>{{ date('m/d/Y',strtotime($rework->deadline)) }}</td>
					</tr>
				@else

				@endif
			@endforeach	
		{{-- Show All Reworks --}}
		@else 
			@foreach ($reworks as $rework)
				<tr>
					<td><a href="{{ route('view.job', $rework->job()->id) }}">{{ '#'.$rework->job()->job_number.' - '.$rework->job()->name }}</a></td>
					<td> {{ $rework->description }} </td>
					<td>{{ date('m/d/Y',strtotime($rework->deadline)) }}</td>
				</tr>		
			@endforeach	 
		@endif

	</tbody>
</table>


{{-- Only Sort Table if there is Data to Sort --}}
@if ($reworks->count())
	<script>
	    jQuery.noConflict();
	    (function( $ ) {
	        $(function() {

	            $(document).ready(function(){
	                $('table.reworks').dataTable( {
	                    "info": false,
	                    "lengthChange": false,
	                    "dom": '<"table-search"<f> > t <"F"ip>'
	                })
			  		.columnFilter({ sPlaceHolder: "head:before",
						aoColumns: [ { type: "text" },{ type: "text" },{ type: "date-range" }  ]
					});
	            });

	        });
	    })(jQuery);
	</script>
@endif	
