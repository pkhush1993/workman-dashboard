<h3>Estimator Pipeline</h3>

<table class="ui striped celled table estimator-pipeline">
	<thead>
		<tr>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th style="width: 200px;font-size: 12px;"></th>
		</tr>
		<tr>
			<th>Status</th>
			<th>Job</th>
			<th>Account</th>
			<th>Amount</th>
			<th>Priority</th>
			<th>Deadline</th>
		</tr>
	</thead>
	<tbody>
		@if ($jobs->count())
			@foreach ($jobs as $job)
				@if ($job->estimator_priority == "High") <tr class="error"> @else <tr> @endif
					<td>{{ ucwords($job->status) }}</td>
					<td><a href="{{ route('view.job', $job->id) }}">{{ '#'.$job->job_number.' - '.$job->name }}</a></td>
					<td>{{ $job->account() ? $job->account()->name : '--Account Deleted--' }}</td>
					<td>
						@if ($job->getBid())
							<span style="color:#5BBD72">$ {{ $job->getBid()->getAttribute('subtotal') }}</span>
						@else
  							<span style="color:#D95C5C">Bid Not Yet Created</span>
						@endif
					</td>
					<td>
						@if ($job->estimator_priority)
							{{ $job->estimator_priority }}
						@else
  							None
						@endif
					</td>
					<td>
						@if ($job->estimator_deadline)
							{{ date('m/d/Y',strtotime($job->estimator_deadline)) }}
						@else
  							None
						@endif
					</td>
				</tr>
			@endforeach
		@else
			<tr>
				<td colspan="6">
			        <div class="ui large blue message">
						<div class="header">There Are Currently No Jobs in the Status of Requested or Outstanding Assigned to this User</div>
			        </div>
				</td>
			</tr>
		@endif

	</tbody>
</table>


{{-- Only Sort Table if there is Data to Sort --}}
@if ($jobs->count())
	<script>
	    jQuery.noConflict();
	    (function( $ ) {
	        $(function() {

	            $(document).ready(function(){
	                $('.estimator-pipeline').dataTable( {
	                    "info": false,
	                    "lengthChange": false,
	                    "dom": '<"table-search"<f> > t <"F"ip>'
	                } )
			  		.columnFilter({ sPlaceHolder: "head:before",
						aoColumns: [ { type: "text" },{ type: "text" },{ type: "text" },{ type: "text" },{ type: "text" },{ type: "date-range" }  ]
					});
	            });

	        });
	    })(jQuery);
	</script>
@endif
