<h2>Jobs To Complete</h2>

<table class="ui striped celled table foreman-pipeline">
	<thead>
		<tr>
			<th></th>
			<th></th>
			<th style="width: 200px;font-size: 12px;"></th>
		</tr>	
		<tr>
			<th>Job</th>
			<th>Account</th>
			<th>Last Update</th>
		</tr>
	</thead>
	<tbody>
		@if ($jobs->count())
			@foreach ($jobs as $job)
				<tr>
					<td><a target="_blank" href="{{ route('job.work.order', $job->id) }}">{{ '#'.$job->job_number.' - '.$job->name }}</a>0000</td>
					<td>@if(isset($job->account()->name)){{ $job->account()->name }}@endif</td>
					<td>{{ date('m/d/Y',strtotime($job->updated_at)) }}</td>
				</tr>			
			@endforeach
		@else
			<tr>
				<td colspan="3">
			        <div class="ui large blue message">
						<div class="header">There Are Currently No Jobs Assigned to this User</div>
			        </div>					
				</td>
			</tr>
		@endif	
	
	</tbody>
</table>


{{-- Only Sort Table if there is Data to Sort --}}
@if ($jobs->count())
	<script>
	    jQuery.noConflict();
	    (function( $ ) {
	        $(function() {

	            $(document).ready(function(){
	                $('.foreman-pipeline').dataTable( {
	                    "info": false,
	                    "lengthChange": false,
						"order": [[ 0, "desc" ]],
	                    "dom": '<"table-search"<f> > t <"F"ip>'                  
	                } )
			  		.columnFilter({ sPlaceHolder: "head:before",
						aoColumns: [ { type: "text" },{ type: "text" },{ type: "date-range" }  ]
					});	                
	            });

	        });
	    })(jQuery);
	</script>
@endif	

