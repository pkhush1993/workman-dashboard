<h2>Follow Ups</h2>

<table class="ui striped celled table follow-ups-table">
	<thead>
		<tr>
			<th></th>
			<th></th>
			<th style="width: 200px;font-size: 12px;"></th>
			<th></th>
			<th></th>
		</tr>
		<tr>
			<th>Job</th>
			<th>Activity</th>
			<th>Deadline</th>
			<th>Assigned To</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		@if ($follow_ups->count())
			@foreach ($follow_ups as $follow_up)
				{{-- Make Sure the Job is Still Active --}}
				{{--@if ($follow_up->job())--}}
					<tr>
						<td><a href="{{ route('view.job', $follow_up['jobData']['id']) }}">{{ '#'.$follow_up['jobData']['job_number'].' - '.$follow_up['jobData']['name'] }}</a></td>
						<td>{{ $follow_up->activity }}</td>
						<td>{{ date('m/d/Y',strtotime($follow_up->deadline)) }}</td>
						<td>{{ $follow_up['user_profile_data']['first_name'] .' '. $follow_up['user_profile_data']['last_name']  }}</td>
						<td class="center aligned">
							<i class="icon large orange circle info follow-up-info" style="cursor:pointer"></i>
							<div class="ui large fluid bottom middle popup" style="text-align:left">
								<div class="header" style="font-size:22px">{{ $follow_up->activity }}</div>
								<div class="content">
									@if ($follow_up['contact_profile_data'])
										Contact with: <a href="{{ route('company.contact', $follow_up->job_contacts_id) }}">{{ $follow_up['contact_profile_data']['first_name'] .' '. $follow_up['contact_profile_data']['last_name']  }}</a>
									@endif
									<br>
									@if ($follow_up['user_profile_data'])
										Assigned To: {{ $follow_up['user_profile_data']['first_name'] .' '. $follow_up['user_profile_data']['last_name']  }}
									@endif
									<div class="ui divider"></div>
									<div><b>{{ $follow_up->note }} - <small>{{ date('m/d/Y',strtotime($follow_up->updated_at)) }} by &nbsp;&nbsp;<img class="ui avatar image" src="{{ $follow_up['user_profile_data']['avatar'] }}"> {{ $follow_up['user_profile_data']['first_name'] .' '. $follow_up['user_profile_data']['last_name']  }}</small></b></div>
									{{-- If There Are Notes Attached Display --}}
									@if (count($follow_up['notesData']) > 0)
										<ul>
										@foreach ($follow_up['notesData'] as $follow_up_note)
											<li>
												<b>{{ $follow_up_note['note'] }} - <small>{{ date('m/d/Y',strtotime($follow_up_note['updated_at'])) }} by &nbsp;&nbsp;<img class="ui avatar image" src="{{ $follow_up_note['user_profile_data']['avatar'] }}"> {{ $follow_up_note['user_profile_data']['first_name'] .' '. $follow_up_note['user_profile_data']['last_name']  }}</small></b>
											</li>
										@endforeach
										</ul>
									@endif
									{{ Form::open(array('class' => 'ui form','route' => 'resolve.follow-up' )) }}
										{{ Form::hidden('job_note_id',$follow_up->id) }}
										<button class="ui tiny green button" style="margin-top:15px;">RESOLVE</button>
									{{ Form::close() }}
									<br>
									<div class="ui divider"></div>
									<h3>Add Note to Follow Up</h3>
									{{ Form::open(array('class' => 'ui form attach-follow-up-note','route' => 'follow-up.create.note' )) }}
										{{ Form::hidden('job_id',$follow_up['jobData']['id']) }}
										{{ Form::hidden('job_note_id',$follow_up['id']) }}
										<div class="field">
											{{ Form::label('note', 'Note') }}
											{{ Form::textarea('note',$value='', array('placeholder'=>'Enter Note')) }}
										</div>
						                <div class="field">
						                    <div class="ui checkbox" style="font-weight:bold">
						                        {{ Form::label('reassign', 'Re-Assign Follow Up?') }}
						                        {{ Form::checkbox('reassign') }}
						                    </div>
						                </div>
						                <div class="field assigned" style="display:none">
						                    {{ Form::select('assigned', $users, $follow_up['assigned'] , array('class'=>'ui dropdown','autocomplete' =>'off','style'=>'height:auto',)) }}
						                </div>
						                <div class="field">
						                    <div class="ui checkbox" style="font-weight:bold">
						                        {{ Form::label('new_deadline', 'New Deadline?') }}
						                        {{ Form::checkbox('new_deadline') }}
						                    </div>
						                </div>
						                <div class="field deadline" style="display:none">
						                    {{ Form::text('deadline',date('m/d/Y'), array('placeholder'=>'Choose a Deadline','class'=>'date')) }}
						                </div>
						                <button class="ui tiny green button">ADD NOTE</button>
									{{ Form::close() }}
									<br>
									{{-- <small>Created by: {{ $follow_up->created_profile()->first_name .' '. $follow_up->created_profile()->last_name  }}</small> --}}
								</div>
							</div>
						</td>
					</tr>
				{{--@endif--}}
			@endforeach
		@else
			<tr>
				<td colspan="4">
			        <div class="ui large blue message">
						<div class="header">There Are Currently No Follow-Ups</div>
			        </div>
				</td>
			</tr>
		@endif
	</tbody>
</table>

<div class="ui tiny green button create-follow-up-button">CREATE FOLLOW UP</div>

{{-- Create New Follow Up Modal --}}
@include('admin.dashboard.modals.create-follow-up')

<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {

        	// Pop Create Follow Up Modal
        	$('.create-follow-up-button').pop('.ui.modal.create-follow-up');

			// Follow Up Info Tooltip
			$('.follow-up-info')
			  .popup({
			    inline: true,
			    on    : 'click',
			    position : 'bottom center',
			    lastResort: 'bottom center'
			});

			// Show Deadline Field if Checkbox is Checked
            $('input[name=new_deadline]').change(function(){
                if(this.checked){
                    $(this).closest('form').find('.deadline').show();
                }
                else{
                    $(this).closest('form').find('.deadline').hide();
                }
            });

            // Show Re-Assign Field if Checkbox is Checked
            $('input[name=reassign]').change(function(){
                if(this.checked){
                    $(this).closest('form').find('.assigned').show();
                }
                else{
                    $(this).closest('form').find('.assigned').hide();
                }
            });

            //Semantic UI Form Validation For Job Notes =========================
            $('.ui.form.attach-follow-up-note').form(
                {
                    note: {
                        identifier  : 'note',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Note is required'
                            }
                        ]
                    }
                },
				{
				    inline : true,
				    on     : 'blur'
				}
            );

        });
    })(jQuery);
</script>

{{-- Only Sort Table if there is Data to Sort --}}
@if ($follow_ups->count())
	<script>
	    jQuery.noConflict();
	    (function( $ ) {
	        $(function() {

	            $(document).ready(function(){
	                $('.follow-ups-table').dataTable( {
	                    "info": false,
	                    "lengthChange": false,
	                    "dom": '<"table-search"<f> > t <"F"ip>',
	                    "order": [[ 2, "asc" ]]
	                })
			  		.columnFilter({ sPlaceHolder: "head:before",
						aoColumns: [ { type: "text" },{ type: "text" },{ type: "date-range" },{ type: "text" }  ]
					});
	            });

	        });
	    })(jQuery);
	</script>
@endif
