@extends('layouts.default')

@section('content')

<div id="dashboard">

    <div class="basic segment">
        <h1><i class="dashboard icon"></i>Estimator Dashboard</h1>
        <div style="position: absolute;top: 15px;right: 14px;">{{ Form::select('project_manager', $estimators_select, $user_id, array('class'=>'ui search dropdown user-select','autocomplete' =>'off')) }}</div>
    </div>

    {{-- Request Jobs --}}
    @include('admin.dashboard.includes.jobs-estimator')

    <div class="ui divider"></div>

    <div class="ui stackable grid">

        <div class="sixteen wide column">
    		{{-- Job Follow Ups --}}
            @include('admin.dashboard.includes.follow-ups')
    	</div>
        <?php /*
    	<div class="six wide column">
    		{{-- Job Status Summaries  --}}
    		@include('admin.dashboard.includes.job-summary')
    	</div>
         */?>
    </div>

    <div class="ui divider"></div>

    {{-- Activity --}}
    @include('admin.dashboard.includes.activity')

</div>

<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {

            $('body').on('change', '.user-select', function(event) {
                event.preventDefault();
                var url = "{{route('dashboard.estimator','') }}/" + $(this).dropdown('get value');
                window.location.href = url;
            });

        });
    })(jQuery);
</script>

@stop
