@extends('layouts.default')

@section('content')


<div id="dashboard">
    
    <div class="basic segment">
        <h1><i class="dashboard icon"></i>Foreman Dashboard </h1>
        <div style="position: absolute;top: 15px;right: 14px;">{{ Form::select('project_manager', $foremen_select, $user_id, array('class'=>'ui search dropdown user-select','autocomplete' =>'off')) }}</div>
    </div>

    <div class="ui two column stackable grid">
    	<div class="column">
    		{{-- Job Follow Ups --}}
            @include('admin.dashboard.includes.jobs-foreman')
    	</div>

    	<div class="column">
    		{{-- Job Follow Ups --}}
    		@include('admin.dashboard.includes.follow-ups')
    	</div>
    </div>

    <div class="ui divider"></div>

    <div class="ui two column stackable grid">
        <div class="column">
            {{-- Job Reworks--}}
            @include('admin.dashboard.includes.reworks')
        </div>

        <div class="column">
        </div>
    </div>

    <div class="ui divider"></div>
    
</div>

<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {

            $('body').on('change', '.user-select', function(event) {
                event.preventDefault();
                var url = "{{route('dashboard.foreman','') }}/" + $(this).dropdown('get value');

                window.location.href = url;
            });

        });
    })(jQuery);
</script>

@stop

