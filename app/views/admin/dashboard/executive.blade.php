@extends('layouts.default')

@section('content')


<div id="dashboard">

    @if (Sentinel::inRole('admin') OR Sentinel::inRole('executive'))
        {{-- expr --}}
    @endif
    <h1><i class="dashboard icon"></i>Executive Dashboard</h1>

    <div class="ui stackable grid">
    	<div class="sixteen wide column">
    		{{-- Job Follow Ups --}}
            @include('admin.dashboard.includes.follow-ups')
    	</div>
        {{--<?php /*--}}
    	{{--<div class="six wide column">--}}
    		{{-- Job Status Summaries  --}}
    		{{--@include('admin.dashboard.includes.job-summary')--}}
    	{{--</div>--}}
         {{--*/?>--}}
    </div>
    <div class="ui divider"></div>

    {{-- Activity --}}
    @include('admin.dashboard.includes.activity')

</div>

@stop
