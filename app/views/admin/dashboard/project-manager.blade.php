@extends('layouts.default')

@section('content')

<div id="dashboard">
    
    <div class="basic segment">
        <h1><i class="dashboard icon"></i>Project Manager Dashboard</h1>
        <div style="position: absolute;top: 20px;right: 14px;min-width:168px">{{ Form::select('project_manager', $project_manager_select, $user_id, array('class'=>'ui search dropdown user-select','autocomplete' =>'off')) }}</div>
    </div>

    {{-- Request Jobs --}}
    @include('admin.dashboard.includes.jobs-project-manager')
    
    <div class="ui divider"></div>

    <div class="ui two column stackable grid">
    	<div class="column">
    		{{-- Job Reworks--}}
            @include('admin.dashboard.includes.reworks')
    	</div>

    	<div class="column">
    		{{-- Job Follow Ups --}}
    		@include('admin.dashboard.includes.follow-ups')
    	</div>
    </div> 

    <div class="ui divider"></div>
    
    {{-- Activity --}}
    @include('admin.dashboard.includes.activity')  
    
</div>

<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {

            $('body').on('change', '.user-select', function(event) {
                event.preventDefault();
                var url = "{{route('dashboard.project-manager','') }}/" + $(this).dropdown('get value');

                window.location.href = url;
            });

        });
    })(jQuery);
</script>

@stop
