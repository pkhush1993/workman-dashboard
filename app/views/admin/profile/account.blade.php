<!-- Edit Users Account Info -->
<div class="ui bottom attached tab segment" data-tab="account">
	<h3><i class="privacy icon"></i>Change Password</h3>
	{{ Form::open(array('class' => 'ui form')) }}
		<div class="two fields">
			<div class="required field">
				{{ Form::label('password', 'Password') }}
				{{ Form::password('password',$value = null, array('placeholder'=>'Password')) }}
			</div>
			<div class="required field">
				{{ Form::label('retype_password', 'Retype Password') }}
				{{ Form::password('retype_password',$value = null, array('placeholder'=>'Retype Password')) }}
			</div>					
		</div>
		{{ Form::submit('UPDATE',array('class' => 'ui submit blue button')); }}
	{{ Form::close() }}
	<br><hr>
	<h3><i class="mail icon"></i>Change Email</h3>
	{{ Form::open(array('class' => 'ui form')) }}
		<div class="two fields">
			<div class="required field">
				{{ Form::label('email', 'Email/Username') }}
				{{ Form::text('email',$profile->getEmail(), array('placeholder'=>'Email/Username')) }}
			</div>				
		</div>
		{{ Form::submit('UPDATE',array('class' => 'ui submit blue button')); }}
	{{ Form::close() }}				
</div>