<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
		<style>
			body { font-family: Helvetica,sans-serif;font-size:14px; }
		</style>
	</head>
	<body>
		<h1>Activate Your Account</h1>
		<hr>
		<div>
			Thank you for registering with {{ URL::to('/') }}. Click <a href="{{ URL::to('auth/activate', array($activationCode, $email)) }}">here</a> to activate your account. 
		</div>
		<br><br><br><br>
		<div>
			Link not working? Copy and paste the following into your browsers URL bar. <br>
			<a href="{{ URL::to('auth/activate', array($activationCode, $email)) }}">{{ URL::to('auth/activate', array($activationCode, $email)) }}</a>
		</div>
	</body>
</html>