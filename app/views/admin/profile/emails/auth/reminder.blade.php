<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
		<style>
			body { font-family: Helvetica,sans-serif;font-size:14px; }
		</style>		
	</head>
	<body>
		<h1>Password Reset</h1>

		<div>
			To reset your password, <a href="{{ URL::to('auth/reset', array($token,$email)) }}">click here</a> and complete the reset password form.
			<hr>
			<br/><br><br><br>
			This link will expire in {{ Config::get('auth.reminder.expire', 60) }} minutes.
		</div>
	</body>
</html>
