<!-- Edit Users Wage Rates -->
<div class="ui bottom attached tab segment" data-tab="wages">
	<h3 class="ui header">
		Employee Wages
		<div class="sub header">
			Update Wages to Reflect in Timesheets and Reports
		</div>
	</h3>
	{{ Form::open(array('class' => 'ui form update-profile','route' => ['update.profile', $current_user->id])) }}
		<br>
		<div class="field">
            {{ Form::label('wage_rate', 'Wage Rate (per hour)') }}
            <div class="ui huge left icon input">
                <i class="dollar icon"></i>
                {{ Form::text('wage_rate',$profile->wage_rate, array('placeholder'=>'Enter Wage Rate','class'=>'money ')) }}
            </div>
		</div>
		<br>
		<input type="submit" class="ui submit blue button" value="UPDATE">
	{{ Form::close() }}
</div>
