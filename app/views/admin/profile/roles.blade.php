<!-- Edit User Roles -->
@if (Sentinel::inRole('admin'))
	<div class="ui bottom attached tab segment" data-tab="roles">
		<h3>Change Users Roles</h3> 
		{{ Form::open(array('class' => 'ui form','route' => ['user.profile.roles', Sentinel::getUser()->id ])) }}
			<div class="ui stackable four column grid">
				<div class="row">
					<div class="column">
						<div class="field">
							<div class="ui toggle checkbox">
								{{ Form::checkbox('user_role[]', 'executive', Sentinel::inRole('executive') ? true : false ) }} {{ Form::label('executive', 'Executive') }}
							</div>
						</div>				
					</div>
					<div class="column">
						<div class="field">
							<div class="ui toggle checkbox">

								{{ Form::checkbox('user_role[]', 'accounting', Sentinel::inRole('accounting') ? true : false ) }} {{ Form::label('accounting', 'Accounting') }}
							</div>
						</div>
					</div>
					<div class="column">
						<div class="field">
							<div class="ui toggle checkbox">
								{{ Form::checkbox('user_role[]', 'time_entry', Sentinel::inRole('time_entry') ? true : false  ) }} {{ Form::label('time_entry', 'Time') }}
							</div>
						</div>
					</div>
					<div class="column">
						<div class="field">
							<div class="ui toggle checkbox">
								{{ Form::checkbox('user_role[]', 'project_manager', Sentinel::inRole('project_manager') ? true : false  ) }} {{ Form::label('project_manager', 'Project') }}
							</div>
						</div>	
					</div>
				</div>

				<div class="row">
					<div class="column">
						<div class="field">
							<div class="ui toggle checkbox">
								{{ Form::checkbox('user_role[]', 'estimator', Sentinel::inRole('estimator') ? true : false  ) }} {{ Form::label('estimator', 'Estimator') }}
							</div>
						</div>		
					</div>
					<div class="column">
						<div class="field">
							<div class="ui toggle checkbox">
								{{ Form::checkbox('user_role[]', 'foreman', Sentinel::inRole('foreman') ? true : false  ) }} {{ Form::label('foreman', 'Foreman') }}
							</div>
						</div>
					</div>
					<div class="column">
						<div class="field">
							<div class="ui toggle checkbox">
								{{ Form::checkbox('user_role[]', 'salesman', Sentinel::inRole('salesman') ? true : false  ) }} <label>Salesman</label>
							</div>
						</div>
					</div>						
					<div class="column">
						<div class="field">
							<div class="ui toggle checkbox">
								{{ Form::checkbox('user_role[]', 'admin', Sentinel::inRole('admin') ? true : false  ) }} <label><i class="user icon"></i> Admin</label>
							</div>
						</div>
					</div>				
				</div>
			</div>
			<br><br>								
			<input type="submit" class="ui submit blue button" value="UPDATE">
		{{ Form::close() }}
	</div>
@endif