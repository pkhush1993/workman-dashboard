<!-- Add Account Form (Used Throughout Different Views)-->
<div id="add-account">
    {{ Form::open(array('class' => 'ui form create-account','id' => 'create-account','route' => 'create.account')) }}
        <div id="account">
            {{ Form::hidden('company_id',1) }}
            <div class="required field">
                {{ Form::label('name', 'Account Name') }}
                {{ Form::text('account_name',$value = null, array('placeholder'=>'Company Name')) }}
            </div>
            <div class="two fields">
                <div class="field">
                    {{ Form::label('email', 'Account Email') }}
                    {{ Form::email('account_email',$value = null, array('placeholder'=>'Enter Email')) }}
                </div>
                <div class="field">
                    {{ Form::label('phone', 'Account Phone') }}
                    {{ Form::text('account_phone',$value = null, array('placeholder'=>'Enter Phone','class'=>'phone')) }}
                </div>
            </div>
           <div class="two fields">
                <div class="field">
                    {{ Form::label('mobile', 'Account Mobile Number') }}
                    {{ Form::text('account_mobile',$value = null, array('placeholder'=>'Enter Mobile Number','class'=>'phone')) }}
                </div>
                <div class="field">
                    {{ Form::label('fax', 'Account Fax Number') }}
                    {{ Form::text('account_fax',$value = null, array('placeholder'=>'Enter Fax Number','class'=>'phone')) }}
                </div>
            </div>
            {{ Form::label('address', 'Account Address') }}
            <div class="two fields">
                <div class="field">
                    {{ Form::text('account_address_1',$value = null, array('placeholder'=>'Address')) }}
                </div>
                <div class="field">
                    {{ Form::text('account_address_2',$value = null, array('placeholder'=>'Address 2')) }}
                </div>
            </div>
            <div class="fields">
                <div class="seven wide field">
                    {{ Form::label('city', 'Account City') }}
                    {{ Form::text('account_city',$value = null, array('placeholder'=>'City')) }}
                </div>
                <div class="five wide field">
                   {{ Form::label('state', 'Account State') }}
                    {{ Form::select('account_state', array(
                        '' => 'Select State',
                        'AL' => 'Alabama',
                        'AK' => 'Alaska',
                        'AZ' => 'Arizona',
                        'AR' => 'Arkansas',
                        'CA' => 'California',
                        'CO' => 'Colorado',
                        'CT' => 'Connecticut',
                        'DE' => 'Delaware',
                        'DC' => 'District Of Columbia',
                        'FL' => 'Florida',
                        'GA' => 'Georgia',
                        'HI' => 'Hawaii',
                        'ID' => 'Idaho',
                        'IL' => 'Illinois',
                        'IN' => 'Indiana',
                        'IA' => 'Iowa',
                        'KS' => 'Kansas',
                        'KY' => 'Kentucky',
                        'LA' => 'Louisiana',
                        'ME' => 'Maine',
                        'MD' => 'Maryland',
                        'MA' => 'Massachusetts',
                        'MI' => 'Michigan',
                        'MN' => 'Minnesota',
                        'MS' => 'Mississippi',
                        'MO' => 'Missouri',
                        'MT' => 'Montana',
                        'NE' => 'Nebraska',
                        'NV' => 'Nevada',
                        'NH' => 'New Hampshire',
                        'NJ' => 'New Jersey',
                        'NM' => 'New Mexico',
                        'NY' => 'New York',
                        'NC' => 'North Carolina',
                        'ND' => 'North Dakota',
                        'OH' => 'Ohio',
                        'OK' => 'Oklahoma',
                        'OR' => 'Oregon',
                        'PA' => 'Pennsylvania',
                        'RI' => 'Rhode Island',
                        'SC' => 'South Carolina',
                        'SD' => 'South Dakota',
                        'TN' => 'Tennessee',
                        'TX' => 'Texas',
                        'UT' => 'Utah',
                        'VT' => 'Vermont',
                        'VA' => 'Virginia',
                        'WA' => 'Washington',
                        'WV' => 'West Virginia',
                        'WI' => 'Wisconsin',
                        'WY' => 'Wyoming',
                    ), $value = null , array('class'=>'ui dropdown','autocomplete' =>'off')) }}
                </div>
                <div class="four wide field">
                    {{ Form::label('zip', 'Account Zip') }}
                    {{ Form::text('account_zip',$value = null, array('placeholder'=>'Zip Code')) }}
                </div>
            </div>
            <div style="display:none">
                <h4>Account Contact</h4>
                <div class="inline field">
                    <div class="ui radio checkbox account_contact">
                        {{ Form::radio('account_contact_type',$value = 'existing_contact', false) }}
                        {{ Form::label('name', 'Existing Contact') }}
                    </div>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="ui radio checkbox new_account_contact">
                        {{ Form::radio('account_contact_type',$value = 'new_contact', true) }}
                        {{ Form::label('name', 'New Contact') }}
                    </div>
                </div>
                <div id="assigned_contact" class="field required" style="display:none">
                    {{-- {{ Form::select('assigned_contact', $contacts_dropdowns, $value = null , array('class'=>'ui search dropdown','autocomplete' =>'off')) }} --}}
                </div>
            </div>
        </div>

        <div class="ui divider"></div>

        <div id="contact">
            <h3><i class="child icon"></i>New Contact</small></h3>
            <div class="field">
                {{ Form::label('title', 'Contact Title') }}
                {{ Form::text('contact_title',$value = null, array('placeholder'=>'Enter Title')) }}
            </div>
            <div class="required field">
                {{ Form::label('name', 'Contact Name') }}
                <div class="two fields">
                    <div class="field">
                        {{ Form::text('contact_first_name',$value = null, array('placeholder'=>'First Name')) }}
                    </div>
                    <div class="field">
                        {{ Form::text('contact_last_name',$value = null, array('placeholder'=>'Last Name')) }}
                    </div>
                </div>
            </div>
            <div class="two fields">
                <div class="required field">
                    {{ Form::label('email', 'Contact Email') }}
                    {{ Form::email('contact_email',$value = null, array('placeholder'=>'Enter Email')) }}
                </div>
                <div class="field">
                    {{ Form::label('phone', 'Contact Phone') }}
                    {{ Form::text('contact_phone',$value = null, array('placeholder'=>'Enter Phone','class'=>'phone')) }}
                </div>
            </div>
           <div class="two fields">
                <div class="field">
                    {{ Form::label('mobile', 'Contact Mobile Number') }}
                    {{ Form::text('contact_mobile',$value = null, array('placeholder'=>'Enter Mobile Number','class'=>'phone')) }}
                </div>
                <div class="field">
                    {{ Form::label('fax', 'Contact Fax Number') }}
                    {{ Form::text('contact_fax',$value = null, array('placeholder'=>'Enter Fax Number','class'=>'phone')) }}
                </div>
            </div>
            {{ Form::label('address', 'Contact Address') }}
            <div class="two fields">
                <div class="field">
                    {{ Form::text('contact_address_1',$value = null, array('placeholder'=>'Address')) }}
                </div>
                <div class="field">
                    {{ Form::text('contact_address_2',$value = null, array('placeholder'=>'Address 2')) }}
                </div>
            </div>
            <div class="fields">
                <div class="seven wide field">
                    {{ Form::label('city', 'Contact City') }}
                    {{ Form::text('contact_city',$value = null, array('placeholder'=>'Enter City')) }}
                </div>
                <div class="five wide field">
                    {{ Form::label('state', 'Contact State') }}
                    {{ Form::select('contact_state', array(
                        '' => 'Select State',
                        'AL' => 'Alabama',
                        'AK' => 'Alaska',
                        'AZ' => 'Arizona',
                        'AR' => 'Arkansas',
                        'CA' => 'California',
                        'CO' => 'Colorado',
                        'CT' => 'Connecticut',
                        'DE' => 'Delaware',
                        'DC' => 'District Of Columbia',
                        'FL' => 'Florida',
                        'GA' => 'Georgia',
                        'HI' => 'Hawaii',
                        'ID' => 'Idaho',
                        'IL' => 'Illinois',
                        'IN' => 'Indiana',
                        'IA' => 'Iowa',
                        'KS' => 'Kansas',
                        'KY' => 'Kentucky',
                        'LA' => 'Louisiana',
                        'ME' => 'Maine',
                        'MD' => 'Maryland',
                        'MA' => 'Massachusetts',
                        'MI' => 'Michigan',
                        'MN' => 'Minnesota',
                        'MS' => 'Mississippi',
                        'MO' => 'Missouri',
                        'MT' => 'Montana',
                        'NE' => 'Nebraska',
                        'NV' => 'Nevada',
                        'NH' => 'New Hampshire',
                        'NJ' => 'New Jersey',
                        'NM' => 'New Mexico',
                        'NY' => 'New York',
                        'NC' => 'North Carolina',
                        'ND' => 'North Dakota',
                        'OH' => 'Ohio',
                        'OK' => 'Oklahoma',
                        'OR' => 'Oregon',
                        'PA' => 'Pennsylvania',
                        'RI' => 'Rhode Island',
                        'SC' => 'South Carolina',
                        'SD' => 'South Dakota',
                        'TN' => 'Tennessee',
                        'TX' => 'Texas',
                        'UT' => 'Utah',
                        'VT' => 'Vermont',
                        'VA' => 'Virginia',
                        'WA' => 'Washington',
                        'WV' => 'West Virginia',
                        'WI' => 'Wisconsin',
                        'WY' => 'Wyoming',
                    ), $value = null , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
                </div>
                <div class="four wide field">
                    {{ Form::label('zip', 'Contact Zip') }}
                    {{ Form::text('contact_zip',$value = null, array('placeholder'=>'Zip Code')) }}
                </div>
            </div>
            <br>
        </div>

        <div class="ui action buttons">
            {{ Form::submit('CREATE ACCOUNT',array('class' => 'ui submit green button')); }}
        </div>

        <div class="ui error message"></div>

    {{ Form::close() }}
</div>

<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {

                //Semantic UI Form Validation =========================
                $('#create-account').form(
                    {
                        accountName: {
                            identifier  : 'account_name',
                            rules: [
                                {
                                    type   : 'empty',
                                    prompt : 'Account Name is required'
                                }
                            ]
                        },
                        contactFirstName: {
                            identifier  : 'contact_first_name',
                            rules: [
                                {
                                    type   : 'empty',
                                    prompt : 'Contact First Name is required'
                                }
                            ]
                        },
                       contactLastName: {
                            identifier  : 'contact_last_name',
                            rules: [
                                {
                                    type   : 'empty',
                                    prompt : 'Contact Last Name is required'
                                }
                            ]
                        },
                    },
                    {
                        inline : true,
                        on     : 'blur',
                        onSuccess: function () {
                           $('.ui.modal.add-account').modal('hide');
                        }
                    }
                );

        });
    })(jQuery);
</script>
