<div id="account">
    <h1><i class="child icon"></i>Account - <small>{{ $account->getName() }}</small><a href="{{ route('company.accounts') }}" class="ui right floated tiny basic button "><i class="reply icon"></i>BACK</a></h1>
    <div class="ui blue segment">
        {{ Form::open(array('class' => 'ui form','route' => 'update.account')) }}
            {{ Form::hidden('account_id',$account_id) }}
            <div class="two fields">
                <div class="required field">
                    {{ Form::label('name', 'Company Name') }}
                    {{ Form::text('name',$account->getName(), array('placeholder'=>'First Name','readonly'=>'readonly')) }}
                </div>
                <div class="required field">
                    @if ($contacts)
                        {{ Form::label('name', 'Primary Contact') }}
                        @if ($primary_contact)
                            {{ Form::select('primary_contact', $contacts , $primary_contact->id, array('class'=>'ui search disabled dropdown','autocomplete' =>'off','readonly'=>'readonly')) }}
                        @else
                            {{ Form::select('primary_contact', $contacts , '', array('class'=>'ui search disabled dropdown','autocomplete' =>'off','readonly'=>'readonly')) }}
                        @endif
                    @else
                        {{ Form::label('name', 'Primary Contact') }}
                        {{ Form::text('name','No Contacts On This Account', array('disabled'=>'disabled','readonly'=>'readonly')) }}
                    @endif

                </div>
            </div>                                    
            <div class="two fields">
                <div class="required field">
                    {{ Form::label('email', 'Email') }}
                    {{ Form::email('email',$account->getEmail(), array('placeholder'=>'Enter Email','readonly'=>'readonly')) }}
                </div>
                <div class="required field">
                    {{ Form::label('phone', 'Phone') }}
                    {{ Form::text('phone',$account->getPhone(), array('placeholder'=>'Enter Phone','readonly'=>'readonly','class'=>'phone')) }}
                </div>
            </div>
           <div class="two fields">
                <div class="required field">
                    {{ Form::label('mobile', 'Mobile Number') }}
                    {{ Form::text('mobile',$account->getMobile(), array('placeholder'=>'Enter Mobile Number','readonly'=>'readonly','class'=>'phone')) }}
                </div>
                <div class="required field">
                    {{ Form::label('fax', 'Fax Number') }}
                    {{ Form::text('fax',$account->getFax(), array('placeholder'=>'Enter Fax Number','readonly'=>'readonly','class'=>'phone')) }}
                </div>
            </div>                
            {{ Form::label('address', 'Address') }}
            <div class="two fields">
                <div class="field">
                    {{ Form::text('address_1',$account->getAddress1(), array('placeholder'=>'Address','readonly'=>'readonly')) }}
                </div>
                <div class="field">
                    {{ Form::text('address_2',$account->getAddress2(), array('placeholder'=>'Address 2','readonly'=>'readonly')) }}
                </div>
            </div>
            <div class="fields">
                <div class="seven wide field">
                    {{ Form::label('city', 'City') }}
                    {{ Form::text('city',$account->getCity(), array('placeholder'=>'City','readonly'=>'readonly')) }}
                </div>
                <div class="five wide field">
                   {{ Form::label('state', 'State') }}
                    {{ Form::select('state', array(
                        '' => 'Select State',
                        'AL' => 'Alabama',
                        'AK' => 'Alaska',
                        'AZ' => 'Arizona',
                        'AR' => 'Arkansas',
                        'CA' => 'California',
                        'CO' => 'Colorado',
                        'CT' => 'Connecticut',
                        'DE' => 'Delaware',
                        'DC' => 'District Of Columbia',
                        'FL' => 'Florida',
                        'GA' => 'Georgia',
                        'HI' => 'Hawaii',
                        'ID' => 'Idaho',
                        'IL' => 'Illinois',
                        'IN' => 'Indiana',
                        'IA' => 'Iowa',
                        'KS' => 'Kansas',
                        'KY' => 'Kentucky',
                        'LA' => 'Louisiana',
                        'ME' => 'Maine',
                        'MD' => 'Maryland',
                        'MA' => 'Massachusetts',
                        'MI' => 'Michigan',
                        'MN' => 'Minnesota',
                        'MS' => 'Mississippi',
                        'MO' => 'Missouri',
                        'MT' => 'Montana',
                        'NE' => 'Nebraska',
                        'NV' => 'Nevada',
                        'NH' => 'New Hampshire',
                        'NJ' => 'New Jersey',
                        'NM' => 'New Mexico',
                        'NY' => 'New York',
                        'NC' => 'North Carolina',
                        'ND' => 'North Dakota',
                        'OH' => 'Ohio',
                        'OK' => 'Oklahoma',
                        'OR' => 'Oregon',
                        'PA' => 'Pennsylvania',
                        'RI' => 'Rhode Island',
                        'SC' => 'South Carolina',
                        'SD' => 'South Dakota',
                        'TN' => 'Tennessee',
                        'TX' => 'Texas',
                        'UT' => 'Utah',
                        'VT' => 'Vermont',
                        'VA' => 'Virginia',
                        'WA' => 'Washington',
                        'WV' => 'West Virginia',
                        'WI' => 'Wisconsin',
                        'WY' => 'Wyoming',
                    ), $account->getState() , array('class'=>'ui dropdown disabled','autocomplete' =>'off')) }}
                </div>
                <div class="four wide field">
                    {{ Form::label('zip', 'Zip') }}
                    {{ Form::text('zip',$account->getZip(), array('placeholder'=>'Zip Code','readonly'=>'readonly')) }}
                </div>
            </div>
            <div class="ui action buttons" style="display:none">
                <button class="ui green update-account button">UPDATE</button>
                <div class="or"></div>
                <div class="ui red cancel-edit button">CANCEL</div>
            </div>
            <div class="ui error message"></div>
        {{ Form::close() }}
        <div class="ui labeled icon blue edit button">
          <i class="edit icon"></i>
          EDIT
        </div>
        {{-- <a href="{{ route('remove.account', $account_id) }}" class="ui red remove button">REMOVE</a> --}}
    </div>
</div>

<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {


            $('.edit').click(function(event) {
                event.preventDefault();
                var inputs = find('input');
                $('input').each(function() {
                    $(this).removeAttr('readonly');
                });
                $('.ui.dropdown').removeClass('disabled');

                //Fake Loading
                var container = $('body').find('.ui.blue.segment');
                container.addClass('loading');
                setTimeout(function(){ container.removeClass().addClass('ui green segment'); }, 400);
                
                $('.remove').hide();
                $('.edit').hide();
                $('.action.buttons').show();
                $('input[name=first_name]').focus();                          

            });
            $('.cancel-edit').click(function(event) {
                event.preventDefault();
                var inputs = find('input');
                $('input').each(function() {
                    $(this).attr('readonly', 'readonly');
                });

                $('.ui.dropdown').addClass('disabled');

                //Fake Loading
                var container = $('body').find('.ui.green.segment');
                container.addClass('loading');
                setTimeout(function(){ container.removeClass().addClass('ui blue segment'); }, 400);

                $('.action.buttons').hide();
                $('.edit').show();     
                $('.remove').show();           
            });                

        });
    })(jQuery);
</script>