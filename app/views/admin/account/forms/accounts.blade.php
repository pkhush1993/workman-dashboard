<div id="accounts">
    {{-- Check if Request is Archived --}}
    @if ($archive)
        {{ Form::open(array('class' => 'ui form','route' => 'unarchive.account')) }}
    @else
        {{ Form::open(array('class' => 'ui form','route' => 'remove.accounts')) }}
    @endif
        <table class="ui padded celled table view-accounts-table">
            <thead>
                  <tr>
                      <th></th>
                      <th>Name</th>
                      <th>Phone</th>
                      <th>Email</th>
                      <th>Address</th>
                      <th>Contacts</th>
                  </tr>
            </thead>
            <tbody></tbody>
        </table>

        {{-- Check if Request is Archived --}}
        @if ($archive)
            <a href="{{ route('company.accounts') }}" class="ui labeled icon tiny right floated button">
                <i class="reply icon"></i>
                VIEW ACCOUNTS
            </a>
            {{ Form::submit('UNARCHIVE ACCOUNT',array('class' => 'ui submit green button')); }}
        @else
            <a href="{{ route('add.account') }}" class="ui labeled icon green button">
                <i class="plus icon"></i>
                ADD ACCOUNT
            </a>
            {{ Form::submit('ARCHIVE ACCOUNTS',array('class' => 'ui submit red button','style' => 'display:none')); }}
        @endif

    {{ Form::close() }}
</div>

<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {

            $(document).ready(function(){
                $('.view-accounts-table').dataTable( {
                    "info": false,
                    "lengthChange": false,
                    "dom": '<"table-search"<f> > t <"F"ip>',
                    "aoColumnDefs": [
                        { 'bSortable': false, 'aTargets': [ 0 ] }
                    ],
                    "order": [[ 1, "asc" ]],
                    "iDisplayLength": 50,
                    "processing": true,
                    "serverSide": true,
                    "ajax": "{{route('view.accounts.ajax')}}",
                })
            });

        });
    })(jQuery);
</script>


