@extends('layouts.default')

@section('content')

<h1><i class="child icon"></i>Add Account</h1>
<div class="ui blue segment">
    @include('admin.account.forms.add-account')
</div>

@stop