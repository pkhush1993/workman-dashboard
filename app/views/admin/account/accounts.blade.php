@extends('layouts.default')

@section('content')
	<h1><i class="book icon"></i>Accounts</h1>
	@include('admin.account.forms.accounts')
@stop