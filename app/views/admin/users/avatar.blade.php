<!-- Edit Users Avatar -->
<div class="ui bottom attached tab segment" data-tab="avatar">
	<h3 class="ui header">
		Edit Avatar
		<div class="sub header">
			Drag Image Or Click Your Avatar to Edit
		</div>
	</h3>
	{{ Form::open(array('class' => 'ui form','route' => ['user.profile.roles', $user->id ])) }}
		<br>
		<div class="field">
			<div class="avatar-upload-wrapper">
				<div id="upload-zone" class="upload-zone dropzone avatar dz-clickable" style="background-image:url({{$profile->getAvatar()}})" action="/upload.php"></div>
			</div>
		</div>
		<br>
		<input type="submit" class="ui submit blue button" value="UPDATE">
	{{ Form::close() }}
</div>

<script>
    (function( $ ) {
        $(function() {

			Dropzone.autoDiscover = false;

	  		var myDropzone = $("#upload-zone").dropzone({
	  			url: "/upload.php",
	  			dictDefaultMessage: "Click Here",
	  			maxFilesize: 2,
	  			addRemoveLinks:true,
	  			// success: function (file, response) {
		    //         var imgName = response;
		    //         if(imgName) {
		    //         	$('section.submit').append('<input type="hidden" name="images[]" value="'+imgName+'" />');
		    //         }
		    //     },
		    //     error: function (file, response) {
		    //         file.previewElement.classList.add("dz-error");
		    //     }
	  		});

        });
    })(jQuery);
</script>
