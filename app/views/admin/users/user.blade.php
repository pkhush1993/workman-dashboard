@extends('layouts.default')

@section('content')

    <div id="profile">

		<h1>

			<img class="ui avatar circular image" src="{{$profile->getAvatar()}}">Edit User
			<a href="{{ route('admin.users') }}" class="ui right floated tiny basic button "><i class="reply icon"></i>BACK</a>
		</h1>

		<div class="ui top attached tabular menu">
		  	<a class="active item" data-tab="personal"><i class="user icon"></i>Personal Info</a>
		  	{{-- @TODO: Setup Password Changes and Email Updates --}}
		  	{{-- <a class="item" data-tab="account"><i class="lock icon"></i>Account Info</a> --}}
			@if (Sentinel::inRole('admin') OR Sentinel::inRole('executive'))
				<a class="item" data-tab="roles"><i class="users icon"></i>Roles</a>
                <a class="item" data-tab="wages"><i class="money icon"></i>Wages</a>
			@endif
		</div>

		<!-- Edit Users Personal Info -->
		@include('admin.users.info')


		<!-- Edit Users Account Info -->
		@include('admin.users.account')


		<!-- Edit User Roles -->
		@include('admin.users.roles')

        <!-- Edit User Wages -->
		@include('admin.users.wages')


	</div>

@stop
