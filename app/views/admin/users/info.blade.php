<!-- Edit Users Personal Info -->
<div class="ui bottom attached active tab segment" data-tab="personal">

	{{ Form::open(array('class' => 'ui form update-profile','route' => ['update.user', $profile->id])) }}
		{{ Form::hidden('avatar',$profile->getAvatar()) }}
		<!-- Edit Users Avatar -->
		<h3 class="ui header">
			Edit Avatar
			<div class="sub header">
				Drag Image Or Click Your Avatar to Edit
			</div>
		</h3>
		<br>
		<div class="field">
			<div class="avatar-upload-wrapper">
				<div id="upload-zone" class="upload-zone dropzone avatar dz-clickable" style="background-image:url({{url().$profile->getAvatar()}})" action="/upload.php"></div>
			</div>
		</div>
		<cite style="color:#A2A2A2;">
			Recommended Size: 150x150px | Max File Size:1MB
		</cite>
		<div class="ui divider"></div>

		<h3>Profile Information</h3>
		{{ Form::label('first_name', 'Name') }}
		<div class="two fields">
	        <div class="field">
	        	{{ Form::text('first_name',$value = $profile->getFirstName(), array('placeholder'=>'First Name')) }}
	        </div>
			<div class="field">
				{{ Form::text('last_name',$profile->getLastName(), array('placeholder'=>'Last Name')) }}
			</div>
			</div>
		<div class="two fields">
	        <div class="field">
	        	{{ Form::label('email', 'Email') }}
	        	{{ Form::text('email',$value = $profile->getEmail(), array('placeholder'=>'Email')) }}
	        </div>
			<div class="field">
				{{ Form::label('website', 'Website') }}
				{{ Form::text('website',$profile->getWebsite(), array('placeholder'=>'Website')) }}
			</div>
			</div>	
		<div class="three fields">
			<div class="field">
				{{ Form::label('phone', 'Phone Number') }}
				{{ Form::text('phone',$profile->getPhone(), array('placeholder'=>'Phone Number','class'=>'phone')) }}
			</div>
			<div class="field">
				{{ Form::label('mobile', 'Mobile Number') }}
				{{ Form::text('mobile',$profile->getMobile(), array('placeholder'=>'Mobile Number','class'=>'phone')) }}
			</div>
			<div class="field">
				{{ Form::label('fax', 'Fax Number') }}
				{{ Form::text('fax',$profile->getFax(), array('placeholder'=>'Fax Number','class'=>'phone')) }}
			</div>					
		</div>	 			
		{{ Form::label('address_1', 'Address') }}
		<div class="two fields">
			<div class="field">
				{{ Form::text('address_1',$profile->getAddress1(), array('placeholder'=>'Address')) }}
			</div>
			<div class="field">
				{{ Form::text('address_2',$profile->getAddress2(), array('placeholder'=>'Address 2')) }}
			</div>
		</div>					
		<div class="fields">
			<div class="seven wide field">
				{{ Form::label('city', 'City') }}
				{{ Form::text('city',$profile->getCity(), array('placeholder'=>'City')) }}
			</div>
			<div class="five wide field">
				{{ Form::label('state', 'State') }}
				{{ Form::select('state', array(
					'' => 'Select State',
					'AL' => 'Alabama',
					'AK' => 'Alaska',
					'AZ' => 'Arizona',
					'AR' => 'Arkansas',
					'CA' => 'California',
					'CO' => 'Colorado',
					'CT' => 'Connecticut',
					'DE' => 'Delaware',
					'DC' => 'District Of Columbia',
					'FL' => 'Florida',
					'GA' => 'Georgia',
					'HI' => 'Hawaii',
					'ID' => 'Idaho',
					'IL' => 'Illinois',
					'IN' => 'Indiana',
					'IA' => 'Iowa',
					'KS' => 'Kansas',
					'KY' => 'Kentucky',
					'LA' => 'Louisiana',
					'ME' => 'Maine',
					'MD' => 'Maryland',
					'MA' => 'Massachusetts',
					'MI' => 'Michigan',
					'MN' => 'Minnesota',
					'MS' => 'Mississippi',
					'MO' => 'Missouri',
					'MT' => 'Montana',
					'NE' => 'Nebraska',
					'NV' => 'Nevada',
					'NH' => 'New Hampshire',
					'NJ' => 'New Jersey',
					'NM' => 'New Mexico',
					'NY' => 'New York',
					'NC' => 'North Carolina',
					'ND' => 'North Dakota',
					'OH' => 'Ohio',
					'OK' => 'Oklahoma',
					'OR' => 'Oregon',
					'PA' => 'Pennsylvania',
					'RI' => 'Rhode Island',
					'SC' => 'South Carolina',
					'SD' => 'South Dakota',
					'TN' => 'Tennessee',
					'TX' => 'Texas',
					'UT' => 'Utah',
					'VT' => 'Vermont',
					'VA' => 'Virginia',
					'WA' => 'Washington',
					'WV' => 'West Virginia',
					'WI' => 'Wisconsin',
					'WY' => 'Wyoming',
				), $profile->getState() , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
			</div>
			<div class="four wide field">
				{{ Form::label('zip', 'Zip') }}
				{{ Form::text('zip',$profile->getZip(), array('placeholder'=>'Zip Code')) }}
			</div>
		</div>		
		<input type="submit" class="ui submit blue button" value="UPDATE">
		<div class="ui error message"></div>
	{{ Form::close() }}
</div>

<script>
    (function( $ ) {
        $(function() {

			Dropzone.autoDiscover = false;

	  		var myDropzone = $("#upload-zone").dropzone({
	  			url: "/upload.php",
	  			dictDefaultMessage: "Click Here",
	  			maxFilesize: 2,
	  			addRemoveLinks:true,
	  			success: function (file, response) {
	  				console.log(response)
		            var imgName = response;
		            if(imgName) {
		            	$('.update-profile input[name=avatar').val(imgName);
		            }
		        },
		        error: function (file, response) {
					setTimeout(function(){
						alert('boom');
					  $('.dz-error-mark').hide('slow');
					}, 2000);
		        }
	  		});        

        });
    })(jQuery);
</script>
