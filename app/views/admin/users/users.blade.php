@extends('layouts.default')

@section('content')

    <div id="view-jobs">
        <h1>
            <i class="users icon"></i> 
            Users
        </h1>

        <table class="ui table view-users-table">
            <thead>
                  <tr>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Roles</th>
                  </tr>
            </thead>
            <tbody>
                @foreach ($users as $user)
                    <?php 
                        $roles = $user->roles();
                     ?>
                    <tr>
                        <td>
                            <a href="{{ route('edit.user', $user->id) }}">
                                @if ($user->profile()->avatar)
                                    <img class="ui avatar image" src="{{$user->profile()->avatar}}">
                                @endif
                                {{ $user->getFullName() }}
                            </a>
                        </td>
                        <td><a href="mailto:{{ $user->email }}">{{ $user->profile()->email }}</a></td>
                        <td>
                            @foreach ($roles as $role)
                                <b>{{ $role->name }}</b>, 
                            @endforeach
                        </td>
                    </tr>
                @endforeach                                       
            </tbody>
        </table>

    </div>

<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {

            $(document).ready(function(){
                $('.view-users-table').dataTable( {
                    "info": false,
                    "lengthChange": false,
                    "dom": '<"table-search"<f> > t <"F"ip>',
                    "order": [[ 0, "asc" ]]
                })
            });

        });
    })(jQuery);
</script>

@stop