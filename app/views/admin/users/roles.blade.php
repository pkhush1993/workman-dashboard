<!-- Edit User Roles -->
@if (Sentinel::inRole('admin') OR Sentinel::inRole('executive'))
	<div class="ui bottom attached tab segment" data-tab="roles">
		<h3>Change Users Roles</h3>
		{{ Form::open(array('class' => 'ui form','route' => ['user.profile.roles', $user->id ])) }}
			<div class="ui stackable four column grid">
				<div class="row">
					<div class="column">
						<div class="field">
							<div class="ui toggle checkbox">
								{{ Form::checkbox('user_role[]', 'executive', $user->inRole('executive') ? true : false ) }} {{ Form::label('executive', 'Executive') }}
							</div>
						</div>
					</div>
					<div class="column">
						<div class="field">
							<div class="ui toggle checkbox">

								{{ Form::checkbox('user_role[]', 'accounting', $user->inRole('accounting') ? true : false ) }} {{ Form::label('accounting', 'Accounting') }}
							</div>
						</div>
					</div>
					<div class="column">
						<div class="field">
							<div class="ui toggle checkbox time-entry">
								{{ Form::checkbox('user_role[]', 'time_entry', $user->inRole('time_entry') ? true : false  ) }} {{ Form::label('time_entry', 'Time') }}
							</div>
						</div>
					</div>
					<div class="column">
						<div class="field">
							<div class="ui toggle checkbox">
								{{ Form::checkbox('user_role[]', 'project_manager', $user->inRole('project_manager') ? true : false  ) }} {{ Form::label('project_manager', 'Project') }}
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="column">
						<div class="field">
							<div class="ui toggle checkbox">
								{{ Form::checkbox('user_role[]', 'estimator', $user->inRole('estimator') ? true : false  ) }} {{ Form::label('estimator', 'Estimator') }}
							</div>
						</div>
					</div>
					<div class="column">
						<div class="field">
							<div class="ui toggle checkbox">
								{{ Form::checkbox('user_role[]', 'foreman', $user->inRole('foreman') ? true : false  ) }} {{ Form::label('foreman', 'Foreman') }}
							</div>
						</div>
					</div>
					<div class="column">
						<div class="field">
							<div class="ui toggle checkbox">
								{{ Form::checkbox('user_role[]', 'salesman', $user->inRole('salesman') ? true : false  ) }} <label>Salesman</label>
							</div>
						</div>
					</div>
					<div class="column">
						<div class="field">
							<div class="ui toggle checkbox">
								{{ Form::checkbox('user_role[]', 'admin', $user->inRole('admin') ? true : false  ) }} <label><i class="user icon"></i> Admin</label>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br><br>
			<input type="submit" class="ui submit blue button" value="UPDATE">
		{{ Form::close() }}
	</div>
	<script>
	    jQuery.noConflict();
	    (function( $ ) {
	        $(function() {
				// Restrict Users from Checking Time Entry with Other Roles
//				$('body').on('click', '.checkbox', function(event) {
//					var roleType = $(this).find('input').val();
//					if (roleType === 'time_entry') {
//						alert('Time Entry users are restricted to one role, all other roles have the priveledge to enter Time');
//						$('.checkbox').each(function() {
//							if ($(this).find('input').val() === 'time_entry') {
//								$(this).checkbox('check');
//							}
//							else {
//								$(this).checkbox('uncheck');
//							}
//						});
//					}
//					else {
//						$('.time-entry').checkbox('uncheck');
//					}
//				});

	        });
	    })(jQuery);
	</script>
@endif
