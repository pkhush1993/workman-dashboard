<!-- Create Activities-->
<div class="ui bottom attached tab segment" data-tab="lead-sources">
    <h3><i class="checkered flag icon"></i>Manage Default Lead Sources</h3>
    {{ Form::open(array('class' => 'ui form lead-form UpdateLeadSource','route' => ['update.company', 1,'leads'])) }}
    <div class="lead-field-repeater">
        {{ Form::hidden('removed_ids', $value = NULL) }}
        <div class="two fields lead-field hidden">
            <div class="fourteen wide field">
                {{ Form::label('lead', 'New Lead Source') }}
                {{ Form::text('lead[]', $value=NULL, array('placeholder' => 'Enter Lead Source','disabled' => 'disabled')) }}
            </div>
            <div class="two wide field">
                {{ Form::label('', '&nbsp;') }}
                <i class="large green add circle icon add-field"></i>
                <i class="large red minus circle icon remove-field"></i>
            </div>
        </div>
        @foreach ($leads as $lead)
            <div class="two fields lead-field">
                <div class="fourteen wide field">
                    {{ Form::text('lead['.$lead->id.']', $lead->lead, array('placeholder' => 'Enter Activity','data-id' => $lead->id,'class' => 'lead-input')) }}
                </div>
                <div class="two wide field">
                    <i class="large red minus circle icon remove-lead remove-field"></i>
                </div>
            </div>
        @endforeach
        <div class="two fields lead-field">
            <div class="fourteen wide field">
                {{ Form::label('lead', 'New Lead') }}
                {{ Form::text('lead[]', $value=NULL, array('placeholder' => 'Enter Lead','data-name' => 'lead')) }}
            </div>
            <div class="two wide field">
                {{ Form::label('', '&nbsp;') }}
                <i class="large green add circle icon add-field"></i>
                <i class="large red minus circle icon remove-field"></i>
            </div>
        </div>
    </div>
    {{ Form::submit('UPDATE LEAD SOURCES',array('class' => 'ui submit green button LeadSourceBtn')); }}
    {{ Form::close() }}
    <br>
    <div class="UpdateLeadsAlert"></div>
</div>



<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {
            $('.lead-field-repeater').repeater('.lead-field',false);
        });
    })(jQuery);
</script>
