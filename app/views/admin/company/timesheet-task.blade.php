<div class="ui bottom attached tab segment" data-tab="timesheet-task">
    <h3><i class="tasks icon"></i>Manage Timesheet Task</h3>
    {{ Form::open(array('class' => 'ui form task-form','route' => ['update.company', 1,'tasks'])) }}
    <div class="task-field-repeater">
        {{ Form::hidden('removed_ids', $value=NULL) }}
        <div class="two fields task-field hidden">
            <div class="fourteen wide field">
                {{ Form::label('task', 'New Task') }}
                {{ Form::text('task[]', $value=NULL, array('placeholder' => 'Enter Task','disabled' => 'disabled')) }}
            </div>
            <div class="two wide field">
                {{ Form::label('', '&nbsp;') }}
                <i class="large green add circle icon add-field"></i>
                <i class="large red minus circle icon remove-field"></i>
            </div>
        </div>
        @foreach ($timesheet_tasks as $timesheet_task)
            <div class="two fields task-field">
                <div class="fourteen wide field">
                    {{ Form::text('task['.$timesheet_task->id.']', $timesheet_task->task, array('placeholder' => 'Enter Task','data-id' => $timesheet_task->id,'class' => 'task-input')) }}
                </div>
                <div class="two wide field">
                    <i class="large red minus circle icon remove-task remove-field"></i>
                </div>
            </div>
        @endforeach
        <div class="two fields task-field">
            <div class="fourteen wide field">
                {{ Form::label('task', 'New Task') }}
                {{ Form::text('task[]', $value=NULL, array('placeholder' => 'Enter Task','data-name' => 'task')) }}
            </div>
            <div class="two wide field">
                {{ Form::label('', '&nbsp;') }}
                <i class="large green add circle icon add-field"></i>
                <i class="large red minus circle icon remove-field"></i>
            </div>
        </div>
    </div>
    {{ Form::submit('UPDATE TASKS',array('class' => 'ui submit green button')); }}
    {{ Form::close() }}
</div>

<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {
            $('.task-field-repeater').repeater('.task-field',false);
        });
    })(jQuery);
</script>
