<!-- Create Activities-->
<div class="ui bottom attached tab segment" data-tab="activities">
	<h3><i class="checkered flag icon"></i>Manage Default Activities</h3>
	{{ Form::open(array('class' => 'ui form activity-form UpdateActivitiesPg','route' => ['update.company', 1,'activities'])) }}
		<div class="activity-field-repeater">
			{{ Form::hidden('removed_ids', $value=NULL) }}
			<div class="two fields activity-field hidden">
				<div class="fourteen wide field">
					{{ Form::label('activity', 'New Activity') }}
					{{ Form::text('activity[]', $value=NULL, array('placeholder' => 'Enter Activity','disabled' => 'disabled')) }}
				</div>
				<div class="two wide field">
					{{ Form::label('', '&nbsp;') }}
					<i class="large green add circle icon add-field"></i>
					<i class="large red minus circle icon remove-field"></i>
				</div>
			</div>
			@foreach ($activities as $activity)
				<div class="two fields activity-field">
					<div class="fourteen wide field">
						{{ Form::text('activity['.$activity->id.']', $activity->activity, array('placeholder' => 'Enter Activity','data-id' => $activity->id,'class' => 'activity-input')) }}
					</div>
					<div class="two wide field">
						<i class="large red minus circle icon remove-activity remove-field"></i>
					</div>
				</div>
			@endforeach
			<div class="two fields activity-field">
				<div class="fourteen wide field">
					{{ Form::label('activity', 'New Activity') }}
					{{ Form::text('activity[]', $value=NULL, array('placeholder' => 'Enter Activity','data-name' => 'activity')) }}
				</div>
				<div class="two wide field">
					{{ Form::label('', '&nbsp;') }}
					<i class="large green add circle icon add-field"></i>
					<i class="large red minus circle icon remove-field"></i>
				</div>
			</div>
		</div>
		{{ Form::submit('UPDATE ACTIVITIES',array('class' => 'ui submit green button ActivityBtn')); }}
	{{ Form::close() }}
	<br>
	<div class="UpdateActivitiesAlert"></div>
</div>



<script>
	jQuery.noConflict();
	(function( $ ) {
		$(function() {

			// $('body').on('click', '.activity-form .remove-activity', function(event) {
			// 	alert('a');
			// 	removeId = $(this).closest('.activity-input').data('id');

			// 	console.log(removeId);
			// });

			$('.activity-field-repeater').repeater('.activity-field',false);


		});
	})(jQuery);
</script>
