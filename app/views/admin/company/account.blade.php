<!-- Edit Company Account-->
<div class="ui bottom attached tab segment" data-tab="account">
	<h3><i class="payment icon"></i> Account Maintenance</h3>
	<div class="ui stackable grid ui dimmable dimmed">

		

		<div class="ten wide column">
			<h5>Current Number of Users: 7</h5>
			<table class="ui celled table">
				<thead>
					<tr>
						<th>Name</th>
						<th>Role</th>
						<th>Last In</th>
						<th>Email</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				</tbody>			
			</table>
		</div>
		<div class="six wide column">
			<h5>Billing Information</h5>
			<div class="ui center aligned blue segment">
				<div class="ui small statistic">
					<div class="label">
						Monthly Level
					</div>
					<div class="value">
						$
					</div>
				</div>
				<br>
				<div class="ui tiny statistic">
					<div class="label">
						Last Billed On:
					</div>
					<div class="value">
						
					</div>
				</div>
				<div class="ui tiny statistic">
					<div class="label">
						Next Bill Due
					</div>
					<div class="value">
						
					</div>
				</div>												
			</div>
			<button class="ui green button">Update Payment Info</button>
			<button class="ui red button">Cancel Account</button>
		</div>

		<div class="ui dimmer inverted transition visible active" style="display: block !important;">
          <div class="content">
            <div class="center">
              <h1 style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color: #000000">
                	Billing Portal Coming Soon <span style="font-size: 80%; vertical-align: middle;">*</span>
              </h1>
            </div>
          </div>
        </div>


	</div>	
</div>	


<script>

	 (function($, window, document) {


	 	$(document).ready(function(){

	 		//$('div.ui.stackable.grid').dimmer('show');

	 		$('div.ui.dimmer.inverted').on('click', function(event){
	 			event.preventDefault();
	 			//$(this).removeClass('hidden').addClass('active');
	 			//console.log('click');
	 		});


	 	});

	  }(window.jQuery, window, document));

</script>