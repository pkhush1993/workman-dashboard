<!-- Edit Company Settings -->

<div class="ui bottom attached active tab segment" data-tab="settings">
	<h3><i class="setting icon"></i>Edit Company Settings</h3>
	{{ Form::open(array('class' => 'ui form settings-form UpdateCompanySettings','route' => ['update.company', 1,'settings'])) }}

		{{ Form::hidden('avatar',$company->getAvatar()) }}
		<!-- Edit Users Avatar -->
		<h3 class="ui header">
			Edit Avatar
			<div class="sub header">
				Drag Image Or Click Your Avatar to Edit
			</div>
		</h3>
		<br>
		<div class="field">
			<div class="avatar-upload-wrapper">
				<div id="upload-zone" class="upload-zone dropzone avatar dz-clickable" style="background-image:url({{url().$company->getAvatar()}})" action="/upload.php"></div>
			</div>
		</div>
		<cite style="color:#A2A2A2;">
			Recommended Size: 150x150px | Max File Size:1MB
		</cite>
		<div class="ui divider"></div>

		<div class="two fields">
			<div class="required field">
				{{ Form::label('next_invoice_number', 'Next Invoice #') }}
				{{ Form::number('next_invoice_number',$company->getNextInvoice(), array('placeholder'=>'Next Invoice #','min'=>'0','id'=>'next_invoice_number')) }}
			</div>
			<div class="field">
				<div class="two fields">
					<div class="required field">
						{{ Form::label('next_job_number', 'Next Job #') }}
						{{ Form::number('next_job_number',$company->getNextJob(), array('placeholder'=>'Next Job #','min'=>$company->getNextJob(),'id'=>'next_job_number')) }}
					</div>
					<div class="required field">
						{{ Form::label('time_entry_grace_period', 'Time Entry Grace Period (days)') }}
						{{ Form::number('time_entry_grace_period',$company->getTimeEntryGracePeriod(), array('placeholder'=>'Time Entry Grace Period','id'=>'time_entry_grace_period')) }}
					</div>
				</div>
			</div>
		</div>
		<div class="two fields">
			<div class="field">
				{{ Form::label('pay_period_type', 'Pay Period') }}
				{{ Form::select('pay_period_type', array(
					'' => 'Select Pay Period',
					'2_weeks' => '2 Weeks',
					'bi-monthly' => 'Bi-Monthly'
				), $company->getPayPeriod() , array('class'=>'ui search pay-period-type dropdown','autocomplete' =>'off')) }}
				<div class="two fields">
					<div class="required field pay_period_start_date" style="display:none">
						<br>
						{{ Form::label('pay_period_start', 'Pay Period Start Date') }}
						{{ Form::text('pay_period_start',date('m/d/Y',strtotime($company->getPayPeriodStart())), array('placeholder'=>'Period Date Start','class'=>'pay_period date','data-format'=>'MMMM dd yyyy')) }}
					</div>
					<div class="required field pay_period_start_1" style="display:none">
						<br>
						{{ Form::label('pay_period_start_1', 'Pay Period Start 1') }}
						{{ Form::select('pay_period_start_1', array('01'=>'1st' ,'02'=>'2nd' ,'03'=>'3rd' ,'04'=>'4th' ,'05'=>'5th' ,'06'=>'6th' ,'07'=>'7th' ,'08'=>'8th' ,'09'=>'9th' ,'10'=>'10th' ,'11'=>'11th' ,'12'=>'12th' ,'13'=>'13th' ,'14'=>'14th' ,'15'=>'15th' ,'16'=>'16th' ,'17'=>'17th' ,'18'=>'18th' ,'19'=>'19th' ,'20'=>'20th' ,'21'=>'21st' ,'22'=>'22nd' ,'23'=>'23rd' ,'24'=>'24th' ,'25'=>'25th' ,'26'=>'26th' ,'27'=>'27th' ,'28'=>'28th' ,'29'=>'29th' ,'30'=>'30th' ,'31'=>'31st'), $company->getPayPeriodStart1() , array('placeholder'=>'Pay Period Start 1','class'=>'ui search pay_period dropdown','autocomplete' =>'off')) }}
					</div>
					<div class="required field pay_period_start_2" style="display:none">
						<br>
						{{ Form::label('pay_period_start_2', 'Pay Period Start 2') }}
						{{ Form::select('pay_period_start_2', array('1'=>'1st' ,'2'=>'2nd' ,'3'=>'3rd' ,'4'=>'4th' ,'5'=>'5th' ,'6'=>'6th' ,'7'=>'7th' ,'8'=>'8th' ,'9'=>'9th' ,'10'=>'10th' ,'11'=>'11th' ,'12'=>'12th' ,'13'=>'13th' ,'14'=>'14th' ,'15'=>'15th' ,'16'=>'16th' ,'17'=>'17th' ,'18'=>'18th' ,'19'=>'19th' ,'20'=>'20th' ,'21'=>'21st' ,'22'=>'22nd' ,'23'=>'23rd' ,'24'=>'24th' ,'25'=>'25th' ,'26'=>'26th' ,'27'=>'27th' ,'28'=>'28th' ,'29'=>'29th' ,'30'=>'30th' ,'31'=>'31st'), $company->getPayPeriodStart2() , array('placeholder'=>'Pay Period Start 2','class'=>'ui search pay_period dropdown','autocomplete' =>'off')) }}
					</div>
				</div>
			</div>
			<div class="field">
				<div class="two fields">
					<div class="field">
						{{ Form::label('overtime_pay', 'Overtime Pay') }}
						<div class="ui left labeled input">
			                <div class="ui label"><b>x</b></div>
			                {{ Form::text('overtime_pay',$company->overtime_pay, array('placeholder'=>'Enter Overtime Pay')) }}
			            </div>
					</div>
					<div class="field">
						{{ Form::label('weekly_hours', 'Hours in a Work Week') }}
						{{ Form::text('weekly_hours',$company->weekly_hours, array('placeholder'=>'Enter Hours','class'=>'number ')) }}
					</div>
				</div>
				<div class="two fields">
					<div class="field">
						{{ Form::label('job_defaults', 'Copy Job Defaults') }}
						<div class="ui teal button job_defaults-button"><i class="icon checkmark box"></i>Set Copy Job Defaults</div>
					</div>
					<div class="field">
						{{ Form::label('contract-language', 'Contract Language') }}
						<div class="ui teal button contract-language-button"><i class="icon write"></i>Edit Contract Language</div>
					</div>
				</div>
			</div>
		</div>
		<div class="field">
			{{ Form::label('bid_header', 'Bid Header') }}
			{{ Form::textarea('bid_header',$company->getBidHeader(), array('placeholder'=>'Bid Header')) }}
		</div>
		<br>
		{{ Form::submit('UPDATE',array('class' => 'ui submit blue button CoSettingsBtn')); }}
	{{ Form::close() }}
	<br>
	<div class="CompanySettingsAlerts"></div>
	<div class="ui divider"></div>

	<!-- Custom Fields -->
	{{ Form::open(array('class' => 'ui form company_custom_fields','route' => ['update.company', 1,'custom_fields'])) }}
		<h3><i class="list layout icon"></i>Custom Fields</h3>
		<?php $custom_drop_1 = $company->getCustomDrop1(); ?>
		<?php $custom_drop_2 = $company->getCustomDrop2(); ?>
		<?php $custom_drop_3 = $company->getCustomDrop3(); ?>
		<?php $custom_text_1 = $company->getCustomText1(); ?>
		<?php $custom_text_2 = $company->getCustomText2(); ?>
		<?php $custom_text_3 = $company->getCustomText3(); ?>
		<br>
		<div class="ui stackable two column grid">
			<div class="column">
				<div class="ui accordion field">
					{{ Form::label('custom_drop_1', 'Custom Drop Field 1') }}
					<div class="title">
						<div class="ui grid">
							<div class="one wide column">
								<i class="large grey dropdown icon" style="margin-top: 10px;"></i>
							</div>
							<div class="fifteen wide column">
								<div class="field">
									<div class="ui icon input">
										{{ Form::text('custom_drop_1',($custom_drop_1 ? key($custom_drop_1) : ''), array('placeholder'=>'Enter Custom Drop Down Name')) }}
									  	<i class="caret down icon"></i>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="content field">
						<div class="custom_field_repeater repeater-field">
							@if ($custom_drop_1)
								@foreach ($custom_drop_1[key($custom_drop_1)] as $key => $field)
									<div class="custom_field cf">
								        <div class="field ui mini input">
								        	{{ Form::text('custom_drop_1_item[]',$field, array('placeholder'=>'Enter Custom Field Option' )) }}
								        </div>
										<div class="actions">
											<button type="button" class="ui button mini green add-field">Add field</button>
											<button type="button" class="ui button mini red remove-field">Remove</button>
										</div>
									</div>
								@endforeach
							@else
								<div class="custom_field cf">
							        <div class="field ui mini input">
							        	{{ Form::text('custom_drop_1_item[]',$value=NULL, array('placeholder'=>'Enter Custom Field Option' )) }}
							        </div>
									<div class="actions">
										<button type="button" class="ui button mini green add-field">Add field</button>
										<button type="button" class="ui button mini red remove-field">Remove</button>
									</div>
								</div>
							@endif
						</div>
					</div>
				</div>
				<div class="ui accordion field">
					{{ Form::label('custom_drop_2', 'Custom Drop Field 2') }}
					<div class="title">
						<div class="ui grid">
							<div class="one wide column">
								<i class="large grey dropdown icon" style="margin-top: 10px;"></i>
							</div>
							<div class="fifteen wide column">
								<div class="field">
									<div class="ui icon input">
										{{ Form::text('custom_drop_2',($custom_drop_2 ? key($custom_drop_2) : ''), array('placeholder'=>'Enter Custom Drop Down Name')) }}
									  	<i class="caret down icon"></i>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="content field">
						<div class="custom_field_repeater repeater-field">
							@if ($custom_drop_2)
								@foreach ($custom_drop_2[key($custom_drop_2)] as $key => $field)
									<div class="custom_field cf">
								        <div class="field ui mini input">
								        	{{ Form::text('custom_drop_2_item[]',$field, array('placeholder'=>'Enter Custom Field Option' )) }}
								        </div>
										<div class="actions">
											<button type="button" class="ui button mini green add-field">Add field</button>
											<button type="button" class="ui button mini red remove-field">Remove</button>
										</div>
									</div>
								@endforeach
							@else
								<div class="custom_field cf">
							        <div class="field ui mini input">
							        	{{ Form::text('custom_drop_2_item[]',$value=NULL, array('placeholder'=>'Enter Custom Field Option' )) }}
							        </div>
									<div class="actions">
										<button type="button" class="ui button mini green add-field">Add field</button>
										<button type="button" class="ui button mini red remove-field">Remove</button>
									</div>
								</div>
							@endif
						</div>
					</div>
				</div>
				<div class="ui accordion field">
					{{ Form::label('custom_drop_3', 'Custom Drop Field 3') }}
					<div class="title">
						<div class="ui grid">
							<div class="one wide column">
								<i class="large grey dropdown icon" style="margin-top: 10px;"></i>
							</div>
							<div class="fifteen wide column">
								<div class="field">
									<div class="ui icon input">
										{{ Form::text('custom_drop_3',($custom_drop_3 ? key($custom_drop_3) : ''), array('placeholder'=>'Enter Custom Drop Down Name')) }}
									  	<i class="caret down icon"></i>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="content field">
						<div class="custom_field_repeater repeater-field">
							@if ($custom_drop_3)
								@foreach ($custom_drop_3[key($custom_drop_3)] as $key => $field)
									<div class="custom_field cf">
								        <div class="field ui mini input">
								        	{{ Form::text('custom_drop_3_item[]',$field, array('placeholder'=>'Enter Custom Field Option' )) }}
								        </div>
										<div class="actions">
											<button type="button" class="ui button mini green add-field">Add field</button>
											<button type="button" class="ui button mini red remove-field">Remove</button>
										</div>
									</div>
								@endforeach
							@else
								<div class="custom_field cf">
							        <div class="field ui mini input">
							        	{{ Form::text('custom_drop_3_item[]',$value=NULL, array('placeholder'=>'Enter Custom Field Option' )) }}
							        </div>
									<div class="actions">
										<button type="button" class="ui button mini green add-field">Add field</button>
										<button type="button" class="ui button mini red remove-field">Remove</button>
									</div>
								</div>
							@endif
						</div>
					</div>
				</div>
	        </div>
			<div class="column">
		        <div class="field">
		        	{{ Form::label('custom_text_1', 'Custom Text Field 1') }}
		        	{{ Form::text('custom_text_1',$custom_text_1, array('placeholder'=>'Enter Custom Text Field' )) }}
		        </div>
		        <div class="field">
		        	{{ Form::label('custom_text_2', 'Custom Text Field 2') }}
		        	{{ Form::text('custom_text_2',$custom_text_2, array('placeholder'=>'Enter Custom Text Field')) }}
		        </div>
		        <div class="field">
		        	{{ Form::label('custom_text_3', 'Custom Text Field 3') }}
		        	{{ Form::text('custom_text_3',$custom_text_3, array('placeholder'=>'Enter Custom Text Field')) }}
		        </div>
	        </div>
	    </div>
	    <br><br>
		{{ Form::submit('UPDATE',array('class' => 'ui submit blue button CustmFieldsBtn')); }}
		<div class="ui error message"></div>
	{{ Form::close() }}
	<br>
	<div class="CompanyCustomFieldsAlert"></div>
</div>
<script>
	jQuery.noConflict();
	(function( $ ) {
		$(function() {

			//Semantic UI Custom Form Validation Rule For Pay Periods
			$.fn.form.settings.rules.payPeriod = function(value) {
				var periodStart1 = $('select[name=pay_period_start_1]').val();
				var periodStart2 = $('select[name=pay_period_start_2]').val();
				if ($('.pay-period-type').dropdown('get value') === 'bi-monthly'){
					if (periodStart1 > periodStart2){
						return false;
					}
					else{
						return true;
					}
				}
				else{
					return true;
				}
			}

			//Semantic UI Form Validation For Company Settings =========================
			$('.ui.form.settings-form').form(
				{
				    nextInvoice: {
						identifier  : 'next_invoice_number',
						rules: [
							{
								type   : 'integer[1..]',
								prompt : 'Number must be Positive'
							},
							{
								type   : 'integer',
								prompt : 'Please enter a number'
							},
							{
								type   : 'empty',
								prompt : 'Next Invoice # is required'
							}
						]
				    },
				    nextJob: {
						identifier  : 'next_job_number',
						rules: [
							{
								type   : 'integer[1..]',
								prompt : 'Number must be Positive'
							},
							{
								type   : 'integer',
								prompt : 'Please enter a number'
							},
							{
								type   : 'empty',
								prompt : 'Next Job # is required'
							}
						]
				    },
				    payStart: {
						identifier  : 'pay_period_start',
						rules: [
							{
								type   : 'empty',
								prompt : 'Pay Period Start is required'
							},
						]
				    },
				    payStart1: {
						identifier  : 'pay_period_start_1',
						rules: [
							{
								type   : 'empty',
								prompt : 'Pay Period End is required'
							},
							{
								type   : 'payPeriod',
								prompt : 'Period Start 1 Must be Before End Date'
							},
						]
				    },
				    payStart2: {
						identifier  : 'pay_period_start_2',
						rules: [
							{
								type   : 'empty',
								prompt : 'Pay Period End is required'
							},
							{
								type   : 'payPeriod',
								prompt : 'Period Start 2 Must be Greater Than Period Start 1'
							},
						]
				    }
				},
                {
                    inline : true,
                    on     : 'blur',
                        onSuccess: function () {
                           $('.ui.modal.add-account').modal('hide');
                        }
                }
			);

			// Pay Period Hidden Date Fields on Load
			if( $('select[name=pay_period_type]').val() == '2_weeks'){
				$('.pay_period_start_date').show();
			}

			if( $('select[name=pay_period_type]').val() == 'bi-monthly'){
				$('.pay_period_start_1').show();
				$('.pay_period_start_2').show();
			}

			// Pay Period Hidden Date Fields on Change
			$('select[name=pay_period_type]').change(function() {

				$('.pay_period_start_1').hide();
				$('.pay_period_start_2').hide();
				$('.pay_period_start_date').hide();

				if( $(this).val() == '2_weeks'){
					$('.pay_period_start_date').show();
				}
				else if( $(this).val() == 'bi-monthly' ){
					$('.pay_period_start_1').show();
					$('.pay_period_start_2').show();
				}

			});

			// Job Defaults Modal
			$('.job_defaults-button').pop('.ui.modal.job-defaults');

			// Contract Language Modal
			$('.contract-language-button').pop('.ui.modal.contract-language',true,false);

			// Custom Field Repeater Field
			$('.custom_field_repeater').repeater('.custom_field',false);

			// Company Avatar
			Dropzone.autoDiscover = false;

	  		var myDropzone = $("#upload-zone").dropzone({
	  			url: "/upload.php",
	  			dictDefaultMessage: "Click Here",
	  			maxFilesize: 2,
	  			addRemoveLinks:true,
	  			success: function (file, response) {
	  				console.log(response)
		            var imgName = response;
		            if(imgName) {
		            	$('.settings-form input[name=avatar').val(imgName);
		            }
		        },
		        error: function (file, response) {
					setTimeout(function(){
						alert('boom');
					  $('.dz-error-mark').hide('slow');
					}, 2000);
		        }
	  		});



		});
	})(jQuery);
</script>
