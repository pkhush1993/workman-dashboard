<!-- Job Defaults Modal -->
<div class="ui modal job-defaults">
	<h2 class="ui header">
	  <i class="configure icon"></i>
	  <div class="content">
			Set Job Copying Defaults
			<div class="sub header">Only the fields flagged in green will be used when copying a job.</div>
	  </div>
	</h2>
	{{ Form::open(array('class' => 'ui form','route' => ['update.company', 1,'job_defaults'])) }}		
		<div class="content" style="padding: 2rem;">	
			<div class="ui stackable four column grid">
				<div class="row">
					<div class="column">
						<div class="ui toggle checkbox">
							{{ Form::checkbox( 'account', 'account', in_array('account', $company->getJobDefaults()) ? true : false ) }} {{ Form::label('account', 'Account') }}
						</div>
					</div>
					<div class="column">
						<div class="ui toggle checkbox">
							{{ Form::checkbox( 'contacts', 'contacts', in_array('contacts', $company->getJobDefaults()) ? true : false ) }} {{ Form::label('contacts', 'Contacts') }}
						</div>
					</div>
					<div class="column">
						<div class="ui toggle checkbox">
							{{ Form::checkbox( 'prime_contact', 'prime_contact', in_array('prime_contact', $company->getJobDefaults()) ? true : false ) }} {{ Form::label('prime_contact', 'Prime Contact') }}
						</div>
					</div>
					<div class="column">
						<div class="ui toggle checkbox">
							{{ Form::checkbox( 'bid_date', 'bid_date', in_array('bid_date', $company->getJobDefaults()) ? true : false ) }} {{ Form::label('bid_date', 'Bid Date') }}
						</div>
					</div>					
				</div>
				<div class="row">
					<div class="column">
						<div class="ui toggle checkbox">
							{{ Form::checkbox( 'status', 'status', in_array('status', $company->getJobDefaults()) ? true : false ) }} {{ Form::label('status', 'Status') }}
						</div>
					</div>
					<div class="column">
						<div class="ui toggle checkbox">
							{{ Form::checkbox( 'site_address', 'site_address', in_array('site_address', $company->getJobDefaults()) ? true : false ) }} {{ Form::label('site_address', 'Site Address') }}
						</div>
					</div>
					<div class="column">
						<div class="ui toggle checkbox">
							{{ Form::checkbox( 'billing_address', 'billing_address', in_array('billing_address', $company->getJobDefaults()) ? true : false ) }} {{ Form::label('billing_address', 'Billing Address') }}
						</div>
					</div>
					<div class="column">
						<div class="ui toggle checkbox">
							{{ Form::checkbox( 'deadlines', 'deadlines', in_array('deadlines', $company->getJobDefaults()) ? true : false ) }} {{ Form::label('deadlines', 'Deadlines') }}
						</div>
					</div>					
				</div>
				<div class="row">
					<div class="column">
						<div class="ui toggle checkbox">
							{{ Form::checkbox( 'line_items', 'line_items', in_array('line_items', $company->getJobDefaults()) ? true : false ) }} {{ Form::label('line_items', 'Line Items') }}
						</div>
					</div>
					<div class="column">
						<div class="ui toggle checkbox">
							{{ Form::checkbox( 'comments', 'comments', in_array('comments', $company->getJobDefaults()) ? true : false ) }} {{ Form::label('comments', 'Comments') }}
						</div>
					</div>
					<div class="column">
						<div class="ui toggle checkbox">
							{{ Form::checkbox( 'notes', 'notes', in_array('notes', $company->getJobDefaults()) ? true : false ) }} {{ Form::label('notes', 'Notes') }}
						</div>
					</div>
					<div class="column">
						<div class="ui toggle checkbox">
							{{ Form::checkbox( 'estimator', 'estimator', in_array('estimator', $company->getJobDefaults()) ? true : false ) }} {{ Form::label('estimator', 'Estimator') }}
						</div>
					</div>					
				</div>
				<div class="row">
					<div class="column">
						<div class="ui toggle checkbox">
							{{ Form::checkbox( 'pm', 'pm', in_array('pm', $company->getJobDefaults()) ? true : false ) }} {{ Form::label('pm', 'Project Manager ') }}
						</div>
					</div>
					<div class="column">
						<div class="ui toggle checkbox">
							{{ Form::checkbox( 'foreman', 'foreman', in_array('foreman', $company->getJobDefaults()) ? true : false ) }} {{ Form::label('foreman', 'Foreman') }}
						</div>
					</div>
					<div class="column">
						<div class="ui toggle checkbox">
							{{ Form::checkbox( 'lead_source', 'lead_source', in_array('lead_source', $company->getJobDefaults()) ? true : false ) }} {{ Form::label('lead_source', 'Lead Source') }}
						</div>
					</div>
					<div class="column">
						<div class="ui toggle checkbox">
							{{ Form::checkbox( 'custom1', 'custom1', in_array('custom1', $company->getJobDefaults()) ? true : false ) }} {{ Form::label('custom1', 'Custom 1') }}
						</div>
					</div>					
				</div>
				<div class="row">
					<div class="column">
						<div class="ui toggle checkbox">
							{{ Form::checkbox( 'custom2', 'custom2', in_array('custom2', $company->getJobDefaults()) ? true : false ) }} {{ Form::label('custom2', 'Custom 2') }}
						</div>
					</div>
					<div class="column">
						<div class="ui toggle checkbox">
							{{ Form::checkbox( 'custom3', 'custom3', in_array('custom3', $company->getJobDefaults()) ? true : false ) }} {{ Form::label('custom3', 'Custom 3') }}
						</div>
					</div>
					<div class="column">
						<div class="ui toggle checkbox">
							{{ Form::checkbox( 'custom4', 'custom4', in_array('custom4', $company->getJobDefaults()) ? true : false ) }} {{ Form::label('custom4', 'Custom 4') }}
						</div>
					</div>
					<div class="column">
						<div class="ui toggle checkbox">
							{{ Form::checkbox( 'custom5', 'custom5', in_array('custom5', $company->getJobDefaults()) ? true : false ) }} {{ Form::label('custom5', 'Custom 5') }}
						</div>
					</div>									
				</div>												
			</div>
		</div>
		<div class="actions">
			{{ Form::submit('Save Job Defaults',array('class' => 'ui positive right icon button')); }}
		</div>
		<div class="ui error message"></div>
	{{ Form::close() }}
</div>