<div class="ui bottom attached tab segment" data-tab="comments">
    <h3><i class="comments icon"></i>Manage Standard Bid Comments</h3>
    {{ Form::open(array('class' => 'ui form comment-form','route' => ['update.company', 1,'comments'])) }}
    <div class="comment-field-repeater">
        {{ Form::hidden('removed_ids', $value=NULL) }}
        <div class="two fields comment-field hidden">
            <div class="fourteen wide field">
                {{ Form::label('comment', 'New Comment') }}
                {{ Form::text('comment[]', $value=NULL, array('placeholder' => 'Enter Comment','disabled' => 'disabled')) }}
            </div>
            <div class="two wide field">
                {{ Form::label('', '&nbsp;') }}
                <i class="large green add circle icon add-field"></i>
                <i class="large red minus circle icon remove-field"></i>
            </div>
        </div>
        @foreach ($comments as $comment)
            <div class="two fields comment-field">
                <div class="fourteen wide field">
                    {{ Form::text('comment['.$comment->id.']', $comment->comment, array('placeholder' => 'Enter Activity','data-id' => $comment->id,'class' => 'comment-input')) }}
                </div>
                <div class="two wide field">
                    <i class="large red minus circle icon remove-comment remove-field"></i>
                </div>
            </div>
        @endforeach
        <div class="two fields comment-field">
            <div class="fourteen wide field">
                {{ Form::label('comment', 'New Comment') }}
                {{ Form::text('comment[]', $value=NULL, array('placeholder' => 'Enter Comment','data-name' => 'comment')) }}
            </div>
            <div class="two wide field">
                {{ Form::label('', '&nbsp;') }}
                <i class="large green add circle icon add-field"></i>
                <i class="large red minus circle icon remove-field"></i>
            </div>
        </div>
    </div>
    {{ Form::submit('UPDATE BID COMMENTS',array('class' => 'ui submit green button')); }}
    {{ Form::close() }}
</div>

<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {
            $('.comment-field-repeater').repeater('.comment-field',false);
        });
    })(jQuery);
</script>
