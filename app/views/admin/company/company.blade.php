@extends('layouts.default')

@section('content')

    <div id="company_settings">

		<h1><img class="ui avatar circular image" src="{{ $company->avatar }}">Edit Company</h1>

		<div class="ui top attached tabular menu">
			<a class="active item" data-tab="settings"><i class="setting icon"></i>Settings</a>	
			<a class="item" data-tab="personal"><i class="building icon"></i>Company Info</a>
		  	<a class="item" data-tab="users"><i class="users icon"></i>User Management</a>
		  	<a class="item" data-tab="bid"><i class="circle icon"></i>Bid Fields</a>
			<a class="item" data-tab="comments"><i class="comments icon"></i>Bid Comments</a>
		  	<a class="item" data-tab="activities"><i class="checkered flag icon"></i>Activities</a>
			<a class="item" data-tab="lead-sources"><i class="unordered list icon"></i>Lead Sources</a>
		  	<a class="item" data-tab="account"><i class="payment icon"></i>Account</a>
		  	<a class="item" data-tab="tax-rates"><i class="calculator icon"></i>Tax Rates</a>
		  	<a class="item" data-tab="timesheet-task"><i class="tasks icon"></i>Timesheet Task</a>
		</div>


		<!-- Edit Company Settings -->
		@include('admin.company.settings')

		<!-- Edit Company Info -->
		@include('admin.company.info')
		
		<!-- Manage Users -->
		@include('admin.company.manage-users')

		<!-- Edit User Bid Fields -->
		@include('admin.company.bid-fields')

		<!-- Edit User Bid Fields -->
		@include('admin.company.bid-comments')

		<!-- Edit Company Account-->
		@include('admin.company.account')

		<!-- Job Defaults Modal -->
		@include('admin.company.job-defaults')

		<!-- Contract Language Modal -->
		@include('admin.company.contract-language')

		<!-- Setup Default Activity Options -->
		@include('admin.company.activities')

		<!-- Setup Default Lead Source Options -->
		@include('admin.company.lead-sources')

		<!-- Setup Default Tax Rates -->
		@include('admin.company.tax-rates')

		<!-- Setup Timesheet Task -->
		@include('admin.company.timesheet-task')

	</div>
	<script>
		(function($) {
			$(function() {
				$('.ui.tabular')
						.tab({
							historyType : 'state',
							path        : '{{ \Request::url() }}'
						})
				;
			});
			// ************
			function AlertMsg(Type,  Msg1, Msg2){
				var Data =  '<div class="ui ' + Type + ' message">'+
						'<i class="close icon"></i>'+
						'<div class="header">'+
						Msg1 +
				'</div>'+
				'<p>' + Msg2 + '</p>'+
				'</div>';
				return Data;
			}
			//Updating Company Info
			$(".CompanyInfoForm").submit(function(e){
				e.preventDefault();
				var Url = $(this).attr("action");


				var request = $.ajax({
					url: Url,
					method: "POST",
					data: $(this).serialize(),
					dataType: "json"
				});

				request.done(function( msg ) {
//					console.log(msg);
//					var Data = msg.substring(6);
//					var DataMsg = $.parseJSON(Data );
					if (msg.detail == "success") {
						$(".CompanyInfMsgPlace").html(AlertMsg('success',  'Success!', 'Your settings has been updated successfully!'));

					}
				});

				request.fail(function( jqXHR, textStatus ) {
					$(".CompanyInfMsgPlace").html(AlertMsg('error',  'Error!', 'Please make sure all your entered information are correct and try again!'));
				});

				setTimeout(function(){
					$(".UpdateButton").removeAttr("disabled");
				}, 700);
			});
			// Updating user management
			$(".InviteUserFrm").submit(function(e){
				e.preventDefault();
				var Url = $(this).attr("action");

				var request = $.ajax({
					url: Url,
					method: "POST",
					data: $(this).serialize(),
					dataType: "json"
				});

				request.done(function( msg ) {
//					console.log(msg);
//					var Data = msg.substring(6);
//					var DataMsg = $.parseJSON(Data );
					if (msg.detail == "success") {
						$(".CompanyUserMsgPlace").html(AlertMsg('success',  'Success!', msg.message));
						$(".InviteUserFrm").find("input[type=text]").val("");
						$(".InviteUserFrm").find("input[type=checkbox]").prop("checked", false);
					} else {
						$(".CompanyUserMsgPlace").html(AlertMsg('error',  'Error!', msg.message));
					}
				});

				request.fail(function( msg, textStatus ) {
//					console.log(msg);
					$(".CompanyUserMsgPlace").html(AlertMsg('error',  'Error!', msg.message));
				});

				setTimeout(function(){
					$(".ManageUserBtn").removeAttr("disabled");
				}, 700);
			});
			// Updating Bid Fields
			$(".UpdateBidFields").submit(function(e){
				e.preventDefault();
				var Url = $(this).attr("action");

				var request = $.ajax({
					url: Url,
					method: "POST",
					data: $(this).serialize(),
					dataType: "json"
				});

				request.done(function( msg ) {
					console.log(msg);
//					var Data = msg.substring(6);
//					var DataMsg = $.parseJSON(Data );
					if (msg.detail == "success") {
						$(".UpdatedFieldsMsg").html(AlertMsg('success',  'Success!', 'Your settings has been updated successfully!'));

					}
				});

				request.fail(function( jqXHR, textStatus ) {
					$(".UpdatedFieldsMsg").html(AlertMsg('error',  'Error!', 'Please make sure all your entered information are correct and try again!'));
				});

				setTimeout(function(){
					$(".AddNewFieldsBtn").removeAttr("disabled");
					$(".UpdateBidFields").find("button").removeAttr("disabled");
				}, 700);
			});

			// Updating Bid Comments
			$(".UpdateBidComment").submit(function(e){
				e.preventDefault();
				var Url = $(this).attr("action");

				var request = $.ajax({
					url: Url,
					method: "POST",
					data: $(this).serialize(),
					dataType: "json"
				});

				request.done(function( msg ) {
//					console.log(msg);
//					var Data = msg.substring(6);
//					var DataMsg = $.parseJSON(Data );
					if (msg.detail == "success") {
						$(".UpdateBidsComments").html(AlertMsg('success',  'Success!', msg.message));
					}else{
						$(".UpdateBidsComments").html(AlertMsg('error',  'Error!', msg.message));
					}
				});

				request.fail(function( msg, textStatus ) {
//					console.log(msg);
					$(".UpdateBidsComments").html(AlertMsg('error',  'Error!', msg.message));
				});

				setTimeout(function(){
					$(".UpdateBidCommt").removeAttr("disabled");
					$(".UpdateBidsComments").find("button").removeAttr("disabled");
				}, 700);
			});

			// Updating Activities
			$(".UpdateActivitiesPg").submit(function(e){
				e.preventDefault();
				var Url = $(this).attr("action");

				var request = $.ajax({
					url: Url,
					method: "POST",
					data: $(this).serialize(),
					dataType: "json"
				});

				request.done(function( msg ) {
//					console.log(msg);
//					var Data = msg.substring(6);
//					var DataMsg = $.parseJSON(Data );
					if (msg.detail == "success") {
						$(".UpdateActivitiesAlert").html(AlertMsg('success',  'Success!', msg.message));
					}else{
						$(".UpdateActivitiesAlert").html(AlertMsg('error',  'Error!', msg.message));
					}
				});

				request.fail(function( msg, textStatus ) {
//					console.log(msg);
					$(".UpdateActivitiesAlert").html(AlertMsg('error',  'Error!', msg.message));
				});

				setTimeout(function(){
					$(".ActivityBtn").removeAttr("disabled");
					$(".UpdateActivitiesAlert").find("button").removeAttr("disabled");
				}, 700);
			});

			// Updating Lead Source
			$(".UpdateLeadSource").submit(function(e){
				e.preventDefault();
				var Url = $(this).attr("action");

				var request = $.ajax({
					url: Url,
					method: "POST",
					data: $(this).serialize(),
					dataType: "json"
				});

				request.done(function( msg ) {
//					console.log(msg);
//					var Data = msg.substring(6);
//					var DataMsg = $.parseJSON(Data );
					if (msg.detail == "success") {
						$(".UpdateLeadsAlert").html(AlertMsg('success',  'Success!', msg.message));
					}else{
						$(".UpdateLeadsAlert").html(AlertMsg('error',  'Error!', msg.message));
					}
				});

				request.fail(function( msg, textStatus ) {
//					console.log(msg);
					$(".UpdateLeadsAlert").html(AlertMsg('error',  'Error!', msg.message));
				});

				setTimeout(function(){
					$(".LeadSourceBtn").removeAttr("disabled");
					$(".UpdateLeadsAlert").find("button").removeAttr("disabled");
				}, 700);
			});
			// Updating Tax rates
			$(".UpdateTaxRates").submit(function(e){
				e.preventDefault();
				var Url = $(this).attr("action");

				var request = $.ajax({
					url: Url,
					method: "POST",
					data: $(this).serialize(),
					dataType: "json"
				});

				request.done(function( msg ) {
//					console.log(msg);
//					var Data = msg.substring(6);
//					var DataMsg = $.parseJSON(Data );
					if (msg.detail == "success") {
						$(".UpdateTaxRatesAlert").html(AlertMsg('success',  'Success!', msg.message));
					}else{
						$(".UpdateTaxRatesAlert").html(AlertMsg('error',  'Error!', msg.message));
					}
				});

				request.fail(function( msg, textStatus ) {
//					console.log(msg);
					$(".UpdateTaxRatesAlert").html(AlertMsg('error',  'Error!', msg.message));
				});

				setTimeout(function(){
					$(".UpdateTaxRatesBtn").removeAttr("disabled");
					$(".UpdateTaxRatesAlert").find("button").removeAttr("disabled");
				}, 700);
			});
			// Updating Company settings
			$(".UpdateCompanySettings").submit(function(e){
				e.preventDefault();
				var Url = $(this).attr("action");

				var request = $.ajax({
					url: Url,
					method: "POST",
					data: $(this).serialize(),
					dataType: "json"
				});

				request.done(function( msg ) {
//					console.log(msg);
//					var Data = msg.substring(6);
//					var DataMsg = $.parseJSON(Data );
					if (msg.detail == "success") {
						$(".CompanySettingsAlerts").html(AlertMsg('success',  'Success!', msg.message));
					}else{
						$(".CompanySettingsAlerts").html(AlertMsg('error',  'Error!', msg.message));
					}
				});

				request.fail(function( msg, textStatus ) {
//					console.log(msg);
					$(".CompanySettingsAlerts").html(AlertMsg('error',  'Error!', msg.message));
				});

				setTimeout(function(){
					$(".CoSettingsBtn").removeAttr("disabled");
					$(".UpdateCompanySettings").find("button").removeAttr("disabled");
				}, 700);
			});

			// Updating Company settings custom fields
			$(".company_custom_fields").submit(function(e){
				e.preventDefault();
				var Url = $(this).attr("action");

				var request = $.ajax({
					url: Url,
					method: "POST",
					data: $(this).serialize(),
					dataType: "json"
				});

				request.done(function( msg ) {
//					console.log(msg);
//					var Data = msg.substring(6);
//					var DataMsg = $.parseJSON(Data );
					if (msg.detail == "success") {
						$(".CompanyCustomFieldsAlert").html(AlertMsg('success',  'Success!', msg.message));
					}else{
						$(".CompanyCustomFieldsAlert").html(AlertMsg('error',  'Error!', msg.message));
					}
				});

				request.fail(function( msg, textStatus ) {
//					console.log(msg);
					$(".CompanyCustomFieldsAlert").html(AlertMsg('error',  'Error!', msg.message));
				});

				setTimeout(function(){
					$(".CustmFieldsBtn").removeAttr("disabled");
					$(".company_custom_fields").find("button").removeAttr("disabled");
				}, 700);
			});
			//****
			$("body").on('click', '.close.icon', function(){
				$(this).closest(".ui.message").css("display", "none");
			});
			//******
		})(jQuery)

	</script>
@stop