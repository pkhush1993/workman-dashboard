<!-- Contract Language Modal -->
<div class="ui contract-language scrollable modal">
	<h2 class="ui header">
	  <i class="configure icon"></i>
	  <div class="content">
			Contract Language
			<div class="sub header">Copy in your Contract Text and Use the Top Bar to Format</div>
	  </div>
	</h2>
	<div class="content" style="padding: 2rem;">
		<div class="quill-wrapper" style="border: 1px solid #ccc;">
			<div id="toolbar" class="toolbar ql-toolbar ql-snow" style="border-bottom: 1px solid #ccc;background: #F5F5F5;">
				<select title="Size" class="ql-size">
			      	<option value="10px">Small</option>
			      	<option value="13px" selected="">Normal</option>
			      	<option value="18px">Large</option>
			      	<option value="32px">Huge</option>
			 	</select>
				<span class="ql-format-group">
					<span title="Bold" class="ql-format-button ql-bold"></span>
					<span class="ql-format-separator"></span>
					<span title="Italic" class="ql-format-button ql-italic"></span>
					<span class="ql-format-separator"></span>
					<span title="Underline" class="ql-format-button ql-underline"></span>
					<span class="ql-format-separator"></span>
					<span title="Strikethrough" class="ql-format-button ql-strike"></span>
				</span>
				<span class="ql-format-group">
					<span title="List" class="ql-format-button ql-list"></span>
					<span class="ql-format-separator"></span>
					<span title="Bullet" class="ql-format-button ql-bullet"></span>
					<span class="ql-format-separator"></span>
					<select title="Text Alignment" class="ql-align">
				      	<option value="left"></option>
				      	<option value="center"></option>
				      	<option value="right"></option>
				      	<option value="justify"></option>
				 	</select>						
				</span>
				<span class="ql-format-group">
					<span title="Link" class="ql-format-button ql-link"></span>
				</span>
			</div>
			<!-- Create the editor container -->
			<div class="contract-language-text" style="height: 250px;">
			  	{{$company->getContractLanguage()}}
			</div>				
		</div>		
		{{ Form::open(array('class' => 'ui form','route' => ['update.company', 1,'contract_language'])) }}
			{{ Form::textarea('contract_language',$company->getContractLanguage(), array('placeholder'=>'Contract Language','style' => 'display:none')) }}
			<br>
			{{ Form::submit('Save Contract Language',array('class' => 'ui positive right icon button')); }}
		{{ Form::close() }}	
	</div>
</div>


<link rel="stylesheet" href="http://cdn.quilljs.com/0.19.12/quill.snow.css">


<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {
			// Initialize editor with custom theme and modules
			var fullEditor = new Quill('.contract-language-text', {
			  modules: {
			    'toolbar': { container: '#toolbar' },
			    'link-tooltip': true
			  },
			  theme: 'snow'
			});

			// ======== Contract Language WYSIWYG Copy Changes of Canvas over to Hidden Text Area ========

			//Editor Changes
			$('body').on('input propertychange keyup','.contract-language-text .ql-editor', function () {
				console.log($(this).html());
				$('textarea[name=contract_language]').text($(this).html());
			});

			//Toolbar Changes
			$('body').on('click','.ql-toolbar', function () {
				console.log($('.contract-language-text .ql-editor').html());
				$('textarea[name=contract_language]').text($('.contract-language-text .ql-editor').html());
			});

        });
    })(jQuery);	
</script>