<!-- Edit Company Info -->
<div class="ui bottom attached tab segment" data-tab="personal">
	<h3><i class="building icon"></i>Contact Information</h3>
	<?php
		if ($company){
			$CompanyId = $company->id;
		}else{
			$CompanyId = 1;
		}
	?>
	{{ Form::open(array('class' => 'ui form CompanyInfoForm','route' => ['update.company', $CompanyId,'info'])) }}
        <div class="field">
        	{{ Form::label('company_name', 'Name') }}
        	{{ Form::text('company_name',$value = $company->getName(), array('placeholder'=>'Company Name')) }}
        </div>
		<div class="two fields">
	        <div class="field">
	        	{{ Form::label('email', 'Email') }}
	        	{{ Form::text('email',$value = $company->getEmail(), array('placeholder'=>'Email')) }}
	        </div>
			<div class="field">
				{{ Form::label('website', 'Website') }}
				{{ Form::text('website',$company->getWebsite(), array('placeholder'=>'Website')) }}
			</div>
			</div>	
		<div class="two fields">
			<div class="field">
				{{ Form::label('phone', 'Phone Number') }}
				{{ Form::text('phone',$company->getPhone(), array('placeholder'=>'Phone Number','class'=>'phone')) }}
			</div>
			<div class="field">
				{{ Form::label('fax', 'Fax Number') }}
				{{ Form::text('fax',$company->getFax(), array('placeholder'=>'Fax Number','class'=>'phone')) }}
			</div>					
		</div>	 			
		{{ Form::label('address_1', 'Address') }}
		<div class="two fields">
			<div class="field">
				{{ Form::text('address_1',$company->getAddress1(), array('placeholder'=>'Address')) }}
			</div>
			<div class="field">
				{{ Form::text('address_2',$company->getAddress2(), array('placeholder'=>'Address 2')) }}
			</div>
		</div>					
		<div class="fields">
			<div class="seven wide field">
				{{ Form::label('city', 'City') }}
				{{ Form::text('city',$company->getCity(), array('placeholder'=>'City')) }}
			</div>
			<div class="five wide field">
				{{ Form::label('state', 'State') }}
				{{ Form::select('state', array(
					'' => 'Select State',
					'AL' => 'Alabama',
					'AK' => 'Alaska',
					'AZ' => 'Arizona',
					'AR' => 'Arkansas',
					'CA' => 'California',
					'CO' => 'Colorado',
					'CT' => 'Connecticut',
					'DE' => 'Delaware',
					'DC' => 'District Of Columbia',
					'FL' => 'Florida',
					'GA' => 'Georgia',
					'HI' => 'Hawaii',
					'ID' => 'Idaho',
					'IL' => 'Illinois',
					'IN' => 'Indiana',
					'IA' => 'Iowa',
					'KS' => 'Kansas',
					'KY' => 'Kentucky',
					'LA' => 'Louisiana',
					'ME' => 'Maine',
					'MD' => 'Maryland',
					'MA' => 'Massachusetts',
					'MI' => 'Michigan',
					'MN' => 'Minnesota',
					'MS' => 'Mississippi',
					'MO' => 'Missouri',
					'MT' => 'Montana',
					'NE' => 'Nebraska',
					'NV' => 'Nevada',
					'NH' => 'New Hampshire',
					'NJ' => 'New Jersey',
					'NM' => 'New Mexico',
					'NY' => 'New York',
					'NC' => 'North Carolina',
					'ND' => 'North Dakota',
					'OH' => 'Ohio',
					'OK' => 'Oklahoma',
					'OR' => 'Oregon',
					'PA' => 'Pennsylvania',
					'RI' => 'Rhode Island',
					'SC' => 'South Carolina',
					'SD' => 'South Dakota',
					'TN' => 'Tennessee',
					'TX' => 'Texas',
					'UT' => 'Utah',
					'VT' => 'Vermont',
					'VA' => 'Virginia',
					'WA' => 'Washington',
					'WV' => 'West Virginia',
					'WI' => 'Wisconsin',
					'WY' => 'Wyoming',
				), $company->getState() , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
			</div>
			<div class="four wide field">
				{{ Form::label('zip', 'Zip') }}
				{{ Form::text('zip',$company->getZip(), array('placeholder'=>'Zip Code')) }}
			</div>
		</div>

		<div class="ui grid">
			<div class="six wide column">
				{{ Form::submit('UPDATE',array('class' => 'ui submit blue button UpdateButton')); }}
			</div>
			<div class="ten wide column">
			</div>
		</div>

		<div class="ui error message"></div>
	{{ Form::close() }}
	<br>
	<div class="CompanyInfMsgPlace"></div>
</div>

