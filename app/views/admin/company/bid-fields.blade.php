<!-- Edit User Bid Fields -->
<div class="ui bottom attached tab segment" data-tab="bid">
	{{ Form::open(array('class' => 'ui form UpdateBidFields','route' => ['create.bid.fields', 1])) }}
		<div class="bid_fields_repeater repeater-field">
			<div class="ui stackable grid hidden bid_field">
				<div class="two wide column">
					<br>
					<i class="large caret down icon icon"></i>
					<i class="large green add circle icon add-bid-field"></i>
					<i class="large red minus circle icon remove-bid-field"></i>
				</div>
				<div class="fourteen wide column">
					<div class="row">
						<div class="ui stackable grid">
							<div class="column">
								<div class="field">
									{{ Form::hidden('', 0 ,array('data-name' => 'bid_field_id')) }}
									{{ Form::label('bid_field', 'Category') }}
									{{ Form::text('', $value=NULL, array('placeholder' => 'Category Name','data-name' => 'bid_field')) }}
								</div>							
							</div>
							<!-- <div class="one wide column">
								<br>
								<i class="large sort icon"></i>
							</div> -->	
						</div>
					</div>
					<div class="row bid_fields">
						<div class="ui stackable grid bid_fields_row">
							<div class="six wide column">
								<div class="field">
									{{ Form::label('bid_description', 'Product/Service') }}
									<div class="ui mini input">
										{{ Form::text('', $value=NULL, array('placeholder' => 'Product/Service','data-name' => 'bid_description')) }}
									</div>
								</div>	
							</div>
							<div class="three wide column">
								<div class="field">
									{{ Form::label('bid_units', 'Units') }}
									<div class="ui mini input">
										{{ Form::text('', $value=NULL, array('placeholder' => 'Units','data-name' => 'bid_units')) }}
									</div>
								</div>	
							</div>
							<div class="three wide column">
								<div class="field">
									{{ Form::label('bid_service', 'Price') }}
									<div class="ui mini input">
										{{ Form::text('', $value=NULL, array('placeholder' => 'Price','data-name' => 'bid_price')) }}
									</div>
								</div>
							</div>
							<div class="four wide column">
								<div class="actions">
									<br>
									<button type="button" class="ui button mini green add-field">Add field</button>
									<button type="button" class="ui button mini red remove-field">Remove</button>
								</div>							
							</div>							
						</div>								
					</div>																				
				</div>
			</div>			
			<!-- Save New Bid Fields -->
			<h3>Add New Default Fields</h3>
			<div class="ui stackable grid bid_field">
				<div class="two wide column">
					<br>
					<i class="large caret down icon icon"></i>
					<i class="large green add circle icon add-bid-field"></i>
					<i class="large red minus circle icon remove-bid-field"></i>
				</div>
				<div class="fourteen wide column">
					<div class="row">
						<div class="ui stackable grid">
							<div class="column">
								<div class="field">
									{{ Form::hidden('bid_field_id[1]', 0 ) }}
									{{ Form::label('bid_field', 'Category') }}
									{{ Form::text('bid_field[1]', $value=NULL, array('placeholder' => 'Category Name','data-name' => 'bid_field')) }}
								</div>							
							</div>
							<!-- <div class="one wide column">
								<br>
								<i class="large sort icon"></i>
							</div> -->	
						</div>
					</div>
					<div class="row bid_fields">
						<div class="ui stackable grid bid_fields_row">
							<div class="six wide column">
								<div class="field">
									{{ Form::label('bid_description', 'Product/Service') }}
									<div class="ui mini input">
										{{ Form::text('bid_description[1][]', $value=NULL, array('placeholder' => 'Product/Service','data-name' => 'bid_description')) }}
									</div>
								</div>	
							</div>
							<div class="three wide column">
								<div class="field">
									{{ Form::label('bid_units', 'Units') }}
									<div class="ui mini input">
										{{ Form::text('bid_units[1][]', $value=NULL, array('placeholder' => 'Units','data-name' => 'bid_units')) }}
									</div>
								</div>	
							</div>
							<div class="three wide column">
								<div class="field">
									{{ Form::label('bid_service', 'Price') }}
									<div class="ui mini input">
										{{ Form::text('bid_price[1][]', $value=NULL, array('placeholder' => 'Price','data-name' => 'bid_price')) }}
									</div>
								</div>
							</div>
							<div class="four wide column">
								<div class="actions">
									<br>
									<button type="button" class="ui button mini green add-field">Add field</button>
									<button type="button" class="ui button mini red remove-field">Remove</button>
								</div>							
							</div>							
						</div>								
					</div>																				
				</div>
			</div>
		</div>	
		<br><br>	
		{{ Form::submit('ADD FIELD',array('class' => 'ui submit blue button AddNewFieldsBtn')); }}
	{{ Form::close() }}
	<br>
	<div class="UpdatedFieldsMsg"></div>
	<div class="ui divider"></div>
	<!-- Edit Existing Bid Fields -->
	{{ Form::open(array('class' => 'ui form', 'id' => 'edit_bid_fields', 'route' => ['update.bid.fields', 1])) }}
		<h3>Edit Bid Fields</h3>
		@foreach ($bid_fields as $key => $bid_field)
			<div class="ui stackable grid bid_field">
				<div class="two wide column">
					<br>
					<i class="large caret right icon icon"></i>
					<!-- <i class="large green add circle icon add-bid-field"></i> -->
					<i class="large red minus circle icon remove-bid-field"  data-id="{{ $key }}"></i><small>Remove</small>
				</div>
				<div class="fourteen wide column">
					<div class="row">
						<div class="ui stackable grid">
							<div class="column">
								<div class="field">
									{{ Form::hidden('bid_field_id['.$key.']', $key ) }} 
									{{ Form::hidden('remove_bid_field['.$key.']', $key, array('disabled' => 'disabled' )) }}
									{{ Form::label('bid_field', 'Category') }}
									{{ Form::text('bid_field['.$key.']', $bid_field['name'], array('placeholder' => 'Category Name','data-name' => 'bid_field')) }}
								</div>							
							</div>
						</div>
					</div>
					<div class="row bid_fields" style="display:none">
						@foreach ($bid_field['items'] as $bid_field_items)
						<div class="ui stackable grid bid_fields_row">
							{{ Form::hidden('bid_field_item_id['.$key.'][]', $bid_field_items['bid_field_item_id'] ) }}	
							<div class="six wide column">
								<div class="field">
									{{ Form::label('bid_description', 'Product/Service') }}
									<div class="ui mini input">
										{{ Form::text('bid_description['.$key.'][]', $bid_field_items['description'], array('placeholder' => 'Product/Service','data-name' => 'bid_description')) }}
									</div>
								</div>	
							</div>
							<div class="three wide column">
								<div class="field">
									{{ Form::label('bid_units', 'Units') }}
									<div class="ui mini input">
										{{ Form::text('bid_units['.$key.'][]', $bid_field_items['units'], array('placeholder' => 'Units','data-name' => 'bid_units')) }}
									</div>
								</div>	
							</div>
							<div class="three wide column">
								<div class="field">
									{{ Form::label('bid_service', 'Price') }}
									<div class="ui mini input">
										{{ Form::text('bid_price['.$key.'][]', $bid_field_items['price'], array('placeholder' => 'Price','data-name' => 'bid_price')) }}
									</div>
								</div>
							</div>
							<div class="four wide column">
								<div class="actions">
									<br>
									<button type="button" class="ui button mini green add-field">Add field</button>
									<button type="button" data-id="{{ $bid_field_items['bid_field_item_id'] }}" class="ui button mini red remove-field">Remove</button>
								</div>							
							</div>							
						</div>
						@endforeach		
					</div>																		
				</div>
			</div>
		@endforeach
		<br><br>	
		{{ Form::submit('UPDATE',array('class' => 'ui submit blue button')); }}
	{{ Form::close() }}		
	<br><br>


	<script>
		jQuery.noConflict();
		(function( $ ) {
			$(function() {

				// BID FIELD CATEGORY REPEATER FIELD FUNCTION
				// ------------------------------------------------------------
				var maxInputs = 20; //maximum input boxes allowed
				var fieldCount = 1; //Track the field amounts
				$.fn.bidFieldRepeater = function(fieldClass,increment) {
					this.each(function() {
					    
					    var fieldWrapper = $(this);

					    fieldWrapper.find(".add-bid-field").click(function(e) {
					    	if(fieldCount <= maxInputs) //max input boxes allowed
					    	{

					        	var clonedObject = $(fieldClass+':first-child', fieldWrapper).clone(true);
					        	clonedObject.appendTo(fieldWrapper).find('input').focus().val('');
					        	clonedObject.removeClass('hidden');
					        	fieldCount++;//text box added increment

					        		// Setup Naming Convention
									clonedObject.find('input').each(function () {
										if ($(this).attr('data-name') == 'bid_field'  || $(this).attr('data-name') == 'bid_field_id' )  {
				    						
				    						var originalName = $(this).attr('data-name');
				    						$(this).attr('name', originalName + "["+fieldCount+"]" );

										}
										else{
				    						var originalName = $(this).attr('data-name');
				    						$(this).attr('name', originalName + "["+fieldCount+"]" + "[]" );								
										}
										
										// Set Default Value for Bid Field ID to 0
										if ($(this).attr('data-name') == 'bid_field_id' )  {
											$(this).val(0);
										}	

									});

					    	}
					    });
					    $(fieldClass + ' .remove-bid-field').click(function() {
					        if (fieldWrapper.find(fieldClass).length > 1)
					        {
					        	$(this).closest(fieldClass).remove();
					        }
					    });
					});
				return this;
				};

				// Bid Fields Repeater Add
				    $('body').on('click', '.bid_fields_row .add-field', function(e) {

			    	var rowWrapper = $(this).closest('.bid_fields_row');
		        	var clonedObject = $(rowWrapper).clone(true);

		        	clonedObject.insertAfter(rowWrapper).find('input').val('');
					clonedObject.find('input').each(function () {
    					var originalName = $(this).attr('name');
					});
			  
			    });

				// Bid Fields Repeater Remove
				    $('body').on('click', '.bid_fields_row .remove-field', function(e) {
			    	var rowWrapper = $(this).closest('.bid_fields');
			        if (rowWrapper.find('.bid_fields_row').length > 1)
			        {

			        	$(this).closest('.bid_fields_row').remove();
			        }
			    });

				$('.remove-field').click(function() {
					var id = $(this).data('id');
					$("<input type='hidden' />")
							.attr("name", "remove_bid_line_field[]")
							.attr("value", id)
							.prependTo("#edit_bid_fields");
				})

				$('.remove-bid-field').click(function() {
					var id = $(this).data('id');
					$("<input type='hidden' />")
							.attr("name", "remove_bid_field[]")
							.attr("value", id)
							.prependTo("#edit_bid_fields");
				});
				// ------------------------------------------------------------

				// Bid Fields Repeater Field 
				$('.bid_fields_repeater').bidFieldRepeater('.bid_field',true);	

				// Bid Field Reveals
				$('.bid_field i.caret').click(function() {
			  		if ( $(this).hasClass('down') ) {
						$(this).removeClass('down').addClass('right');
						$(this).closest('.bid_field').find('.bid_fields').slideUp('fast');
						$(this).closest('.bid_fields').find('label').hide();
			  		}
			  		else{
			  			$(this).removeClass('right').addClass('down');
						$(this).closest('.bid_field').find('.bid_fields').slideDown('fast');	
						$(this).closest('.bid_fields').find('label').show();
			  		}
				});				

			});
		})(jQuery);
	</script>


</div>