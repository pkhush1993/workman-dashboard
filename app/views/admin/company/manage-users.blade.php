<!-- Manage Users -->
<div class="ui bottom attached tab segment" data-tab="users">
	<!-- Invite Users -->
	<?php
		if ($company){
			$CompanyId = $company->id;
		}else{
			$CompanyId = 1;
		}
	?>
	<h3><i class="users icon"></i> Invite Users</h3>
	{{ Form::open(array('class' => 'ui form InviteUserFrm','route' => ['update.company', $CompanyId,'invite_users'])) }}
		<div class="invite_users_repeater repeater-field">
			<div class="invite_user cf">
				{{ Form::hidden('form_group','form_group' ) }}
				<div class="two fields">
					<div class="required field">
						{{ Form::label('invite_first_name', 'First Name') }}
						{{ Form::text('invite_first_name', $value=NULL, array('placeholder' => 'First Name','id' => 'first_name')) }}
					</div>
					<div class="required field">
						{{ Form::label('invite_last_name', 'Last Name') }}
						{{ Form::text('invite_last_name', $value=NULL, array('placeholder' => 'Last Name','id' => 'last_name')) }}
					</div>
				</div>
				<div class="two fields">
					<div class="field">
						{{ Form::label('invite_email', 'Email') }}
						{{ Form::text('invite_email', $value=NULL, array('placeholder' => 'Email','id' => 'email')) }}
					</div>
					<div class="field">
						{{ Form::label('invite_name', 'Role') }}
						<div class="inline fields">
							<div class="field">
								<div class="ui checkbox">
									{{ Form::checkbox('invite_user_role[]', 'executive','',array('id' => 'executive') ) }} {{ Form::label('executive', 'Executive') }}
								</div>
							</div>
							<div class="field">
								<div class="ui checkbox">
									{{ Form::checkbox('invite_user_role[]', 'accounting','',array('id' => 'accounting') ) }} {{ Form::label('accounting', 'Accounting') }}
								</div>
							</div>
							<div class="field">
								<div class="ui checkbox">
									{{ Form::checkbox('invite_user_role[]', 'project_manager','',array('id' => 'project_manager') ) }} {{ Form::label('project_manager', 'Project') }}
								</div>
							</div>
							<div class="field">
								<div class="ui checkbox">
									{{ Form::checkbox('invite_user_role[]', 'estimator','',array('id' => 'estimator') ) }} {{ Form::label('estimator', 'Estimator') }}
								</div>
							</div>
							<div class="field">
								<div class="ui checkbox">
									{{ Form::checkbox('invite_user_role[]', 'foreman','',array('id' => 'foreman') ) }} {{ Form::label('foreman', 'Foreman') }}
								</div>
							</div>
							<div class="field">
								<div class="ui checkbox">
									{{ Form::checkbox('invite_user_role[]', 'time_entry','',array('id' => 'time_entry') ) }} {{ Form::label('time_entry', 'Time') }}
								</div>
							</div>
							<div class="field">
								<div class="ui checkbox">
									{{ Form::checkbox('invite_user_role[]', 'admin','',array('id' => 'admin') ) }} {{ Form::label('admin', 'Admin') }}
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="actions" style="display:none">
					<button type="button" class="ui button mini green add-field">Add field</button>
					<button type="button" class="ui button mini red remove-field">Remove</button>
				</div>
			</div>
		</div>
		{{ Form::submit('Invite',array('class' => 'ui submit blue button ManageUserBtn')); }}
	{{ Form::close() }}
	<br>
	<div class="CompanyUserMsgPlace"></div>
	<br>
	@if ($invited_users)
	<!-- Remove Administrators	 -->
	<h3><i class="send icon"></i>Invited Users</h3>
		<div class="ui animated divided list">
			@foreach ($invited_users as $user)
				<div class="item">
					{{ Form::open(array('class' => 'ui form invited_users_form','route' => ['update.company', 1,'send_invite'])) }}
						{{ Form::hidden('invite_email',$user->email ) }}
						{{ Form::hidden('action_type', 'resend') }}
						<div class="right floated">
							{{ Form::submit('Resend', ['value' => 'Resend', 'name' => 'action', 'class' => 'tiny compact inverted green ui invite_user_button button']) }}
							{{ Form::submit('Revoke', ['value' => 'Revoke', 'name' => 'action', 'class' => 'tiny compact inverted red ui invite_user_button button']) }}
						</div>
						<div class="content">
							<div class="header">
								{{ $user->profile->first_name.' '.$user->profile->last_name }} - <small>Invited on {{ date('m/d/Y', strtotime($user->created_at)) }}</small>
							</div>
						</div>
					{{ Form::close() }}
				</div>
			@endforeach
		</div>
	@endif
	<br><hr>
	<!-- Remove Administrators	 -->
	<h3><i class="protect icon"></i>Administrators</h3>
	<div class="ui animated divided list">
		@foreach ($all_admins as $admin)
			<div class="item">
				{{ Form::open(array('class' => 'ui form','route' => ['update.company', 1,'remove_admin'])) }}
					{{ Form::hidden('admin_email',$admin->email ) }}
					<div class="right floated">
						@if($all_admins->count() > 1)
							{{ Form::submit('Remove',array('class' => 'tiny compact inverted red ui button')); }}
						@endif
					</div>
					<img class="ui avatar image" src="{{ $admin->profile->avatar }}">
					<div class="content">
						<div class="header">
							{{ $admin->profile->first_name.' '.$admin->profile->last_name }}
						</div>
					</div>
				{{ Form::close() }}
			</div>
		@endforeach
	</div>
</div>


<script>
	jQuery.noConflict();
	(function( $ ) {
		$(function() {

			// Invite Users Repeater Field
			$('.invite_users_repeater').repeater('.invite_user',true);

			$('.invite_user_button').click(function() {
				$('input[name="action_type"]').val($(this).val());
			})
		});
	})(jQuery);
</script>
