<!-- Create Activities-->
<div class="ui bottom attached tab segment" data-tab="tax-rates">
	<h3><i class="calculator icon"></i>Manage Default Tax Rates</h3>
	{{ Form::open(array('class' => 'ui form activity-form UpdateTaxRates','route' => ['update.company', 1,'tax-rates'])) }}
		<div class="tax-rate-repeater">
			{{ Form::hidden('removed_ids', $value=NULL) }}
			<div class="fields tax-rate hidden">
				<div class="nine wide field">
					{{ Form::label('tax_description', 'Tax Code/Description') }}
					{{ Form::text('tax_description[]', $value=NULL, array('placeholder' => 'Enter Tax Description','data-name' => 'tax_description','disabled' => 'disabled')) }}
				</div>
				<div class="five wide field">
					{{ Form::label('tax_rate', 'Tax Rate') }}
					<div class="ui left labeled input">
						<div class="ui label">%</div>						
						{{ Form::text('tax_rate[]', $value=NULL, array('placeholder' => 'Enter Tax Rate','data-name' => 'tax_rate','disabled' => 'disabled')) }}
					</div>	
				</div>								
				<div class="two wide field">
					{{ Form::label('', '&nbsp;') }}
					<i class="large green add circle icon add-field"></i>
					<i class="large red minus circle icon remove-field"></i>
				</div>		
			</div>				
			@foreach ($tax_rates as $tax_rate)
				<div class="fields tax-rate">
					<div class="nine wide field">
						{{ Form::label('tax_description', 'Tax Code/Description') }}
						{{ Form::text('tax_description['.$tax_rate->id.']', $tax_rate->tax_description, array('placeholder' => 'Enter Tax Description','data-name' => 'tax_description','data-id' => $tax_rate->id)) }}
					</div>
					<div class="five wide field">
						{{ Form::label('tax_rate', 'Tax Rate') }}
						<div class="ui left labeled input">
							<div class="ui label">%</div>						
							{{ Form::text('tax_rate['.$tax_rate->id.']',$tax_rate->tax_rate, array('placeholder' => 'Enter Tax Rate','data-name' => 'tax_rate','data-id' => $tax_rate->id)) }}
						</div>	
					</div>								
					<div class="two wide field">
						{{ Form::label('', '&nbsp;') }}
						<i class="large red minus circle icon remove-field"></i>
					</div>		
				</div>					
			@endforeach
			<h3>Create New Tax Rates</h3>
			<div class="fields tax-rate">
				<div class="nine wide field">
					{{ Form::label('tax_description', 'New Tax Code/Description') }}
					{{ Form::text('tax_description[]', $value=NULL, array('placeholder' => 'Enter Tax Description','data-name' => 'tax_description')) }}
				</div>
				<div class="five wide field">				
					{{ Form::label('tax_rate', 'New Tax Rate') }}
					<div class="ui left labeled input">
						<div class="ui label">%</div>						
						{{ Form::text('tax_rate[]', $value=NULL, array('placeholder' => 'Enter Tax Rate','data-name' => 'tax_rate')) }}
					</div>	
				</div>								
				<div class="two wide field">
					{{ Form::label('', '&nbsp;') }}
					<i class="large green add circle icon add-field"></i>
					<i class="large red minus circle icon remove-field"></i>
				</div>		
			</div>		
		</div>
		{{ Form::submit('UPDATE TAX RATES',array('class' => 'ui submit green button UpdateTaxRatesBtn')); }}
	{{ Form::close() }}
	<br>
	<div class="UpdateTaxRatesAlert"></div>
</div>



<script>
	jQuery.noConflict();
	(function( $ ) {
		$(function() {		

			// $('body').on('click', '.activity-form .remove-activity', function(event) {
			// 	alert('a');
			// 	removeId = $(this).closest('.activity-input').data('id');

			// 	console.log(removeId);
			// });

			$('.tax-rate-repeater').repeater('.tax-rate',false);


		});
	})(jQuery);
</script>