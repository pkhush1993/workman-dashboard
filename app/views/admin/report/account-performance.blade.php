@extends('layouts.default')

@section('content')
    <div class="ui green large labeled icon button print-button">
        <i class="print icon"></i>
        SAVE/PRINT
    </div>

    <div class="labor-cost">
        <table class="ui compact small very basic table BoxedTable">
            <thead>
            <tr>
                <th>Account Name</th>
                <th>Jobs Bid</th>
                <th>Dollars Bid</th>
                <th>Jobs Secured</th>
                <th>Dollars Secured</th>
                <th>Jobs Won %</th>
                <th>Dollars Won %</th>
            </tr>
            </thead>
            <tbody>
            @if ( count($data) )
                @foreach ( $data as $item )
                    <tr>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->jobs_bid_count }}</td>
                        <td>${{ number_format($item->jobs_bid_sum, 2) }}</td>
                        <td>{{ $item->jobs_secure_count }}</td>
                        <td>${{ number_format($item->jobs_secure_sum, 2) }}</td>
                        <td>{{ $item->jobs_secure_pct == 'NA' ? 'NA' : ($item->jobs_secure_pct * 100) .'%'}}</td>
                        <td>{{ $item->dollars_secure_pct == 'NA' ? 'NA' : ($item->dollars_secure_pct * 100) .'%'}}</td>

                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>

@stop
