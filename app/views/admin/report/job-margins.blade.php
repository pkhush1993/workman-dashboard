{{-- =========================================================================================================
-----------------
JOBS MARGIN TEMPALTE
-----------------
========================================================================================================= --}}

@extends('layouts.default')

@section('content')
	<div class="ui green large labeled icon button print-button">
		<i class="print icon"></i>
		SAVE/PRINT
	</div>
	<div id="bid-history">

		<h1 class="ui block header header-5">Job Margins <small>({{ date('m/d/Y',strtotime($start_date)).' - '.date('m/d/Y',strtotime($end_date)) }})</small></h1>

		{{-- Reset Bids Group By Month Array for Each User and Setup Vars --}}
		<?php
			$bids_by_month = '';
			$grand_total = 0;
		?>

        <table class="ui compact small very basic table">
            <thead>
                  	<tr>
                      	@if ($report_view == 'summary')
		              		<th class="two wide header-5">Month</th>
		              		<th class="one wide header-5">Number of Bids</th>
							<th class="five wide header-5">&nbsp;</th>
		                  	<th class="five wide header-5">&nbsp;</th>
                      	@elseif ($report_view == 'detailed')
                      		<th class="two wide header-5">Bid Date</th>
                      		<th class="one wide header-5">Job #</th>
							<th class="five wide header-5">Job Name</th>
							<th class="five wide header-5">Job Account</th>
                      	@endif
                      	<th class="four wide right aligned">Bid Total</th>
                  	</tr>
            </thead>
            <tbody>

				{{-- Group the Results by Month by Creating an Accusative Array out of a Object  --}}
				@foreach ($bids  as $bid)
					<?php $bids_by_month[date('F',strtotime($bid['bid_date']))][]= $bid; ?>
				@endforeach

				{{-- Irritate through each Bid by month --}}
				@foreach ($bids_by_month as $month => $bids_in_month)

					@if ($report_view == 'detailed')
						<tr>
							<td colspan="6"><h4>{{$month}}</h4></td>
						</tr>
					@endif

					<?php
						$number_of_bids_in_month = count($bids_in_month);
						$bid_count = 0;
						$monthly_total = 0;
					?>
					{{-- Irritate through each Bid within the current month --}}
					@foreach ($bids_in_month as $bid)
						<?php
							$monthly_total = $monthly_total + $bid->grand_total;
							$employee_totals[] = $bid->grand_total;
							$report_totals[] = $bid->grand_total;
						?>
						@if ($report_view == 'detailed')
		                    <tr>
								<td>{{ date('m/d/Y',strtotime($bid->bid_date)) }}</td>
		                        <td>{{ $bid->job()->job_number }}</td>
		                        <td>{{ $bid->job()->name }}</td>
		                        <td>{{ $bid->job()->account()->name }}</td>
		                        <td class="right aligned">${{ number_format($bid->grand_total,2) }}</td>
		                    </tr>
		                    <?php $bid_count++; ?>
		                    @if ($bid_count == $number_of_bids_in_month)
								<tr>
									<td class="right aligned" colspan="5"><p><b>{{strtoupper($month)}} TOTAL:&nbsp;&nbsp; {{'$'.number_format($monthly_total,2)}}</b></p></td>
								</tr>
		                    @endif
						@endif
					@endforeach

					@if ($report_view == 'summary')
						<tr>
							<td colspan="1"><h4>{{ $month }}</h4></td>
							<td colspan="3"><h4>{{ count($bids_in_month) }}</h4></td>
							<td class="right aligned" colspan="1"><h4>{{ number_format($monthly_total,2) }}</h4></td>
						</tr>
					@endif

				@endforeach
            </tbody>
        </table>

        <div class="ui divider"></div>

		<table class="ui compact small very basic table">
            <thead>
                  <tr>
                      <th class="two  wide" style="border: none;">&nbsp;</th>
                      <th class="one  wide" style="border: none;">&nbsp;</th>
                      <th class="five wide" style="border: none;">&nbsp;</th>
                      <th class="five wide" style="border: none;">&nbsp;</th>
                      <th class="four wide right aligned" style="border: none;">&nbsp;</th>
                  </tr>
            </thead>
			<tbody>
				<tr>
					<td class="right aligned" colspan="4" style="border: none;"><h4><b>BID TOTAL: </b></h4></td>
					<td class="right aligned" colspan="1"><h4><b>${{number_format(array_sum($report_totals),2)}} </b></h4></td>
				</tr>
				<tr>
					<td class="right aligned" colspan="4" style="border: none;"><h4><b>LOWEST BID: </b></h4></td>
					<td class="right aligned" colspan="1"><h4><b>${{number_format(min($report_totals),2)}}</b></h4></td>
				</tr>
				<tr>
					<td class="right aligned" colspan="4" style="border: none;"><h4><b>HIGHEST BID: </b></h4></td>
					<td class="right aligned" colspan="1"><h4><b>${{number_format(max($report_totals),2)}}</b></h4></td>
				</tr>
				<tr>
					<td class="right aligned" colspan="4" style="border: none;"><h4><b>BID AVERAGE: </b></h4></td>
					<td class="right aligned" colspan="1"><h4><b>${{number_format(array_sum($report_totals)/count($report_totals),2)}}</b></h4></td>
				</tr>
			</tbody>
		</table>

	</div>


	<script>
		jQuery.noConflict();
		(function( $ ) {
			$(function() {



			});
		})(jQuery);
	</script>

@stop
