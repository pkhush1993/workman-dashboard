{{-- 
=========================================================================================================
DOLLARS BID AND PERFORMED CHART TEMPALTE 
--------------------------------------------------------------------------------------------------------------
Using highcharts.js to generate Charts
-----------------
========================================================================================================= --}}

@extends('layouts.default')

@section('content')


	<div id="gross-revenue-chart" style="height:700px;">
		{{-- Use HighCharts.js to Generate Chart Here --}}
	</div>


	<script>
		jQuery.noConflict();
		(function( $ ) {
			$(function() {

				// Build Out Data
				var revenueData = [];
				var revenue = <?php echo $revenue_data;?>;

				// Iterate through each Performed Bid and Build Out Chart Data
				revenue.forEach(function(entry) {
				    revenueData.push([Date.parse(entry[0]),entry[1]]);
				});				

			    $('#gross-revenue-chart').highcharts({
			        chart: {
			            type: 'area'
			        },			    	
			        title: {
			            text: 'Gross Revenue'
			        },
			        // subtitle: {
			        //     text: 'Lets Describe Each Chart Here',
			        //     x: -20
			        // },
					xAxis: {
					    type: 'datetime',
			            plotLines: [{
			                value: 0,
			                width: 20,
			                color: '#dcdcdc'
			            }],
						units: [
							['month']			            					    
						]
					},
			        yAxis: {
			            title: {
			                text: 'Total Dollars'
			            },
			            plotLines: [{
			                value: 0,
			                width: 2,
			                color: '#808080'
			            }],
			            labels: {
			                formatter: function () {
			                    return '$'+this.value;
			                }
			            }
			        },
			        tooltip: {
			            valuePrefix: '$'
			        },
			        legend: {
			            layout: 'vertical',
			            align: 'right',
			            verticalAlign: 'middle',
			            borderWidth: 0
			        },
				    plotOptions: {
				        line: {
				            connectNulls: true
				        }
				    },			        
			        series: [{
			            name: 'Gross Revenues',
						data: revenueData
			        }]			        
			    });


			});
		})(jQuery);	
	</script>

@stop