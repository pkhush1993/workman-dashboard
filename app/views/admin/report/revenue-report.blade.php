{{-- =========================================================================================================
-----------------
REVENUE TEMPALTE 
-----------------
Passing One of the Following Report Types in the URL to manipulate template

summary
detailed-monthly
========================================================================================================= --}}

@extends('layouts.default')

@section('content')
	<div class="ui green large labeled icon button print-button">
		<i class="print icon"></i>
		SAVE/PRINT
	</div>
	<div id="production-history">

		<h1 class="ui block header">{{($report_view == 'detailed') ? 'Detailed':'Summary'}} Revenue Report <small>({{ date('m/d/Y',strtotime($start_date)).' - '.date('m/d/Y',strtotime($end_date)) }})</small></h1>

			{{-- Reset Jobs Group By Month Array for Each User and Setup Vars --}}
			<?php 
				$project_managers_jobs_by_month = '';
				$grand_total = 0;  
			?>
		
		        <table class="ui compact small very basic table">
		            <thead>
		                  	<tr>
		                      	@if ($report_view == 'detailed')
		                      		<th class="two wide">Job Completed</th>
		                      		<th class="one wide">Job #</th>
			                      	<th class="five wide">Job Name</th>
			                      	<th class="five wide">Job Account</th>		                      		                      
		                      	@elseif ($report_view == 'summary')
		                      		<th class="two wide">Month</th>
		                      		<th class="one wide">Number of Jobs</th>
									<th class="five wide">&nbsp;</th>
			                      	<th class="five wide">&nbsp;</th>			                      		
		                      	@endif
		                      	<th class="four wide right aligned">Bid Total</th>
		                  	</tr>
		            </thead>
		            <tbody>

						{{-- Group the Results by Month by Creating an Accusative Array out of a Object  --}}
						@foreach ($jobs  as $job)
							<?php $jobs_by_month[date('F',strtotime($job['completed_date']))][]= $job; ?>
						@endforeach

						{{-- Irritate through each job by month --}}
						@foreach ($jobs_by_month as $month => $jobs_in_month)

							@if ($report_view == 'detailed')
								<tr>
									<td colspan="6"><h4>{{$month}}</h4></td>
								</tr>						
							@endif	

							<?php 
								$number_of_jobs_in_month = count($jobs_in_month);
								$job_count = 0;
								$monthly_total = 0;
							?>
							{{-- Irritate through each job within the current month --}}
							@foreach ($jobs_in_month as $job)
								<?php 
									$monthly_total = $monthly_total + $job->getBid()->grand_total; 
									$employee_totals[] = $job->getBid()->grand_total;
									$report_totals[] = $job->getBid()->grand_total;
								?>
								@if ($report_view == 'detailed')
				                    <tr>
										<td>{{ date('m/d/Y',strtotime($job->completed_date)) }}</td>
				                        <td>{{ $job->job_number }}</td>
				                        <td>{{ $job->name }}</td>
				                        <td>{{ $job->account()->name }}</td>
				                        <td class="right aligned">${{ $job->getBid()->grand_total }}</td>
				                    </tr>
				                    <?php $job_count++; ?>
				                    @if ($job_count == $number_of_jobs_in_month)
										<tr>
											<td class="right aligned" colspan="5"><p><b>{{strtoupper($month)}} TOTAL:&nbsp;&nbsp; {{'$'.number_format($monthly_total,2)}}</b></p></td>
										</tr>
				                    @endif
								@endif
							@endforeach

							@if ($report_view == 'summary')
								<tr>
									<td colspan="1"><h4>{{ $month }}</h4></td>
									<td colspan="3"><h4>{{ count($jobs_in_month) }}</h4></td>
									<td class="right aligned" colspan="1"><h4>{{ number_format($monthly_total,2) }}</h4></td>
								</tr>							
							@endif	

						@endforeach				                                      
		            </tbody>
		        </table>

		        <div class="ui divider"></div>

		<table class="ui compact small very basic table">
            <thead>
                  <tr>
                      <th class="two  wide" style="border: none;">&nbsp;</th>
                      <th class="one  wide" style="border: none;">&nbsp;</th>
                      <th class="five wide" style="border: none;">&nbsp;</th>
                      <th class="five wide" style="border: none;">&nbsp;</th>
                      <th class="four wide right aligned" style="border: none;">&nbsp;</th>
                  </tr>
            </thead>		
			<tbody>
				<tr>
					<td class="right aligned" colspan="4" style="border: none;"><h4><b>REPORT TOTAL: </b></h4></td>
					<td class="right aligned" colspan="1"><h4><b>${{number_format(array_sum($report_totals),2)}} </b></h4></td>
				</tr>
				<tr>
					<td class="right aligned" colspan="4" style="border: none;"><h4><b>LOWEST JOB: </b></h4></td>
					<td class="right aligned" colspan="1"><h4><b>${{number_format(min($report_totals),2)}}</b></h4></td>
				</tr>
				<tr>
					<td class="right aligned" colspan="4" style="border: none;"><h4><b>HIGHEST JOB: </b></h4></td>
					<td class="right aligned" colspan="1"><h4><b>${{number_format(max($report_totals),2)}}</b></h4></td>
				</tr>
				<tr>
					<td class="right aligned" colspan="4" style="border: none;"><h4><b>REPORT AVERAGE: </b></h4></td>
					<td class="right aligned" colspan="1"><h4><b>${{number_format(array_sum($report_totals)/count($report_totals),2)}}</b></h4></td>
				</tr>												
			</tbody>
		</table>

	</div>



	<script>
		jQuery.noConflict();
		(function( $ ) {
			$(function() {



			});
		})(jQuery);	
	</script>

@stop