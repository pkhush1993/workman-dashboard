{{-- =========================================================================================================
-----------------
INVOICES TEMPALTE

Populate the Invoices Across All Jobs
-----------------
========================================================================================================= --}}


@extends('layouts.default')

@section('content')
	<?php $InvoiceAmount = [];  ?>
	<div class="ui green large labeled icon button print-button">
		<i class="print icon"></i>
		SAVE/PRINT
	</div>

	<div id="invoices-report">
		<h1 class="ui block header header-5">Invoices Created Between {{date('m/d/Y',strtotime($start_date))}} and {{date('m/d/Y',strtotime($end_date))}}</h1>
        <table class="ui compact very basic table">
            <thead>
              	<tr>
              		<th class="header-5">Created Date</th>
              		<th class="header-5">Invoice #</th>
              		<th class="header-5">Invoice Type</th>
              		<th class="header-5">Invoice Total</th>
              		<th class="header-5">Job</th>
              		<th class="header-5">Created By</th>
              	</tr>
            </thead>
            <tbody>
        		{{-- Irritate through each line item by category by month --}}
				@foreach ($invoices as $invoice)
					<tr>
						<td> {{date('m/d/Y',strtotime($invoice->created_at))}} </td>
						<td> <a target="_blank" href="{{ route('job.invoice', [$invoice->invoice_type,$invoice->job_id,$invoice->down_payment,$invoice->id]) }}" class="item">#{{ $invoice->invoice_number }}</a></td>
						<td> {{ ucwords(str_replace('-',' ',$invoice->invoice_type)) }} </td>
						<td> ${{ number_format($invoice->grand_total,2) }} </td>
						<td> {{ $invoice->job()->name }} </td>
						<td> {{ $invoice->getCreatedByProfile()->getFullName() }} </td>
						<?php $InvoiceAmount[] = $invoice->grand_total;  ?>
					</tr>
				@endforeach
			</tbody>
		</table>
		<div class="ui divider"></div>

		<table class="ui compact small very basic table">
			<thead>
			<tr>
				<th class="two  wide" style="border: none;">&nbsp;</th>
				<th class="one  wide" style="border: none;">&nbsp;</th>
				<th class="five wide" style="border: none;">&nbsp;</th>
				<th class="five wide" style="border: none;">&nbsp;</th>
				<th class="four wide right aligned" style="border: none;">&nbsp;</th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td class="right aligned" colspan="4" style="border: none;"><h4><b>Report Total: </b></h4></td>
				<td class="right aligned" colspan="1"><h4><b> ${{ number_format(array_sum($InvoiceAmount), 2) }} </b></h4></td>
			</tr>
			<tr>
				<td class="right aligned" colspan="4" style="border: none;"><h4><b>Lowest Job: </b></h4></td>
				<td class="right aligned" colspan="1"><h4><b> ${{ number_format(min($InvoiceAmount), 2) }}</b></h4></td>
			</tr>
			<tr>
				<td class="right aligned" colspan="4" style="border: none;"><h4><b>Highest Job: </b></h4></td>
				<td class="right aligned" colspan="1"><h4><b>${{ number_format(max($InvoiceAmount), 2) }}</b></h4></td>
			</tr>
			<tr>
				<td class="right aligned" colspan="4" style="border: none;"><h4><b>Report Average:</b></h4></td>
				<td class="right aligned" colspan="1"><h4><b>${{ number_format(array_sum($InvoiceAmount)/count($InvoiceAmount), 2) }}</b></h4></td>
			</tr>
			</tbody>
		</table>
	</div>
@stop	