{{-- =========================================================================================================
-----------------
BID REQUESTS TEMPALTE

Takes both Specified and All Employee Queries
-----------------
========================================================================================================= --}}


@extends('layouts.default')

@section('content')

	<div class="ui green large labeled icon button print-button">
		<i class="print icon"></i>
		SAVE/PRINT
	</div>
	<div id="bid-requests-report">
		<h1 class="ui block header header-5">Incoming Bid Requests {{ ($estimator) ? 'for ' . $estimator->getFullName():'' }} <small style="float:right">{{ date('l, F jS, Y') }}</small></h1>
			@if ( count($jobs))
			<table class="ui compact very basic table">
				<thead>
				<tr>
					<th class="header-5">Bid Date</th>
					<th class="header-5">Job</th>
					<th class="header-5">Contact Name</th>
					<th class="header-5">Contact Info</th>
					<th class="header-5">Account</th>
					<th class="header-5">Estimator</th>
					<th class="header-5">Notes</th>
				</tr>
				</thead>
				<tbody>
				{{-- Irritate through each line item by category by month --}}
				@foreach ($jobs as $job)
					<tr>
						<td>{{date('m/d/Y',strtotime($job->bid_date))}}</td>
						<td>{{ '#'. $job->job_number .' - '. $job->name }}</td>
						@if ( $job->getPrimaryContact() )
							<td>{{ $job->getPrimaryContact()->getFullName() }}</td>
							<td>
								@if($job->getPrimaryContact()->phone)
									<b>P:</b> {{ $job->getPrimaryContact()->phone }}<br>
								@endif
								@if($job->getPrimaryContact()->mobile)
									<b>M:</b> {{ $job->getPrimaryContact()->mobile }}<br>
								@endif
								@if($job->getPrimaryContact()->email)
									<b>E:</b> <a href="mailto:{{ $job->getPrimaryContact()->email }}">{{ $job->getPrimaryContact()->email }}</a><br>
								@endif
								@if($job->getPrimaryContact()->fax)
									<b>F:</b> {{ $job->getPrimaryContact()->fax }}
								@endif
							</td>
						@else
							<td>-</td>
							<td>-</td>
						@endif
						<td>
							<b>{{ $job->account() ? $job->account()->name : '--Account Deleted--'}}</b><br>
							@if ($job->site_address_1)
								{{ $job->site_address_1; }} {{ $job->site_address_2; }} <br>{{ $job->site_city; }}, {{ $job->site_state; }} {{ $job->site_zip; }}
							@else
								No Site Address Assigned
							@endif
						</td>
						<td>
							@if ($job->getEstimator())
								{{$job->getEstimator()->getFullName()}}
							@endif
						</td>
						<td>
							@if (count($notes = $job->getEstimatingNotes()))
								@foreach ($notes as $note)
									<i style="margin-bottom: 10px;display: block;border-bottom: 1px solid #dcdcdc;">{{ $note->note }}</i>
								@endforeach
							@else
								No Notes on the Job
							@endif
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		@endif
	</div>
@stop
