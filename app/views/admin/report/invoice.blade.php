{{-- =========================================================================================================
-----------------
INVOICE TEMPALTE
-----------------
Passing One of the Following Invoice Types in the URL ( /{invoice_type}/{job_id}/ ) to manipulate template

down-payment
grand-total
category-subtotals
item-totals
========================================================================================================= --}}


@extends('layouts.report')

@section('content')

	<div class="ui green large labeled icon button print-button">
		<i class="print icon"></i>
		SAVE/PRINT
	</div>

	<div id="invoice" class="">

		<!--============ LetterHead ============-->
		@include('admin.report.includes.letterhead')

		{{--============ Job Info ============--}}
		@include('admin.report.includes.job-info')

		<div class="ui divider"></div>

		<div style="margin-top:0;display:none" class="ui header">
			<h1>Job  #{{ $job->job_number }}</h1>
		</div>

		{{--============ Contact Information ============--}}
		@include('admin.report.includes.contact-info')

		<div class="ui divider"></div>

		{{--====== Verbiage for Different Invoice Types ======--}}
		@if ($invoice_type == 'down-payment')
			<div class="ui aligned grid">
				<div class="four wide center aligned column">
					<h4>** DOWN PAYMENT **</h4>
				</div>
				<div class="twelve wide column">
					<h5>We will soon be commencing with work at your property. Please remit the down payment indicated below before the first scheduled that of work. Thank You</h5>
				</div>
			</div>
			<div class="ui divider"></div>
			<h1 class="ui center aligned header" style="margin-bottom: 0px;">
				Invoice
				{{-- <small style="border-bottom: 1px solid #dcdcdc;">Job #{{ $job->job_number }} - {{ $job->name}}</small> --}}
			</h1>
			{{-- <h3 class="ui center aligned red header" style="margin-top:0">{{$down_payment}}<small>%</small>  Down Payment Invoice</h3> --}}
		@elseif (!$invoice_id)
			<h5 style="margin-top:0;text-align: center;">
				{{ $company->bid_header }}
			</h5>
			<div class="ui divider"></div>
			<h1 class="ui center aligned header">Bid Proposal</h1>
		@elseif ($invoice_id)
			<h1 class="ui center aligned header">
				Invoice
			</h1>
		@endif

		{{--============ Line Items ============--}}
		@foreach ($line_items as $line_item_category => $line_item_category_items)
			<h3>{{$line_item_category}}</h3>
			<table class="ui very basic compact table">
	            <thead>
	                  <tr>
	                      <th class="two wide">Quantity</th>
	                      <th class="six wide">Description</th>
	                      @if (! in_array($invoice_type,array('grand-total', 'category-subtotals')) && ! in_array($view_type,array('grand-total', 'category-subtotals')))
	                      	<th class="three wide right aligned">Total</th>
	                      @endif

	                  </tr>
	            </thead>
				<tbody>
					@foreach ($line_item_category_items as $item)
						<tr>
							<td>{{$item['quantity']}} {{$item['unit']}}</td>
							<td>{{$item['product']}}</td>
							@if (! in_array($invoice_type,array('grand-total', 'category-subtotals')) && ! in_array($view_type,array('grand-total', 'category-subtotals')))
								<td class="right aligned">${{ number_format($item['amount'],2) }}</td>
							@endif
						</tr>
					@endforeach
				</tbody>
			</table>

			@if (! in_array($invoice_type,array('grand-total')) && ! in_array($view_type,array('grand-total')))
				<?php $category_total = 0; ?>
				@foreach ($line_item_category_items as $item)
					<?php $category_total = $category_total + $item['amount']; ?>
				@endforeach
				<h5 class="ui header right floated">${{ number_format($category_total,2) }}</h5>
			@endif

		@endforeach

		<br><br>

		<div class="ui horizontal divider">TOTALS</div>

		{{--========== Invoice Totals ==========--}}
		<table class="ui very basic small compact table invoice-totals">
			<tbody>
				<tr>
					<td class="ten wide">&nbsp;</td>
					<td class="four wide right aligned"><h4>Subtotal</h4></td>
					@if ($down_payment)
					{{-- 60 / (50/100) = 60 / 0.5 = 120 --}}
						<td class="one wide right aligned"><h4>${{ number_format( $bid->subtotal / ($down_payment/100) ,2) }}</h4></td>
					@else
						<td class="one wide right aligned"><h4>${{ number_format($bid->subtotal,2) }}</h4></td>
					@endif
				</tr>
				<tr>
					<td class="ten wide">&nbsp;</td>
					<td class="four wide right aligned"><h4>Sales Tax @ {{number_format($bid->tax_rate,2)}}%</h4></td>
					@if ($down_payment)
						<td class="one wide right aligned"><h4>${{ number_format($bid->sales_tax_total / ($down_payment/100),2) }}</h4></td>
					@else
						<td class="one wide right aligned"><h4>${{ number_format($bid->sales_tax_total,2) }}</h4></td>
					@endif
				</tr>
				@if ($down_payment)
					<tr style="border-bottom:1px solid #dcdcdc">
						<td class="ten wide">&nbsp;</td>
						<td class="four wide right aligned"><h4>Grand Total</h4></td>
						<td class="one wide right aligned"><h4>${{ number_format($bid->grand_total / ($down_payment/100),2) }}</h4></td>
					</tr>
					<tr style="color:#078007">
						<td class="ten wide">&nbsp;</td>
						<td class="four wide right aligned"><h3>{{$down_payment}}<small>%</small> Down - Payment Due</h3></td>
						<td class="one wide right aligned"><h3>${{ number_format($bid->grand_total,2) }}</h3></td>
					</tr>
				@else
					<tr>
						<td class="ten wide">&nbsp;</td>
						<td class="four wide right aligned"><h4>Total</h4></td>
						<td class="one wide right aligned"><h4>${{ number_format($bid->grand_total,2) }}</h4></td>
					</tr>
				@endif
			</tbody>
		</table>

		{{--============ Bid Comments ============--}}
		@if (! $job->getBid()->getBidComments()->isEmpty() )
			<h3>Comments</h3>
			<ul class="ui list">
				@foreach ($job->getBid()->getBidComments() as $comment)
					<li>{{ $comment->comment }}</li>
				@endforeach
			</ul>
		@endif

		<div class="ui divider"></div>

		@if ($down_payment)
			<i>**Please remit payment to @if($company->address_1)<b>{{ $company->address_1; }} {{ $company->address_2; }} {{ $company->city; }}, {{ $company->state; }} {{ $company->zip; }}</b>@endif</i>
		@endif

		{{--============  Add Company Legal if Language Exists ============--}}
		@if ($company->contract_language && !$invoice_id)
			<br><br>
			<div class="contract-language ui segment">
				{{ $company->contract_language; }}
			</div>
		@endif
	</div>

@stop
