{{-- =========================================================================================================
-----------------
REVENUE TEMPALTE
-----------------
Passing One of the Following Report Types in the URL to manipulate template

summary
detailed-monthly
========================================================================================================= --}}

@extends('layouts.default')

@section('content')

	<div class="ui green large labeled icon button print-button">
		<i class="print icon"></i>
		SAVE/PRINT
	</div>
	<div id="production-history">
		<?php $report_totals = []; ?>
		<h1 class="ui block header header-5">{{($report_view == 'detailed') ? 'Detailed':'Summary'}} Performed Jobs by Month Report <small>({{ date('m/d/Y',strtotime($start_date)).' - '.date('m/d/Y',strtotime($end_date)) }})</small></h1>
		{{-- If a User is Defined and Exists --}}
		@if ($project_manager)
			{{-- Reset Jobs Group By Month Array for Each User and Setup Vars --}}
			<?php
				$project_managers_jobs_by_month = '';
				$grand_total = 0;
			?>
        	{{-- Check to see if Jobs Exists for the Users Results --}}
        	@if (count($project_manager_jobs))
        		{{-- Reset Employee Totals Array --}}
				<?php $employee_totals = []; ?>

        		<h3 class="header-5">{{ $project_manager->profile()->getFullName() }}</h3>

		        <table class="ui compact small very basic table">
		            <thead>
		                  	<tr>
		                      	@if ($report_view == 'detailed')
		                      		<th class="two wide">Job Completed</th>
		                      		<th class="one wide">Job #</th>
			                      	<th class="five wide">Job Name</th>
			                      	<th class="five wide">Job Account</th>
		                      	@elseif ($report_view == 'summary')
		                      		<th class="two wide">Month</th>
		                      		<th class="one wide">Number of Jobs</th>
									<th class="five wide">&nbsp;</th>
			                      	<th class="five wide">&nbsp;</th>
		                      	@endif
		                      	<th class="four wide right aligned">Bid Total</th>
		                  	</tr>
		            </thead>
		            <tbody>

						{{-- Group the Results by Month by Creating an Accusative Array out of a Object  --}}
						@foreach ($project_manager_jobs as $project_managers_job)
							<?php $project_managers_jobs_by_month[date('F',strtotime($project_managers_job['completed_date']))][]= $project_managers_job; ?>
						@endforeach

						{{-- Irritate through each job by month --}}
						@foreach ($project_managers_jobs_by_month as $month => $jobs_in_month)

							@if ($report_view == 'detailed')
								<tr>
									<td colspan="6"><h4>{{$month}}</h4></td>
								</tr>
							@endif

							<?php
								$number_of_jobs_in_month = count($jobs_in_month);
								$job_count = 0;
								$monthly_total = 0;
							?>
							{{-- Irritate through each job within the current month --}}
							@foreach ($jobs_in_month as $job)
								<?php
									$monthly_total = $monthly_total + $job->getBid()->subtotal;
									$employee_totals[] = $job->getBid()->subtotal;
									$report_totals[] = $job->getBid()->subtotal;
								?>
								@if ($report_view == 'detailed')
				                    <tr>
										<td>{{ date('m/d/Y',strtotime($job->completed_date)) }}</td>
				                        <td>{{ $job->job_number }}</td>
				                        <td>{{ $job->name }}</td>
				                        <td>{{ $job->account()->name }}</td>
				                        <td class="right aligned">${{ $job->getBid()->subtotal }}</td>
				                    </tr>
				                    <?php $job_count++; ?>
				                    @if ($job_count == $number_of_jobs_in_month)
										<tr>
											<td class="right aligned" colspan="5"><p><b>{{strtoupper($month)}} TOTAL:&nbsp;&nbsp; {{'$'.number_format($monthly_total,2)}}</b></p></td>
										</tr>
				                    @endif
								@endif
							@endforeach

							@if ($report_view == 'summary')
								<tr>
									<td colspan="1"><h4>{{ $month }}</h4></td>
									<td colspan="3"><h4>{{ count($jobs_in_month) }}</h4></td>
									<td class="right aligned" colspan="1"><h4>{{ number_format($monthly_total,2) }}</h4></td>
								</tr>
							@endif

						@endforeach
		            </tbody>
		        </table>

		        <h5 class="ui header right aligned">EMPLOYEES TOTAL:&nbsp;&nbsp;${{number_format(array_sum($employee_totals),2)}}</h5>
		        <div class="ui divider"></div>

	        @endif
	    {{-- If the request is all users --}}
		@else
			@foreach ($project_managers as $project_manager)

				{{-- Reset Jobs Group By Month Array for Each User and Setup Vars --}}
				<?php
					$project_managers_jobs_by_month = '';
					$grand_total = 0;
				?>

	        	{{-- Check to see if Jobs Exists for the Users Results --}}
	        	@if (count($project_managers_jobs = $project_manager->getUserJobsByRole('project_manager',['completed','paid'],$start_date,$end_date)))

	        		{{-- Reset Employee Totals Array --}}
					<?php $employee_totals = []; ?>

	        		<h3>{{ $project_manager->profile()->getFullName() }}</h3>

			        <table class="ui compact small very basic table">
			            <thead>
			                  	<tr>
			                      	@if ($report_view == 'detailed')
			                      		<th class="two wide">Job Completed</th>
			                      		<th class="one wide">Job #</th>
				                      	<th class="five wide">Job Name</th>
				                      	<th class="five wide">Job Account</th>
			                      	@elseif ($report_view == 'summary')
			                      		<th class="two wide">Month</th>
			                      		<th class="one wide">Number of Jobs</th>
										<th class="five wide">&nbsp;</th>
				                      	<th class="five wide">&nbsp;</th>
			                      	@endif
			                      	<th class="four wide right aligned">Bid Total</th>
			                  	</tr>
			            </thead>
			            <tbody>

							<?php
							$start_date_counter = date('Y-m-d',strtotime($start_date));
							$end_date_counter = date('Y-m-d',strtotime($end_date));
							while ($start_date_counter <= $end_date_counter) {
								$project_managers_jobs_by_month[date('F Y', strtotime($start_date_counter))][] = '';
								$start_date_counter = date('Y-m-d', strtotime('+1 month', strtotime($start_date_counter)));
							}
							?>
							{{-- Group the Results by Month by Creating an Accusative Array out of a Object  --}}
							@foreach ($project_managers_jobs as $project_managers_job)
								<?php $project_managers_jobs_by_month[date('F Y',strtotime($project_managers_job['completed_date']))][]= $project_managers_job; ?>
							@endforeach

							{{-- Irritate through each job by month --}}
							@foreach ($project_managers_jobs_by_month as $month => $jobs_in_month)


								<?php
									$number_of_jobs_in_month = count($jobs_in_month);
									$job_count = 0;
									$monthly_total = 0;
								?>
								@if ($report_view == 'detailed' && $number_of_jobs_in_month > 1)
									<tr>
										<td colspan="6"><h4>{{$month}}</h4></td>
									</tr>
								@endif
								{{-- Irritate through each job within the current month --}}
								@foreach ($jobs_in_month as $job)
									{{-- Make Sure there is a Bid Attached to the Job --}}
									@if (is_object($job) && $job->getBid())
										<?php
											$monthly_total = $monthly_total + $job->getBid()->subtotal;
											$employee_totals[] = $job->getBid()->subtotal;
											$report_totals[] = $job->getBid()->subtotal;
										?>
										@if ($report_view == 'detailed')
						                    <tr>
												<td>{{ date('m/d/Y',strtotime($job->completed_date)) }}</td>
						                        <td>{{ $job->job_number }}</td>
						                        <td>{{ $job->name }}</td>
						                        <td>{{ $job->account()->name }}</td>
						                        <td class="right aligned">${{ $job->getBid()->subtotal }}</td>
						                    </tr>
						                    <?php $job_count++; ?>
						                    @if ($job_count == $number_of_jobs_in_month)
												<tr>
													<td class="right aligned" colspan="5"><p><b>{{strtoupper($month)}} TOTAL:&nbsp;&nbsp; {{'$'.number_format($monthly_total,2)}}</b></p></td>
												</tr>
						                    @endif
										@endif
									@endif
								@endforeach

								@if ($report_view == 'summary')
									<tr>
										<td colspan="1"><h4>{{ $month }}</h4></td>
										<td colspan="3"><h4>{{ count($jobs_in_month) - 1 }}</h4></td>
										<td class="right aligned" colspan="1"><h4>{{ number_format($monthly_total,2) }}</h4></td>
									</tr>
								@endif

							@endforeach
			            </tbody>
			        </table>

			        <div class="ui divider"></div>

		        @endif
			@endforeach
		@endif

		{{-- Check to see if Jobs Exists for the Users Results --}}
		@if ( count($project_managers) OR count($project_manager))
		<table class="ui compact small very basic table">
            <thead>
                  <tr>
                      <th class="two  wide" style="border: none;">&nbsp;</th>
                      <th class="one  wide" style="border: none;">&nbsp;</th>
                      <th class="five wide" style="border: none;">&nbsp;</th>
                      <th class="five wide" style="border: none;">&nbsp;</th>
                      <th class="four wide right aligned" style="border: none;">&nbsp;</th>
                  </tr>
            </thead>
			<tbody>
				<tr>
					<td class="right aligned" colspan="4" style="border: none;"><h4><b>REPORT TOTAL: </b></h4></td>
					<td class="right aligned" colspan="1"><h4><b>${{number_format(array_sum($report_totals),2)}} </b></h4></td>
				</tr>
				<tr>
					<td class="right aligned" colspan="4" style="border: none;"><h4><b>LOWEST JOB: </b></h4></td>
					<td class="right aligned" colspan="1"><h4><b>${{number_format(min($report_totals),2)}}</b></h4></td>
				</tr>
				<tr>
					<td class="right aligned" colspan="4" style="border: none;"><h4><b>HIGHEST JOB: </b></h4></td>
					<td class="right aligned" colspan="1"><h4><b>${{number_format(max($report_totals),2)}}</b></h4></td>
				</tr>
				<tr>
					<td class="right aligned" colspan="4" style="border: none;"><h4><b>REPORT AVERAGE: </b></h4></td>
					<td class="right aligned" colspan="1"><h4><b>${{number_format(array_sum($report_totals)/count($report_totals),2)}}</b></h4></td>
				</tr>
			</tbody>
		</table>
		@endif
	</div>



	<script>
		jQuery.noConflict();
		(function( $ ) {
			$(function() {



			});
		})(jQuery);
	</script>

@stop
