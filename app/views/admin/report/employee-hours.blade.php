{{-- =========================================================================================================
-----------------
BID HISTORY TEMPALTE
-----------------
========================================================================================================= --}}

@extends('layouts.default')

@section('content')
    <?php
    $hours_total = 0;
    $ot_total    = 0;
    $st_total    = 0;
    ?>
	<div class="ui green large labeled icon button print-button"><i class="print icon"></i>SAVE/PRINT</div>
	<div id="jobs-margin">
		<h1 class="ui block header header-5">Employee Hours</h1>
        <table class="ui compact small very basic table">
            <thead>
              	<tr>
                    <th>#</th>
                    <th></th>
                    <th>Employee</th>
                    <th>Wage Rate</th>
                    <th>Week 1 ST</th>
                    <th>Week 1 OT</th>
                    <th>Total ST</th>
                    <th>Total OT</th>a
                    <th>ST Cost</th>
                    <th>OT Cost</th>
                </tr>
            </thead>

            <tbody>
                @foreach ( $employees as $employee )
                    <tr>
                        <?php
                            $hours_total += ((float)$employee->total_st + (float)$employee->total_ot);
                            $ot_total += (float)$employee->ot_pay;
                            $st_total += (float)$employee->st_pay;
                        ?>
                        <td>{{ $employee->id }}</td>
                        <td></td>
                        <td>{{ sprintf('%s %s', $employee->first_name, $employee->last_name) }}</td>
                        <td>${{ $employee->wage_rate }}</td>
                        <td>{{ $employee->week_1_st }}</td>
                        <td>{{ $employee->week_1_ot }}</td>
                        <td>{{ $employee->total_st }}</td>
                        <td>{{ $employee->total_ot }}</td>
                        <td>{{ $employee->st_pay }}</td>
                        <td>{{ $employee->ot_pay }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <div class="ui divider"></div>

		<table class="ui compact small very basic table">
            <thead>
                  <tr>
                      <th class="one wide">&nbsp;</th>
                      <th class="one wide">&nbsp;</th>
                      <th class="one wide">&nbsp;</th>
                  </tr>
            </thead>
            <tbody>
            <tr>
                <td class="right aligned" colspan="2" style="border: none;"><h4><b>TOTAL HOURS WORKED: </b></h4></td>
                <td class="right aligned" colspan="1"><h4><b>{{ $hours_total }}</b></h4></td>
            </tr>
            <tr>
                <td class="right aligned" colspan="2" style="border: none;"><h4><b>TOTAL ST Pay: </b></h4></td>
                <td class="right aligned" colspan="1"><h4><b>${{$st_total}}</b></h4></td>
            </tr>
            <tr>
                <td class="right aligned" colspan="2" style="border: none;"><h4><b>TOTAL OT Pay: </b></h4></td>
                <td class="right aligned" colspan="1"><h4><b>${{$ot_total}}</b></h4></td>
            </tr>
            </tbody>
		</table>

	</div>
@stop
