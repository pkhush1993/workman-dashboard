{{-- 
=========================================================================================================
DOLLARS BID AND PERFORMED CHART TEMPALTE 
--------------------------------------------------------------------------------------------------------------
Using highcharts.js to generate Charts
-----------------
========================================================================================================= --}}

@extends('layouts.default')

@section('content')


	<div id="dollars-bid-and-performed-chart" style="height:700px;">
		{{-- Use HighCharts.js to Generate Chart Here --}}
	</div>


	<script>
		jQuery.noConflict();
		(function( $ ) {
			$(function() {

				// Build Out Data
				var bidData = [];
				var performedData = [];
				var bids = <?php echo $bid_data;?>;
				var performed = <?php echo $performed_data;?>;
				
				// Iterate through each Bid and Build To Match Chart Data Format
				bids.forEach(function(entry) {
				    bidData.push([Date.parse(entry[0]),entry[1]]);
				});

				// Iterate through each Performed Bid and Build To Match Chart Data Format
				performed.forEach(function(entry) {
				    performedData.push([Date.parse(entry[0]),entry[1]]);
				});
						

			    $('#dollars-bid-and-performed-chart').highcharts({
			        chart: {
			            type: 'area'
			        },			    	
			        title: {
			            text: 'Dollars Bid and Performed'
			        },
			        // subtitle: {
			        //     text: 'Lets Describe Each Chart Here',
			        //     x: -20
			        // },
					xAxis: {
					    type: 'datetime',
			            plotLines: [{
			                value: 0,
			                width: 20,
			                color: '#dcdcdc'
			            }],
						units: [
							['month']			            					    
						]
					},
			        yAxis: {
			            title: {
			                text: 'Bid Totals in Dollars'
			            },
			            plotLines: [{
			                value: 0,
			                width: 2,
			                color: '#808080'
			            }],
			            labels: {
			                formatter: function () {
			                    return '$'+this.value;
			                }
			            }
			        },
			        tooltip: {
			            valuePrefix: '$'
			        },
			        legend: {
			            layout: 'vertical',
			            align: 'right',
			            verticalAlign: 'middle',
			            borderWidth: 0
			        },
				    plotOptions: {
				        line: {
				            connectNulls: true
				        }
				    },			        
			        series: [{
			            name: 'Dollars Bid',
						data: bidData
			        },
			        {
			            name: 'Dollars Performed',
						data: performedData
			        }]			        
			    });


			});
		})(jQuery);	
	</script>

@stop