@extends('layouts.default')

@section('content')


	<div id="search">
		<h1>Reports</h1>
		<h3 class="header-5"><i class="file text icon"></i> Text Reports</h3>

		{{ Form::open(array('class' => 'ui form run-report','route' => 'run.report')) }}
			<div class="five fields">
				<div class="field report-type">
					{{ Form::label('report_type', 'Report Type') }}
					{{ Form::select('report_type', array(
						''                      => 'Select A Report Type',
						'bid_requests'          => 'Open Bid Requests',
						'bid_history'           => 'Bid History',
						'account_hst_performance'  => 'Account History & Performance',
						'employee_revenue'      => 'Performed Jobs by Month',
						'booked_work'           => 'Work by Status',
						'production_pipeline'   => 'Production Pipeline',
						'completed_jobs'        => 'Performed Jobs',
						'follow_ups'            => 'Follow Ups',
						'invoices'              => 'Invoices',
						'jobs-margin'           => 'Jobs Margin',
						'uninvoiced-jobs-labor' => 'Uninvoiced Jobs Labor',
						'employee-hours' 		=> 'Employee Hours',
						'labor-cost-by-job'		=> 'Labor Cost by Job'
					), null , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
				</div>
				<div class="field report-view" style="display:none">
					{{ Form::label('report_view', 'Report View') }}
					{{ Form::select('report_view', array(
						'summary'  => 'Summary',
						'detailed' => 'Detailed'
					), null , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
				</div>
				{{--================== Employee Selects ==================--}}
				<div class="field employees_select" style="display:none">
					{{ Form::label('user_id', 'Employee') }}
					{{ Form::select('user_id', $employees_select, null , array('class'=>'ui search dropdown','autocomplete' =>'off','disabled' =>'disabled')) }}
				</div>
				<div class="field project_manager_select" style="display:none">
					{{ Form::label('user_id', 'Project Managers') }}
					{{ Form::select('user_id', $project_manager_select, null , array('class'=>'ui search dropdown','autocomplete' =>'off','disabled' =>'disabled')) }}
				</div>
				<div class="field estimator_select" style="display:none">
					{{ Form::label('user_id', 'Estimators') }}
					{{ Form::select('user_id', $estimator_select, null , array('class'=>'ui search dropdown','autocomplete' =>'off','disabled' =>'disabled')) }}
				</div>
				<div class="field foreman_select" style="display:none">
					{{ Form::label('user_id', 'Foreman') }}
					{{ Form::select('user_id', $foreman_select, null , array('class'=>'ui search dropdown','autocomplete' =>'off','disabled' =>'disabled')) }}
				</div>
				<div class="field accounting_select" style="display:none">
					{{ Form::label('user_id', 'Account') }}
					{{ Form::select('user_id', $accounting_select, null , array('class'=>'ui search dropdown','autocomplete' =>'off','disabled' =>'disabled')) }}
				</div>
				{{-- ====================================================== --}}
				<div class="field job-status" style="display:none">
					{{ Form::label('job_status', 'Job Status') }}
					{{ Form::select('job_status', array(
						''     		  => 'Select Status',
						'request'     => 'Request',
						'outstanding' => 'Outstanding',
						'contracted'  => 'Contracted',
						'proceed'     => 'Proceed',
						'scheduled'   => 'Scheduled',
						'completed'   => 'Completed',
						'invoicing'   => 'Invoicing',
						'paid'        => 'Paid',
						'rejected'    => 'Rejected'
					), null , array('class'=>'ui search dropdown','autocomplete' =>'off','disabled' =>'disabled')) }}
				</div>
				<div class="field start-date" style="display:none">
					{{ Form::label('start_date', 'Start Date') }}
					{{ Form::text('start_date',NULL, array('placeholder'=>'Date Start','class'=>'date','disabled' =>'disabled')) }}
				</div>
				<div class="field end-date" style="display:none">
					{{ Form::label('end_date', 'End Date') }}
					{{ Form::text('end_date',NULL, array('placeholder'=>'Date End','class'=>'date','disabled' =>'disabled')) }}
				</div>

				<div class="field start-date-limited" style="display:none">
					{{ Form::label('start_date_limited', 'Start Date') }}
					{{ Form::text('start_date_limited',NULL, array('placeholder'=>'Date Start','class'=>'','disabled' =>'disabled')) }}
				</div>
				<div class="field end-date-limited" style="display:none">
					{{ Form::label('end_date_limited', 'End Date') }}
					{{ Form::text('end_date_limited',NULL, array('placeholder'=>'Date End','class'=>'','disabled' =>'disabled')) }}
				</div>
			</div>
			{{ Form::submit('RUN REPORT',array('class' => 'ui submit green button')); }}
		{{ Form::close() }}

		<div class="ui divider"></div>

		<h3><i class="area chart icon"></i> Chart Reports</h3>

		<div class="ui three doubling cards">
			<div class="card">
				<div class="content">
					<a class="header">Total Dollars Bid</a>
					<div class="meta"><i class="bar chart icon"></i> Bar Chart</div>
					<div class="description">
						<p>Select a date range to see the bids that were created within that time frame</p>
					</div>
					<br>
					{{ Form::open(array('class' => 'ui form total-dollars-bid-form','route' => 'post.bid.dollars.chart')) }}
					<div class="two fields">
						<div class="field">
							{{ Form::label('start_dates', 'Start Date') }}
							{{ Form::text('start_date',NULL, array('placeholder'=>'Date Start','class'=>'date')) }}
						</div>
						<div class="field">
							{{ Form::label('end_dates', 'End Date') }}
							{{ Form::text('end_date',NULL, array('placeholder'=>'Date End','class'=>'date')) }}
						</div>
					</div>
					<div class="two fields">
						<div class="field">
							{{ Form::label('project_manager', 'Project Managers') }}
							{{ Form::select('project_manager', $project_manager_select, null , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
						</div>
						<div class="field">
							{{ Form::label('estimator', 'Estimators') }}
							{{ Form::select('estimator', $estimator_select, null , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
						</div>
					</div>
					<div class="field">
						{{ Form::label('report_view', 'Report View') }}
						{{ Form::select('report_view', array(
							''         => 'Select A Report View',
							'monthly'  => 'By Month',
							'12-month' => 'Trailing 12 Month'
						), null , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
					</div>
				    <button class="ui fluid blue button">
				    	<i class="bar chart icon"></i>
				      	VIEW REPORT
				    </button>
			    	{{ Form::close() }}
				</div>
			</div>
			<div class="card">
				<div class="content">
					<a class="header">Performed Jobs</a>
					<div class="meta"><i class="bar chart icon"></i> Bar Chart</div>
					<div class="description">
						<p>Select a date range to see the bids with jobs marked as <b>COMPLETED</b>, <b>INVOICING</b>, and <b>PAID</b> that were created within that time frame</p>
					</div>
					<br>
					{{ Form::open(array('class' => 'ui form revenue-by-month-form','route' => 'post.revenue')) }}
					<div class="two fields">
						<div class="field">
							{{ Form::label('start_dates', 'Start Date') }}
							{{ Form::text('start_date',NULL, array('placeholder'=>'Date Start','class'=>'date')) }}
						</div>
						<div class="field">
							{{ Form::label('end_dates', 'End Date') }}
							{{ Form::text('end_date',NULL, array('placeholder'=>'Date End','class'=>'date')) }}
						</div>
					</div>
					<div class="two fields">
						<div class="field">
							{{ Form::label('project_manager', 'Project Managers') }}
							{{ Form::select('project_manager', $project_manager_select, null , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
						</div>
						<div class="field">
							{{ Form::label('estimator', 'Estimators') }}
							{{ Form::select('estimator', $estimator_select, null , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
						</div>
					</div>
					<div class="field">
						{{ Form::label('report_view', 'Report View') }}
						{{ Form::select('report_view', array(
							''         => 'Select A Report View',
							'monthly'  => 'By Month',
							'12-month' => 'Trailing 12 Month'
						), null , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
					</div>
				    <button class="ui fluid blue button">
				    	<i class="bar chart icon"></i>
				      	VIEW REPORT
				    </button>
			    	{{ Form::close() }}
			    </div>
			</div>
			<div class="card">
				<div class="content">
					<a class="header">Dollars Bid/Performed Chart</a>
					<div class="meta"><i class="line chart icon"></i> Line Chart</div>
					<div class="description">
						<p>Select a date range to see the bids with jobs marked as <b>PAID</b> that were created within that time frame</p>
					</div>
					<br>
					{{ Form::open(array('class' => 'ui form revenue-by-month-form','route' => 'post.dollars.bid.and.performed')) }}
					<div class="two fields">
						<div class="field">
							{{ Form::label('start_dates', 'Start Date') }}
							{{ Form::text('start_date',NULL, array('placeholder'=>'Date Start','class'=>'date')) }}
						</div>
						<div class="field">
							{{ Form::label('end_dates', 'End Date') }}
							{{ Form::text('end_date',NULL, array('placeholder'=>'Date End','class'=>'date')) }}
						</div>
					</div>
					<div class="two fields">
						<div class="field">
							{{ Form::label('project_manager', 'Project Managers') }}
							{{ Form::select('project_manager', $project_manager_select, null , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
						</div>
						<div class="field">
							{{ Form::label('estimator', 'Estimators') }}
							{{ Form::select('estimator', $estimator_select, null , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
						</div>
					</div>
					<div class="field">
						{{ Form::label('report_view', 'Report View') }}
						{{ Form::select('report_view', array(
							''         => 'Select A Report View',
							'monthly'  => 'By Month',
							'12-month' => 'Trailing 12 Month'
						), null , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
					</div>
				    <button class="ui fluid blue button">
				    	<i class="line chart icon"></i>
				      	VIEW REPORT
				    </button>
			    	{{ Form::close() }}
			    </div>
			</div>
			<div class="card">
				<div class="content">
					<a class="header">Jobs Bid/Performed Chart</a>
					<div class="meta"><i class="line chart icon"></i> Line Chart</div>
					<div class="description">
						<p>Select a date range to see the jobs marked as <b>COMPLETED</b>, <b>INVOICING</b>, and <b>PAID</b> as Performed Jobs within the specified time frame</p>
					</div>
					<br>
					{{ Form::open(array('class' => 'ui form revenue-by-month-form','route' => 'post.jobs.bid.and.performed')) }}
					<div class="two fields">
						<div class="field">
							{{ Form::label('start_dates', 'Start Date') }}
							{{ Form::text('start_date',NULL, array('placeholder'=>'Date Start','class'=>'date')) }}
						</div>
						<div class="field">
							{{ Form::label('end_dates', 'End Date') }}
							{{ Form::text('end_date',NULL, array('placeholder'=>'Date End','class'=>'date')) }}
						</div>
					</div>
					<div class="two fields">
						<div class="field">
							{{ Form::label('project_manager', 'Project Managers') }}
							{{ Form::select('project_manager', $project_manager_select, null , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
						</div>
						<div class="field">
							{{ Form::label('estimator', 'Estimators') }}
							{{ Form::select('estimator', $estimator_select, null , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
						</div>
					</div>
					<div class="field">
						{{ Form::label('report_view', 'Report View') }}
						{{ Form::select('report_view', array(
							''         => 'Select A Report View',
							'monthly'  => 'By Month',
							'12-month' => 'Trailing 12 Month'
						), null , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
					</div>
				    <button class="ui fluid blue button">
				    	<i class="line chart icon"></i>
				      	VIEW REPORT
				    </button>
			    	{{ Form::close() }}
			    </div>
			</div>
			<div class="card">
				<div class="content">
					<a class="header">Dollars Bid/Secured</a>
					<div class="meta"><i class="pie chart icon"></i> Line Chart</div>
					<div class="description">
						<p>This used to be Winning Percentage by Dollars.We changed the title because we think it will explain the Report Better</p>
					</div>
					<br>
					{{ Form::open(array('class' => 'ui form bid-completion-by-dollars','route' => 'post.bid.completion.by.dollars')) }}
					<div class="two fields">
						<div class="field">
							{{ Form::label('start_dates', 'Start Date') }}
							{{ Form::text('start_date',NULL, array('placeholder'=>'Date Start','class'=>'date')) }}
						</div>
						<div class="field">
							{{ Form::label('end_dates', 'End Date') }}
							{{ Form::text('end_date',NULL, array('placeholder'=>'Date End','class'=>'date')) }}
						</div>
					</div>
					<div class="two fields">
						<div class="field">
							{{ Form::label('project_manager', 'Project Managers') }}
							{{ Form::select('project_manager', $project_manager_select, null , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
						</div>
						<div class="field">
							{{ Form::label('estimator', 'Estimators') }}
							{{ Form::select('estimator', $estimator_select, null , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
						</div>
					</div>
					<div class="field">
						{{ Form::label('report_view', 'Report View') }}
						{{ Form::select('report_view', array(
							''         => 'Select A Report View',
							'monthly'  => 'By Month',
							'12-month' => 'Trailing 12 Month'
						), null , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
					</div>
				    <button class="ui fluid blue button">
				    	<i class="line chart icon"></i>
				      	VIEW REPORT
				    </button>
			    	{{ Form::close() }}
				</div>
			</div>
			<div class="card">
				<div class="content">
					<a class="header">Secured Jobs Bid/Secured</a>
					<div class="meta"><i class="pie chart icon"></i> Line Chart</div>
					<div class="description">
						<p>This used to be Winning Percentage by Jobs. We changed the title because we think it will explain the Report Better</p>
					</div>
					<br>
					{{ Form::open(array('class' => 'ui form bid-completion-by-jobs','route' => 'post.bid.completion.by.jobs')) }}
					<div class="two fields">
						<div class="field">
							{{ Form::label('start_dates', 'Start Date') }}
							{{ Form::text('start_date',NULL, array('placeholder'=>'Date Start','class'=>'date')) }}
						</div>
						<div class="field">
							{{ Form::label('end_dates', 'End Date') }}
							{{ Form::text('end_date',NULL, array('placeholder'=>'Date End','class'=>'date')) }}
						</div>
					</div>
					<div class="two fields">
						<div class="field">
							{{ Form::label('project_manager', 'Project Managers') }}
							{{ Form::select('project_manager', $project_manager_select, null , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
						</div>
						<div class="field">
							{{ Form::label('estimator', 'Estimators') }}
							{{ Form::select('estimator', $estimator_select, null , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
						</div>
					</div>
					<div class="field">
						{{ Form::label('report_view', 'Report View') }}
						{{ Form::select('report_view', array(
							''         => 'Select A Report View',
							'monthly'  => 'By Month',
							'12-month' => 'Trailing 12 Month'
						), null , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
					</div>
				    <button class="ui fluid blue button">
				    	<i class="line chart icon"></i>
				      	VIEW REPORT
				    </button>
			    	{{ Form::close() }}
				</div>
			</div>
			<div class="card">
				<div class="content">
					<a class="header">Bid Completion Histogram</a>
					<div class="meta"><i class="pie chart icon"></i> Line Chart</div>
					<div class="description">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus sunt perspiciatis labore nobis nesciunt palabore.</p>
					</div>
					<br>
					{{ Form::open(array('class' => 'ui form bid-completion-by-jobs','route' => 'post.bid.completion.histogram')) }}
					<div class="two fields">
						<div class="field">
							{{ Form::label('start_dates', 'Start Date') }}
							{{ Form::text('start_date',NULL, array('placeholder'=>'Date Start','class'=>'date')) }}
						</div>
						<div class="field">
							{{ Form::label('end_dates', 'End Date') }}
							{{ Form::text('end_date',NULL, array('placeholder'=>'Date End','class'=>'date')) }}
						</div>
					</div>
					<div class="two fields">
						<div class="field">
							{{ Form::label('project_manager', 'Project Managers') }}
							{{ Form::select('project_manager', $project_manager_select, null , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
						</div>
						<div class="field">
							{{ Form::label('estimator', 'Estimators') }}
							{{ Form::select('estimator', $estimator_select, null , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
						</div>
					</div>
				    <button class="ui fluid blue button">
				    	<i class="line chart icon"></i>
				      	VIEW REPORT
				    </button>
			    	{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>

	<script>
		jQuery.noConflict();
		(function( $ ) {
			$(function() {
				$('.start-date-limited').find('input').datepicker({
					onSelect: function(date) {
						date = $(this).datepicker('getDate');
						var maxDate = new Date(date.getTime());
						maxDate.setDate(maxDate.getDate() + 7);

						if ( $('.end-date-limited').find('input').hasClass('hasDatepicker') ) {
							$('.end-date-limited').find('input').val('');
							$('.end-date-limited').find('input').datepicker('destroy');
						}
						$('.end-date-limited').find('input').datepicker({
							minDate: date,
							maxDate: maxDate
						})
					}
				});

				$('body').on('change', 'select[name=report_type]', function(e){
					var reportType = $(this).val();

					// Reset Fields and Disabled Fields
					$('.report-view').hide().find('input').attr('disabled', 'disabled');
					$('.employees_select').hide().find('input').attr('disabled', 'disabled');
					$('.project_manager_select').hide().find('select').attr('disabled', 'disabled');
					$('.estimator_select').hide().find('select').attr('disabled', 'disabled');
					$('.foreman_select').hide().find('select').attr('disabled', 'disabled');
					$('.accounting_select').hide().find('select').attr('disabled', 'disabled');
					$('.job-status').hide().find('input').attr('disabled', 'disabled');
					$('.start-date').hide().find('input').attr('disabled', 'disabled');
					$('.end-date').hide().find('input').attr('disabled', 'disabled');


					if (reportType == 'employee_revenue') {
						// Show/Enabled Fields
						$('.report-view').show().find('input').removeAttr('disabled');
						$('.project_manager_select').show().find('select').removeAttr('disabled');
						$('.start-date').show().find('input').removeAttr('disabled');
						$('.end-date').show().find('input').removeAttr('disabled');
					}
					else if (reportType == 'revenue') {
						// Show/Enabled Fields
						$('.report-view').show().find('input').removeAttr('disabled');
						$('.start-date').show().find('input').removeAttr('disabled');
						$('.end-date').show().find('input').removeAttr('disabled');
					} else if (reportType == 'bid_history') {
						$('.report-view').show().find('input').removeAttr('disabled');
						$('.start-date').show().find('input').removeAttr('disabled');
						$('.end-date').show().find('input').removeAttr('disabled');
						$('.estimator_select').show().find('select').removeAttr('disabled');
						$('.estimator_select').show().find('input').removeAttr('disabled');
					} else if (reportType == 'account_hst_performance') {
//						$('.report-view').show().find('input').removeAttr('disabled');
						$('.start-date').show().find('input').removeAttr('disabled');
						$('.end-date').show().find('input').removeAttr('disabled');
//						$('.estimator_select').show().find('select').removeAttr('disabled');
//						$('.estimator_select').show().find('input').removeAttr('disabled');
					}
					else if (reportType == 'completed_jobs') {
						// Show/Enabled Fields
						$('.project_manager_select').show().find('select').removeAttr('disabled');
						$('.start-date').show().find('input').removeAttr('disabled');
						$('.end-date').show().find('input').removeAttr('disabled');
					} else if (reportType == 'follow_ups') {
						$('.employees_select').show().find('select').removeAttr('disabled');
						$('.employees_select').show().find('input').removeAttr('disabled');
						$('.start-date').show().find('input').removeAttr('disabled');
						$('.end-date').show().find('input').removeAttr('disabled');
					} else if ( reportType == 'labor-cost-by-job' ) {
						$('.start-date').show().find('input').removeAttr('disabled');
						$('.end-date').show().find('input').removeAttr('disabled');
					}
					else if (reportType == 'invoices' || reportType == 'jobs-margin' || reportType == 'uninvoiced-jobs-labor') {
						// Show/Enabled Fields
						$('.start-date').show().find('input').removeAttr('disabled');
						$('.end-date').show().find('input').removeAttr('disabled');
					}
					else if (reportType == 'employee-hours') {
						$('.start-date-limited').show().find('input').removeAttr('disabled');
						$('.end-date-limited').show().find('input').removeAttr('disabled');
					}
					else if (reportType == 'booked_work') {
						// Show/Enabled Fields
						$('.project_manager_select').show().find('select').removeAttr('disabled');
						$('.job-status').show().find('select').removeAttr('disabled');
						$('.job-status').show().find('input').removeAttr('disabled');
					}
					else if (reportType == 'bid_requests') {
						$('.estimator_select').show().find('select').removeAttr('disabled');
						$('.estimator_select').show().find('input').removeAttr('disabled');
					}
					else if (reportType == 'production_pipeline') {
						// Show/Enabled Fields
						$('.project_manager_select').show().find('select').removeAttr('disabled');
						$('.project_manager_select').show().find('input').removeAttr('disabled');
					}
					else{
						console.log('Selection Not Correct');
					}

				});

				$('select[name=report_type]').trigger('change');

			//Semantic UI Custom Form Validation Rule For Pay Periods
			$.fn.form.settings.rules.dateRange = function(value) {
				var startDate = new Date($('input[name=start_date]').val());
				var endDate   = new Date($('input[name=end_date]').val());
				if (startDate > endDate){
					return false;
				}
				else{
					return true;
				}
			}

			//Semantic UI Form Validation =========================
			$('.ui.form').form(
				{
				    startDate: {
						identifier  : 'start_date',
						rules: [
							{
								type   : 'empty',
								prompt : 'Start Date is required'
							},
							{
								type   : 'dateRange',
								prompt : 'Start Date Must be Before End Date'
							},
						]
				    },
				    endDate: {
						identifier  : 'end_date',
						rules: [
							{
								type   : 'empty',
								prompt : 'End Date is required'
							},
							{
								type   : 'dateRange',
								prompt : 'End Date Must be After Start Date'
							},
						]
				    },
				    reportType: {
						identifier  : 'report_type',
						rules: [
							{
								type   : 'empty',
								prompt : 'End Date is required'
							}
						]
				    },
					jobStatus: {
						identifier  : 'job_status',
						rules: [
							{
								type   : 'empty',
								prompt : 'Job Status is required'
							}
						]
				    },
					reportView: {
						identifier	:	'report_view',
						rules:	[
							{
								type	:	'empty',
								prompt	:	'Please select a report type'
							}
						]
					}
				},
                {
                    inline : true,
                    on     : 'submit',
                }
			);

			});

		})(jQuery);
	</script>

@stop
