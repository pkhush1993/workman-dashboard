{{-- =========================================================================================================
-----------------
BID HISTORY TEMPALTE
-----------------
Passing One of the Following Report Types in the URL to manipulate template

summary
detailed
========================================================================================================= --}}

@extends('layouts.default')

@section('content')
	<div class="ui green large labeled icon button print-button">
		<i class="print icon"></i>
		SAVE/PRINT
	</div>
	<div id="jobs-margin">

		<h1 class="ui block header header-5">Job Margins <small>({{ date('m/d/Y',strtotime($start_date)).' - '.date('m/d/Y',strtotime($end_date)) }})</small></h1>


        <table class="ui compact small very basic table">
            <thead>
              	<tr>
              		<th class="one wide header-5">Completed Date</th>
					<th class="one wide header-5">Job #</th>
                    <th class="one wide header-5">Job Name</th>
                    <th class="one wide header-5">Revenue</th>
					<th class="one wide header-5">Labor Cost</th>
                    <th class="one wide header-5">Hours Worked</th>
					<th class="one wide header-5">Margin</th>
              	</tr>
            </thead>
            <tbody>
				<?php
					$total_labor_costs = 0;
					$total_grand_total = 0;
					$total_hours = 0;
				 ?>
                @foreach ($jobs as $job)
					<?php
						$grand_total         = $job->getBid('subtotal',0);
						$labor_costs         = $job->labor()['cost'];
						$total_labor_costs += $labor_costs;
						$total_grand_total +=  $grand_total;
						$total_hours       +=  $job->labor()['hours'];
					 ?>
	                <tr>
	                    <td class="one wide">{{ date('m/d/Y',strtotime($job->completed_date)) }}</td>
	                    <td class="one wide">{{ $job->job_number }}</td>
	                    <td class="one wide">{{ $job->name  }}</td>
	                    <td class="one wide">${{ number_format($grand_total ,2) }}</td>
						<td class="one wide">${{ number_format($labor_costs,2) }}</td>
	                    <td class="one wide">{{ $job->labor()['hours'] }}</td>
	                    <td class="one wide">{{ ($job->getBid('grand_total',0)) ? number_format( ($grand_total - $labor_costs) / $grand_total * 100).'%' : '100%'}}</td>
	                </tr>
				@endforeach
            </tbody>
        </table>

        <div class="ui divider"></div>

		<table class="ui compact small very basic table">
            <thead>
                  <tr>
                      <th class="one wide">&nbsp;</th>
                      <th class="one wide">&nbsp;</th>
                      <th class="one wide">&nbsp;</th>
                  </tr>
            </thead>
			<tbody>
				<tr>
					<td class="right aligned" colspan="2" style="border: none;"><h4><b>TOTAL REVENUE: </b></h4></td>
					<td class="right aligned" colspan="1"><h4><b>${{ number_format($total_grand_total,2) }}</b></h4></td>
				</tr>
				<tr>
					<td class="right aligned" colspan="2" style="border: none;"><h4><b>TOTAL HOURS: </b></h4></td>
					<td class="right aligned" colspan="1"><h4><b>{{ number_format($total_hours,2) }} </b></h4></td>
				</tr>
				<tr>
					<td class="right aligned" colspan="2" style="border: none;"><h4><b>TOTAL LABOR COSTS: </b></h4></td>
					<td class="right aligned" colspan="1"><h4><b>${{ number_format($total_labor_costs,2) }}</b></h4></td>
				</tr>
				<tr>
					<td class="right aligned" colspan="2" style="border: none;"><h4><b>MARGIN: </b></h4></td>
					<td class="right aligned" colspan="1"><h4><b>{{ number_format( ($total_grand_total - $total_labor_costs) / $total_grand_total * 100).'%' }}</b></h4></td>
				</tr>
			</tbody>
		</table>
	</div>


	<script>
		jQuery.noConflict();
		(function( $ ) {
			$(function() {



			});
		})(jQuery);
	</script>

@stop
