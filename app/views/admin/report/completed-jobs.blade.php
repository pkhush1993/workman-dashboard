{{-- =========================================================================================================
-----------------
COMPLETED JOBS TEMPALTE
-----------------
========================================================================================================= --}}


@extends('layouts.default')

@section('content')

	<div class="ui green large labeled icon button print-button">
		<i class="print icon"></i>
		SAVE/PRINT
	</div>
	<?php $JobBidAmount = [] ?>

	<div id="completed-jobs">
		<h1 class="ui block header header-5">Performed Jobs {{ ($project_manager) ? 'for ' . $project_manager->getFullName():'' }} <small>({{ date('m/d/Y',strtotime($start_date)).' - '.date('m/d/Y',strtotime($end_date)) }})</small></h1>
		{{-- If a User is Defined and Exists --}}
		@if ($project_manager)
        	{{-- Check to see if Jobs Exists for the Users Results --}}
        	@if (count($jobs))
		        <table class="ui compact very basic table">
		            <thead>
	                  	<tr>
                      		<th><h3 class="header-5">Job #</h3></th>
                      		<th><h3 class="header-5">Job Name</h3></th>
                      		<th><h3 class="header-5">Date Completed</h3></th>
                      		<th><h3 class="header-5">Site Location</h3></th>
                      		<th><h3 class="header-5">Project Manager</h3></th>
                      		<th><h3 class="header-5">Job Bid Amount</h3></th>
							<th><h3 class="header-5">Sales Tax</h3></th>
							<th><h3 class="header-5">Grand Total</h3></th>
	                  	</tr>
		            </thead>
		            <tbody>
		        		{{-- Irritate through each line item by category by month --}}
						@foreach ($jobs as $job)
							<tr>
								<td>{{$job->job_number}}</td>
								<td>{{$job->name}}</td>
								<td>{{date('m/d/Y',strtotime($job->completed_date))}}</td>
								<td>
									@if ($job->site_address_1)
										{{ $job->site_address_1; }} {{ $job->site_address_2; }} <br>{{ $job->site_city; }}, {{ $job->site_state; }} {{ $job->site_zip; }}
									@else
										No Site Address Assigned
									@endif
								</td>
								<td>{{$project_manager->getFullName()}}</td>
								<td>{{($job->getBid())? '$'.number_format($job->getBid()->getAttribute('subtotal'),2) : 'No Bid' }}</td>
								@if ($job->getBid())
									<?php $JobBidAmount[] = $job->getBid()->getAttribute('subtotal'); ?>
								@endif
								<td>${{ number_format($job->getBid() ? $job->getBid()->getAttribute("sales_tax_total") : 0, 2) }}</td>
								<td>${{ number_format($job->getBid() ? $job->getBid()->getAttribute("grand_total") : 0, 2) }}</td>

							</tr>
						@endforeach
					</tbody>
				</table>
			@else
				<div class="ui orange message">No Jobs for this User</div>
        	@endif
		@elseif (count($project_managers))
	        <table class="ui compact very basic table">
	            <thead>
	              	<tr>
	              		<th><h3 class="header-5">Job #</h3></th>
	              		<th><h3 class="header-5">Job Name</h3></th>
	              		<th><h3 class="header-5">Date Completed</h3></th>
	              		<th><h3 class="header-5">Site Location</h3></th>
	              		<th><h3 class="header-5">Project Manager</h3></th>
	              		<th><h3 class="header-5">Job Bid Amount</h3></th>
	              		<th><h3 class="header-5">Sales Tax</h3></th>
	              		<th><h3 class="header-5">Grand Total</h3></th>
	              	</tr>
	            </thead>
	            <tbody>
	        		{{-- Irritate through each line item by category by month --}}
					@foreach ($jobs as $job)
						<tr>
							<td>{{$job->job_number}}</td>
							<td>{{$job->name}}</td>
							<td>{{date('m/d/Y',strtotime($job->completed_date))}}</td>
							<td>
								@if ($job->site_address_1)
									{{ $job->site_address_1; }} {{ $job->site_address_2; }} <br>{{ $job->site_city; }}, {{ $job->site_state; }} {{ $job->site_zip; }}
								@else
									No Site Address Assigned
								@endif
							</td>
							<td>{{ ( $job->getProjectManager() ) ? $job->getProjectManager()->getFullName() : 'No Project Manager'}}</td>
							<td>{{($job->getBid())? '$'.number_format($job->getBid()->getAttribute('subtotal'),2) : 'No Bid' }}</td>
							<td>${{ number_format($job->getBid() ? $job->getBid()->getAttribute("sales_tax_total") : 0, 2) }}</td>
							<td>${{ number_format($job->getBid() ? $job->getBid()->getAttribute("grand_total") : 0, 2) }}</td>
							<?php if ( $job->getBid() ) {$JobBidAmount[] = $job->getBid()->getAttribute('subtotal');} ?>

						</tr>
					@endforeach
				</tbody>
			</table>
		@endif
	</div>
	<div class="ui divider"></div>

	<table class="ui compact small very basic table">
		<thead>
		<tr>
			<th class="two  wide" style="border: none;">&nbsp;</th>
			<th class="one  wide" style="border: none;">&nbsp;</th>
			<th class="five wide" style="border: none;">&nbsp;</th>
			<th class="five wide" style="border: none;">&nbsp;</th>
			<th class="four wide right aligned" style="border: none;">&nbsp;</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td class="right aligned" colspan="4" style="border: none;"><h4><b>Report Total: </b></h4></td>
			<td class="right aligned" colspan="1"><h4><b> ${{ number_format(array_sum($JobBidAmount), 2) }} </b></h4></td>
		</tr>
		<tr>
			<td class="right aligned" colspan="4" style="border: none;"><h4><b>Lowest Job: </b></h4></td>
			<td class="right aligned" colspan="1"><h4><b> ${{ number_format(min($JobBidAmount), 2) }}</b></h4></td>
		</tr>
		<tr>
			<td class="right aligned" colspan="4" style="border: none;"><h4><b>Highest Job: </b></h4></td>
			<td class="right aligned" colspan="1"><h4><b>${{ number_format(max($JobBidAmount), 2) }}</b></h4></td>
		</tr>
		<tr>
			<td class="right aligned" colspan="4" style="border: none;"><h4><b>Report Average:</b></h4></td>
			<td class="right aligned" colspan="1"><h4><b>${{ number_format(array_sum($JobBidAmount)/count($JobBidAmount), 2) }}</b></h4></td>
		</tr>
		</tbody>
	</table>
@stop
