{{-- =========================================================================================================
-----------------
INVOICE TEMPALTE 
-----------------
Passing One of the Following Invoice Types in the URL ( /{invoice_type}/{job_id}/ ) to manipulate template

down-payment
bid-grand-total
bid-category-subtotals
bid-item-totals
========================================================================================================= --}}


@extends('layouts.report')

@section('content')

	<div id="invoice" class="">
		
		<!-- LetterHead -->
		@include('admin.report.includes.letterhead')

		{{-- Job Info --}}
		@include('admin.report.includes.job-info')

		<div style="text-align:right;margin-top:0" class="ui header">
			<h1 class="header-5">Job  #{{ $job->job_number }}</h1>
		</div>

		{{-- Verbiage for Different Invoice Types --}}
		@if ($invoice_type == 'down-payment')
			<div class="ui aligned grid">
				<div class="four wide center aligned column">
					<h4>** DOWN PAYMENT **</h4>
				</div>
				<div class="twelve wide column">
					<h5>We will soon be commencing with work at your property. Please remit the down payment indicated below before the first scheduled that of work. Thank You</h5>
				</div>
			</div>
		@elseif (in_array(array('bid-grand-total', 'bid-category-subtotals', 'bid-item-totals')))
			<ul class="ui list">
			  <li>Gaining Access</li>
			  <li>Inviting Friends</li>
			  <li>Benefits
			    <ul>
			      <li>Use Anywhere</li>
			      <li>Rebates</li>
			      <li>Discounts</li>
			    </ul>
			  </li>
			  <li>Warranty</li>
			</ul>	
		@endif

		<div class="ui divider"></div>

		<h2 class="ui center aligned header">{{$down_payment}}<small>%</small>  Down Payment Invoice</h2>

		{{-- Line Items --}}
		@foreach ($line_items as $line_item_category => $line_item_category_items)
			<h3>{{$line_item_category}}</h3>
			<table class="ui very basic small compact table">
	            <thead>
	                  <tr>
	                      <th class="two wide">Quantity</th>
	                      <th class="six wide">Description</th>
	                      <th class="three wide right aligned">Total</th>
	                  </tr>
	            </thead>			
				<tbody>				
					@foreach ($line_item_category_items as $item)
						<tr>
							<td>{{$item['quantity']}} {{$item['unit']}} @ ${{$item['rate']}}</td>
							<td>{{$item['product']}}</td>
							<td class="right aligned">{{$item['amount']}}</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		@endforeach

		<div class="ui divider"></div>

		{{-- Invoice Line Items --}}
		<table class="ui very basic small compact table invoice-totals">
			<tbody>			
				<tr>
					<td class="ten wide">&nbsp;</td>
					<td class="four wide right aligned"><h4>Subtotal</h4></td>
					<td class="one wide right aligned"><h4>${{$bid->subtotal}}</h4></td>
				</tr>
				<tr>
					<td class="ten wide">&nbsp;</td>
					<td class="four wide right aligned"><h4>Estimated Sales Tax @ {{number_format($bid->tax_rate)}}%</h4></td>
					<td class="one wide right aligned"><h4>${{$bid->sales_tax_total}}</h4></td>
				</tr>
				<tr>
					<td class="ten wide">&nbsp;</td>
					<td class="four wide right aligned"><h4>Total</h4></td>
					<td class="one wide right aligned"><h4>${{$bid->grand_total * $down_payment}}</h4></td>
				</tr>
				<tr style="color:#078007">
					<td class="ten wide">&nbsp;</td>
					<td class="four wide right aligned"><h3>{{$down_payment}}<small>%</small> Down - Payment Due</h3></td>
					<td class="one wide right aligned"><h3>${{($down_payment / 100) * $bid->grand_total}}</h3></td>
				</tr>				
			</tbody>
		</table>

		<div class="ui divider"></div>

		<i>**Please remit payment to @if($company->address_1)<b>{{ $company->address_1; }} {{ $company->address_2; }} {{ $company->city; }}, {{ $company->state; }} {{ $company->zip; }}</b>@endif</i>

		<br><br><br>
		{{date('l F,dS Y')}}

	</div>

	<script>
		jQuery.noConflict();
		(function( $ ) {
			$(function() {
				// html2canvas($("#downpayment"), {
				// 	timeout : 4000,
				// 	onrendered: function(canvas) {
				// 		var doc = new jsPDF(1, 'px', 'letter');
				// 		var imgData = canvas.toDataURL(
				// 		    'image/png',1.0);
				// 		console.log(canvas.width);
				// 		doc.addImage(canvas, 'PNG', 5, 5);
				// 		doc.save('sample-file.pdf');		                
				// 	}
				// });
			});
		})(jQuery);	
	</script>

@stop