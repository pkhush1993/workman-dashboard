@extends('layouts.default')

@section('content')
    <div class="ui green large labeled icon button print-button">
        <i class="print icon"></i>
        SAVE/PRINT
    </div>

    <div class="labor-cost">
        <table class="ui compact small very basic table">
            <thead>
                <tr>
                    <th>Job Number</th>
                    <th>Job Name</th>
                    <th>Task</th>
                    <th>Cost</th>
                </tr>
            </thead>
            <tbody>
                @if ( count($data) )
                    @foreach ( $data as $item )
                        <tr>
                            <td>{{ $item->job_number }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ strlen($item->task) ? $item->task : 'No Task' }}</td>
                            <td>${{ number_format($item->cost, 2) }}</td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>

    <script>
        jQuery.noConflict();
        (function( $ ) {
            $(function() {

            });
        })(jQuery);
    </script>

@stop
