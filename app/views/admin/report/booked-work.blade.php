{{-- =========================================================================================================
-----------------
Booked Work TEMPALTE
-----------------
========================================================================================================= --}}


@extends('layouts.default')

@section('content')

	<?php
		$grand_total = 0;
		$line_items_by_category = [];
		$Item_Amounts = [];
	?>

	<div class="ui green large labeled icon button print-button">
		<i class="print icon"></i>
		SAVE/PRINT
	</div>

	<div id="booked-work">

		<h1 class="ui block header header-5">Summary of {{ ($job_status ) ? ucwords($job_status) : '' }} Work by Status {{ ($project_manager) ? 'for ' . $project_manager->getFullName():'' }} </h1>

		{{-- If a User is Defined and Exists --}}
		@if ($project_manager)
        	{{-- Check to see if Jobs Exists for the Users Results --}}
        	@if (count($jobs = $project_manager->getUserJobsByRole('project_manager',[$job_status])))
        		{{-- Iterate through Jobs and Group the Results by Line Item Category by Creating an Accusative Array out of a Object  --}}
        		@foreach ($jobs as $job)
        			@if ($job->getBid())
						@foreach ($job->getBid()->getLineItems() as $line_item)
							<?php
								$line_items_by_category[ $line_item['category'] ][]= $line_item;
								$grand_total = $line_item->amount + $grand_total;
							?>
						@endforeach
					@endif
        		@endforeach
		        <table class="ui compact very basic table">
		            <thead>
	                  	<tr>
                      		<th class="left aligned"><h3>Category</h3></th>
                      		<th class="right aligned"><h3>Amount</h3></th>
	                  	</tr>
		            </thead>
		            <tbody>
		        		{{-- Irritate through each line item by category by month --}}
						@foreach ($line_items_by_category as $category => $line_items)
							{{-- Reset Category Total  --}}
							<?php $category_total = 0; ?>
							{{-- Add Up Totals for Current Category --}}
							@foreach ($line_items as $line_item)
								<?php $category_total = $line_item->amount + $category_total; ?>

							@endforeach
							<?php $Item_Amounts[] = $category_total;  ?>
							<tr>
								<td>{{$category}}</td>
								<td class="right aligned">${{number_format($category_total,2)}}</td>


							</tr>
						@endforeach
					</tbody>
				</table>
				{{-- Totals for all Line Items --}}
				<table class="ui compact small very basic table">
		            <thead>
		                  <tr>
		                      <th class="fourteen wide" style="border: none;">&nbsp;</th>
		                      <th class="two wide" style="border: none;">&nbsp;</th>
		                  </tr>
		            </thead>
					<tbody>
						<tr>
							<td class="right aligned" colspan="14" style="border: none;"><h3><b>Report Total:</b></h3></td>
							<td class="right aligned" colspan="2"><h3><b> ${{number_format($grand_total,2)}} </b></h3></td>
						</tr>
						<tr>
							<td class="right aligned" colspan="14" style="border: none;"><h3><b>Lowest Job:</b></h3></td>
							<td class="right aligned" colspan="2"><h3><b> ${{ number_format(min($Item_Amounts), 2) }} </b></h3></td>
						</tr>
						<tr>
							<td class="right aligned" colspan="14" style="border: none;"><h3><b>Highest Job:</b></h3></td>
							<td class="right aligned" colspan="2"><h3><b>${{ number_format(max($Item_Amounts), 2) }}</b></h3></td>
						</tr>
						<tr>
							<td class="right aligned" colspan="14" style="border: none;"><h3><b>Report Average:</b></h3></td>
							<td class="right aligned" colspan="2"><h3><b> ${{number_format(array_sum($Item_Amounts)/count($Item_Amounts),2)}}</b></h3></td>
						</tr>

					</tbody>
				</table>
			@else
				<div class="ui orange message">No Jobs for this User</div>
        	@endif
		@elseif (count($project_managers))
			@foreach ($project_managers as $project_manager)
	        	{{-- Check to see if Jobs Exists for the Users Results --}}
	        	@if (count($jobs = $project_manager->getUserJobsByRole('project_manager',[$job_status])))
	        		{{-- Iterate through Jobs and Group the Results by Line Item Category by Creating an Accusative Array out of a Object  --}}
	        		@foreach ($jobs as $job)
	        			@if ($job->getBid())
							@foreach ($job->getBid()->getLineItems() as $line_item)
								<?php
									$line_items_by_category[ $line_item['category'] ][]= $line_item;
									$grand_total = $line_item->amount + $grand_total;
								?>
							@endforeach
						@endif
	        		@endforeach
	        	@endif
			@endforeach
			<table class="ui compact very basic table">
	            <thead>
                  	<tr>
                  		<th class="left aligned"><h3>Category</h3></th>
                  		<th class="right aligned"><h3>Amount</h3></th>
                  	</tr>
	            </thead>
	            <tbody>
	        		{{-- Irritate through each line item by category by month --}}
					@foreach ($line_items_by_category as $category => $line_items)
						{{-- Reset Category Total  --}}
						<?php $category_total = 0; ?>
						{{-- Add Up Totals for Current Category --}}
						@foreach ($line_items as $line_item)
							<?php $category_total = $line_item->amount + $category_total; ?>
						@endforeach
						<tr>
							<td>{{$category}}</td>
							<td class="right aligned">${{number_format($category_total,2)}}</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			{{-- Totals for all Line Items --}}
			<table class="ui compact small very basic table">
	            <thead>
	                  <tr>
	                      <th class="fourteen wide" style="border: none;">&nbsp;</th>
	                      <th class="two wide" style="border: none;">&nbsp;</th>
	                  </tr>
	            </thead>
				<tbody>

					<tr>
						<td class="right aligned" colspan="14" style="border: none;"><h3><b>Report Total:</b></h3></td>
						<td class="right aligned" colspan="2"><h3><b> ${{number_format($grand_total,2)}} </b></h3></td>
					</tr>

				</tbody>
			</table>
		@endif
	</div>

@stop
