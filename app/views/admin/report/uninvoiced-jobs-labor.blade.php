{{-- =========================================================================================================
-----------------
BID HISTORY TEMPALTE
-----------------
========================================================================================================= --}}

@extends('layouts.default')

@section('content')
	<div class="ui green large labeled icon button print-button">
		<i class="print icon"></i>
		SAVE/PRINT
	</div>
	<div id="jobs-margin">

		<h1 class="ui block header">Uninvoiced Jobs Labor <span style="float: right; font-size:14px;">Date: {{ date("m/d/Y", strtotime($start_date)) }} - {{ date("m/d/Y", strtotime($end_date)) }}</span></h1>

        <table class="ui compact small very basic table">
            <thead>
              	<tr>
					<th class="one wide">Job #</th>
					<th class="one wide">Job Name</th>
                    <th class="one wide">Current Status</th>
                    <th class="one wide">Job Amount</th>
					<th class="one wide">Hours</th>
					<th class="one wide">Gross Payroll</th>
              	</tr>
            </thead>
            <tbody>
				<?php
					$total_labor_costs = 0;
					$total_grand_total = 0;
					$total_hours = 0;

					$sum_Total_Hours = [];
					$sum_Total_Amounts = [];
					$sum_Total_Labor = [];
				 ?>
                @foreach ($jobs as $job)
					<?php
						$grand_total         = (float)$job->bid_total;
						$labor_costs         = (float)$job->labor;
						$total_labor_costs +=  (float)$labor_costs;
						$total_grand_total +=  (float)$grand_total;
						$total_hours       +=  (float)$job->hours;

                        $sum_Total_Hours[] = (float)str_replace(',', '', $job->hours);
                        $sum_Total_Amounts[] = (float)str_replace(',', '', $job->bid_total);
                        $sum_Total_Labor[] = (float)str_replace(',', '', $job->labor);
					 ?>
	                <tr>
	                    <td class="one wide">{{ $job->job_number }}</td>
	                    <td class="one wide">{{ $job->name }}</td>
						<td class="one wide">{{ ucwords($job->status) }}</td>
	                    <td class="one wide">${{ $job->bid_total }}</td>
						<td class="one wide">{{ $job->hours }}</td>
						<td class="one wide">${{ number_format((float)str_replace(',', '', $job->labor),2) }}</td>
	                </tr>
				@endforeach
            </tbody>
        </table>

        <div class="ui divider"></div>

		<table class="ui compact small very basic table">
            <thead>
                  <tr>
                      <th class="one wide">&nbsp;</th>
                      <th class="one wide">&nbsp;</th>
                      <th class="one wide">&nbsp;</th>
                  </tr>
            </thead>
			<tbody>
				<tr>
					<td class="right aligned" colspan="2" style="border: none;"><h4><b>TOTAL JOB AMOUNT: </b></h4></td>
					<td class="right aligned" colspan="1"><h4><b>${{ number_format(array_sum($sum_Total_Amounts),2) }}</b></h4></td>
				</tr>
				<tr>
					<td class="right aligned" colspan="2" style="border: none;"><h4><b>TOTAL HOURS WORKED: </b></h4></td>
					<td class="right aligned" colspan="1"><h4><b>{{ array_sum($sum_Total_Hours) }}</b></h4></td>
				</tr>
				<tr>
					<td class="right aligned" colspan="2" style="border: none;"><h4><b>TOTAL LABOR COSTS: </b></h4></td>
					<td class="right aligned" colspan="1"><h4><b>${{ number_format(array_sum($sum_Total_Labor),2) }}</b></h4></td>
				</tr>
			</tbody>
		</table>

		<div class="ui green large labeled icon button print-button">
			<i class="print icon"></i>
			SAVE/PRINT
		</div>

	</div>


	<script>
		jQuery.noConflict();
		(function( $ ) {
			$(function() {



			});
		})(jQuery);
	</script>

@stop
