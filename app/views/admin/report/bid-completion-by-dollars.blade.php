{{--
=========================================================================================================
DOLLARS BID AND PERFORMED TREND IN PERCENT CHART TEMPALTE
--------------------------------------------------------------------------------------------------------------
Using highcharts.js to generate Charts
-----------------
========================================================================================================= --}}

@extends('layouts.default')

@section('content')


	<div id="bid-completion-by-dollars-chart" style="height:700px;">
		{{-- Use HighCharts.js to Generate Chart Here --}}
	</div>


	<script>
		jQuery.noConflict();
		(function( $ ) {
			$(function() {

				// Build Out Data
				var percentData = [];
				var percent = <?php echo $percent_data;?>;

				// Iterate through each Percent Won and Build To Match Chart Data Format
				percent.forEach(function(entry) {
				    percentData.push([Date.parse(entry[0]),entry[1]]);
				});

			    $('#bid-completion-by-dollars-chart').highcharts({
			        chart: {
			            type: 'area'
			        },
			        title: {
			            text: 'Dollars Bid/Secured'
			        },
			        // subtitle: {
			        //     text: 'Lets Describe Each Chart Here',
			        //     x: -20
			        // },
					xAxis: {
					    type: 'datetime',
			            plotLines: [{
			                value: 0,
			                width: 20,
			                color: '#dcdcdc'
			            }],
						units: [
							['month']
						]
					},
			        yAxis: {
			            title: {
			                text: 'Percent'
			            },
			            plotLines: [{
			                value: 0,
			                width: 2,
			                color: '#808080'
			            }],
			            labels: {
			                formatter: function () {
			                    return this.value+'%';
			                }
			            },
			            max: 100
			        },
			        tooltip: {
			            valueSuffix: '%'
			        },
			        legend: {
			            layout: 'vertical',
			            align: 'right',
			            verticalAlign: 'middle',
			            borderWidth: 0
			        },
				    plotOptions: {
				        line: {
				            connectNulls: true
				        }
				    },
			        series: [{
			            name: 'Percent of Paid Jobs',
						data: percentData
			        }]
			    });


			});
		})(jQuery);
	</script>

@stop
