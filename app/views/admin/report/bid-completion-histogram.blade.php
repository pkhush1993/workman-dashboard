{{-- 
=========================================================================================================
BID COMPLETION HISTOGRAM
--------------------------------------------------------------------------------------------------------------
Using highcharts.js to generate Charts
-----------------
========================================================================================================= --}}

@extends('layouts.default')

@section('content')

	<div class="ui green large labeled icon button print-button">
		<i class="print icon"></i>
		SAVE/PRINT
	</div>
	<div id="bid-completion-histogram-chart" style="height:700px;width:1024px;margin:0 auto">
		{{-- Use HighCharts.js to Generate Chart Here --}}
	</div>


	<script>
		jQuery.noConflict();
		(function( $ ) {
			$(function() {

			    $('#bid-completion-histogram-chart').highcharts({
					chart: {
						type: 'column'
					},
					title: {
						text: "{{ $title }}"
					},
					xAxis: {
						categories: {{  json_encode($labels) }},
						align: "left"
					},
					yAxis: {
						min: 0,
						title: {
							text: 'Number of Bids'
						},
						stackLabels: {
							enabled: true,
							style: {
								fontWeight: 'bold',
								color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
							}
						}
					},
					legend: {
						align: 'left',
						verticalAlign: 'bottom',
						floating: false,
						backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
						borderColor: '#CCC',
						borderWidth: 1,
						shadow: false
					},
					tooltip: {
						headerFormat: '',
						pointFormat: '{point.y}'
					},
					plotOptions: {
						column: {
							stacking: 'normal',
							dataLabels: {
								enabled: true,
								color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
								style: {
									textShadow: '0 0 3px black'
								}
							}
						}
					},
					series: [{
						name: 'Won',
						data: {{ json_encode($won) }}
					}, {
						name: 'Lost',
						data: {{ json_encode($lost) }}
					}]
			    });
			});
		})(jQuery);	
	</script>

@stop