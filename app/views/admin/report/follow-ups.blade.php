{{-- =========================================================================================================
-----------------
FOLLOW UPS TEMPALTE
-----------------
========================================================================================================= --}}


@extends('layouts.default')

@section('content')

	<div class="ui green large labeled icon button print-button">
		<i class="print icon"></i>
		SAVE/PRINT
	</div>

	<div id="follow-ups-report">
		<h1 class="ui block header header-5">{{ ($user) ? $user->getFullName()."'s":'' }} Follow Ups</h1>
		{{-- If a User is Defined and Exists --}}
		@if ($user)
        	{{-- Check to see if follow_ups Exists for the Users Results --}}
        	@if (count($follow_ups))
		        <table class="ui compact very basic table">
		            <thead>
		              	<tr>
	                  		<th class="header-5">Created Date</th>
	                  		<th class="header-5">Deadline Date</th>
							<th class="header-5">Job Number</th>
	                  		<th class="header-5">Type</th>
	                  		<th class="header-5">Activity</th>
	                  		<th class="header-5">Note</th>
	                  		<th class="header-5">Contact</th>
	                  		<th class="header-5">Assigned</th>
	                  		<th class="header-5">Created By</th>
		              	</tr>
		            </thead>
		            <tbody>
		        		{{-- Irritate through each line item by category by month --}}
						@foreach ($follow_ups as $follow_up)
							<tr>
								<td> {{date('m/d/Y',strtotime($follow_up->created_at))}} </td>
								<td> {{date('m/d/Y',strtotime($follow_up->deadline))}} </td>
								<td>
									@if ( $follow_up->job() )
										<a href="{{ route('view.job', [$follow_up->getAttribute('job_id')]) }}">
											{{ $follow_up->job()->getAttribute('job_number') }}
										</a>
									@endif
								</td>
								<td> {{ ucwords($follow_up->type) }} </td>
								<td> {{ $follow_up->activity }} </td>
								<td> {{ $follow_up->note }} </td>
								<td> {{ $follow_up->contact_profile() ? $follow_up->contact_profile()->getFullName() : 'No Contact Assigned' }} </td>
								<td> {{ $follow_up->user_profile() ? $follow_up->user_profile()->getFullName() : 'No User Assigned'}} </td>
								<td> {{ $follow_up->created_profile() ? $follow_up->created_profile()->getFullName() : 'No Contact Assigned' }} </td>
						@endforeach
					</tbody>
				</table>
			@else
				<div class="ui orange message">No Jobs for this User</div>
        	@endif
		@elseif (count($users))
	        <table class="ui compact very basic table">
	            <thead>
	              	<tr>
                  		<th class="header-5">Created Date</th>
                  		<th class="header-5">Deadline Date</th>
						<th class="header-5">Job Number</th>
                  		<th class="header-5">Type</th>
                  		<th class="header-5">Activity</th>
                  		<th class="header-5">Note</th>
                  		<th class="header-5">Contact</th>
                  		<th class="header-5">Assigned</th>
                  		<th class="header-5">Created By</th>
	              	</tr>
	            </thead>
	            <tbody>
	        		{{-- Irritate through each line item by category by month --}}
					@foreach ($follow_ups as $follow_up)
						<tr>
							<td> {{date('m/d/Y',strtotime($follow_up->created_at))}} </td>
							<td> {{date('m/d/Y',strtotime($follow_up->deadline))}} </td>
							<td>
								@if ( $follow_up->job() )
									<a href="{{ route('view.job', [$follow_up->getAttribute('job_id')]) }}">
										{{ $follow_up->job()->getAttribute('job_number') }}
									</a>
								@endif
							</td>
							<td> {{ ucwords($follow_up->type) }} </td>
							<td> {{ $follow_up->activity }} </td>
							<td> {{ $follow_up->note }} </td>
							<td> {{ $follow_up->contact_profile() ? $follow_up->contact_profile()->getFullName() : 'No Contact Assigned' }} </td>
							<td> {{ $follow_up->user_profile() ? $follow_up->user_profile()->getFullName() : 'No User Assigned'}} </td>
							<td> {{ $follow_up->created_profile() ? $follow_up->created_profile()->getFullName() : 'No Contact Assigned' }} </td>
						</tr>
					@endforeach
				</tbody>
			</table>
		@endif
	</div>
@stop
