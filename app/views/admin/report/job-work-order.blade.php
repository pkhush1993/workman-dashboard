@extends('layouts.report')

@section('content')

	<div class="ui green large labeled icon button print-button">
		<i class="print icon"></i>
		SAVE/PRINT
	</div>

	<div id="invoice" class="work-order-report">

		<!--============ Header ============-->
		<div class="ui aligned grid">
			<div class="left floated left aligned eight wide column">
				<br>
				<h1 class="ui header" style="font-size: 1.25rem;">
					Job Work Order
					<div class="sub header" style="font-size: 1.5rem;">Job Number #{{ $job->job_number }}</div>
				</h1>
			</div>
			<div class="right floated right aligned four wide column company-info">
				<br>
				@if($company->phone)<p>Phone: <b>{{ $company->phone; }}</b></p>@endif
				@if($company->fax)<p>Fax: <b>{{ $company->fax; }}</b></p>@endif
				@if($company->website)<p><b><a href="{{ $company->website }}">{{ $company->website }}</a></b></p>@endif
				@if($company->address_1)<p>{{ $company->address_1; }} {{ $company->address_2; }}</p>@endif
				@if($company->address_1)<p>{{ $company->city; }}, {{ $company->state; }} {{ $company->zip; }}</p>@endif
			</div>
		</div>

		<div class="ui divider"></div>

		{{--============ Job Info ============--}}
		@include('admin.report.includes.job-info')


		{{--============ Contact Information ============--}}
		<div class="ui segment">
			@include('admin.report.includes.contact-info')
		</div>

		{{-- @TODO: Dates and Times for this Job --}}

		{{--============ JOB NOTES ============--}}
		@if (count($job->getProductionNotes()))
			<h3 class="ui header">JOB NOTES</h3>
			<div class="ui segment">
				@foreach ($job->getProductionNotes() as $note)
					<table class="ui very basic compact table" style="margin: 5px 0 15px 0;">
			            <thead>
			                  <tr>
			                      <th class="two wide">Date</th>
			                      <th class="four wide">Employee</th>
			                      <th class="eight wide">Comments</th>
			                  </tr>
			            </thead>
						<tbody>
							<tr>
								<td>{{ date('m/d/Y',strtotime($note->created_at)) }}</td>
								<td>{{$note->created_profile()->first_name.' '.$note->created_profile()->last_name}}</td>
								<td>{{$note->note}}</td>
							</tr>
						</tbody>
					</table>
				@endforeach
			</div>
		@endif

		{{-- Check if Bid Exists for Job --}}
		@if ($line_items)
			{{--============ JOB SPECS ============--}}
			<h3>JOB SPECS</h3>
			<div class="ui segment">
				@foreach ($line_items as $line_item_category => $line_item_category_items)
					<h4 style="margin:0">{{$line_item_category}}</h4>
					<table class="ui very basic compact table" style="margin: 5px 0 15px 0;">
			            <thead>
			                  <tr>
			                      <th class="two wide">Quantity</th>
			                      <th class="six wide">Description</th>
			                  </tr>
			            </thead>
						<tbody>
							@foreach ($line_item_category_items as $item)
								<tr>
									<td>{{$item['quantity']}} {{$item['unit']}}</td>
									<td>{{$item['product']}}</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				@endforeach
			</div>
		@endif

		<div class="work-order-signatures">
			<div class="ui divider"></div>
			<h2>Certify completion (Check one box below)</h2>
			<h3 style="line-height: 32px;"><div style="border: 1px solid #191919;width: 40px;height: 40px;display: inline-block;margin:0 10px 0 0">&nbsp;</div>This job is COMPLETE according to the specs above</h3>
			<h3 style="line-height: 32px;"><div style="border: 1px solid #191919;width: 40px;height: 40px;display: inline-block;margin:0 10px 0 0">&nbsp;</div>The following items must be done for this job to be COMPLETE</h3>
			<br><br>
			<div class="ui divider" style="border-bottom: 1px solid #000;"></div>
			<br>
			<div class="ui divider" style="border-bottom: 1px solid #000;"></div>
			<br>
			<div class="ui divider" style="border-bottom: 1px solid #000;"></div>
			<br>
			<div class="ui divider" style="border-bottom: 1px solid #000;"></div>
			<br>
			<div class="ui divider" style="border-bottom: 1px solid #000;"></div>
			<br><br>
			<div class="ui two column grid">
				<div class="column">
					<div class="ui divider" style="border-bottom: 1px solid #000;"></div>
					<p style="font-size:16px;font-weight:bold">Project Coordinator (Print)</p>
				</div>
				<div class="column">
					<div class="ui divider" style="border-bottom: 1px solid #000;"></div>
					<p style="font-size:16px;font-weight:bold">Project Coordinator (Sign)</p>
				</div>
			</div>
		</div>

		<br><br><br>
		{{date('l F,dS Y')}}

	</div>

	<script>
		jQuery.noConflict();
		(function( $ ) {
			$(function() {
				// html2canvas($("#downpayment"), {
				// 	timeout : 4000,
				// 	onrendered: function(canvas) {
				// 		var doc = new jsPDF(1, 'px', 'letter');
				// 		var imgData = canvas.toDataURL(
				// 		    'image/png',1.0);
				// 		console.log(canvas.width);
				// 		doc.addImage(canvas, 'PNG', 5, 5);
				// 		doc.save('sample-file.pdf');
				// 	}
				// });
			});
		})(jQuery);
	</script>

@stop
