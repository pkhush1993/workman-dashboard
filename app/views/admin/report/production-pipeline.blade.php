{{-- =========================================================================================================
-----------------
PRODUCTION PIPELINE TEMPALTE 
-----------------
========================================================================================================= --}}


@extends('layouts.default')

@section('content')
	<div class="ui green large labeled icon button print-button">
		<i class="print icon"></i>
		SAVE/PRINT
	</div>
	<?php $JobBidAmount = [];  ?>
	<div id="completed-jobs">
		<h1 class="ui block header header-5">Production Pipeline - Bid and Contract Work {{ ($project_manager) ? 'for ' . $project_manager->getFullName():'' }}</h1>
		{{-- If a User is Defined and Exists --}}
		@if ($project_manager)
        	{{-- Check to see if Jobs Exists for the Users Results --}}
        	@if (count($jobs))
		        <table class="ui compact very basic table">
		            <thead>
	                  	<tr>
                      		<th><h3 class="header-5">Job #</h3></th>
                      		<th><h3 class="header-5">Job Name</h3></th>
                      		<th><h3 class="header-5">Status</h3></th>
                      		<th><h3 class="header-5">Site Location</h3></th>
                      		<th><h3 class="header-5">Project Manager</h3></th>
                      		<th><h3 class="header-5">Job Bid Amount</h3></th>
	                  	</tr>
		            </thead>
		            <tbody>
		        		{{-- Irritate through each line item by category by month --}}
						@foreach ($jobs as $job)
								<tr>
									<td>{{$job->job_number}}</td>
									<td>{{$job->name}}</td>
									<td>{{ucwords($job->status)}}</td>
									<td>
										@if ($job->site_address_1)
											{{ $job->site_address_1; }} {{ $job->site_address_2; }} <br>{{ $job->site_city; }}, {{ $job->site_state; }} {{ $job->site_zip; }}
										@else
											No Site Address Assigned
										@endif
									</td>
									<td>
										@if ( $job->getProjectManager() )
											{{$job->getProjectManager()->getFullName()}}
										@else
											-
										@endif
									</td>
									<td>
										@if ( $job->getBid() )
											{{ str_money($job->getBid()->getAttribute('subtotal')) }}
											<?php
//											$JobBidAmount[] = str_money($job->getBid()->getAttribute('subtotal'));
											$JobBidItem = $job->getBid()->getAttribute('subtotal');
											$JobBidAmount[] = $JobBidItem;
											?>
										@else
											$0.00
										@endif
									</td>
								</tr>
						@endforeach
					</tbody>
				</table> 
			@else
				<div class="ui orange message">No Jobs for this User</div>
        	@endif
		@elseif (count($project_managers))
	        <table class="ui compact very basic table">
	            <thead>
                  	<tr>
                  		<th><h3 class="header-5">Job #</h3></th>
                  		<th><h3 class="header-5">Job Name</h3></th>
                  		<th><h3 class="header-5">Status</h3></th>
                  		<th><h3 class="header-5">Site Location</h3></th>
                  		<th><h3 class="header-5">Project Manager</h3></th>
                  		<th><h3 class="header-5">Job Bid Amount</h3></th>
                  	</tr>
	            </thead>
	            <tbody>
	        		{{-- Irritate through each line item by category by month --}}
					@foreach ($jobs as $job)
						@if ($job->getBid())
							<tr>
								<td>{{$job->job_number}}</td>
								<td>{{$job->name}}</td>
								<td>{{ucwords($job->status)}}</td>
								<td>
									@if ($job->site_address_1)
										{{ $job->site_address_1; }} {{ $job->site_address_2; }} <br>{{ $job->site_city; }}, {{ $job->site_state; }} {{ $job->site_zip; }}
									@else
										No Site Address Assigned
									@endif
								</td>
								<td>
									@if ( $job->getProjectManager() )
										{{$job->getProjectManager()->getFullName()}}
									@else
										-
									@endif
								</td>
								<td>{{ str_money($job->getBid()->getAttribute('subtotal')) }}</td>
								<?php
								$JobBidItem = substr($job->getBid()->getAttribute('subtotal'), -1, 0);
								$JobBidAmount[] = $JobBidItem;

								?>
							</tr>
						@endif	
					@endforeach
				</tbody>
			</table> 
		@endif
	</div>

	<div class="ui divider"></div>

	@if (count($JobBidAmount) != 0)
	<table class="ui compact small very basic table">
		<thead>
		<tr>
			<th class="two  wide" style="border: none;">&nbsp;</th>
			<th class="one  wide" style="border: none;">&nbsp;</th>
			<th class="five wide" style="border: none;">&nbsp;</th>
			<th class="five wide" style="border: none;">&nbsp;</th>
			<th class="four wide right aligned" style="border: none;">&nbsp;</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td class="right aligned" colspan="4" style="border: none;"><h4><b>Report Total: </b></h4></td>
			<td class="right aligned" colspan="1"><h4><b> ${{ number_format(array_sum($JobBidAmount), 2) }} </b></h4></td>
		</tr>
		<tr>
			<td class="right aligned" colspan="4" style="border: none;"><h4><b>Lowest Job: </b></h4></td>
			<td class="right aligned" colspan="1"><h4><b> ${{ min($JobBidAmount) }}</b></h4></td>
		</tr>
		<tr>
			<td class="right aligned" colspan="4" style="border: none;"><h4><b>Highest Job: </b></h4></td>
			<td class="right aligned" colspan="1"><h4><b>${{ max($JobBidAmount) }}</b></h4></td>
		</tr>
		<tr>
			<td class="right aligned" colspan="4" style="border: none;"><h4><b>Report Average:</b></h4></td>
			<td class="right aligned" colspan="1"><h4><b>${{ number_format(array_sum($JobBidAmount)/count($JobBidAmount), 2) }}</b></h4></td>
		</tr>
		</tbody>
	</table>
	@endif
@stop	