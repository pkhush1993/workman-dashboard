{{--
=========================================================================================================
BID HISTORY CHART TEMPALTE
--------------------------------------------------------------------------------------------------------------
Using highcharts.js to generate Charts
-----------------
Passing One of the Following Report Types in the URL to manipulate template

summary
detailed
-----------------
========================================================================================================= --}}

@extends('layouts.default')

@section('content')
	<div class="ui green large labeled icon button print-button">
		<i class="print icon"></i>
		SAVE/PRINT
	</div>

	<div id="revenue-by-month-chart" style="height:700px;width:1024px;margin:0 auto">
		{{-- Use HighCharts.js to Generate Chart Here --}}
	</div>


	<script>
		jQuery.noConflict();
		(function( $ ) {
			$(function() {

				// Build Out Data
				var chartData = [];
				var bids = <?php echo $bid_data;?>;

				// Iterate through each Bid and Build Out Chart Data
				bids.forEach(function(entry) {
				    chartData.push([Date.parse(entry[0]),entry[1]]);
				});

			    $('#revenue-by-month-chart').highcharts({
			        chart: {
			            type: 'column'
			        },
			        title: {
			            text: 'Performed Jobs by Month'
			        },
			        // subtitle: {
			        //     text: 'Lets Describe Each Chart Here',
			        //     x: -20
			        // },
					xAxis: {
					    type: 'datetime',
			            plotLines: [{
			                value: 0,
			                width: 20,
			                color: '#dcdcdc'
			            }]
					    // tickInterval: 24 * 3600 * 1000
					},
			        yAxis: {
			            title: {
			                text: 'Bid Totals in Dollars'
			            },
			            plotLines: [{
			                value: 0,
			                width: 2,
			                color: '#808080'
			            }],
			            valuePrefix: '$'
			        },
			        tooltip: {
			            valuePrefix: '$'
			        },
			        legend: {
			            layout: 'vertical',
			            align: 'right',
			            verticalAlign: 'middle',
			            borderWidth: 0
			        },
			        series: [{
			            name: 'Bid Total',
						data: chartData
			        }]
			    });


			});
		})(jQuery);
	</script>

@stop
