<div class="ui four column aligned grid">

	{{-- Job/Invoice Info --}}
	<div class="left aligned column">
		@if ($invoice_id)
			<p>
				<b>Invoice Number:</b>
				<br>
				#{{ $bid->invoice_number }}
			</p>
			<p>
				<b>Date Created:</b>
				<br>
				{{ date('m/d/Y') }}
			</p>
		@else
			<p>
				<b>Bid Date:</b>
				<br>
				{{ date('m/d/Y',strtotime($job->bid_date)) }}
			</p>
		@endif
		<p>
			<b>Job:</b>
			<br>
			#{{ $job->job_number }} - {{ $job->name }}
		</p>
	</div>

	{{-- Primary Contact Info --}}
	<div class="left aligned column">
		@if(isset($primary_contact->first_name))
			<p>
				<b>Contact:</b>
				<br>
				{{ $primary_contact->first_name.' '.$primary_contact->last_name }}
			</p>
		@endif
		@if(isset($primary_contact->phone))
			<p>
				<b>Phone:</b>
				<br>
				{{ $primary_contact->phone }}
			</p>
		@endif
		@if(isset($primary_contact->email))
			<p>
				<b>Email:</b>
				<br>
				<a href="mailto:{{ $primary_contact->email }}">{{ $primary_contact->email }}</a>
			</p>
		@endif
		@if(isset($primary_contact->fax))
			<p>
				<b>Fax:</b>
				<br>
				{{ $primary_contact->fax }}
			</p>
		@endif
	</div>
	{{-- Job Site Address --}}
	<div class="left aligned column">
		@if($job->site_address_1)
		<p>
			<b>Site Address:</b>
			<br>
			<p>
				{{$job_account->getName()}}
				<br>
				{{ $job->site_address_1; }}
				<br>
				{{ $job->site_address_2; }}
				<br>
				{{ $job->site_city; }}, {{ $job->site_state; }} {{ $job->site_zip; }}
			</p>
		@else
			<b>Account:</b>
			<br>
			<p>
				{{$job_account->getName()}}
			</p>
		@endif
	</div>
	{{-- Job Billing Address --}}
	<div class="left aligned column">
		@if($job->billing_address_1)
		<p>
			<b>Billing Address:</b>
			<br>
			<p>
				{{$job_account->getName()}}
				<br>
				{{ $job->billing_address_1; }}
				<br>
				{{ $job->billing_address_2; }}
				<br>
				{{ $job->billing_city; }}, {{ $job->billing_state; }} {{ $job->billing_zip; }}
			</p>
		@endif
	</div>
</div>
