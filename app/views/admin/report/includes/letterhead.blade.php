<div class="ui aligned grid">
	<div class="left floated left aligned four wide column">
		<div class="ui small image">
			<img class="invoice-logo" src="{{ $company->avatar }}" alt="">
		</div>
	</div>
	<div class="right floated right aligned four wide column company-info">
		<br>
		@if($company->phone)<p>Phone: <b>{{ $company->phone; }}</b></p>@endif
		@if($company->fax)<p>Fax: <b>{{ $company->fax; }}</b></p>@endif
		@if($company->website)<p><b><a target="_blank" href="//{{ $company->website }}">{{ $company->website }}</a></b></p>@endif
		@if($company->address_1)<p>{{ $company->address_1; }} {{ $company->address_2; }}</p>@endif
		@if($company->address_1)<p>{{ $company->city; }}, {{ $company->state; }} {{ $company->zip; }}</p>@endif
	</div>
</div>
<div class="ui divider"></div>
