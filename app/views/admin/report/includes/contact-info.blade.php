{{--============ Contact Information ============--}}
<div class="ui three column grid">
	{{-- Company Contact--}}
	<div class="column">
		<h3 class="ui header" style="margin-bottom: 0;">
			@if ( $company->getAvatar() )
				<img class="ui circular image" src="{{ $company->getAvatar() }}">
			@endif	
			<div class="content">
				{{ $company->name }}
				<div class="sub header">Company Contact Info</div>
			</div>
		</h3>
		<div class="ui small list" style="margin-bottom:5px 0;">
			<div class="item">
				<a href="tel:{{ $company->phone }}"><i class="ui call icon"></i>{{ $company->phone }}</a>
			</div>
			<div class="item">
				<a href="mailto:{{ $company->email }}"><i class="ui mail icon"></i>{{ $company->email }}</a>
			</div>
		</div>
	</div>
	{{-- Estimator --}}
	@if ($job->getEstimator())
		<div class="column">
			<h3 class="ui header" style="margin-bottom: 0;">

				@if ( $job->getEstimator()->avatar )
					<img class="ui circular image" src="{{ $job->getEstimator()->avatar }}">
				@endif	
				<div class="content">
					Estimator
					<div class="sub header">{{ $job->getEstimator()->first_name.' '.$job->getEstimator()->last_name }}</div>
				</div>
			</h3>
			<div class="ui small list" style="margin-bottom:5px 0;">
				<div class="item">
					<a href="tel:{{ $job->getEstimator()->phone }}"><i class="ui call icon"></i>{{ $job->getEstimator()->phone }}</a>
				</div>
				<div class="item">
					<a href="mailto:{{ $job->getEstimator()->email }}"><i class="ui mail icon"></i>{{ $job->getEstimator()->email }}</a>
				</div>
			</div>
		</div>
	@endif
	{{-- Project Manager --}}
	@if ($job->getProjectManager())
		<div class="column">
			<h3 class="ui header" style="margin-bottom: 0;">
				@if ( $job->getProjectManager()->avatar )
					<img class="ui circular image" src="{{ $job->getProjectManager()->avatar }}">
				@endif	
				<div class="content">
					Project Manager
					<div class="sub header">{{ $job->getProjectManager()->first_name.' '.$job->getProjectManager()->last_name }}</div>
				</div>
			</h3>
			<div class="ui small list" style="margin-bottom:5px 0;">
				<div class="item">
					<a href="tel:{{ $job->getProjectManager()->phone }}"><i class="ui call icon"></i>{{ $job->getProjectManager()->phone }}</a>
				</div>
				<div class="item">
					<a href="mailto:{{ $job->getProjectManager()->email }}"><i class="ui mail icon"></i>{{ $job->getProjectManager()->email }}</a>
				</div>
			</div>
			<br>
		</div>
	@endif
</div>