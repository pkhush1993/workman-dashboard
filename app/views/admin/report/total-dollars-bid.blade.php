{{-- 
=========================================================================================================
BID HISTORY CHART TEMPALTE 
--------------------------------------------------------------------------------------------------------------
Using highcharts.js to generate Charts
-----------------
Passing One of the Following Report Types in the URL to manipulate template

summary
detailed
-----------------
========================================================================================================= --}}

@extends('layouts.default')

@section('content')
	<div class="ui green large labeled icon button print-button">
		<i class="print icon"></i>
		SAVE/PRINT
	</div>

	<div id="bid-history-chart" style="height:700px;">
		{{-- Use HighCharts.js to Generate Chart Here --}}
	</div>


	<script>
		jQuery.noConflict();
		(function( $ ) {
			$(function() {			


				// Build Out Data
				var chartData = [];
				var bids = <?php echo $bid_data;?>;
				
				// Iterate through each Bid and Build Out Chart Data
				bids.forEach(function(entry) {
				    chartData.push([Date.parse(entry[0]),entry[1]]);
				});


			    $('#bid-history-chart').highcharts({
			        chart: {
			            type: 'column'
			        },			    	
			        title: {
			            text: 'Total Dollars Bid by Month'
			        },
			        // subtitle: {
			        //     text: 'Lets Describe Each Chart Here',
			        //     x: -20
			        // },
					xAxis: {
					    type: 'datetime',
						units: [
							['month']			            					    
						]				    
					    // tickInterval: 24 * 3600 * 1000
					},
			        yAxis: {
			            title: {
			                text: 'Bid Totals in Dollars'
			            },
			            plotLines: [{
			                value: 0,
			                width: 2,
			                color: '#808080'
			            }],
			            labels: {
			                formatter: function () {
			                    return '$'+this.value;
			                }
			            }			            
			        },
			        tooltip: {
			            valuePrefix: '$'
			        },
			        legend: {
			            layout: 'vertical',
			            align: 'right',
			            verticalAlign: 'middle',
			            borderWidth: 0
			        },

			        series: [{
			            name: 'Bid Total',
						data: chartData
			        }]
			    });


			});
		})(jQuery);	
	</script>

@stop