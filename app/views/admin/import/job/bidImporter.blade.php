@extends('layouts.default')

@section('content')
    <div class="ui page grid">
        <div class="column">
            <div id="contact">
                <h1>
                    <i class="file text icon"></i>Import Bid Line Items - <span style="color:#888">{{ $job->getName() }}</span>
                    <a href="#" id="submit" class="ui inverted green submit right floated icon button ImportBtn">Import</a>
                </h1>
                <div class="ui green message ImportMsg" style="display: none">
                    <h2 class="header">Please wait, Import in progress:</h2>
                    <p>Please wait while we importing your data entries.</p>
                </div>
                <div class="ui yellow message">
                    <div class="header">Please Note:</div>
                    <p>Special characters, such as dollar signs($) and commas(,) can cause import issues. If imported data seems incorrect, please try removing these characters from your import and trying again.</p>
                </div>
                <div class="ui blue segment">
                    <div id="import_table"></div>
                    <div class="ui dimmer">
                        <div class="ui loader"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        jQuery.noConflict();
        (function( $ ) {
            $(function() {
                $(".ImportBtn").click(function(){
                    $(".ImportMsg").show();
                });
                var handsontable = new Handsontable(document.getElementById('import_table'), {
                    height: 396,
                    colHeaders: ["Category", "Product/Description", "QTY", "Unit", "Rate"],
                    rowHeaders: true,
                    stretchH: 'all',
                    columnSorting: true,
                    contextMenu: true,
                    columns: [
                        {data: 0, type: 'text'},
                        {data: 1, type: 'text'},
                        {data: 2, type: 'text'},
                        {data: 3, type: 'text'},
                        {data: 4, type: 'text'},
                    ]
                });

                var dimmer = $('.ui.dimmer');

                $('#submit').on('click', function() {
                    dimmer.addClass('active');
                    $.ajax({
                        data: {"data": handsontable.getData()},
                        type: 'POST',
                        success: function (data) {
                            window.location.href = data.url + "?fromImport=true"
                        },
                        error: function () {
                           alert('Something Went Wrong')
                        },
                        complete: function() {
                            dimmer.removeClass('active')
                        },
                    });
                })
            });
        })(jQuery);
    </script>

@stop