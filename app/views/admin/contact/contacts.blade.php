@extends('layouts.default')

@section('content')

  	<h1><i class="child icon"></i>Contacts</h1>
  	@include('admin.contact.forms.contacts')

@stop