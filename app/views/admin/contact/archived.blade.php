@extends('layouts.default')

@section('content')

	<div id="archived_contacts">
		<h1><i class="child icon"></i>Archived Contacts</h1>
  		@include('admin.contact.forms.contacts')
	</div>

@stop