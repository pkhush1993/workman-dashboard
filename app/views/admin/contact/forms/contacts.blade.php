<div id="contacts">
    @if ($archive)
        {{ Form::open(array('class' => 'ui form','route' => 'unarchive.contact')) }}
    @else
        {{ Form::open(array('class' => 'ui form','route' => 'remove.contacts')) }}
    @endif    
        <table class="ui small padded celled table">
          <thead>
                <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Account</th>
                </tr>
          </thead>
            <tbody>
            </tbody>
        </table>
        {{-- Check if Request is Archived --}}
        @if ($archive)
            <a href="{{ route('company.contacts') }}" class="ui labeled icon tiny right floated button">
                <i class="reply icon"></i>
                VIEW CONTACTS
            </a>
            {{ Form::submit('ACTIVATE ACCOUNT',array('class' => 'ui submit green button')); }}
        @else
            <a href="{{ route('add.contact') }}" class="ui labeled icon green add-contact button">
                <i class="plus icon"></i>
                ADD CONTACT
            </a>
            {{ Form::submit('DEACTIVATE CONTACTS',array('class' => 'ui submit red button','style' => 'display:none')); }}
            <a href="{{ route('archived.contacts') }}" class="ui labeled icon tiny right floated button">
                <i class="archive icon"></i>
                VIEW DEACTIVATED CONTACTS
            </a>
        @endif        

    {{ Form::close() }}         
</div>

<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {

            $('.selected_contact').click(function(event) {
                $('input[type=submit]').show();
            });


            $(document).ready(function(){
                $('#contacts table').dataTable( {
                    "info": false,
                    "lengthChange": false,
                    "dom": '<"table-search"<f> > t <"F"ip>',
                    "aoColumnDefs": [
                        { 'bSortable': false, 'aTargets': [ 0 ] }
                    ],
                    "order": [[ 1, "asc" ]],
                    "iDisplayLength": 50,
                    "processing": true,
                    "serverSide": true,
                    "ajax": "{{route('view.contacts.ajax', ['account_id' => isset($account_id) ? $account_id : 'null'] )}}",
                    "columns": [
                        {data: 'id', name: 'contacts.id'},
                        {data: 'name', name: 'name'},
                        {data: 'phone', name: 'contacts.phone'},
                        {data: 'email', name: 'contacts.email'},
                        {data: 'address', name: 'contacts.address_1'},
                        {data: 'account_name', name: 'accounts.name'}
                    ]
                } );
            });            

        });
    })(jQuery);
</script>