<div id="contact">
    <h1><i class="child icon"></i>Contact - <small>{{ $contact->getFirstName().' '.$contact->getLastName() }}</small><a href="{{ route('company.contacts') }}" class="ui right floated tiny basic button "><i class="reply icon"></i>BACK</a></h1>
    <div class="ui blue segment">
        @if ( $contact->trashed() )
            <div class="ui error message">
                <div class="header">Pleas Note:</div>
                <p>
                    This contact has been deactivated. The information below cannot be edited.
                </p>
            </div>
        @endif
        {{ Form::open(array('class' => 'ui form contact_form','route' => 'update.contact')) }}
            {{ Form::hidden('contact_id',$contact_id) }}
            <div class="required field">
                {{ Form::label('name', 'Name') }}
                <div class="two fields">
                    <div class="field">
                        {{ Form::text('first_name',$contact->getFirstName(), array('placeholder'=>'First Name','readonly'=>'readonly')) }}
                    </div>
                    <div class="field">
                        {{ Form::text('last_name',$contact->getLastName(), array('placeholder'=>'Last Name','readonly'=>'readonly')) }}
                    </div>
                </div>
            </div>
            <div class="two fields">
                <div class="field">
                    {{ Form::label('title', 'Title') }}
                    {{ Form::text('title',$contact->getTitle(), array('placeholder'=>'Enter Title','readonly'=>'readonly')) }}
                </div>
                <div class="field">
                    {{ Form::label('account', 'Account') }}
                    {{ Form::select('account_id', $accounts_dropdowns, $contact->getAccountId() , array('class'=>'ui large fluid search icon disabled dropdown','autocomplete' =>'off')) }}
                </div>
            </div>
            <div class="two fields">
                <div class="required field">
                    {{ Form::label('email', 'Email') }}
                    {{ Form::email('email',$contact->getEmail(), array('placeholder'=>'Enter Email','readonly'=>'readonly')) }}
                </div>
                <div class="required field">
                    {{ Form::label('phone', 'Phone') }}
                    {{ Form::text('phone',$contact->getPhone(), array('placeholder'=>'Enter Phone','readonly'=>'readonly','class'=>'phone')) }}
                </div>
            </div>
           <div class="two fields">
                <div class="required field">
                    {{ Form::label('mobile', 'Mobile Number') }}
                    {{ Form::text('mobile',$contact->getMobile(), array('placeholder'=>'Enter Mobile Number','readonly'=>'readonly','class'=>'phone')) }}
                </div>
                <div class="required field">
                    {{ Form::label('fax', 'Fax Number') }}
                    {{ Form::text('fax',$contact->getFax(), array('placeholder'=>'Enter Fax Number','readonly'=>'readonly','class'=>'phone')) }}
                </div>
            </div>
            {{ Form::label('address', 'Address') }}
            <div class="two fields">
                <div class="field">
                    {{ Form::text('address_1',$contact->getAddress1(), array('placeholder'=>'Address','readonly'=>'readonly')) }}
                </div>
                <div class="field">
                    {{ Form::text('address_2',$contact->getAddress2(), array('placeholder'=>'Address 2','readonly'=>'readonly')) }}
                </div>
            </div>
            <div class="fields">
                <div class="seven wide field">
                    {{ Form::label('city', 'City') }}
                    {{ Form::text('city',$contact->getCity(), array('placeholder'=>'City','readonly'=>'readonly')) }}
                </div>
                <div class="five wide field">
                    {{ Form::label('state', 'State') }}
                    {{ Form::select('state', array(
                        '' => 'Select State',
                        'AL' => 'Alabama',
                        'AK' => 'Alaska',
                        'AZ' => 'Arizona',
                        'AR' => 'Arkansas',
                        'CA' => 'California',
                        'CO' => 'Colorado',
                        'CT' => 'Connecticut',
                        'DE' => 'Delaware',
                        'DC' => 'District Of Columbia',
                        'FL' => 'Florida',
                        'GA' => 'Georgia',
                        'HI' => 'Hawaii',
                        'ID' => 'Idaho',
                        'IL' => 'Illinois',
                        'IN' => 'Indiana',
                        'IA' => 'Iowa',
                        'KS' => 'Kansas',
                        'KY' => 'Kentucky',
                        'LA' => 'Louisiana',
                        'ME' => 'Maine',
                        'MD' => 'Maryland',
                        'MA' => 'Massachusetts',
                        'MI' => 'Michigan',
                        'MN' => 'Minnesota',
                        'MS' => 'Mississippi',
                        'MO' => 'Missouri',
                        'MT' => 'Montana',
                        'NE' => 'Nebraska',
                        'NV' => 'Nevada',
                        'NH' => 'New Hampshire',
                        'NJ' => 'New Jersey',
                        'NM' => 'New Mexico',
                        'NY' => 'New York',
                        'NC' => 'North Carolina',
                        'ND' => 'North Dakota',
                        'OH' => 'Ohio',
                        'OK' => 'Oklahoma',
                        'OR' => 'Oregon',
                        'PA' => 'Pennsylvania',
                        'RI' => 'Rhode Island',
                        'SC' => 'South Carolina',
                        'SD' => 'South Dakota',
                        'TN' => 'Tennessee',
                        'TX' => 'Texas',
                        'UT' => 'Utah',
                        'VT' => 'Vermont',
                        'VA' => 'Virginia',
                        'WA' => 'Washington',
                        'WV' => 'West Virginia',
                        'WI' => 'Wisconsin',
                        'WY' => 'Wyoming',
                    ), $contact->getState() , array('class'=>'ui disabled dropdown','autocomplete' =>'off')) }}
                </div>
                <div class="four wide field">
                    {{ Form::label('zip', 'Zip') }}
                    {{ Form::text('zip',$contact->getZip(), array('placeholder'=>'Zip Code','readonly'=>'readonly')) }}
                </div>

            </div>
             <div class="ui form">
                <div class="field">
                    {{ Form::label('comment', 'Notes') }}
                    {{ Form::textarea('notes',$contact->getNotes(), array('placeholder'=>'Notes')) }}
                </div>
            </div>
            <br>
            <br>
        @if (!$contact->trashed())
            <div class="ui action buttons" style="display:none">
                <button class="ui green update-contact button">UPDATE</button>
                <div class="or"></div>
                <div class="ui red cancel-edit button">CANCEL</div>
            </div>
            <div class="ui error message"></div>
        {{ Form::close() }}
        <div class="ui labeled icon blue edit button">
          <i class="edit icon"></i>
          EDIT
        </div>
        <a href="{{ route('remove.contact', $contact_id) }}" class="ui red remove button">DEACTIVATE</a>
        @endif
    </div>
</div>

<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {

            $('.edit').click(function(event) {
                event.preventDefault();

                // Remove All Inputs Read Only Attribute
                var inputs = find('input');
                $('input').each(function() {
                    $(this).removeAttr('readonly');
                });
                $('.ui.dropdown').removeClass('disabled');


                //Fake Loading
                var container = $('body').find('.ui.blue.segment');
                container.addClass('loading');
                setTimeout(function(){ container.removeClass().addClass('ui green segment'); }, 400);

                //Buttons
                $('.edit').hide();
                $('.remove').hide();
                $('.action.buttons').show();
                $('input[name=first_name]').focus();

            });
            $('.cancel-edit').click(function(event) {
                event.preventDefault();

                // Add All Inputs Read Only Attribute
                var inputs = find('input');
                $('input').each(function() {
                    $(this).attr('readonly', 'readonly');
                });
                $('.ui.dropdown').addClass('disabled');

                //Fake Loading
                var container = $('body').find('.ui.green.segment');
                container.addClass('loading');
                setTimeout(function(){ container.removeClass().addClass('ui blue segment'); }, 400);

                //Buttons
                $('.action.buttons').hide();
                $('.edit').show();
                $('.remove').show();
            });

            @if ( $contact->trashed() )
                $('.contact_form :input').prop('disabled', true);
            @endif

        });
    })(jQuery);
</script>
