<div id="inactive-contacts-form" style="display:none">
    <h3 style="margin-top:0">Inactive Contacts</h3>
    @if ($inactive_job_contacts->isEmpty() )
        <div class="ui large red icon message">
            <i class="warning sign icon"></i>
            <div class="header">
                There Are Currently No Inactive Contacts
            </div>
            <p>If you'd like to remove contacts from the job click the far right column in the table and click "REMOVE CONTACT FROM JOB"</p>
        </div>
    @else
        {{ Form::open(array('class' => 'ui form','route' => 'job.restore.contacts')) }}
            <table class="ui compact celled table">
              <thead>
                    <tr>
                        <th class="center aligned"><i class="ui large icon plus"></i></th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>Address</th>
                    </tr>
              </thead>
                <tbody>
                    @foreach ($inactive_job_contacts as $inactive_job_contact)
                        <tr>
                            <td class="center aligned">
                                <div class="ui checkbox restore_job_contact">{{ Form::checkbox('restore_job_contact[]',$inactive_job_contact->pivot->id, false) }}</div>
                            </td>
                            <td><a href="" class="view-job-contact" data-contact-id="{{$inactive_job_contact->id}}">{{ $inactive_job_contact->first_name .' '. $inactive_job_contact->last_name}}</a></b></td>
                            <td><a href="tel:{{ $inactive_job_contact->phone }}">{{ $inactive_job_contact->phone }}</a></td>
                            <td><a href="mailto:{{ $inactive_job_contact->email }}">{{ $inactive_job_contact->email }}</a></td>
                            <td>{{ $inactive_job_contact->address_1.' '.$inactive_job_contact->address_2.' '.$inactive_job_contact->city.' '.$inactive_job_contact->state }}{{($inactive_job_contact->zip) ? ', '.$inactive_job_contact->zip : ''}}</td>
                        </tr>
                    @endforeach                                           
                </tbody>
            </table>
            {{ Form::submit('REACTIVATE CONTACTS',array('class' => 'ui submit reactivate-contact tiny button','style' => 'display:none;')); }}
        {{ Form::close() }}    
    @endif   
</div>

<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {

            $('.restore_job_contact').click(function(event) {
                $('input.reactivate-contact').show();
            });

        });
    })(jQuery);
</script>