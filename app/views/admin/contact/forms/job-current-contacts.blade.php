<div id="current-contacts">
    <h3 style="margin-top:0">Active Contacts <a class="ui right floated red inverted tiny button view-inactive-job-contacts" href="">View Inactive Contacts</a></h3>
    @if ($job_contacts->isEmpty() )
        <div class="ui large red icon message">
            <i class="warning sign icon"></i>
            <div class="header">
                There Are Currently No Contacts on this Job
            </div>
            <p>Click Add Contacts Button to Attach Contacts to the Job</p>
        </div>
    @else
        {{ Form::open(array('class' => 'ui form','route' => 'job.remove.contacts')) }}
            <table class="ui padded celled table">
              <thead>
                    <tr>
                        <th class="center aligned"><i class="ui large icon trash"></i></th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>Address</th>
                    </tr>
              </thead>
                <tbody>
                    @foreach ($job_contacts as $job_contact)
                        <tr>
                            <td class="center aligned">
                                <div class="ui checkbox remove_job_contact">{{ Form::checkbox('remove_job_contact[]',$job_contact->pivot->id, false) }}</div>
                            </td>
                            <td><a href="" class="view-job-contact" data-contact-id="{{$job_contact->id}}">{{ $job_contact->first_name .' '. $job_contact->last_name}}</a></b></td>
                            <td><a href="tel:{{ $job_contact->phone }}">{{ $job_contact->phone }}</a></td>
                            <td><a href="mailto:{{ $job_contact->email }}">{{ $job_contact->email }}</a></td>
                            <td>{{ $job_contact->address_1.' '.$job_contact->address_2.' '.$job_contact->city.' '.$job_contact->state }}{{($job_contact->zip) ? ', '.$job_contact->zip : ''}}</td>
                        </tr>
                    @endforeach                                           
                </tbody>
            </table>
            {{ Form::submit('REMOVE CONTACT FROM JOB',array('class' => 'ui submit deactivate-contact tiny red button','style' => 'display:none')); }}
        {{ Form::close() }}    
    @endif   
</div>

<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {

            $('.remove_job_contact').click(function(event) {
                $('input.deactivate-contact').show();
            });

        });
    })(jQuery);
</script>