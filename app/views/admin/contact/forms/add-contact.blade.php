<div id="add-contact">
    {{ Form::open(array('class' => 'ui form add-contact-form','route' => 'create.contact')) }}
        {{ Form::hidden('company_id',1) }}
        <div class="required field">
            {{ Form::label('name', 'Name') }}
            <div class="two fields">
                <div class="field">
                    {{ Form::text('first_name',$value = null, array('placeholder'=>'First Name')) }}
                </div>
                <div class="field">
                    {{ Form::text('last_name',$value = null, array('placeholder'=>'Last Name')) }}
                </div>
            </div>
        </div>
        <div class="two fields">
            <div class="field">
                {{ Form::label('title', 'Title') }}
                {{ Form::text('title',$value = null, array('placeholder'=>'Enter Title')) }}
            </div>
            <div class="required field">
                {{ Form::label('account', 'Account') }}
                <div class="ui search account-ajax-search">
                    {{ Form::text('', isset($job) ?$job->account()->name : '', array('placeholder'=>'Search for Account...','class'=>'prompt ui search')) }}
                    {{ Form::hidden('assigned_job_account',isset($job) ? $job->account : '', array('class'=>'contact-account account-id')) }}
                    <div class="results"></div>
                </div>
                {{-- {{ Form::select('account_id', $accounts_dropdowns, $value = null , array('class'=>'ui search contact-account dropdown','autocomplete' =>'off')) }} --}}
            </div>
        </div>
        <div class="two fields">
            <div class="required field">
                {{ Form::label('email', 'Email') }}
                {{ Form::email('email',$value = null, array('placeholder'=>'Enter Email')) }}
            </div>
            <div class="field">
                {{ Form::label('phone', 'Phone') }}
                {{ Form::text('phone',$value = null, array('placeholder'=>'Enter Phone','class'=>'phone')) }}
            </div>
        </div>
       <div class="two fields">
            <div class="field">
                {{ Form::label('mobile', 'Mobile Number') }}
                {{ Form::text('mobile',$value = null, array('placeholder'=>'Enter Mobile Number','class'=>'phone')) }}
            </div>
            <div class="field">
                {{ Form::label('fax', 'Fax Number') }}
                {{ Form::text('fax',$value = null, array('placeholder'=>'Enter Fax Number','class'=>'phone')) }}
            </div>
        </div>
        {{ Form::label('address', 'Address') }}
        <div class="two fields">
            <div class="field">
                {{ Form::text('address_1',$value = null, array('placeholder'=>'Address')) }}
            </div>
            <div class="field">
                {{ Form::text('address_2',$value = null, array('placeholder'=>'Address 2')) }}
            </div>
        </div>
        <div class="fields">
            <div class="seven wide field">
                {{ Form::label('city', 'City') }}
                {{ Form::text('city',$value = null, array('placeholder'=>'City')) }}
            </div>
            <div class="five wide field">
                {{ Form::label('state', 'State') }}
                {{ Form::select('state', array(
                    '' => 'Select State',
                    'AL' => 'Alabama',
                    'AK' => 'Alaska',
                    'AZ' => 'Arizona',
                    'AR' => 'Arkansas',
                    'CA' => 'California',
                    'CO' => 'Colorado',
                    'CT' => 'Connecticut',
                    'DE' => 'Delaware',
                    'DC' => 'District Of Columbia',
                    'FL' => 'Florida',
                    'GA' => 'Georgia',
                    'HI' => 'Hawaii',
                    'ID' => 'Idaho',
                    'IL' => 'Illinois',
                    'IN' => 'Indiana',
                    'IA' => 'Iowa',
                    'KS' => 'Kansas',
                    'KY' => 'Kentucky',
                    'LA' => 'Louisiana',
                    'ME' => 'Maine',
                    'MD' => 'Maryland',
                    'MA' => 'Massachusetts',
                    'MI' => 'Michigan',
                    'MN' => 'Minnesota',
                    'MS' => 'Mississippi',
                    'MO' => 'Missouri',
                    'MT' => 'Montana',
                    'NE' => 'Nebraska',
                    'NV' => 'Nevada',
                    'NH' => 'New Hampshire',
                    'NJ' => 'New Jersey',
                    'NM' => 'New Mexico',
                    'NY' => 'New York',
                    'NC' => 'North Carolina',
                    'ND' => 'North Dakota',
                    'OH' => 'Ohio',
                    'OK' => 'Oklahoma',
                    'OR' => 'Oregon',
                    'PA' => 'Pennsylvania',
                    'RI' => 'Rhode Island',
                    'SC' => 'South Carolina',
                    'SD' => 'South Dakota',
                    'TN' => 'Tennessee',
                    'TX' => 'Texas',
                    'UT' => 'Utah',
                    'VT' => 'Vermont',
                    'VA' => 'Virginia',
                    'WA' => 'Washington',
                    'WV' => 'West Virginia',
                    'WI' => 'Wisconsin',
                    'WY' => 'Wyoming',
                ), $value = null , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
            </div>
            <div class="four wide field">
                {{ Form::label('zip', 'Zip') }}
                {{ Form::text('zip',$value = null, array('placeholder'=>'Zip Code')) }}
            </div>
        </div>
        <div class="ui form">
            <div class="field">
                {{ Form::label('notes', 'Notes') }}
                {{ Form::textarea('notes', '', array('placeholder'=>'Enter Note ')) }}
            </div>
        </div>
        <br>
        <div class="ui action buttons">
            {{ Form::submit('CREATE CONTACT',array('class' => 'ui submit green button')); }}
        </div>
    {{ Form::close() }}
</div>

<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {

        //Semantic UI Global Form Validation =========================
        $('.ui.form.add-contact-form').form(
            {
                firstName: {
                 identifier  : 'first_name',
                 rules: [{
                         type   : 'empty',
                         prompt : 'Please enter the First Name'
                 }]
                },
                lastName: {
                 identifier  : 'last_name',
                 rules: [{
                         type   : 'empty',
                         prompt : 'Please enter the Last Name'
                 }]
                },
                account: {
                 identifier  : 'account_id',
                 rules: [{
                         type   : 'empty',
                         prompt : 'Each Contact must be Attached to an Account'
                 }]
                },
                email: {
                    identifier  : 'email',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter a email for the contact'
                        },
                        {
                            type   : 'email',
                            prompt : 'You must enter a valid email address'
                        }
                    ]
                }
            },
            {
                inline : true,
                on     : 'blur',
                onSuccess: function () {
                   $('.ui.modal.add-accokingnt').modal('hide');
                }
            }
        );

            $('.ui.account-ajax-search')
                    .search({
                        apiSettings: {
                            url: '{{route("accounts.input.ajax")}}/get/?key=' + '{query}'
                        },
                        onSelect(result) {
                            // returning count ID
                            setTimeout(function(){
                                var DataKey = $(document).find('input.job-account-search').val();
                                $.get( "/accounts-details-ajax/post-key/", { key: DataKey},  function(data, status) {
                                    var InputId = $(document).find('input[name=assigned_job_account]');
                                    InputId.val(data.account_id);
//
                                });

                            }, 300);

                        }
                    });
            
        // ------------------------------------------------------------

        });
    })(jQuery);
</script>
