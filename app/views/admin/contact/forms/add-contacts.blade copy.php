{{ Form::open(array('class' => 'ui form','id' => 'add-contacts')) }}
    <table class="ui padded celled table">
      <thead>
            <tr>
                <th></th>
                <th>Name</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Address</th>
                <th>Account</th>
            </tr>
      </thead>
        <tbody>
            @foreach ($contacts as $contact)
                <tr>
                    <td class="center aligned">
                        <div class="ui checkbox">{{ Form::checkbox('add_contact',$value = $contact->id, false) }}</div>
                    </td>
                    <td>{{ $contact->first_name .' '. $contact->last_name}}</td>
                    <td>{{ $contact->phone }}</td>
                    <td>{{ $contact->email }}</td>
                    <td>{{ $contact->address_1.' '.$contact->address_2.' '.$contact->city.' '.$contact->state.', '.$contact->zip }}</td>
                    <td>{{ $contact->account_name }}</td>
                </tr>
            @endforeach                                           
        </tbody>
    </table>
    <button class="ui labeled icon green button close">
    <i class="plus icon"></i>
    ADD CONTACTS
    </button>
{{ Form::close() }}     