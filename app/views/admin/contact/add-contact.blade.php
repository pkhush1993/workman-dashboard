@extends('layouts.default')

@section('content')
    <div id="contact">
        <h1><i class="child icon"></i>Add Contact</small></h1>
        <div class="ui blue segment">
            @include('admin.contact.forms.add-new-contact-form')
        </div>
    </div>

@stop