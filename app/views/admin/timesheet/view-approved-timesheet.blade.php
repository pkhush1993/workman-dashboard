@extends('layouts.default')

@section('content')

<div id="view-timesheet">
    {{ Form::open( array('class' => 'ui form approve-timesheet-form','route' => ['approve.timesheet',$timesheet->id]) ) }}
    @if ($timesheet->approval_date)
        <h1>Approved Timesheet for {{ $timesheet->profile()->first_name.' '.$timesheet->profile()->last_name }}  <small style="font-weight: normal;font-size: 18px;">(Period {{ date('m/d/Y',strtotime($timesheet->period_start)).' - '.date('m/d/Y',strtotime($timesheet->period_end)) }})</small></h1>
    @else
        <h1 class="ui red header">Timesheet Pending Approval for {{ $timesheet->profile()->first_name.' '.$timesheet->profile()->last_name }}  <small style="font-weight: normal;font-size: 18px;">(Period {{ date('m/d/Y',strtotime($timesheet->period_start)).' - '.date('m/d/Y',strtotime($timesheet->period_end)) }})</small></h1>
    @endif

        <!-- Line Items -->
        <table class="ui compact celled striped table">
            <thead>
                  <tr>
                      <th >Time In</th>
                      <th >Time Out</th>
                      <th >Break <small>(Mins.)</small></th>
                      <th >Total Hours</th>
                      <th >Job</th>
                      <th >Task Category</th>
                  </tr>
            </thead>
            <tbody class="time-entry-repeater">
                <?php $i = 1; ?>
                @foreach ($timesheet_details as $time_entry)
                    <tr class="time-entry">
                        {{ Form::hidden('time_entry_id['.$i.']', $time_entry->id) }}
                        <td class="time-range">
                            <div class="field">
                                <b>
                                {{ date('m/d/Y',strtotime($time_entry->date_in)) }}
                                @ {{ date('g:ia',strtotime($time_entry->date_in)) }}
                                </b>
                            </div>
                        </td>
                        <td class="time-range">
                            <div class="field">
                                <b>
                                {{ date('m/d/Y',strtotime($time_entry->date_out)) }}
                                @ {{ date('g:ia',strtotime($time_entry->date_out)) }}
                                </b>
                            </div>
                        </td>
                        <td>
                            <div class="required field">
                                {{ $time_entry->break }} Minutes
                            </div>
                        </td>
                        <td>
                            <div class="field">
                                <b>{{ $time_entry->total }}</b> Hours
                            </div>
                        </td>
                        <td>
                            <div class="field">
                                #{{ $time_entry->job()->job_number }} - {{ $time_entry->job()->name }}
                            </div>
                        </td>
                        <td>
                            <div class="field">
                                {{ Form::text('task['.$time_entry->id.']',$time_entry->task, array('placeholder'=>'Enter Task')) }}
                            </div>
                        </td>
                    </tr>
                <?php $i++; ?>
                @endforeach
            </tbody>
        </table>
        {{-- <div class="ui basic segment">
            <button class="ui huge fluid green button submit-timesheet"><i class="checkmark box icon"></i>APPROVE THIS TIMESHEET</button>
        </div> --}}
    {{ Form::close() }}
</div>

<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {


        });
    })(jQuery);
</script>

@stop
