@extends('layouts.default')

@section('content')
<div id="create-timesheet">
	{{ Form::open( array('class' => 'ui form create-timesheet-form','route' => 'create.timesheet') ) }}
    <h1>
		Add Timesheet <small style="font-weight: normal;font-size: 18px;">(Period {{ date('m/d/Y',strtotime($period_start)).' - '.date('m/d/Y',strtotime($period_end)) }})</small>
		{{-- If Todays Date is Equal to or less than the Grace Period GIve Option to Edit --}}
		@if  ( $entry_grace_period AND $last_timesheet )
			<a href="{{ route('view.timesheet',$last_timesheet->id) }}" class="ui orange button right floated past-timesheet">EDIT PAST TIMESHEET</a>
		@endif
		{{ Form::submit('SAVE TIMESHEET',array('class' => 'ui submit right floated green icon button save-timesheet')); }}
	</h1>
	<div class="ui error message"></div>
		<!-- Line Items -->
        <table class="ui compact small celled striped table">
            <thead>
                  <tr>
                      <th class="four wide">Time</th>
                      <th class="one wide">Break <small>(Mins.)</small></th>
                      <th class="one wide">Total Hours</th>
                      <th class="two wide">Job</th>
                      <th class="one wide">Task Category</th>
                      <th class="two wide">Description</th>
                      <th style="width:1%" class="one wide center aligned collapsing"></th>
                  </tr>
            </thead>
			{{ Form::hidden('removed_ids', $value=NULL) }}
            <tbody class="time-entry-repeater">

					<tr class="time-entry hidden">
						{{-- {{ Form::hidden( 'user_id',Sentinel::getUser()->id,array('disabled' => 'disabled') ) }} --}}
						<td class="time-range">
							<div class="required fields">
								<div class="field">
									{{ Form::label('date-in', 'Date In') }}
									{{ Form::text('date_in',$default = NULL, array('placeholder'=>'Date In','class'=>'date-in datetime-entry','style' => 'padding:8px;width:155px','disabled' => 'disabled')) }}
								</div>
								<div class="field">
									<b style="top: 25px;position: relative;">- TO - </b>
								</div>
								<div class="field">
									{{ Form::label('date-in', 'Date Out') }}
									{{ Form::text('date_out',$default = NULL, array('placeholder'=>'Date Out','class'=>'date-out datetime-entry','style' => 'padding:8px;width:155px','disabled' => 'disabled')) }}
								</div>
							</div>
						</td>
						<td>
							<div class="required field">
								{{ Form::number('break',$default = 0, array('placeholder'=>'Break','class'=>'break','disabled' => 'disabled')) }}
							</div>
						</td>
						<td>
							<div class="field">
								{{ Form::number('total',$default = NULL, array('placeholder'=>'Total','readonly'=>'readonly','class'=>'total','disabled' => 'disabled','data-validate'=>'total')) }}
							</div>
						</td>
						<td>
							<div class="field">
								<div class="ui search job-input-ajax-repeater">
									{{ Form::text('',$default = NULL, array('placeholder'=>'Search for Job...','class'=>'prompt','disabled' => 'disabled')) }}
									{{ Form::hidden('job_id',$default = NULL, array('data-validate'=>'job','class'=>'job','disabled' => 'disabled')) }}
									<div class="results"></div>
								</div>
				                {{-- {{ Form::select('job_id', $jobs_select, $default = '' , array('class'=>'jobs','autocomplete' =>'off','disabled' => 'disabled')) }} --}}
				            </div>
						</td>
						<td>
						<div class="field">
							{{ Form::select('task', $task_select, $default = null , array('class'=>'search dropdown','autocomplete' =>'on')) }}
						</div>
						</td>
						<td>
							<div class="field">
								{{ Form::text('description',$default = NULL, array('placeholder'=>'Enter Description','disabled' => 'disabled','class' => 'description')) }}
							</div>
						</td>
						<td class="center aligned">
							<i class="large green add circle icon add-field"></i>
							<div class="ui divider" style="margin: 3px 0;"></div>
							<i class="large red minus circle icon remove-field"></i>
						</td>
					</tr>
            	{{-- Repeat 5 Times --}}
				<?php $i = 1; ?>
            	@while ($i <= 1)
					<tr class="time-entry">
						{{ Form::hidden('time_entry_number',$default = $i) }}
						{{ Form::hidden('user_id',Sentinel::getUser()->id) }}
						<td class="time-range">
							<div class="required fields">
								<div class="field">
									{{ Form::label('date-in', 'Date In') }}
									{{ Form::text('date_in['.$i.']',$default = NULL, array('placeholder'=>'Date In','class'=>'datetime-entry date-in','data-validate'=>'date-in','style' => 'padding:8px;width:155px','required'=>'required')) }}
								</div>
								<div class="field">
									<b style="top: 25px;position: relative;">- TO - </b>
								</div>
								<div class="field">
									{{ Form::label('date-in', 'Date Out') }}
									{{ Form::text('date_out['.$i.']',$default = NULL, array('placeholder'=>'Date Out','class'=>'datetime-entry date-out','data-validate'=>'date-out','style' => 'padding:8px;width:155px','required'=>'required')) }}
								</div>
							</div>
						</td>
						<td>
							<div class="required field">
								{{ Form::number('break['.$i.']',$default = 0, array('placeholder'=>'Break','class'=>'break')) }}
							</div>
						</td>
						<td>
							<div class="field">
								{{ Form::text('total['.$i.']',$default = NULL, array('placeholder'=>'Total','readonly'=>'readonly','class'=>'total','data-validate'=>'totals')) }}
							</div>
						</td>
						<td>
							<div class="field">
								<div class="ui search job-input-ajax">
									{{ Form::text('',$default = NULL, array('placeholder'=>'Search for Job...','class'=>'prompt')) }}
									{{ Form::hidden('job_id['.$i.']',$default = NULL, array('data-validate'=>'job','class'=>'job')) }}
								    <div class="results"></div>
								</div>
				                {{-- {{ Form::select('job_id['.$i.']', $jobs_select, $default = '' , array('class'=>'ui dropdown jobs','autocomplete' =>'off','data-validate'=>'job','required'=>'required')) }} --}}
				            </div>
						</td>
						<td>
							<div class="field">
								{{ Form::select('task['.$i.']', $task_select, $default = null , array('class'=>'ui search dropdown task','autocomplete' =>'on')) }}
							</div>
						</td>
						<td>
							<div class="field">
								{{ Form::text('description['.$i.']',$default = NULL, array('placeholder'=>'Enter Description','class' => 'description')) }}
							</div>
						</td>
						<td class="center aligned">
							<i class="large green add circle icon add-field"></i>
							<div class="ui divider" style="margin: 3px 0;"></div>
							<i class="large red minus circle icon remove-field"></i>
						</td>
					</tr>
				<?php $i++; ?>
				@endwhile
            </tbody>
		</table>
		<div class="ui basic segment">
			<div class="ui huge fluid teal button submit-timesheet"><i class="checkmark box icon"></i>SUBMIT TIMESHEET FOR REVIEW</div>
		</div>

	{{ Form::close() }}
</div>

<script>
    jQuery.noConflict();



	(function( $ ) {
		$(function() {
			var view = $("#create-timesheet");
			var form = $('.ui.form.create-timesheet-form', view);
			var minDate = new Date("{{ date('c',strtotime("-1 day", strtotime($period_start))) }}");
			var maxDate = new Date("{{ date('c',strtotime("+2 day", strtotime($period_end))) }}");
			{{--var minDate = new Date("{{ date('Y,m,d',strtotime("-1 day", strtotime($timesheet->period_start))) }}");--}}
			{{--var maxDate = new Date("{{ date('Y,m,d',strtotime("+2 day", strtotime($timesheet->period_end))) }}");--}}
			var timesheet = new TimesheetView(view, form, minDate, maxDate,'{{route("jobs.input.ajax")}}');

			//Submit Timesheet for Approval
			$('body').on('click', '.submit-timesheet', function(){

				event.preventDefault();

				// Store Variables
				var form = $('.create-timesheet-form');
				var formAction = form.attr('action');

				// Update the Forms Route Depending on the Button Clicked
				$(form).attr('action', '/timesheet/create/1' );

				// Submit Form
				$(form).submit();

			});

			//Submit Timesheet for Approval
			console.log($('.save-timesheet'))

			$('body').on('click', '.save-timesheet', function(){
				console.log("SUBMIT");
				return;

				event.preventDefault();

				// Store Variables
				var form = $('.create-timesheet-form');
				var formAction = form.attr('action');

				// Update the Forms Route Depending on the Button Clicked
				$(form).attr('action', '/timesheet/create' );

				// Submit Form
				$(form).submit();

			});

			$(window).bind("pageshow", function() {
				var form = $('.ui.form.create-timesheet-form', view);
				$(form)[0].reset();
			});

		});
	})(jQuery);
</script>


@stop
