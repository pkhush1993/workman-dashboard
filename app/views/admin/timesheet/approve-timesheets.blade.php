@extends('layouts.default')

@section('content')


<div id="view-timesheets">
    <h1>Timesheets</h1>

    <div id="current-timesheets">
        {{--<h3>Timesheets Ready for Approval</h3>--}}
        <table class="ui compact celled striped table view-Timesheets-table">
            <thead>
                    <tr>
                        <th style="width: 100px;"></th>
                        <th></th>
                        <th></th>
                        <th style="width: 200px;font-size: 12px;"></th>
                        <th></th>
                        <th></th>
                    </tr>
                  <tr>
                      <th style="width: 100px;">View/approve</th>
                      <th>Employee Name</th>
                      <th>Pay Period</th>
                      <th>Created On</th>
                      <th>Last Update</th>
                      <th>Created By</th>
                      {{-- <th style="width:1%" class="one wide center aligned collapsing"></th> --}}
                  </tr>
            </thead>
            <tbody>
                @foreach ($timesheets as $timesheet)
                    <tr>
                        <td>
                            <a href="{{ route('view.approve.timesheet',$timesheet->id) }}"><i class="large unhide icon view-contact"></i></a>
                        </td>
                        <td><a href="{{ route('view.approve.timesheet',$timesheet->id) }}">{{ $timesheet->profile()->first_name.' '.$timesheet->profile()->last_name }}</a></td>
                        <td>{{ date('m/d/Y',strtotime($timesheet->period_start)).' - '.date('m/d/Y',strtotime($timesheet->period_end)) }}</td>
                        <td>{{ date('m/d/Y @ g:i a',strtotime($timesheet->created_at)) }}</td>
                        <td>{{ date('m/d/Y @ g:i a',strtotime($timesheet->updated_at)) }}</td>
                        <td>{{$timesheet->profile()->first_name}} {{$timesheet->profile()->last_name}}</td>
                        {{-- <th style="width:1%" class="one wide center aligned collapsing"></th> --}}
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
{{--{{ $timesheets->links() }}--}}
    {{--<div class="ui divider"></div>--}}



</div>

<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {

            $('body').on('change', '.user-select', function(event) {
                event.preventDefault();
                var url = "{{route('dashboard.foreman','') }}/" + $(this).dropdown('get value');

                window.location.href = url;
            });


            $(document).ready(function(){
                $('.view-Timesheets-table').dataTable( {
                    "info": false,
                    "lengthChange": true,
                    "dom": '<"table-search"<f> > t <"F"ip>',
                    "aoColumnDefs": [ {
                        'bSortable': true,
                        'aTargets': [ 0],
                        "searchable": true
                        }
                    ],
                    "order": [[ 0, "desc" ]],
                    "iDisplayLength": 30,
                    "processing": true,

                    "columns": [
                        {data: '10', 'searchable': false},
                        {data: '1', 'searchable': true},
                        {data: '9', 'searchable': false},
                        {data: '7'},
                        {data: '6'},
                        {data: '5'}

                    ]

                } )
                .columnFilter({ sPlaceHolder: "head:before",
                    aoColumns: [ { type: "text" },{ type: "text" },{ type: "date-range" },{ type: "text" }, { type: "text" }, { type: "text" }  ]
                });


            });


        });
    })(jQuery);
</script>

@stop

