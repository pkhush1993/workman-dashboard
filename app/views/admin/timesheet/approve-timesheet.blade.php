@extends('layouts.default')

@section('content')

<div id="view-timesheet">
    {{ Form::open( array('class' => 'ui form approve-timesheet-form','route' => ['approve.timesheet',$timesheet->id]) ) }}
    <h1>Approve Timesheet for {{ $timesheet->profile()->first_name.' '.$timesheet->profile()->last_name }}  <small style="font-weight: normal;font-size: 18px;">(Period {{ date('m/d/Y',strtotime($timesheet->period_start)).' - '.date('m/d/Y',strtotime($timesheet->period_end)) }})</small>{{ Form::submit('SAVE TIMESHEET',array('class' => 'ui submit right floated icon button')); }}</h1>
    <div class="ui error message"></div>
    <!-- Line Items -->
        <table class="ui compact small celled striped table">
            <thead>
                <tr>
                    @if ( Sentinel::inRole('admin') OR Sentinel::inRole('executive') )
                        <th class="four wide">Time</th>
                    @else
                        <th class="two wide">Time In</th>
                        <th class="two wide">Time Out</th>
                    @endif
                    <th class="one wide">Break <small>(Mins.)</small></th>
                    <th class="one wide">Total Hours</th>
                    <th class="two wide">Job</th>
                    <th class="two wide">Task Category</th>
                </tr>
            </thead>
            <tbody class="time-entry-repeater">
                <?php $i = 1; ?>
                @foreach ($timesheet_details as $time_entry)
                    <tr class="time-entry">
                        @if ( Sentinel::inRole('admin') OR Sentinel::inRole('executive') )
                            {{ Form::hidden('time_entry_id['.$time_entry->id.']', $time_entry->id) }}
                            <td class="time-range">
                                <div class="required fields">
                                    <div class="field">
                                        {{ Form::label('date-in', 'Date In') }}
                                        {{ Form::text('date_in['.$time_entry->id.']',date('m/d/Y g:ia',strtotime($time_entry->date_in)), array('placeholder'=>'Date In','class'=>'datetime-entry date-in','data-validate'=>'date-in','style' => 'padding:8px;width:155px')) }}
                                    </div>
                                    <div class="field">
                                        <b style="top: 25px;position: relative;">- TO - </b>
                                    </div>
                                    <div class="field">
                                        {{ Form::label('date-in', 'Date Out') }}
                                        {{ Form::text('date_out['.$time_entry->id.']',date('m/d/Y g:ia',strtotime($time_entry->date_out)), array('placeholder'=>'Date Out','class'=>'datetime-entry date-out','data-validate'=>'date-out','style' => 'padding:8px;width:155px')) }}
                                    </div>
                                </div>
                            </td>
                        @else
                            <td class="time-range">
                                <div class="field">
                                    <b>
                                    {{ date('m/d/Y',strtotime($time_entry->date_in)) }}
                                    @ {{ date('g:ia',strtotime($time_entry->time_in)) }}
                                    </b>
                                </div>
                            </td>
                            <td class="time-range">
                                <div class="field">
                                    <b>
                                    {{ date('m/d/Y',strtotime($time_entry->date_out)) }}
                                    @ {{ date('g:ia',strtotime($time_entry->time_out)) }}
                                    </b>
                                </div>
                            </td>
                        @endif
                        <td>
                            @if ( Sentinel::inRole('admin') OR Sentinel::inRole('executive') )
                                <div class="required field">
                                    {{ Form::text('break['.$time_entry->id.']',$time_entry->break, array('placeholder'=>'Break','class'=>'break')) }}
                                </div>
                            @else
                                <div class="required field">
                                    {{ $time_entry->break }} Minutes
                                </div>
                            @endif
                        </td>
                        <td>
                            @if ( Sentinel::inRole('admin') OR Sentinel::inRole('executive') )
                                <div class="field">
                                    {{ Form::text('total['.$time_entry->id.']',$time_entry->total, array('placeholder'=>'Total','readonly'=>'readonly','class'=>'total')) }}
                                </div>
                            @else
                                <div class="field">
                                    <b>{{ $time_entry->total }}</b> Hours
                                </div>
                            @endif
                        </td>
                        <td>
                            <div class="field">
                                <div class="ui search job-ajax-start">
									{{ Form::text('',$time_entry->job()->name, array('placeholder'=>'Search for Job...','class'=>'prompt')) }}
									{{ Form::hidden('job_id['.$time_entry->id.']',$time_entry->job_id, array('data-validate'=>'job','class'=>'job')) }}
								    <div class="results"></div>
								</div>
                                {{-- {{ Form::select('job_id['.$time_entry->id.']', $jobs_select, $time_entry->job_id , array('class'=>'jobs ui dropdown','autocomplete' =>'off')) }} --}}
                            </div>
                        </td>
                        <td>
                            <div class="field">
                                {{ Form::text('task['.$time_entry->id.']',$time_entry->task, array('placeholder'=>'Enter Task')) }}
                            </div>
                        </td>
                    </tr>
                    <?php $i++; ?>
                @endforeach
            </tbody>
        </table>
        <div class="ui basic segment">
            <div class="ui huge fluid green button submit-timesheet"><i class="checkmark box icon"></i>APPROVE THIS TIMESHEET</div>
        </div>
    {{ Form::close() }}
</div>

<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {
            
            //Semantic UI AJAX Search =========================
			$('.ui.job-ajax-start.search')
			  .search({
				apiSettings: {
					url: '{{route("jobs.input.ajax")}}/' + '{query}',
				},
				onSelect(result) {
					$(this).closest('.time-entry').find('input.job').val(result.job_id);
					console.log(result);
				}
			  })
			;

            // Match End Date with the Date In Selected
            $('body').on('change','.date-in', function () {
                dateIn = $(this).val();
                $(this).closest('.time-entry').find('.date-out').val(dateIn);
            });

            // Mask for Hour Entry on Break
            // $('.break').mask('00:00:00');

            // Calculate Total Time Per Entry on the Fly
			$('body').on('change keyup','.time-range input,.break', function () {

				// Set Variables
				var timeEntry = $(this).closest('.time-entry');
				var dateIn = timeEntry.find('.date-in').val();
				var dateOut = timeEntry.find('.date-out').val();
				var breakTime = (timeEntry.find('.break').val() / 60).toFixed(2);

				// Format and Set Date/Time In and Date/Time Out
				var dateStart = moment(dateIn, "MM/DD/YYYY HH:mma");
				var dateEnd = moment(dateOut, "MM/DD/YYYY HH:mma");

				// get total seconds between the times
				var delta = Math.abs(dateStart - dateEnd) / 1000;
				// calculate hours
				var hours = (delta / 3600);

				if(dateIn && dateOut){
					timeEntry.find('.total').val((hours-breakTime).toFixed(2)).css('border-color','#5BBD72');
				}

			});

            //Semantic UI Custom Form Validation Rule For Pay Periods
//			$.fn.form.settings.rules.timeEntry = function(value) {
//				var dateIn = $(this).closest('.time-range').find('.date-in').val();
//				var dateOut = $(this).closest('.time-range').find('.date-out').val();
//				if (dateIn > dateOut){
//					return false;
//				}
//				else{
//					return true;
//				}
//			}

            $('.ui.form.approve-timesheet-form').form({
				total: {
					identifier  : 'totals',
					rules: [
						{
							type   : 'empty',
							prompt : 'Total Hours is required'
						},
					]
				},
				dateIn: {
					identifier  : 'date-in',
					rules: [
						{
							type   : 'empty',
							prompt : 'Date Out is required'
						},
						{
							type   : 'timeEntry',
							prompt : 'Date In Must be Before End Date'
						},
					]
				},
				dateOut: {
					identifier  : 'date-out',
					rules: [
						{
							type   : 'empty',
							prompt : 'Date In is required'
						},
						{
							type   : 'timeEntry',
							prompt : 'Date In Must be Before End Date'
						},
					]
				},
				job: {
					identifier  : 'job',
					rules: [
						{
							type   : 'empty',
							prompt : 'Job is required'
						},
					]
				}
			});

            //Submit Timesheet for Approval
            $('.submit-timesheet').click(function(event) {
                event.preventDefault();
                // Store Variables
                var form = $('.approve-timesheet-form');
                var formAction = form.attr('action');
                // Update the Forms Route Depending on the Button Clicked
                //
                $(form).attr('action', formAction+'/1' );
                // Submit Form
                $(form).submit();
            });

            // Dynamic Date Picker
			$('body').on('focus','input.datetime-entry', function(){
				$(this).css('border', '#00CDF1 1px solid');
			    $(this).datetimepicker({
					timeFormat: "hh:mm tt",
					minDate: new Date("{{ date('Y,m,d',strtotime($timesheet->period_start)) }}"),
					maxDate: new Date("{{ date('Y,m,d',strtotime($timesheet->period_end)) }}")
				});
			});

        });
    })(jQuery);
</script>

@stop
