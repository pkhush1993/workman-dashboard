@extends('layouts.default')

@section('content')


<div id="view-timesheets">
    <h1>View Timesheets</h1>

    <div id="current-timesheets">
        <h3>View Current Timesheets</h3>
        <table class="ui compact celled striped table">
            <thead>
                  <tr>
                      <th>Pay Period</th>
                      <th>Created On</th>
                      <th>Last Update</th>
                      <th>Created By</th>
                      {{-- <th style="width:1%" class="one wide center aligned collapsing"></th> --}}
                  </tr>
            </thead>
            <tbody>
                @if ($current_timesheets->count())
                    @foreach ($current_timesheets as $timesheet)
                        <tr>
                            <td><a href="{{ route('view.timesheet',$timesheet->id) }}">{{ date('m/d/Y',strtotime($timesheet->period_start)).' - '.date('m/d/Y',strtotime($timesheet->period_end)) }}</a></td>
                            <td>{{ date('m/d/Y @ g:i a',strtotime($timesheet->created_at)) }}</td>
                            <td>{{ date('m/d/Y @ g:i a',strtotime($timesheet->updated_at)) }}</td>
                            <td>{{$timesheet->profile()->first_name}} {{$timesheet->profile()->last_name}}</td>
                            {{-- <th style="width:1%" class="one wide center aligned collapsing"></th> --}}
                        </tr>
                    @endforeach
                @else
                  <tr>
                    <td colspan="4">
                        <div class="ui orange message">
                            <div class="header">You Currently Have No Timesheets</div>
                        </div>
                    </td>
                  </tr>
                @endif
            </tbody>
        </table>
    </div>

    <div class="ui divider"></div>

    <div id="view-timesheets-pending-approval">
        <h3>Timesheets Pending Approval</h3>
        <table class="ui compact celled striped table">
            <thead>
                  <tr>
                      <th>Pay Period</th>
                      <th>Created On</th>
                      <th>Last Update</th>
                      <th>Created By</th>
                      {{-- <th style="width:1%" class="one wide center aligned collapsing"></th> --}}
                  </tr>
            </thead>
            <tbody>
                @if ($pending_timesheets->count())
                    @foreach ($pending_timesheets as $timesheet)
                        <tr>
                            <td><a href="{{ route('view.approved.timesheet',$timesheet->id) }}">{{ date('m/d/Y',strtotime($timesheet->period_start)).' - '.date('m/d/Y',strtotime($timesheet->period_end)) }}</a></td>
                            <td>{{ date('m/d/Y @ g:i a',strtotime($timesheet->created_at)) }}</td>
                            <td>{{ date('m/d/Y @ g:i a',strtotime($timesheet->updated_at)) }}</td>
                            <td>{{$timesheet->profile()->first_name}} {{$timesheet->profile()->last_name}}</td>
                            {{-- <th style="width:1%" class="one wide center aligned collapsing"></th> --}}
                        </tr>
                    @endforeach
                @else
                  <tr>
                    <td colspan="4">
                        <div class="ui orange message">
                            <div class="header">You Currently Have No Timesheets Submitted For Approval</div>
                        </div>
                    </td>
                  </tr>
                @endif
            </tbody>
        </table>
    </div>

    <div class="ui divider"></div>

    <div id="view-timesheets-pending-approval">
        <h3>Approved Timesheets</h3>
        <table class="ui compact celled striped table">
            <thead>
                  <tr>
                      <th>Pay Period</th>
                      <th>Created On</th>
                      <th>Last Update</th>
                      <th>Created By</th>
                  </tr>
            </thead>
            <tbody>
                @if ($approved_timesheets->count())
                    @foreach ($approved_timesheets as $timesheet)
                        <tr>
                            <td><a href="{{ route('view.approved.timesheet',$timesheet->id) }}">{{ date('m/d/Y',strtotime($timesheet->period_start)).' - '.date('m/d/Y',strtotime($timesheet->period_end)) }}</a></td>
                            <td>{{ date('m/d/Y @ g:i a',strtotime($timesheet->created_at)) }}</td>
                            <td>{{ date('m/d/Y @ g:i a',strtotime($timesheet->updated_at)) }}</td>
                            <td>{{$timesheet->profile()->first_name}} {{$timesheet->profile()->last_name}}</td>
                        </tr>
                    @endforeach
                @else
                  <tr>
                    <td colspan="4">
                        <div class="ui orange message">
                            <div class="header">Timesheets Are Awaiting Approval</div>
                        </div>
                    </td>
                  </tr>
                @endif
            </tbody>
        </table>
    </div>

    <div class="ui basic segment">
        @if ($current_timesheet = $current_user_profile->getCurrentTimesheet($current_user_profile->id))
            <a href="{{ route('view.timesheet',$current_timesheet->id) }}" class="ui huge fluid green button submit-timesheet"><i class="add icon"></i>EDIT CURRENT TIMESHEET</a>
        @else
            <a href="{{ route('add.timesheet') }}" class="ui huge fluid green button submit-timesheet"><i class="add icon"></i>CREATE NEW TIMESHEET</a>
        @endif
    </div>
    {{-- If the User is within the grace Period of Fixing the Old TImehseet Show Option to Edit --}}
    @if  ( $entry_grace_period AND $last_timesheet )
        <div class="ui basic segment">
            <a href="{{ route('view.timesheet',$last_timesheet->id) }}" class="ui huge fluid orange button past-timesheet">EDIT PAST TIMESHEET</a>
        </div>
    @endif


</div>

<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {

            $('body').on('change', '.user-select', function(event) {
                event.preventDefault();
                var url = "{{route('dashboard.foreman','') }}/" + $(this).dropdown('get value');

                window.location.href = url;
            });

        });
    })(jQuery);
</script>

@stop
