@extends('layouts.default')

@section('content')

<div id="view-timesheet">
    @if (Sentinel::inRole('admin') OR Sentinel::inRole('executive') OR Sentinel::inRole('project_manager') )
        {{ Form::open( array('class' => 'ui form updatetimesheet-form','route' => ['approve.timesheet',$timesheet->id]) ) }}
    @else
        {{ Form::open( array('class' => 'ui form updatetimesheet-form','route' => ['update.timesheet',$timesheet->id]) ) }}
    @endif
    <h1>Edit/Update Timesheet  <small style="font-weight: normal;font-size: 18px;">(Period {{ date('m/d/Y',strtotime($timesheet->period_start)).' - '.date('m/d/Y',strtotime($timesheet->period_end)) }})</small>
		@if ($timesheet->period_start == $period_start OR (\App\Models\Timesheet::checkEntryGracePeriod()) AND ($timesheet->period_start == $past_period_start))
			<div class="ui submit right floated green icon button update-timesheet">SAVE TIMESHEET</div>
		@endif
	</h1>
		<div class="ui error message"></div>
        <!-- Line Items -->
        <table class="ui compact small celled striped table">
            <thead>
                  <tr>
                      <th class="four wide">Time</th>
                      <th class="one wide">Break <small>(Mins.)</small></th>
                      <th class="one wide">Total Hours</th>
                      <th class="two wide">Job</th>
                      <th class="one wide">Task Category</th>
                      <th class="two wide">Description</th>
                      <th style="width:1%" class="one wide center aligned collapsing"></th>
                  </tr>
            </thead>
			{{ Form::hidden('removed_ids', $value=NULL) }}
            <tbody class="time-entry-repeater">

                    <tr class="time-entry hidden">
                        {{ Form::hidden('time_entry_id[]', '0') }}
                        {{-- {{ Form::hidden( 'user_id',Sentinel::getUser()->id,array('disabled' => 'disabled') ) }} --}}
                        <td class="time-range">
							<div class="required fields">
								<div class="field">
									{{ Form::label('date-in', 'Date In') }}
									{{ Form::text('date_in',$default = NULL, array('placeholder'=>'Date In','class'=>'date-in datetime-entry','style' => 'padding:8px;width:155px','disabled' => 'disabled')) }}
								</div>
								<div class="field">
									<b style="top: 25px;position: relative;">- TO - </b>
								</div>
								<div class="field">
									{{ Form::label('date-in', 'Date Out') }}
									{{ Form::text('date_out',$default = NULL, array('placeholder'=>'Date Out','class'=>'date-out datetime-entry','style' => 'padding:8px;width:155px','disabled' => 'disabled')) }}
								</div>
							</div>
						</td>
                        <td>
                            <div class="required field">
                                {{ Form::number('break',$default = 0, array('placeholder'=>'Break','class'=>'break','disabled' => 'disabled')) }}
                            </div>
                        </td>
                        <td>
                            <div class="field">
                                {{ Form::number('total',$default = NULL, array('placeholder'=>'Total','readonly'=>'readonly','disabled' => 'disabled','class'=>'total')) }}
                            </div>
                        </td>
                        <td>
                            <div class="field">
                                <div class="ui search job-input-ajax-repeater">
									{{ Form::text('',$default = NULL, array('placeholder'=>'Search for Job...','class'=>'prompt')) }}
									{{ Form::hidden('job_id',$default = NULL, array('data-validate'=>'job','class'=>'job','disabled'=>'disabled')) }}
								    <div class="results"></div>
								</div>
                                {{-- {{ Form::select('job_id', $jobs_select, $default = '' , array('class'=>'jobs','autocomplete' =>'off','disabled' => 'disabled')) }} --}}
                            </div>
                        </td>
						<td>
							<div class="field">
								{{ Form::select('task', $task_select, $default = null , array('class'=>'task')) }}
							</div>
						</td>
						<td>
                            <div class="field">
                                {{ Form::text('description',$default = NULL, array('placeholder'=>'Enter description','disabled' => 'disabled','class'=>'description')) }}
                            </div>
                        </td>
                        <td class="center aligned">
                            <i class="large green add circle icon add-field"></i>
                            <div class="ui divider" style="margin: 3px 0;"></div>
                            <i class="large red minus circle icon remove-field"></i>
                        </td>
                    </tr>
                {{-- Repeat 5 Times --}}
				@if ( $timesheet_details->count() )
                	<?php $time_entry_count = 1; ?>
					@foreach ($timesheet_details as $time_entry)
						<tr class="time-entry">
							{{-- {{ Form::hidden('user_id['.$time_entry_count.']',Sentinel::getUser()->id) }} --}}
							{{ Form::hidden('time_entry_id['.$time_entry_count.']', $time_entry->id,array('data-id' => $time_entry->id)) }}
							<td class="time-range">
								<div class="required fields">
									<div class="field">
										{{ Form::label('date-in', 'Date In') }}
										{{ Form::text('date_in['.$time_entry_count.']',date('m/d/Y g:ia',strtotime($time_entry->date_in)), array('placeholder'=>'Date In','class'=>'datetime-entry date-in','style' => 'padding:8px;width:155px')) }}
									</div>
									<div class="field">
										<b style="top: 25px;position: relative;">- TO - </b>
									</div>
									<div class="field">
										{{ Form::label('date-in', 'Date Out') }}
										{{ Form::text('date_out['.$time_entry_count.']',date('m/d/Y g:ia',strtotime($time_entry->date_out)), array('placeholder'=>'Date Out','class'=>'datetime-entry date-out','style' => 'padding:8px;width:155px')) }}
									</div>
								</div>
							</td>
							<td>
								<div class="required field">
									{{ Form::number('break['.$time_entry_count.']',$time_entry->break, array('placeholder'=>'Break','class'=>'break')) }}
								</div>
							</td>
							<td>
								<div class="field">
									{{ Form::text('total['.$time_entry_count.']',$time_entry->total, array('placeholder'=>'Total','readonly'=>'readonly','class'=>'total')) }}
								</div>
							</td>
							<td>
								<div class="field">
									<div class="ui search job-input-ajax">
										{{ Form::text('',$time_entry->job() ? $time_entry->job()->name : '', array('placeholder'=>'Search for Job...','class'=>'prompt')) }}
										{{ Form::hidden('job_id['.$time_entry_count.']',$time_entry->job_id, array('data-validate'=>'job','class'=>'job')) }}
										<div class="results"></div>
									</div>
									{{-- {{ Form::select('job_id['.$time_entry_count.']', $jobs_select, $time_entry->job_id , array('class'=>'ui dropdown jobs','autocomplete' =>'off')) }} --}}
								</div>
							</td>
							<td>
								<div class="field">
									{{ Form::select('task['.$time_entry_count.']', $task_select, $time_entry->task , array('class'=>'ui dropdown task')) }}
								</div>
							</td>
							<td>
								<div class="field">
									{{ Form::text('description['.$time_entry_count.']',$time_entry->description, array('placeholder'=>'Enter Description','class'=>'description')) }}
								</div>
							</td>
							<td class="center aligned">
								<i class="large green add circle icon add-field"></i>
								<div class="ui divider" style="margin: 3px 0;"></div>
								<i class="large red minus circle icon remove-field"></i>
							</td>
						</tr>
						<?php $time_entry_count++; ?>
					@endforeach
				@else
					<?php $i = 1; ?>
					@while ($i <= 1)
						<tr class="time-entry">
							{{ Form::hidden('time_entry_number',$default = $i) }}
							{{ Form::hidden('user_id',Sentinel::getUser()->id) }}
							<td class="time-range">
								<div class="required fields">
									<div class="field">
										{{ Form::label('date-in', 'Date In') }}
										{{ Form::text('date_in['.$i.']',$default = NULL, array('placeholder'=>'Date In','class'=>'datetime-entry date-in','data-validate'=>'date-in','style' => 'padding:8px;width:155px','required'=>'required')) }}
									</div>
									<div class="field">
										<b style="top: 25px;position: relative;">- TO - </b>
									</div>
									<div class="field">
										{{ Form::label('date-in', 'Date Out') }}
										{{ Form::text('date_out['.$i.']',$default = NULL, array('placeholder'=>'Date Out','class'=>'datetime-entry date-out','data-validate'=>'date-out','style' => 'padding:8px;width:155px','required'=>'required')) }}
									</div>
								</div>
							</td>
							<td>
								<div class="required field">
									{{ Form::number('break['.$i.']',$default = 0, array('placeholder'=>'Break','class'=>'break')) }}
								</div>
							</td>
							<td>
								<div class="field">
									{{ Form::text('total['.$i.']',$default = NULL, array('placeholder'=>'Total','readonly'=>'readonly','class'=>'total','data-validate'=>'totals')) }}
								</div>
							</td>
							<td>
								<div class="field">
									<div class="ui search job-input-ajax">
										{{ Form::text('',$default = NULL, array('placeholder'=>'Search for Job...','class'=>'prompt')) }}
										{{ Form::hidden('job_id['.$i.']',$default = NULL, array('data-validate'=>'job','class'=>'job')) }}
										<div class="results"></div>
									</div>
									{{-- {{ Form::select('job_id['.$i.']', $jobs_select, $default = '' , array('class'=>'ui dropdown jobs','autocomplete' =>'off','data-validate'=>'job','required'=>'required')) }} --}}
								</div>
							</td>
							<td>
								<div class="field">
									{{ Form::select('task['.$i.']', $task_select, $default = null , array('class'=>'ui dropdown task')) }}
								</div>
							</td>
							<td>
								<div class="field">
									{{ Form::text('description['.$i.']',$default = NULL, array('placeholder'=>'Enter Description','class' => 'description')) }}
								</div>
							</td>
							<td class="center aligned">
								<i class="large green add circle icon add-field"></i>
								<div class="ui divider" style="margin: 3px 0;"></div>
								<i class="large red minus circle icon remove-field"></i>
							</td>
						</tr>
						<?php $i++; ?>
					@endwhile
				@endif
            </tbody>
            {{-- Only Show Hours if the current Users has privledges --}}
            @if (Sentinel::inRole('!admin') OR Sentinel::inRole('!executive') OR Sentinel::inRole('!project_manager') )
                <tfoot style="font-size:14px">
                    <tr>
                        <th style="vertical-align: middle;text-align: right;" colspan="5">
                            <b>Hours:</b>
                        </th>
                        <th style="vertical-align: middle;text-align: left;" colspan="1">
                            <b>{{ $timesheet->report()['regular_hours'] }}</b>
                        </th>
                    </tr>
                    @if ($timesheet->report()['overtime_hours'] > 0)
                        <tr>
                            <th style="vertical-align: middle;text-align: right;" colspan="5">
                                <b>Overtime Hours:</b>
                            </th>
                            <th style="vertical-align: middle;text-align: left;" colspan="1">
                                <b>{{ $timesheet->report()['overtime_hours'] }}</b>
                            </th>
                        </tr>
                    @endif
                    @if ($timesheet->report()['overtime_hours'] > 0)
                        <tr>
                            <th style="vertical-align: middle;text-align: right;" colspan="5">
                                <b>Overtime Pay:</b>
                            </th>
                            <th style="vertical-align: middle;text-align: left;" colspan="1">
                                <b>${{ $timesheet->report()['overtime_pay'] }}</b>
                            </th>
                        </tr>
                    @endif
                    <tr>
                        <th style="vertical-align: middle;text-align: right;" colspan="5">
                            <b>Total Pay:</b>
                        </th>
                        <th style="vertical-align: middle;text-align: left;" colspan="1">
                            <b>${{ $timesheet->report()['total_pay'] }}</b>
                        </th>
                    </tr>
                </tfoot>
            @endif
        </table>

        <div class="ui basic segment">
            @if (Sentinel::inRole('admin') OR Sentinel::inRole('executive') OR Sentinel::inRole('project_manager') )
                <div class="ui huge fluid blue button submit-timesheet"><i class="checkmark box icon"></i>APPROVE TIMESHEET</div>
            @else
                <div class="ui huge fluid teal button submit-timesheet"><i class="checkmark box icon"></i>SUBMIT TIMESHEET FOR APPROVAL</div>
            @endif
		</div>
    {{ Form::close() }}
</div>

<script>
    jQuery.noConflict();

    (function( $ ) {
        $(function() {
			var view = $("#view-timesheet");
			var form = $('.ui.form.updatetimesheet-form', view);
			var minDate = new Date("{{ date('c',strtotime("-1 day", strtotime($timesheet->period_start))) }}");
			var maxDate = new Date("{{ date('c',strtotime("+2 day", strtotime($timesheet->period_end))) }}");
			var timesheet = new TimesheetView(view, form, minDate, maxDate,'{{route("jobs.input.ajax")}}');

            //Submit Timesheet for Approval
			$('body').on('click', '.update-timesheet', function(){
                event.preventDefault();
				// Submit Form
				$('.updatetimesheet-form').submit();

            });


			//Submit Timesheet for Approval
			$('body').on('click', '.submit-timesheet', function(){
				event.preventDefault();
                // Store Variables
                var form = $('.updatetimesheet-form');
                var formAction = form.attr('action')+'/1';
                // Update the Forms Route Depending on the Button Clicked
                $(form).attr('action', formAction );
				// Submit Form
				$(form).submit();
            });
        });
    })(jQuery);
</script>

@stop
