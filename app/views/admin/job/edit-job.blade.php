@extends('layouts.default')


@section('content')

    <div id="edit-job">
        <h1><i class="file text icon"></i>Edit Job - <span style="color:#888">{{$job->name}}</span></h1>
        {{-- Only Show Order Steps Guide if a Bid Does Not Exist --}}
		@if (!$bid_line_items )
			<div class="ui ordered steps">
				<div class=" step completed set-account">
					<div class="content">
						<div class="title">
							Set Account
						</div>
						<div class="description">
							Select New or Existing Account
						</div>
					</div>
				</div>
				<div class="step completed enter-job-info">
					<div class="content">
						<div class="title">
							Enter Job Info
						</div>
						<div class="description">
							Fill out the job form
						</div>
					</div>
				</div>
				<div class="step completed create-job">
					<div class="content">
						<div class="title">
							Create Job
						</div>
						<div class="description">
							Submit your new job
						</div>
					</div>
				</div>
				<div class="step active setup-bid">
					<div class="content">
						<div class="title">
							Setup Bid
						</div>
						<div class="description">
							Create New Job and Setup Bid Items
						</div>
					</div>
				</div>
			</div>
		@endif
    </div>


    <div class="ui divider"></div>
	{{ Form::open(array('class' => 'ui form','id'=>'edit-job-form','route' => ['update.job',$job->id] )) }}

		<!-- ================== Hidden Form Fields ================== -->

		<!-- Job Contacts -->
		{{ Form::hidden('job_contacts',$value = null) }}

		{{ Form::hidden('bid_date', $job->bid_date ?: null) }}
		{{ Form::hidden('estimator_deadline', $job->estimator_deadline ?: null) }}
		{{ Form::hidden('estimator_priority', $job->estimator_priority ?: null) }}
		{{ Form::hidden('production_deadline', $job->production_deadline ?: null) }}
		{{ Form::hidden('production_priority', $job->production_priority ?: null) }}
		{{ Form::hidden('completed_date', $job->completed_date ?: null) }}
	<input type="hidden" id="fromImport" value="{{$fromImport}}" name="fromImport">
	<!-- ======================================================= -->

		<div class="ui stackable grid">
			<div class="row">
				<!-- Left Column -->
				<div class="ten wide column ">
					<h3 style="margin-top:0"><i class="circle info icon"></i>Job Info</h3>
					<div class="ui blue segment">
						<div class="field">
							{{ Form::label('name', 'Job Name/Number') }}
							<div class="two fields">
								<div class="field">
									{{ Form::text('name',$job->name, array('placeholder'=>'Enter Job Name')) }}
								</div>
								<div class="field">
									{{ Form::text('job_number',$job->job_number, array('placeholder'=>'Enter Job Number')) }}
								</div>
							</div>
						</div>
						<div class="two fields account-contacts">
							<div class="field">
								{{ Form::label('account', 'Account') }}

								<div class="assigned-job-account">
                                    <div class="ui search account-ajax-search">
    									{{ Form::text('',$job->account()->name, array('placeholder'=>'Search for Account...','class'=>'prompt ui large fluid search icon job-account-search')) }}
    									{{ Form::hidden('assigned_job_account',$job->account, array('data-validate'=>'job','class'=>'job-account account-id')) }}
    								    <div class="results"></div>
    								</div>
									{{-- {{ Form::select('assigned_job_account', $accounts_dropdowns, $job->account , array('class'=>'ui large fluid search icon job-account dropdown','autocomplete' =>'off')) }} --}}
								</div>
								<div class="new-account-name" style="display:none">
									{{ Form::text('new_account_name','',array('readonly' => 'readonly')) }}
								</div>
							</div>
							<div class="field actions">
								<div class="ui labeled green icon button view-contact-button"><i class="ui icon add"></i>View Contacts On Job</div>
							</div>
						</div>

						<div class="field">
							{{ Form::label('name', 'Primary Contact') }}
							{{ Form::select('primary_contact', !empty($job_contacts_dropdown) ? $job_contacts_dropdown : [], $job->primary_contact , array('class'=>'ui large fluid search icon job-primary-contact dropdown','autocomplete' =>'off')) }}
							<br>
						</div>
						<div class="field">
							{{ Form::label('status', 'Current Status') }}
							{{ Form::select('status', array(
								''     		  => 'Select Status',
								'request'     => 'Request',
								'outstanding' => 'Outstanding',
								'contracted'  => 'Contracted',
								'proceed'     => 'Proceed',
								'scheduled'   => 'Scheduled',
								'completed'   => 'Completed',
								'invoicing'   => 'Invoicing',
								'paid'        => 'Paid',
								'rejected'    => 'Rejected',
							), strtolower($job->status) , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
						</div>
						<div class="field">
							<div class="ui small fluid orange icon button view-dates-priorities"><i class="calendar icon"></i> View Dates/Priorities</div>
							<!-- Dates/Priorities Modal -->
						</div>
					</div>
					<h3><i class="users icon"></i> Project Roles</h3>
					<div class="ui green segment">
						@if ($estimators)
							<div class="field">
								{{ Form::label('estimator', 'Estimator') }}
								{{ Form::select('estimator', $estimators, $job->estimator  , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
							</div>
						@endif
						@if ($project_managers)
							<div class="field">
								{{ Form::label('project_manager', 'Project Manager') }}
								{{ Form::select('project_manager', $project_managers, $job->project_manager, array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
							</div>
						@endif
						@if ($foremen)
							<div class="field">
								{{ Form::label('foreman', 'Foreman') }}
								{{ Form::select('foreman', $foremen, $job->foreman , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
							</div>
						@endif
						@if ($accountants)
							<div class="field">
								{{ Form::label('accounting', 'Accountant') }}
								{{ Form::select('accounting', $accountants, $job->accounting , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
							</div>
						@endif
						<?php /* Removed Salesman
						@if ($salesman)
							<div class="field">
								{{ Form::label('salesman', 'Salesman') }}
								{{ Form::select('salesman', $salesman, $job->salesman , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
							</div>
						@endif
						*/ ?>
					</div>
					<div class="ui divider"></div>
					<div class="field">
						{{ Form::label('prime_contract', 'Enter Prime Contract') }}
						{{ Form::text('prime_contract',$job->prime_contract) }}
					</div>
				</div>
				<!-- Right Column -->
				<div class="six wide column ">
					{{-- JOB ACTIONS --}}
					<h3 class="ui center aligned header" style="margin-top:0">JOB ACTIONS</h3>
					<div class="ui divider"></div>
					<div class="ui fluid large labeled blue icon button bid-fields-button"><i class="dollar icon"></i>Bid/Invoice</div>
					<br>
					<div class="ui fluid large labeled blue icon job-notes-button button"><i class="comments icon"></i>Activities and Notes</div>
					<br>
					<div class="work-order-button" data-content="You must first create a Job Bid before viewing a Work Order">
						<a target="_blank" href="{{ route('job.work.order', [$job->id]) }}" class="ui fluid large labeled blue icon button">
							<i class="folder icon"></i>Work Order
						</a>
					</div>
					<br>
					<div class="ui fluid large labeled icon blue job-rework-button button"><i class="history icon"></i>Rework ({{count($rework_items)}})</div>
					<br>
					<div class="ui fluid large labeled icon blue copy-job-button button"><i class="newspaper icon"></i>Copy Job</div>
					<br>
					<div class="ui fluid big labeled icon red delete-job-button button"><i class="circle icon"></i>Delete Job</div>
					<br>
					<div class="ui fluid big labeled icon orange button" style="display:none"><i class="copy icon"></i>Copy Job</div>
					<div class="ui divider"></div>
					<h3 style="margin-top:0"><i class="marker info icon"></i>Addresses</h3>
					<!-- Site Address -->
					<div class="ui segment">
						<h4 class="ui header" >Site Address</h4>
						{{ Form::label('address_1', 'Address') }}
						<div class="two fields">
							<div class="field">
								{{ Form::text('site_address_1',$job->site_address_1, array('placeholder'=>'Address')) }}
							</div>
							<div class="field">
								{{ Form::text('site_address_2',$job->site_address_2, array('placeholder'=>'Address 2')) }}
							</div>
						</div>
						<div class="fields">
							<div class="seven wide field">
								{{ Form::label('city', 'City') }}
								{{ Form::text('site_city',$job->site_city, array('placeholder'=>'City')) }}
							</div>
							<div class="five wide field">
								{{ Form::label('state', 'State') }}
								{{ Form::select('site_state', array(
									'' => 'State',
									'AL' => 'AL',
									'AK' => 'AK',
									'AZ' => 'AZ',
									'AR' => 'AR',
									'CA' => 'CA',
									'CO' => 'CO',
									'CT' => 'CT',
									'DE' => 'DE',
									'DC' => 'DC',
									'FL' => 'FL',
									'GA' => 'GA',
									'HI' => 'HI',
									'ID' => 'ID',
									'IL' => 'IL',
									'IN' => 'IN',
									'IA' => 'IA',
									'KS' => 'KS',
									'KY' => 'KY',
									'LA' => 'LA',
									'ME' => 'ME',
									'MD' => 'MD',
									'MA' => 'MA',
									'MI' => 'MI',
									'MN' => 'MN',
									'MS' => 'MS',
									'MO' => 'MO',
									'MT' => 'MT',
									'NE' => 'NE',
									'NV' => 'NV',
									'NH' => 'NH',
									'NJ' => 'NJ',
									'NM' => 'NM',
									'NY' => 'NY',
									'NC' => 'NC',
									'ND' => 'ND',
									'OH' => 'OH',
									'OK' => 'OK',
									'OR' => 'OR',
									'PA' => 'PA',
									'RI' => 'RI',
									'SC' => 'SC',
									'SD' => 'SD',
									'TN' => 'TN',
									'TX' => 'TX',
									'UT' => 'UT',
									'VT' => 'VT',
									'VA' => 'VA',
									'WA' => 'WA',
									'WV' => 'WV',
									'WI' => 'WI',
									'WY' => 'WY',
								), $job->site_state, array('class'=>'ui dropdown site-state fluid','autocomplete' =>'off')) }}
							</div>
							<div class="four wide field">
								{{ Form::label('zip', 'Zip') }}
								{{ Form::text('site_zip',$job->site_zip, array('placeholder'=>'Zip Code')) }}
							</div>
						</div>
						<div class="two fields">
							<div class="field">
								<div class="ui fluid tiny use-billing_address button">Use Billing</div>
							</div>
							<div class="field">
								<div class="ui fluid tiny use-account-address-site button">Use Account</div>
							</div>
						</div>
					</div>
					<!-- Billing Address -->
					<div class="ui segment">
						<h4 class="ui header" >Billing Address</h4>
						{{ Form::label('address_1', 'Address') }}
						<div class="two fields">
							<div class="field">
								{{ Form::text('billing_address_1',$job->billing_address_1, array('placeholder'=>'Address')) }}
							</div>
							<div class="field">
								{{ Form::text('billing_address_2',$job->billing_address_2, array('placeholder'=>'Address 2')) }}
							</div>
						</div>
						<div class="fields">
							<div class="seven wide field">
								{{ Form::label('city', 'City') }}
								{{ Form::text('billing_city',$job->billing_city, array('placeholder'=>'City')) }}
							</div>
							<div class="five wide field">
								{{ Form::label('state', 'State') }}
								{{ Form::select('billing_state', array(
									'' => 'State',
									'AL' => 'AL',
									'AK' => 'AK',
									'AZ' => 'AZ',
									'AR' => 'AR',
									'CA' => 'CA',
									'CO' => 'CO',
									'CT' => 'CT',
									'DE' => 'DE',
									'DC' => 'DC',
									'FL' => 'FL',
									'GA' => 'GA',
									'HI' => 'HI',
									'ID' => 'ID',
									'IL' => 'IL',
									'IN' => 'IN',
									'IA' => 'IA',
									'KS' => 'KS',
									'KY' => 'KY',
									'LA' => 'LA',
									'ME' => 'ME',
									'MD' => 'MD',
									'MA' => 'MA',
									'MI' => 'MI',
									'MN' => 'MN',
									'MS' => 'MS',
									'MO' => 'MO',
									'MT' => 'MT',
									'NE' => 'NE',
									'NV' => 'NV',
									'NH' => 'NH',
									'NJ' => 'NJ',
									'NM' => 'NM',
									'NY' => 'NY',
									'NC' => 'NC',
									'ND' => 'ND',
									'OH' => 'OH',
									'OK' => 'OK',
									'OR' => 'OR',
									'PA' => 'PA',
									'RI' => 'RI',
									'SC' => 'SC',
									'SD' => 'SD',
									'TN' => 'TN',
									'TX' => 'TX',
									'UT' => 'UT',
									'VT' => 'VT',
									'VA' => 'VA',
									'WA' => 'WA',
									'WV' => 'WV',
									'WI' => 'WI',
									'WY' => 'WY',
								), $job->billing_state, array('class'=>'ui dropdown billing-state fluid','autocomplete' =>'off')) }}
							</div>
							<div class="four wide field">
								{{ Form::label('billing_zip', 'Zip') }}
								{{ Form::text('billing_zip',$job->billing_zip, array('placeholder'=>'Zip Code')) }}
							</div>
						</div>
						<div class="two fields">
							<div class="field">
								<div class="ui fluid tiny use-site_address button">Use Site</div>
							</div>
							<div class="field">
								<div class="ui fluid tiny use-account-address-billing button">Use Account</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="ui divider"></div>
			{{-- Custom Fields --}}
			<?php
				// Select Default
				$default = ['' => 'Select Field'];
			?>
			<div class="row">
				<div class="column">
					<div class="field">
						{{ Form::label('lead_source', 'Lead Source') }}
						{{ Form::select('lead_source', empty($lead_sources) ? [] : $lead_sources, $job->lead_source , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
					</div>
					@if ($company->getCustomDrop1())
						<div class="field">
							{{ Form::label('custom_drop_1', key($company->getCustomDrop1())) }}
							{{ Form::select('custom_drop_1', $custom_fields_dropdowns['custom_drop_1'], $job->custom_drop_1 , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
						</div>
					@endif

					@if ($company->getCustomDrop2())
						<div class="field">
							{{ Form::label('custom_drop_2', key($company->getCustomDrop2())) }}
							{{ Form::select('custom_drop_2', $custom_fields_dropdowns['custom_drop_2'], $job->custom_drop_2 , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
						</div>
					@endif

					@if ($company->getCustomDrop3())
						<div class="field">
							{{ Form::label('custom_drop_3', key($company->getCustomDrop3())) }}
							{{ Form::select('custom_drop_3', $custom_fields_dropdowns['custom_drop_3'], $job->custom_drop_3 , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
						</div>
					@endif

					@if ($company->getCustomText1())
						<div class="field">
							{{ Form::label('custom_text_1', $company->getCustomText1()) }}
							{{ Form::text('custom_text_1',$job->custom_text_1, array('placeholder'=>'Enter Value')) }}
						</div>
					@endif
					@if ($company->getCustomText2())
						<div class="field">
							{{ Form::label('custom_text_2', $company->getCustomText2()) }}
							{{ Form::text('custom_text_2',$job->custom_text_2, array('placeholder'=>'Enter Value')) }}
						</div>
					@endif
					@if ($company->getCustomText3())
					<div class="field">
						{{ Form::label('custom_text_3', $company->getCustomText3()) }}
						{{ Form::text('custom_text_3',$job->custom_text_3, array('placeholder'=>'Enter Value')) }}
					</div>
					@endif
				</div>
			</div>
		</div>
		<br>
		<div class="actions">
			<input type="submit" class="ui submit huge fluid green button" value="UPDATE JOB">
		</div>
		{{-- <div class="ui error message"></div> --}}

	{{ Form::close() }}

	{{-- Account Addresses for AutoFill --}}
	{{ Form::hidden('set_account_address_1',$account->address_1) }}
	{{ Form::hidden('set_account_address_2',$account->address_2) }}
	{{ Form::hidden('set_account_city',$account->city) }}
	{{ Form::hidden('set_account_state', $account->state) }}
	{{ Form::hidden('set_account_zip',$account->zip) }}


	<!-- ====================== MODALS ====================== -->
	@include('admin.job.modals.view-dates-priorities')
	<!-- Add Account Modal -->
	@include('admin.job.modals.job-add-account')

	<!-- Copy Job Modal -->
	@include('admin.job.modals.copy-job')

	<!-- Add New Contact Modal -->
	@include('admin.job.modals.job-add-contact')

	<!-- Add Contacts To Job Modal -->
	@include('admin.job.modals.job-attach-contacts')

	<!-- View Contacts Modal -->
	@include('admin.job.modals.job-contacts')

	<!-- View Contact Modal -->
	@include('admin.job.modals.job-contact')

	<!-- Default Modal (a blank modal used with AJAX calls) -->
	@include('admin.job.modals.default')

	<!-- Choose Account Modal -->
	{{-- @include('admin.job.modals.choose-account') --}}

	<!-- Bid Fields Modal -->
	@include('admin.job.modals.job-bid-fields')

	<!-- Job Rework Modal -->
	@include('admin.job.modals.job-rework')

	<!-- Job Notes -->
	@include('admin.job.modals.job-notes')

	<!-- Job Edit Notes -->
	@include('admin.job.modals.job-edit-note')

	<!-- Create Invoice -->
	@include('admin.job.modals.job-create-invoice')

	<!-- Invoice Previous Payments -->
	@include('admin.job.modals.job-previous-payments')

	<!-- Job Invoice Type -->
	@include('admin.job.modals.job-invoice-type')

	<!-- Invoice Payment Amount -->
	@include('admin.job.modals.job-invoice-partial-payment-amount')

	<!-- Bid Type Modal -->
	@include('admin.job.modals.job-bid-type')

	<!-- Delete Job Confirmation Modal -->
	@include('admin.job.modals.job-delete-job')

	<!-- =================================================== -->

	<script>
		jQuery.noConflict();
		(function( $ ) {
			$(function() {

				$(window).on('load',function(){
					var fromImport = $('#fromImport').val();
					if(fromImport == true || fromImport == 'true')
						$('.ui.modal.bid-fields').modal('show');
					$('#fromImport').val(0)
					window.history.replaceState({}, document.title, "/" + "job/{{$job->id}}");

				});

				$("input[name=assigned_job_account]").on('change', function(e){
					console.log(e);
					console.log(this.val());
				});
				$.site('enable debug');

                //Semantic UI AJAX Search =========================

				// Add Copy Job Modal
				$('.copy-job-button').pop('.ui.modal.copy-job');

				// Add Account Modal
				$('.new-account').pop('.ui.modal.add-account');

				// Set Account Modal
				$('.job-account').pop('.ui.modal.set-account');

				// Set Existing Account Modal
				$('.existing-account').pop('.ui.modal.choose-account');

				// Add Contacts to Job Modal
				$('.add-contacts-button	').pop('.ui.modal.add-contacts');

				// Add New Contact Modal
				$('.add-contact-button').pop('.ui.modal.add-contact');

				// Add New Contact Modal
				$('.attach-contacts-button').pop('.ui.modal.attach-contacts');

				// Job Rework Modal
				$('.job-rework-button').pop('.ui.modal.job-rework');

				// Bid Type
				$('.bid-type-button').pop('.ui.modal.bid-type');

				// Dates/Priorities
				$('.view-dates-priorities').pop('.ui.modal.dates-priorities',true,false);

				//Job Notes
				//$('.job-notes-button').pop('.ui.modal.job-notes',false);
				$('.job-notes-button').click(function(event) {
					$('.ui.modal.job-notes').modal("refresh");
					$('.ui.modal.job-notes').modal({closable  : false,autofocus : false}).modal('show');
				});

				//Job Bid Fields
				$('.bid-fields-button').pop('.ui.modal.bid-fields',false,true);

				// Refresh Modal When Form is Expanded for New Contact on Account
				$('.new_account_contact').click(function(event) {
					$('.ui.modal.add-account').modal("refresh");
				});

				// Invoice Type Modal
//				$('.invoice-type-button').pop('.ui.modal.invoice-type');

				// Invoice Payment Amount Modal
				$('.invoice-partial-payment-amount-button').pop('.ui.modal.invoice-partial-payment-amount');

				// Invoice Previous Payments Modal
				$('.previous-payments-button').pop('.ui.modal.previous-payments');

				//View Contact Modal
				$('.view-contact-button').pop('.ui.modal.view-contacts');

				// Delete Job Modal
				$('.delete-job-button').pop('.ui.modal.delete-job');

				// Set Account Default on Create New Contact to Jobs Selected Account
				$('.add-contact.modal .contact-account.dropdown').dropdown('set selected', $('.job-account.dropdown').dropdown('get value') );

				// Copy Site Address
				$('.use-site_address').click(function(event) {
					$('input[name=billing_address_1]').val($('input[name=site_address_1]').val());
					$('input[name=billing_address_2]').val($('input[name=site_address_2]').val());
					$('input[name=billing_city]').val($('input[name=site_city]').val());
					$('.billing-state').dropdown('set selected', $('.site-state').dropdown('get value') );
					$('input[name=billing_zip]').val($('input[name=site_zip]').val());
				});

				// Copy Billing Address
				$('.use-billing_address').click(function(event) {
					$('input[name=site_address_1]').val($('input[name=billing_address_1]').val());
					$('input[name=site_address_2]').val($('input[name=billing_address_2]').val());
					$('input[name=site_city]').val($('input[name=billing_city]').val());
					$('.site-state').dropdown('set selected', $('.billing-state').dropdown('get value') );
					$('input[name=site_zip]').val($('input[name=billing_zip]').val());
				});

				// Copy Account Address for Site Address
				$('.use-account-address-site').click(function(event) {
					$('input[name=site_address_1]').val($('input[name=set_account_address_1]').val());
					$('input[name=site_address_2]').val($('input[name=set_account_address_2]').val());
					$('input[name=site_city]').val($('input[name=set_account_city]').val());
					$('.site-state').dropdown('set selected', $('input[name=set_account_state]').val());
					$('input[name=site_zip]').val($('input[name=set_account_zip]').val());
				});

				// Copy Account Address for Billing Address
				$('.use-account-address-billing').click(function(event) {
					$('input[name=billing_address_1]').val($('input[name=set_account_address_1]').val());
					$('input[name=billing_address_2]').val($('input[name=set_account_address_2]').val());
					$('input[name=billing_city]').val($('input[name=set_account_city]').val());
					$('.billing-state').dropdown('set selected', $('input[name=set_account_state]').val());
					$('input[name=billing_zip]').val($('input[name=set_account_zip]').val());
				});

				// Set Job Contacts Depending on Specified Account in Dropdown
				$("select[name=assigned_job_account]").on('change',function(){

					if ( $(this).val() == "new_account"){
						$('.ui.modal.add-account').modal('show');
					}
					else{
						// Get Request Contacts Id
						var accountId = $(this).val();
						// Build URL using Route
						contactsUrl = "{{route('job.contacts','') }}/" + accountId + "/{{$job_id}}";
						// Set Contacts depending on Account Chosen - AJAX
						console.log(contactsUrl);
						$.ajax({
							type: "GET",
							url: contactsUrl,
						})
						.done(function( data ) {
							//Reset Job Contacts Hidden Input to prep for new contacts from a different account
							$('input[name=job_contacts]').val('');
							//Reload Form in Modal with New Accounts Contacts
							$('.ui.modal.attach-contacts .add-contacts-table').html(data);
							$('.ui.modal.attach-contacts .ui.checkbox').checkbox();
							console.log(data);
						});
					}

				});

				// Close Action on View Contact Form
				$('.default.modal.view-contact .close').click(function(event) {
					$('.ui.modal.add-contact').modal('show');
				});

				// Close Action on View Contact Form
				$('.delete-job-button').click(function(event) {
					$('.ui.modal.delete-job').modal('show');
				});


				// Disabled Work Order Button Tooltip
				// $('.work-order-button.disabled ')
				//   .popup({
				//     inline: true,
				// });

				//Semantic UI Form Validation =========================
				$('#edit-job-form').form(
					{
					    jobName: {
							identifier  : 'name',
							rules: [
								{
									type   : 'empty',
									prompt : 'Job Name is required'
								}
							]
					    },
					    jobAccount: {
							identifier  : 'assigned_job_account',
							rules: [
								{
									type   : 'empty',
									prompt : 'Job Account is required',
								}
							]
					    },
					    jobNumber: {
							identifier  : 'job_number',
							rules: [
								{
									type   : 'empty',
									prompt : 'Job Number is required'
								}
							]
					    },
					    jobStatus: {
							identifier  : 'status',
							rules: [
								{
									type   : 'empty',
									prompt : 'Status is required'
								}
							]
					    },
					    bidDate: {
							identifier  : 'bid_date',
							rules: [
								{
									type   : 'empty',
									prompt : 'Bid Date is required'
								}
							]
					    },
					    jobEstimator: {
							identifier  : 'estimator',
							rules: [
								{
									type   : 'empty',
									prompt : 'Job Estimator is required',
								}
							]
					    },
					    jobPM: {
							identifier  : 'project_manager',
							rules: [
								{
									type   : 'empty',
									prompt : 'Job Project Manager is required',
								}
							]
					    },
					    jobForeman: {
							identifier  : 'foreman',
							rules: [
								{
									type   : 'empty',
									prompt : 'Job Foreman is required',
								}
							]
					    },
					    jobAccountant: {
							identifier  : 'accounting',
							rules: [
								{
									type   : 'empty',
									prompt : 'Job Accountant is required',
								}
							]
					    },
					    primaryContact: {
							identifier  : 'primary_contact',
							rules: [
								{
									type   : 'empty',
									prompt : 'Primary Contact is required on a Job',
								}
							]
					    }
					},
					{
					    inline : true,
					    on     : 'blur'
					}
				);

				//Confirm Page Changes Before Leaving Page =========================
				var editMade = false;

				// Detect if an Input has been changed and set var
				$('#page-content').on('input propertychange keyup','input', function () {
					editMade = true;
				});

				$('#page-content').on('change', 'select', function() {
					editMade = true;
				});

				// Do not prompt message if leaving the page because of a save/update
				$('body').on('click','input[type="submit"]', function () {
					editMade = false;
				});
				$('body').on('submit','form', function () {
					editMade = false;
				});
				// Setup Navigation Confirmation if Unsaved Changes were made
				window.onbeforeunload = function() {
					if(editMade){
				  		confirm("There were changes made that have not been saved.");return true;
				  	}
				}

                $("#flash").animate({'width':'500px','height':'800px'},800, function() {
                  // your code here:
                  document.getElementById('main').height="800";
                });

				$('.save-dates').click(function() {
					$('#edit-job-form').submit();
				});

				$('#job_bid .button.blue').click(function(e){
					e.preventDefault();
					var jobForm = $('#edit-job-form');
					var commentForm = $('.ui.form.activity-form');

					$.ajax({
						type: "POST",
						url: jobForm.attr( 'action' ),
						data: jobForm.serialize(),
					});

					$.ajax({
						type: "POST",
						url: commentForm.attr( 'action' ),
						data: commentForm.serialize(),
					});
					$('#job_bid').submit();
				});

				$('.ui.form.activity-form .ui.submit.green.button').click(function(e){
					e.preventDefault();
					var jobForm = $('#edit-job-form');
					var bidForm = $('#job_bid');

					$.ajax({
						type: "POST",
						url: jobForm.attr( 'action' ),
						data: jobForm.serialize(),
					});

					$.ajax({
						type: "POST",
						url: bidForm.attr( 'action' ),
						data: bidForm.serialize(),
					});
					$('.ui.form.activity-form').submit();
				})


			});
		})(jQuery);
	</script>

@stop
