<!-- Add Account Modal -->
<div class="ui no-account-selected basic modal">
	<div class="content">
		<div class="ui large red icon message">
	        <i class="warning sign icon"></i>
	        <div class="header">
	          Account Not Selected!
	        </div>
	        <p> Before You Create a Job you Need to Select an Account to attach to the Job</p>
	    </div>	
	</div>
	<div class="actions">
		<div class="ui center aligned basic segment">
			<div class="ui  green approve massive button">
				Select Account
		    </div>			
		</div>
	</div>
</div>