<!-- Add Account Modal -->
<div class="ui attach-contacts modal">
	<i class="ui close icon"></i>
	<h2 class="ui header">
		<i class="child icon"></i>
		<div class="content">
			Assign Contacts to Job
			<div class="sub header">Select Contacts and click ASSIGN CONTACTS to assign them to the Job.</div>
		</div>
	</h2>	
	<div class="content add-contacts-table">
		@include('admin.contact.forms.attach-contacts')
	</div>
</div>

<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {
			// Set Contacts to Job
			// $( 'body' ).on( 'submit', '#attach-contacts', function(event) {
			// 	event.preventDefault();

			// 	form = $(this);

			// 	$.ajax({
			// 		type: "POST",
			// 		url: form.attr('action'),
			// 		data: $(this).serialize()
			// 	})
			// 	.done(function( data ) {
			// 		console.log(data);
			// 		if (data.status === 'success'	) {
			// 			form.removeClass('loading');
			// 			$('#job_bid .ui.message .header').html(data.message);
			// 			$('#job_bid .ui.message').addClass('success');
			// 			$('#job_bid .ui.message').show();
			// 			$(form).find('input[name=bid_id]').val(data.bid_id);			
			// 		};		
			// 	});

			// });
        });
    })(jQuery);
</script>