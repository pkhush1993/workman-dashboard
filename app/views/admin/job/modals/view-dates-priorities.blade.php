<!-- Job Defaults Modal -->
<div class="ui dates-priorities modal">

	<i class="ui close icon"></i>
	<h2 class="ui header">
		<i class="circle icon"></i>
		<div class="content">
			Job Dates and Priorities
		</div>
	</h2>
	<div class="content">
		<form action="#" class="ui form">
			<div class="two fields">
				<div class="field">
					{{ Form::label('bid_date', 'Bid Date') }}
					{{ Form::text('bid_date_modal', isset($job) ? date('m/d/Y',strtotime($job->bid_date)) : '', array('placeholder'=>'Enter Date','class'=>'date')) }}
				</div>
				<div class="field">
					{{ Form::label('estimator_deadline', 'Estimator Deadline') }}
					{{ Form::text('estimator_deadline_modal', (isset($job) && $job->estimator_deadline) ? date('m/d/Y',strtotime($job->estimator_deadline)) : '' , array('placeholder'=>'Enter Estimator Deadline','class'=>'date')) }}
				</div>
			</div>
			<div class="field">
				{{ Form::label('estimator_priority', 'Estimator Priority') }}
				{{ Form::select('estimator_priority_modal', array(
                    ''       => 'Select Priority',
                    'Low'    => 'Low',
                    'Medium' => 'Medium',
                    'High'   => 'High'
                ), isset($job) ? $job->estimator_priority :'', array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
			</div>
			<div class="field">
				{{ Form::label('production_deadline', 'Production Deadline') }}
				{{ Form::text('production_deadline_modal', (isset($job) && $job->production_deadline) ? date('m/d/Y',strtotime($job->production_deadline)): '' , array('placeholder'=>'Enter Production Deadline','class'=>'date')) }}
			</div>
			<div class="field">
				{{ Form::label('production_priority', 'Production Priority') }}
				{{ Form::select('production_priority_modal', array(
                    ''       => 'Select Priority',
                    'Low'    => 'Low',
                    'Medium' => 'Medium',
                    'High'   => 'High'
                ), isset($job) ? $job->production_priority : '' , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
			</div>
			<div class="field">
				{{ Form::label('work_completed', 'Work Completed') }}
				{{ Form::text('completed_date_modal',(isset($job) && $job->completed_date) ? date('m/d/Y',strtotime($job->completed_date)) : '', array('placeholder'=>'Work Completed','class'=>'date',)) }}
			</div>
			<a href="#" class="ui submit fluid green button save-dates">SAVE/UPDATE</a>
		</form>
	</div>
</div>



<script>
	jQuery.noConflict();
	(function( $ ) {
		$(function() {
			$('.save-dates').click(function() {
				$('input[name="bid_date"]').val($('input[name="bid_date_modal"]').val());
				$('input[name="estimator_deadline"]').val($('input[name="estimator_deadline_modal"]').val());
				$('input[name="estimator_priority"]').val($('select[name="estimator_priority_modal"]').val());
				$('input[name="production_deadline"]').val($('input[name="production_deadline_modal"]').val());
				$('input[name="production_priority"]').val($('select[name="production_priority_modal"]').val());
				$('input[name="completed_date"]').val($('input[name="completed_date_modal"]').val());

				$('.ui.dates-priorities.modal').modal('hide');
				$('.ui.dates-priorities.modal').modal('refresh');
			})
		});
	})(jQuery);
</script>
