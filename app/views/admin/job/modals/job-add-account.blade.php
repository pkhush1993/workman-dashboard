<!-- Add Account Modal -->
<div class="ui modal add-account">
	<i class="close icon"></i>
	<div class="content">
		<h2 style="margin-top:0"><i class="book icon"></i>Add New Account</h2>
		@include('admin.account.forms.add-account')
	</div>
</div>

<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {

			//Submit Button Text Update
			$('#add-account').find("input[type=submit]").val('ADD ACCOUNT');

			//Add Account Updates
			$('#add-account').submit(function(event) {
				event.preventDefault();
				
				// Hide Action Buttons
				$('.job-account').hide();
				$('.button.add-contacts-button').hide();
				$('.job-account').hide();

				// Remove Validation
				$('.account-contacts').find('.pointing.prompt').remove();
				
				// Show Action Buttons
				$('.account-contacts').show();
				$('.button.new-account').show();
				$('.button.existing-account').show();
				$('.button.use-account-address-site').show();
				$('.button.use-account-address-billing').show();

				
				//Set New Account Hidden Input for Create Logic
				$('input[name=new_account]').val('1');

				//Set Account Name In Input
				$('input[name=new_account_name]').val($('input[name=account_name]').val());
				// Set Place Holder for Assigned Job Account Until Created
				$('select[name=assigned_job_account]').attr('disabled','disabled');
				$('.new-account-name').show();

				// Set Hidden Fields with New Account/Contact Values
				$('input[name=new_account_name]').val( $('input[name=account_name]').val() );
				$('input[name=new_account_email]').val( $('input[name=account_email]').val() );
				$('input[name=new_account_phone]').val( $('input[name=account_phone]').val() );
				$('input[name=new_account_mobile]').val( $('input[name=account_mobile]').val() );
				$('input[name=new_account_fax]').val( $('input[name=account_fax]').val() );
				$('input[name=new_account_address_1]').val( $('input[name=account_address_1]').val() );
				$('input[name=new_account_address_2]').val( $('input[name=account_address_2]').val() );
				$('input[name=new_account_city]').val( $('input[name=account_city]').val() );
				$('input[name=new_account_state]').val( $('select[name=account_state]').val() );
				$('input[name=new_account_zip]').val( $('input[name=account_zip]').val() );
				$('input[name=new_contact_title]').val( $('input[name=contact_title]').val() );
				$('input[name=new_contact_first_name]').val( $('input[name=contact_first_name]').val() );
				$('input[name=new_contact_last_name]').val( $('input[name=contact_last_name]').val() );
				$('input[name=new_contact_email]').val( $('input[name=contact_email]').val() );
				$('input[name=new_contact_phone]').val( $('input[name=contact_phone]').val() );
				$('input[name=new_contact_mobile]').val( $('input[name=contact_mobile]').val() );
				$('input[name=new_contact_fax]').val( $('input[name=contact_fax]').val() );
				$('input[name=new_contact_address_1]').val( $('input[name=contact_address_1]').val() );
				$('input[name=new_contact_address_2]').val( $('input[name=contact_address_2]').val() );
				$('input[name=new_contact_city]').val( $('input[name=contact_city]').val() );
				$('input[name=new_contact_state]').val( $('select[name=contact_state]').val() );
				$('input[name=new_contact_zip]').val( $('input[name=contact_zip]').val() );

				// Set Fill Fields for Use Account Buttons
				$('input[name=set_account_address_1]').val( $('input[name=account_address_1]').val() );
				$('input[name=set_account_address_2]').val( $('input[name=account_address_2]').val() );
				$('input[name=set_account_city]').val( $('input[name=account_city]').val() );
				$('input[name=set_account_state]').val( $('select[name=account_state]').val() );
				$('input[name=set_account_zip]').val( $('input[name=account_zip]').val() );


				// Complete The Step
				$('.set-account').addClass('completed').removeClass('active');
				$('.enter-job-info').addClass('active');				

			});


        });
    })(jQuery);
</script>

				