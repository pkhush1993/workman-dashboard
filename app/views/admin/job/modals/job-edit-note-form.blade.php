{{ Form::open(array('class' => 'ui form update-job-note','route' => ['job.update.note',$note->id])) }}
    <div class="two fields">
        <div class="field">
            {{ Form::label('activity', 'Activity') }}
            {{ Form::text('activity',$note->activity, array('placeholder'=>'Enter Activity')) }}
        </div>
        <div class="field">
            {{ Form::label('type', 'Type') }}
            {{ Form::select('type', array(
                ''       => 'Select Status',
                'accounting' => 'Accounting',
                'estimating' => 'Estimating',
                'production' => 'Production',                  
            ), $note->type , array('class'=>'ui dropdown','autocomplete' =>'off')) }}
        </div>
    </div>
    <div class="field">
        {{ Form::label('contact', 'Contact') }}
        {{ Form::select('contact', $job_contacts_dropdown, $note->job_contacts_id , array('class'=>'ui dropdown','autocomplete' =>'off')) }} 
    </div>
    <div class="two fields">    
        <div class="field assigned">
            {{ Form::label('assigned', 'Assigned') }}
            {{ Form::select('assigned', $users, $note->assigned , array('class'=>'ui dropdown','autocomplete' =>'off')) }} 
        </div>
        <div class="field deadline">
            {{ Form::label('Deadline') }}
            {{ Form::text('deadline',date('m/d/Y',strtotime($note->deadline)), array('placeholder'=>'Choose a Deadline','class'=>'date')) }}
        </div>
    </div>
    <div class="field">
        {{ Form::label('note', 'Note') }}
        {{ Form::textarea('note',$note->note, array('placeholder'=>'Enter Note')) }}
    </div>            
    {{ Form::submit('UPDATE JOB NOTE',array('class' => 'ui submit green button')); }}
{{ Form::close() }}	


<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {

            //Semantic UI Form Validation For Job Notes =========================
            $('.ui.form.update-job-note').form(
                {
                    activity: {
                        identifier  : 'activity',
                        rules: [                       
                            {
                                type   : 'empty',
                                prompt : 'Activity is required'
                            }                           
                        ]
                    },
                    type: {
                        identifier  : 'type',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Type is required'
                            }                           
                        ]
                    },
                    note: {
                        identifier  : 'note',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Note is required'
                            }               
                        ]
                    }                                                                    
                },
                {
                    inline : true,
                    on     : 'blur'                                                 
                }                       
            );                        

        });
    })(jQuery);
</script>