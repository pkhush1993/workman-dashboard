<!-- View Contact Modal -->
<div class="ui large modal view-contacts">
	<i class="close icon"></i>
	<div class="content">
		<h2 style="margin-top:0"><i class="child icon"></i>Current Contacts on Job</h2>
		<div id="current-contacts">
			@include('admin.contact.forms.job-current-contacts')
		</div>
		<br>
		<div class="ui divider"></div>
		<br>
		<h2 style="margin-top:0"><i class="child icon"></i>Contacts in the {{$account_name}} Account</h2>
		<div id="existing-contacts">
			@include('admin.contact.forms.job-contacts')
		</div>
	</div>
</div>


<script>
	jQuery.noConflict();
	(function( $ ) {
		$(function() {

				//View Contact AJAX
				$('.view-contact').click(function(event) {
					event.preventDefault()
					// Get Request Contacts Id
					var contactId = $(this).data('contact-id');
					// Build URL using Route
					contactUrl = "{{route('job.contact','') }}/" + contactId;

					$.ajax({
						type: "GET",
						url: contactUrl,
						// data: html
					})
					.done(function( data ) {
						console.log(data);
						$('.ui.basic.modal.default').addClass('view-contact');
						$('.ui.basic.modal.default').find('.content').html(data);
						$('.ui.basic.modal.default').modal('show');
					});

				});
			
		});
	})(jQuery);
</script>
