{{-- Create New Job Note/Activity --}}
{{ Form::open(array('class' => 'ui form create-job-note','route' => 'job.create.note')) }}
    {{ Form::hidden('job_id',$job_id) }}
    <h3 class="ui dividing header" style="margin-top:0">Create New Job Note/Activity</h3>
    <div class="fields">
        <div class="field">
            {{ Form::label('activity', 'Job Note') }}
            {{ Form::select('activity', $activities_select, $default = '' , array('class'=>'ui dropdown multiple activity-drop','autocomplete' =>'off')) }}
        </div>
        <div class="field">
            {{ Form::label('type', 'Type') }}
            {{ Form::select('type', array(
                ''       => 'Select Status',
                'accounting' => 'Accounting',
                'estimating' => 'Estimating',
                'production' => 'Production',
            ), $default = '' , array('class'=>'ui dropdown','autocomplete' =>'off')) }}
        </div>
        <div class="field">
            {{ Form::label('note', 'Details') }}
            {{ Form::textarea('note',$value = null, array('placeholder'=>'Enter Details')) }}
        </div>
        <div class="field">
            {{ Form::label('contact', 'Contact') }}
            @if ($job_contacts_dropdown)
                {{ Form::select('contact', $job_contacts_dropdown, $default = '' , array('class'=>'ui dropdown','autocomplete' =>'off')) }}
            @else
                No Job Contacts
            @endif
        </div>
        <div class="field">
            {{ Form::label('&nbsp;') }}
            <div class="ui checkbox">
                {{ Form::label('follow-up', 'Follow Up') }}
                {{ Form::checkbox('follow-up') }}
            </div>
        </div>
        <div class="field assigned" style="display:none">
            {{ Form::label('assigned', 'Assigned') }}
            {{ Form::select('assigned', $users, $default = '' , array('class'=>'ui dropdown','autocomplete' =>'off','disabled' =>'disabled')) }}
        </div>
        <div class="field deadline" style="display:none">
            {{ Form::label('Deadline') }}
            {{ Form::text('deadline',date('m/d/Y'), array('placeholder'=>'Choose a Deadline','class'=>'date')) }}
        </div>
    </div>
    {{ Form::submit('CREATE JOB NOTE',array('class' => 'ui submit green button')); }}
    <div class="ui error message"></div>
{{ Form::close() }}
