{{-- Active Job Activities/Notes  --}}
{{ Form::open(array('class' => 'ui form job-notes','route' => 'job.remove.note')) }}
    <h3 class="ui header">Current Job Activities/Notes <a class="ui right floated red inverted tiny button view-hidden-notes" href="">View Inactive Notes</a></h3>
    @if ($job_notes->count())
        <table class="ui celled table">
            <thead>
                <tr>
                  <th class="one wide center aligned"></th>
                  <th>Job Note</th>
                  <th>Type</th>
                  <th>Details</th>
                  <th>Contact</th>
                  <th>Assigned</th>
                  <th>History</th>
                  <th class="one wide center aligned">Follow Up</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($job_notes as $job_note)
                    <tr>
                        <td class="center aligned">
                            <div class="ui checkbox selected-note">{{ Form::checkbox('selected_notes[]',$job_note->id, false) }}</div>
                        </td>
                        <td><a href="" class="edit-note" data-job-id="{{$job_id}}" data-note-id="{{ $job_note->id }}">{{$job_note->activity}}</a></td>
                        <td>{{strtoupper($job_note->type)}}</td>
                        <td>{{$job_note->note}}</td>
                        <td>
                            @if ($job_note->job_contacts_id)
                                <a href="" class="view-job-contact" data-contact-id="{{$job_note->job_contacts_id}}">{{$job_note->contact_profile()->first_name}} {{$job_note->contact_profile()->last_name}}</a>
                            @else
                                 <i class="large red remove icon"></i>
                            @endif
                       </td>
                        <td>
                            @if ($job_note->assigned)
                                {{$job_note->user_profile()->first_name}} {{$job_note->user_profile()->last_name}}
                            @else
                                <i class="large red remove icon"></i>
                            @endif
                        </td>    
                        <td>
                            Created On: {{date('m-d-Y',strtotime($job_note->created_at))}}  by {{$job_note->created_profile()->first_name}}  {{$job_note->created_profile()->last_name}}
                        </td>                            
                        <td class="center aligned">
                            @if ($job_note->follow_up)
                                <i class="large green checkmark icon"></i>
                            @else
                                <i class="large red remove icon"></i>
                            @endif                                
                        </td>
                    </tr>
                @endforeach 
            </tbody>
        </table>
    @else
        <div class="ui large red message">
            There Are Currently No Notes/Activities on this Job
        </div>
    @endif
    {{ Form::submit('CHANGE SELECTED TO INACTIVE',array('class' => 'ui submit hide-note red button','style' => 'display:none')); }}
{{ Form::close() }}