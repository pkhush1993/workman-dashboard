<!-- Select the Type of Bid to Create/View -->
<div class="ui bid-type modal">
    <i class="ui close icon"></i>
	<h2 class="ui header">
		<i class="circle icon"></i>
		<div class="content">
			Bid Type
			<div class="sub header">Select the Type of Bid to Create/View</div>
		</div>
	</h2>
	<div class="content">
        <div class="ui grid three column">
            <div class="column">
                <a target="_blank" href="{{ route('job.invoice', ['grand-total',$job->id]) }}" class="ui blue fluid view-print-bid button">Grand Total</a>
            </div>
            <div class="column">
                <a target="_blank" href="{{ route('job.invoice', ['category-subtotals',$job->id]) }}" class="ui blue view-print-bid fluid button">Category Totals</a>
            </div>
            <div class="column">
                <a target="_blank" href="{{ route('job.invoice', ['item-totals',$job->id]) }}" class="ui blue fluid view-print-bid button">Line Item Totals</a>
            </div>            
        </div>
	</div>
</div>


<!-- =================================================== -->

<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {
            $('.view-print-bid').click(function(event) {
                $('.ui.modal.bid-type').modal("hide");
                location.reload();
            }); 
            $('.view-print-bid .ui.close.icon').click(function(event) {
                $('.ui.modal.bid-type').modal("hide");
            });

        });
    })(jQuery);
</script>                