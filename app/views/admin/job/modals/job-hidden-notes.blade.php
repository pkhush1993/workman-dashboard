<div id="hidden-notes" style="display:none">
    <h3 style="margin-top:0">Inactive Notes</h3>
    @if ($hidden_notes->isEmpty() )
        <div class="ui red message">
            There Are Currently No Inactive Notes
        </div>
    @else
        {{ Form::open(array('class' => 'ui form','route' => 'job.restore.notes')) }}
                <table class="ui celled table">
                    <thead>
                          <tr>
                              <th class="one wide center aligned"></th>
                              <th>Job Note</th>
                              <th>Type</th>
                              <th>Details</th>
                              <th>Contact</th>
                              <th>Assigned</th>
                              <th class="one wide center aligned">Follow Up</th>
                          </tr>
                    </thead>
                    <tbody>
                        @foreach ($hidden_notes as $hidden_note)
                            <tr>
                                <td class="center aligned">
                                    <div class="ui checkbox selected-hidden-note">{{ Form::checkbox('selected_notes[]',$hidden_note->id, false) }}</div>
                                </td>
                                <td>{{$hidden_note->activity}}</td>
                                <td>{{strtoupper($hidden_note->type)}}</td>
                                <td>{{$hidden_note->note}}</td>
                                <td>
                                    @if ($hidden_note->job_contacts_id)
                                        <a href="" class="view-job-contact" data-contact-id="{{$hidden_note->job_contacts_id}}">{{$hidden_note->contact_profile()->first_name}} {{$hidden_note->contact_profile()->last_name}}</a>
                                    @else
                                         <i class="large red remove icon"></i>
                                    @endif
                               </td>
                                <td>
                                    @if ($hidden_note->assigned)
                                        {{$hidden_note->user_profile()->first_name}} {{$hidden_note->user_profile()->last_name}}</td>
                                    @else
                                        <i class="large red remove icon"></i>
                                    @endif
                                <td class="center aligned">
                                    @if ($hidden_note->follow_up)
                                        <i class="large green checkmark icon"></i>
                                    @else
                                        <i class="large red remove icon"></i>
                                    @endif                                
                                </td>
                            </tr>
                        @endforeach 
                    </tbody>
                </table>
            {{ Form::submit('SHOW NOTE',array('class' => 'ui submit restore-note tiny button','style' => 'display:none;')); }}
        {{ Form::close() }}    
    @endif   
</div>

<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {

            $('.selected-hidden-note').click(function(event) {
                $('input.restore-note').show();
            });

        });
    })(jQuery);
</script>