<!-- Add Account Modal -->
<div class="ui choose-account basic modal">
	<div class="content">
		<h2>Search Account</h2>
		<br>
		{{ Form::label('account', 'Account') }}
		<div class="assigned-job-account">
			<div class="ui search account-ajax-search">
				{{ Form::text('set_job_account_name',NULL, array('placeholder'=>'Search for Account...','class'=>'prompt ui fluid search icon job-account-search','style'=>'border-radius: 4px;width: 100%;margin-top: 10px;')) }}
				{{ Form::hidden('set_job_account',NULL, array('data-validate'=>'set_job_account','class'=>'job-account account-id')) }}
				<div class="results"></div>
			</div>
		</div>

		{{-- {{ Form::select('set_job_account', $accounts_dropdowns, $value = null , array('class'=>'ui fluid search dropdown','autocomplete' =>'off')) }} --}}
		<br>
		<br>
		<br>
		<div class="two fluid ui buttons">
			<div class="set-job-account ui large inverted green close button ">OK</div>
			<div class="existing-account ui large inverted red close button  ">CANCEL</div>
		</div>
	</div>
</div>


<script>
	jQuery.noConflict();
	(function( $ ) {
		$(function() {


			//Semantic UI AJAX Search =========================
			$('.ui.account-ajax-search')
			  .search({
				apiSettings: {
					url: '{{url("accounts-details-return/get/?key=")}}' + '{query}',
//					debug: true,
//					verbose: true
				},
				onSelect(result) {
					$(this).closest('.account-ajax-search').find('.account-id').val(result.account_id);
//					console.log(result);
//					console.log(url);
				}

			  });

			// Set Job Contacts Depending on Specified Account in PopUp
			$('.set-job-account').click(function(event) {
				event.preventDefault();

				// Re-Enable Assigned Job Account Field
				$('select[name=assigned_job_account]').removeAttr('disabled');

				// Hide Set Button
				$('.job-account').hide();

				//Unset New Account Hidden Input To False for Job Create Logic
				$('input[name=new_account]').val('0');

				var setJobAccountId = $('input[name=set_job_account]').val();
				var setJobAccountName = $('input[name=set_job_account_name]').val();

				//Setup Account Select Dropdown
				$('select[name=assigned_job_account]').append('<option value="'+setJobAccountId+'">'+setJobAccountName+'</option>');
				$('select[name=assigned_job_account]').append('<option value="new_account">+ Use New Account</option>');
				$('select[name=assigned_job_account]').addClass('dropdown');
				// Set Semantic UI Drop Down
				$('select[name=assigned_job_account]').dropdown('set selected', setJobAccountId );
				//Set Select Value
				$('select[name=assigned_job_account]').val(setJobAccountId);

				// Show Account Action Buttons
				$('.account-contacts').show();
				$('.use-account-address').show();
				$('.button.add-contacts-button').show();
				$('.button.existing-account').show();
				$('.assigned_job_account').show();
				$('.button.use-account-address-site').show();
				$('.button.use-account-address-billing').show();

				// Hide Account Action Buttons
				$('.new-account-name').hide();
				$('.button.new-account').hide();
				$('.button.existing-account').hide();

				// Get Request Contacts Id
				var accountId = $('select[name=set_job_account]').val();

				// Build URLs using Routes
				contactsUrl = "{{route('job.contacts','') }}/" + accountId;
				addressUrl = "{{route('account.address','') }}/" + accountId;

				// Set Contacts depending on Account Chosen - AJAX
				$.ajax({
					type: "GET",
					url: contactsUrl,
				})
				.done(function( data ) {
					$('.ui.basic.modal.add-contacts').find('.content').html(data);
					$('.ui.basic.modal.add-contacts .ui.checkbox').checkbox();
					//Complete the Step
					$('.set-account').addClass('completed').removeClass('active');
					$('.enter-job-info').addClass('active');
				});
				// Set Address depending on Account Chosen - AJAX
				$.ajax({
					type: "GET",
					url: addressUrl,
				})
				.done(function( data ) {
					console.log(data);
					// Set Fill Fields for Use Account Buttons
					$('input[name=set_account_address_1]').val( data.address_1 );
					$('input[name=set_account_address_2]').val( data.address_2 );
					$('input[name=set_account_city]').val( data.city);
					$('input[name=set_account_state]').val( data.state );
					$('input[name=set_account_zip]').val( data.zip );
				});

			});

		});
	})(jQuery);
</script>
