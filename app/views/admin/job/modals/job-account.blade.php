<!-- Add Account Modal -->
<div class="ui basic modal add-account">
	<i class="close icon"></i>
	<div class="content">
		<h2><i class="book icon"></i>Add New Account</h2>
		@include('admin.account.forms.account')
	</div>
</div>