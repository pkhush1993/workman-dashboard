<!-- Add Account Modal -->
<div class="ui add-contacts modal">
	<i class="ui close icon"></i>
	<h2 class="ui header">
		<i class="child icon"></i>
		<div class="content">
			Add Contacts to New Job
			<div class="sub header">Add Contacts to attach them to the New Job.</div>
		</div>
	</h2>	
	<div class="content add-contacts-table">
		@include('admin.contact.forms.add-contacts')
	</div>
</div>
<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {

			// Set Contacts to Job
			$( 'body' ).on( 'submit', '#add-contacts', function(event) {
				event.preventDefault();
				
				//Clear Values Out of Hidden Field
				$('input[name=job_contacts]').val('');

				//Get Checked Checkboxes and Create a String Separated by Commas for Create Job Controller
				contacts = $('input[name=add_contact]:checked').map(function() {
					return this.value;
				}).get().join(',');

				//Add the Contact ID's to Hidden Fields for Controller Logic
				$('input[name=job_contacts]').val(contacts);

				//Force Hiding Modal
				$('.ui.modal.add-contacts').modal('hide');
			});

        });
    })(jQuery);
</script>