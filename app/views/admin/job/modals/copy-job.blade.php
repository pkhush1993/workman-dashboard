<!-- Add Account Modal -->
<div class="ui copy-job  modal">
    <h2 class="ui header">
        <i class="newspaper icon"></i>
        <div class="content">
            Copy Job
            <div class="sub header">Create additional copies of this job</div>
        </div>
    </h2>
    <div class="content">
        <form action="{{ route('job.copy.job', ['job_id' => $job->getKey()]) }}" method="post" class="ui form">

            <div class="inline fields">
                <div class="field">
                    <div class="ui radio checkbox">
                        <input class="copy-custom" type="radio" name="copy">
                        <label>Custom</label>
                    </div>
                </div>
                <div class="field">
                    <div class="ui radio checkbox">
                        <input class="copy-everything" type="radio" name="copy" checked="checked">
                        <label>Everything</label>
                    </div>
                </div>
            </div>
            <hr>
            <div class="ui stackable grid AllCustomeOptions">
                <div class="row">
                    <div class="five wide column">
                        <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" class="CopyOptions" name="account">
                                <label>Account</label>
                            </div>
                        </div>
                         <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" class="CopyOptions" name="contacts">
                                <label>Contacts</label>
                            </div>
                         </div>
                         <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" class="CopyOptions" name="primary_contact">
                                <label>Primary Contact</label>
                            </div>
                         </div>
                         <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" class="CopyOptions" name="bid_date">
                                <label>Bid Date</label>
                            </div>
                         </div>
                         <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" class="CopyOptions" name="status">
                                <label>Status</label>
                            </div>
                         </div>
                         <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" class="CopyOptions" name="site_address">
                                <label>Site Address</label>
                            </div>
                         </div>
                         <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" class="CopyOptions" name="billing_address">
                                <label>Billing Address</label>
                            </div>
                         </div>
                         <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" class="CopyOptions" name="bid_line_items">
                                <label>Line Items</label>
                            </div>
                         </div>
                         <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" class="CopyOptions" name="bid_comments">
                                <label>Comments</label>
                            </div>
                         </div>
                         <div class="field">
                                <label>Notes</label>
                         </div>
                         <div class="field">
                            <div style="margin-left: 15px" class="ui checkbox">
                                <input type="checkbox" class="CopyOptions" name="notes_estimating">
                                <label>Estimating</label>
                            </div>
                         </div>
                         <div class="field">
                            <div style="margin-left: 15px" class="ui checkbox">
                                <input type="checkbox" class="CopyOptions" name="notes_project">
                                <label>Project</label>
                            </div>
                         </div>
                         <div class="field">
                            <div style="margin-left: 15px" class="ui checkbox">
                                <input type="checkbox" class="CopyOptions" name="notes_accounting">
                                <label>Accounting</label>
                            </div>
                         </div>
                    </div>

                    <div class="five wide column">
                        <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" class="CopyOptions" name="estimator_deadline">
                                <label>Estimator Deadline</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" class="CopyOptions" name="estimator_priority">
                                <label>Estimator Priority</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" class="CopyOptions" name="production_deadline">
                                <label>Production Deadline</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" class="CopyOptions" name="production_priority">
                                <label>Production Priority</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" class="CopyOptions" name="completed_date">
                                <label>Work Complete (Date)</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" class="CopyOptions" name="estimator">
                                <label>Estimator</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" class="CopyOptions" name="project_manager">
                                <label>Project Manager</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" class="CopyOptions" name="accounting">
                                <label>Accountant</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" class="CopyOptions" name="primary_contact">
                                <label>Prime Contract</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" class="CopyOptions" name="lead_source">
                                <label>Lead Source</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" class="CopyOptions" name="tax_rate">
                                <label>Sales Tax Rate</label>
                            </div>
                        </div>

                    </div>

                    <div class="five wide column">
                        <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" class="CopyOptions" name="foreman">
                                <label>Foreman</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" class="CopyOptions" name="custom_drop_1">
                                <label>Custom Text Dropdown 1</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" class="CopyOptions" name="custom_drop_2">
                                <label>Custom Dropdown Field 2</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" class="CopyOptions" name="custom_drop_3">
                                <label>Custom Dropdown Field 3</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" class="CopyOptions" name="custom_text_1">
                                <label>Custom Text Field 1</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" class="CopyOptions" name="custom_text_2">
                                <label>Custom Text Field 2</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" class="CopyOptions" name="custom_text_3">
                                <label>Custom Text Field 3</label>
                            </div>
                        </div>
                        <div class="two fields">
                            <div class="field">
                                <input type="text" name="quantity" value="1">
                            </div>
                            <div class="field">
                                <input type="submit" value="Copy Job" class="ui green fluid button submit">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {

            $("input.copy-custom").click(function(){

                $(".AllCustomeOptions").find("input.CopyOptions").prop( "checked", true );
            });
            $(document).find("input.copy-custom");

        });
    })(jQuery);
</script>
