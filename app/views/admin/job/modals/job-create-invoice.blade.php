<!-- Create Invoice Modal -->
<div class="ui create-invoice modal">
	<h2 class="ui header">
		<i class="dollar icon"></i>
		<div class="content">
			Create Invoice
			<div class="sub header">Choose an Invoice Number to Create Invoice</div>
		</div>
	</h2>
	<div class="content">
		{{ Form::open(array('class' => 'ui large form create-invoice-form','target'=>'_blank','route' => ['job.invoice.create', $job_id] )) }}	
			{{-- =================================================================== --}}
            {{ Form::hidden('invoice_previous_payment',$value = null) }}
			{{ Form::hidden('invoice_partial_payment_amount',$value = null) }}
			{{ Form::hidden('invoice_type',$value = null,array('disabled'=>'disabled')) }}
			{{ Form::hidden('invoice_tax_rate',$value = null) }}
			{{ Form::hidden('invoice_grand_total',$value = null) }}
            {{ Form::hidden('is_down_payment', 0) }}
			{{-- =================================================================== --}}
			<div class="field">
				{{ Form::label('invoice_number', 'Invoice Number') }}
				{{ Form::text('invoice_number',$invoice_number, array('placeholder'=>'Enter Invoice Number')) }}
			</div>
			<br>
			<div class="ui mini inline field" style="margin-bottom:0;border-bottom:1px solid #dcdcdc">
				{{ Form::label('invoice_subtotal', 'Invoice Subtotal') }}
				<div class="ui left icon input">
					<i class="ui dollar icon"></i>
					{{ Form::text('invoice_subtotal', '',array('placeholder' => '0.00','readonly' => 'readonly','style' => 'border: none;font-weight: bold;background: transparent;') ) }}
				</div>
			</div>
			<div class="ui mini inline field" style="border-bottom:1px solid #dcdcdc">
				{{ Form::label('invoice_sales_tax_total', 'Total Taxes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;') }}
				<div class="ui left icon input">
					<i class="ui dollar icon"></i>
					{{ Form::text('invoice_sales_tax_total', '',array('placeholder' => '0.00','readonly' => 'readonly','style' => 'border: none;font-weight: bold;background: transparent;') ) }}
				</div>
			</div>
			<h3 style="margin-top:0;font-size:17px">Invoice Balance:&nbsp;&nbsp;$ <span class="invoice-final-balance">&nbsp;</span></h3>

			<div class="ui divider"></div>

			<h5>Select Invoice Type:</h5>

            <div class="inline field">
                <div class="ui radio checkbox">
                    {{ Form::radio('invoice_type',$value = 'grand-total', true) }}
                    {{ Form::label('grand-total', 'Grand Total') }}
                </div>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <div class="ui radio checkbox">
                    {{ Form::radio('invoice_type',$value = 'category-subtotals', false) }}
                    {{ Form::label('name', 'Category Totals') }}
                </div>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <div class="ui radio checkbox">
                    {{ Form::radio('invoice_type',$value = 'item-totals', false) }}
                    {{ Form::label('name', 'Line Item Totals') }}
                </div>                
            </div>

			{{ Form::submit('CREATE INVOICE',array('class' => 'ui fluid green view-print-invoice button')); }}
		{{ Form::close() }}
	</div>
</div>


<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {



        	// Refresh Page to Show to Update the Job with the Newly Created Invoice
            $('.view-print-invoice').click(function(event) {
                event.preventDefault();
                $('.ui.modal.create-invoice').modal("hide");
                $('.create-invoice-form').submit();
                setTimeout(function(){
                    window.location.href=window.location.href + "?fromImport=true";
                },100);
            });

            //Semantic UI Form Validation Job - Create Invoice  =========================
            $('.ui.form.create-invoice-form').form(
                {
                    invoiceNumber: {
                        identifier  : 'invoice_number',
                        rules: [                       
                            {
                                type   : 'integer[1..]',
                                prompt : 'Must be a number'
                            },
							{
								type   : 'empty',
								prompt : 'Invoice Number Required'
							},
                        ]
                    },    
                    invoiceType: {
                        identifier  : 'invoice_type',
                        rules: [                       
                            {
                                type   : 'checked',
                                prompt : 'Specify the type of invoice that you would like to create'
                            },                          
                        ]                    
                    }                                                             
                },
                {
	                inline : true,
	                on     : 'blur',
                    // onSuccess: function () {
                    //    alert('asdfasdf');
                    // }                     
                }                   
            );






        });
    })(jQuery);
</script>

