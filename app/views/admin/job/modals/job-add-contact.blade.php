<!-- Add Contact Modal -->
<div class="ui modal add-contact">
	<i class="ui close icon"></i>
	<h2 class="ui header">
		<i class="child icon"></i>
		<div class="content">
			Create New Contact
		</div>
	</h2>	
	<div class="content">
		@include('admin.contact.forms.add-contact')
	</div>
</div>


<script>
	jQuery.noConflict();
	(function( $ ) {
		$(function() {

		var formAction = $('#add-contact form').attr('action')+'/{{ $job_id }}'
		
		$('#add-contact form').attr('action',formAction);

		});
	})(jQuery);
</script>
