<!-- Rework Modal -->
<div class="ui job-rework fullscreen modal">
	<i class="ui close icon"></i>
	<h2 class="ui header">
		<i class="history icon"></i>
		<div class="content">
			Rework
			<div class="sub header">Manage your Rework Items Attached to this Job.</div>
		</div>
	</h2>
	<div class="content">
        @if ( Session::get('success') AND Session::get('pop') == "job-rework" )
            <div class="ui positive message">
                <i class="close icon"></i>
                <div class="header">
                    {{ Session::get('success') }}
                </div>
            </div>      
        @endif     

        {{-- Create New Job Rework --}}
        {{ Form::open(array('class' => 'ui form create-job-rework','route' => ['job.create.rework',$job_id])) }}
            <h3 class="ui dividing header" style="margin-top:0">Create New Job Rework</h3>
            <div class="four fields">
                <div class="two wide field">
                    {{ Form::label('number', 'Rework Number') }}
                    {{ Form::number('number',$next_rework_number, array('placeholder'=>'Enter Number','readonly'=>'readonly')) }}
                </div>
                <div class="six wide field">
                    {{ Form::label('description', 'Enter Description') }}
                    {{ Form::textarea('description',$value = null, array('placeholder'=>'Enter Description','style'=>'height: 38px;min-height: 0','rows'=>'5')) }}
                </div>
                <div class="field">
                    {{ Form::label('deadline', 'Enter Deadline') }}
                    {{ Form::text('deadline',$value = null, array('placeholder'=>'Enter Deadline','class'=>'date')) }}
                </div>
                <div class="field">
                    {{ Form::label('status', 'Current Status') }}
                    {{ Form::select('status', array(
                        ''       => 'Select Status',
                        'active' => 'Open',
                        'closed' => 'Closed'
                    ), $default = '' , array('class'=>'ui dropdown','autocomplete' =>'off')) }} 
                </div>
            </div>
            {{ Form::submit('CREATE JOB REWORK',array('class' => 'ui submit green button')); }}
            <div class="ui error message"></div>
        {{ Form::close() }}

        <div class="ui divider"></div>

        {{-- Current Job Rework --}}
        {{ Form::open(array('class' => 'ui form job-edit-rework','route' => 'job.delete.rework')) }}
            <h3 class="ui header">Current Job Rework(s)</h3>
            <table class="ui padded celled table">
                <thead>
                      <tr>
                          <th class="one wide center aligned"></th>
                          <th class="one wide center aligned">#</th>
                          <th>Description</th>
                          <th>Deadline</th>
                          <th>History</th>
                          <th>Status</th>
                      </tr>
                </thead>
                <tbody>
                    @foreach ($rework_items as $rework_item)
                        <tr>
                            <td class="center aligned">
                                <div class="ui checkbox selected_rework">{{ Form::checkbox('selected_rework[]',$rework_item->id, false) }}</div>
                            </td>
                            <td class="center aligned">{{$rework_item->number}}</td>
                            <td>{{$rework_item->description}}</td>
                            <td>{{date('Y-m-d',strtotime($rework_item->deadline))}}</td>
                             <td>Opened On: {{date('Y-m-d',strtotime($rework_item->created_at))}}  by {{$rework_item->profile()->first_name}}  {{$rework_item->profile()->last_name}}</td>
                            <td>
                                @if ($rework_item->status == 'active')
                                    <i class="red circle icon"></i> Open
                                @else
                                    <i class="green circle icon"></i> Closed
                                @endif
                            </td>
                        </tr>
                    @endforeach 
                </tbody>
            </table>

            <div class="ui small buttons">
                <div class="ui open-rework red button">OPEN</div>
                <div class="or"></div>
                <div class="ui complete-rework green button">CLOSE</div>
            </div>            
            <div class="ui right floated remove-rework icon button"><i class="ui icon trash"></i>DELETE REWORK</div>
        {{ Form::close() }}
	</div>
</div>


<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {

            //Remove Rework
            $('.remove-rework').click(function(event) {
                event.preventDefault();
                var form = $('form.job-edit-rework');
                // Update the Forms Route Depending on the Button Clicked
                $(form).attr('action', '{{route("job.delete.rework")}}' );
                // Submit Form
                $(form).submit();
            });

            //Complete Rework
            $('.complete-rework').click(function(event) {
                event.preventDefault();
                var form = $('form.job-edit-rework');
                // Update the Forms Route Depending on the Button Clicked
                $(form).attr('action', '{{route("job.complete.rework")}}' );
                // Submit Form
                $(form).submit();
            });

            //ReOpen Rework
            $('.open-rework').click(function(event) {
                event.preventDefault();
                var form = $('form.job-edit-rework');
                // Update the Forms Route Depending on the Button Clicked
                $(form).attr('action', '{{route("job.open.rework")}}' );
                // Submit Form
                $(form).submit();
            });            

            //Semantic UI Form Validation For Company Settings =========================
            $('.ui.form.create-job-rework').form(
                {
                    reworkNumber: {
                        identifier  : 'number',
                        rules: [                         
                            {
                                type   : 'empty',
                                prompt : 'Rework Number is required'
                            }                           
                        ]
                    },
                    reworkDescription: {
                        identifier  : 'description',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Rework Description is required'
                            }                           
                        ]
                    },
                    reworkDeadline: {
                        identifier  : 'deadline',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Rework Deadline is required'
                            }               
                        ]
                    },
                    reworkStatus: {
                        identifier  : 'status',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Rework Status is required'
                            }                     
                        ]
                    }          
                }       
            );

           
        });
    })(jQuery);
</script>