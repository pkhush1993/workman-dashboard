<!-- Invoice Partial Payment Amount Modal -->
<div class="ui invoice-partial-payment-amount modal">
	<h2 class="ui header">
		<i class="circle icon"></i>
		<div class="content">
			Payment Amount
			<div class="sub header">Enter Partial Payment Amount</div>
		</div>
	</h2>
	<div class="content">
		{{ Form::open(array('class' => 'ui large form partial-payment-form' )) }}	
			<div class="ui large left labeled input field">
				<div class="ui label" style="font-size: 19px;"><b>%</b></div>
				{{ Form::text('partial_payment_amount',$default = NULL, array('placeholder'=>'Enter Percent','class' => 'partial-payment-amount percent')) }}
			</div>
			<h3>TOTAL: <span class="partial-payment-total" style="color: #3D9E54;font-size: 22px;">&nbsp;</span></h3>
			<br>
			{{ Form::submit('SUBMIT AMOUNT',array('class' => 'ui fluid green button create-invoice-button')); }}
		{{ Form::close() }}
	</div>
</div>

<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {

			$('body').on('input propertychange','.partial-payment-amount', function () {
				// Set and Calculate Variables
				bidSubTotal = $('#bid_sub_total').val();
				percent = parseFloat($(this).val()/100);
				partialAmount = bidSubTotal * percent;
				// Show Amount Totaled by Entered Percent
			 	$('.partial-payment-total').text('$ ' + partialAmount.toFixed(2));
			 	// Set Amount Totaled by Entered Percent to Create Invoice Hidden Input
			 	$('input[name="invoice_subtotal"]').val(partialAmount.toFixed(2));

			 });

            //Semantic UI Form Validation For Job Notes =========================
            $('.ui.form.partial-payment-form').form(
                {
                    partialPayment: {
                        identifier  : 'partial_payment_amount',
                        rules: [                       
                            {
                                type   : 'integer[1..99]',
                                prompt : 'Enter a percent that is 1-99'
                            },
							{
								type   : 'empty',
								prompt : 'Percent Number is required'
							},                                                      
                        ]
                    }                                                                 
                },
                {
	                inline : true,
	                on     : 'blur',
	                onSuccess: function(event) {
						event.preventDefault();
						// Set Hidden Fields on Create Invoice Form
	                   	$('input[name=invoice_partial_payment_amount]').val($('.partial-payment-amount').val());
	                   	$('input[name=is_down_payment]').val('1');
	                   	$('input[name=invoice_tax_rate]').val($('input[name=bid_tax_amount]').val());

						//Get Tax Rate
						taxRate = parseFloat($('input[name=bid_tax_amount]').val()/100);
						//Get Sub Total
						subTotal = parseFloat($('input[name="invoice_subtotal"').val());

						//Only Calculate Taxes if they exist
						if (taxRate) {

							taxAmount = subTotal * taxRate;
					 		grandTotal = subTotal + taxAmount;
							
							// Set Hidden Inputs
							$('input[name=invoice_sales_tax_total]').val(taxAmount.toFixed(2));
					 		$('input[name=invoice_grand_total]').val(grandTotal.toFixed(2));
			 				// Set Amount Totaled by Entered Percent to Create Invoice Balance Display
							$('.invoice-final-balance').text(grandTotal.toFixed(2));
						}

						// Once the Create Invoice Form Is Setup Show It :)
						$('.ui.modal.create-invoice').modal('show');	                   
	                }    
                }                   
            );
        });
    })(jQuery);
</script>