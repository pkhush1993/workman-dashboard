<!-- View Contact Modal -->
<div class="ui large modal view-contacts">
	<i class="ui close icon"></i>
	<h2 class="ui header">
		<i class="child icon"></i>
		<div class="content">
			Current Contacts on Job
		</div>
	</h2>
	<div class="content">
        @if ( Session::get('success') AND Session::get('pop') == "view-contacts" )
            <div class="ui positive message">
                <i class="close icon"></i>
                <div class="header">
                    {{ Session::get('success') }}
                </div>
            </div>      
        @endif   	
		<div id="current-contacts">
			@include('admin.contact.forms.job-current-contacts')
		</div>
		<div id="inactive-contacts">
			@include('admin.contact.forms.job-inactive-contacts')
		</div>
		<div class="ui divider"></div>
	    <a href="{{ route('add.contact') }}" class="ui labeled icon green attach-contacts-button button">
	    	<i class="plus icon"></i>
	    	ASSIGN CONTACTS TO JOB
	    </a>
	    <div class="ui labeled icon small blue button right floated add-contact-button">
	    	<i class="child icon"></i>
	    	CREATE NEW CONTACT
	    </div>	    		
	</div>
</div>

<script>
	jQuery.noConflict();
	(function( $ ) {
		$(function() {

				//View Contact AJAX
				$('.view-job-contact').click(function(event) {
					event.preventDefault()
					// Get Request Contacts Id
					var contactId = $(this).data('contact-id');
					// Build URL using Route
					contactUrl = "{{route('job.contact','') }}/" + contactId;

					$.ajax({
						type: "GET",
						url: contactUrl,
						// data: html
					})
					.done(function( data ) {
						console.log(data);
						$('.modal.view-contact > .content').html(data);
						$('.modal.view-contact .dropdown').dropdown();
						$('.modal.view-contact').modal('show');
					});

				});
				$('.view-inactive-job-contacts').click(function(event) {
					event.preventDefault();
					$('#inactive-contacts-form').slideToggle();
				});


		});
	})(jQuery);
</script>
