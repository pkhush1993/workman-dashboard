<!-- Job Activities/Notes Modal -->
<div class="ui job-notes fullscreen modal">
	<i class="ui close icon"></i>
	<h2 class="ui header">
		<i class="comments icon"></i>
		<div class="content">
			ACTIVITIES AND NOTES
			<div class="sub header">Manage/View Activities and Notes for this Job.</div>
		</div>
	</h2>
	<div class="content">
        @if ( Session::get('success') AND Session::get('pop') == "job-notes" )
            <div class="ui positive message">
                <i class="close icon"></i>
                <div class="header">
                    {{ Session::get('success') }}
                </div>
            </div>
        @endif

        {{-- Create New Job Note/Activity --}}
        @include('admin.job.modals.job-create-note')

        {{-- Active Job Activities/Notes  --}}
        @include('admin.job.modals.job-active-notes')

        {{-- Job Hidden Note --}}
        @include('admin.job.modals.job-hidden-notes')

	</div>
</div>


<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {

            $('.selected-note').click(function(event) {
                $('input.hide-note').show();
            });

            $('input[name=follow-up]').change(function(){
                if(this.checked){
                    $('.deadline').show();
					$('.assigned').show().find('select').removeAttr('disabled');
                    $('.assigned').find('input').removeAttr('disabled');
                }
                else{
                    $('.deadline').hide();
                    $('.assigned').hide().attr('disabled', 'disabled');;
                }
            });

            // Handle Custom Activity Options
            $('body').on('change', '.activity-drop', function(e){
                activity = $(this).dropdown('get value');
                if(activity === 'custom'){
                    $(this).closest('.field').html('<label for="activity">Activity</label><input type="text" name="activity_custom" placeholder="Enter Activity">');
                    $(this).remove();
                }
            });

            //Semantic UI Form Validation For Job Notes =========================
            $('.ui.form.create-job-note').form(
                {
                    activity: {
                        identifier  : 'activity',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Activity is required'
                            }
                        ]
                    },
                    type: {
                        identifier  : 'type',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Type is required'
                            }
                        ]
                    },
                    note: {
                        identifier  : 'note',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Note is required'
                            }
                        ]
                    },
                    assigned: {
                        identifier  : 'assigned',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'You must assign the follow-up to a user'
                            }
                        ]
                    }
                }
            );

            //Edit Note AJAX
            $('.edit-note').click(function(event) {
                event.preventDefault()
                // Get Request Contacts Id
                var noteId = $(this).data('note-id');
                var jobId = $(this).data('job-id');

                $.ajax({
                    type: "GET",
                    url: "{{route('job.edit.note','') }}/" + jobId +"/"+ noteId,
                })
                .done(function( data ) {
                    console.log(data);
                    $('.modal.job-edit-note > .content').html(data);
                    $('.modal.job-edit-note .dropdown').dropdown();
                    $('.modal.job-edit-note').modal('show');
                });

            });

            // Show Hidden Notes
            $('.view-hidden-notes').click(function(event) {
                event.preventDefault();
                $('#hidden-notes').slideToggle(function() {
                    $('.modal.job-notes').modal('refresh');
                });
            });


        });
    })(jQuery);
</script>
