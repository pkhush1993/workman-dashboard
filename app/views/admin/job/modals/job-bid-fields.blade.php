<!-- Job Defaults Modal -->
<div class="ui bid-fields fullscreen modal">

	<i class="ui close icon"></i>
	<h2 class="ui header">
		<i class="circle icon"></i>
		<div class="content">
			Job Bid
			<div class="sub header">Manage your Job's Bid and the Bid Comments for <b>Job #{{$job->getJobNumber()}} - {{$job->getName()}}</b>.</div>
		</div>
	</h2>
	<div class="content">

        @if ( Session::get('success') AND Session::get('pop') == "bid-fields" )
            <div class="ui positive message">
                <i class="close icon"></i>
                <div class="header">
                    {{ Session::get('success') }}
                </div>
            </div>      
        @endif

		<div id="manage_job_bids">@include('admin.job.includes.manage-job-bid')</div>
		
		<div class="ui divider"></div>

		<!-- Bid Comments -->
		@include('admin.job.includes.bid-comments')


		<div class="ui divider"></div>

		<!-- Past Invoices-->
		@if ($bid_line_items )
			@include('admin.job.includes.past-invoices')
		@endif

	</div>
</div>




