<!-- Invoice Previous Payments Modal -->
<div class="ui previous-payments modal">
	<h2 class="ui header">
		<i class="circle icon"></i>
		<div class="content">
			Invoice Adjustments
			<div class="sub header">If you would like to adjust the invoice total enter the amount to subtract from the total invoice</div>
		</div>
	</h2>
	<div class="content">
		{{ Form::open(array('class' => 'ui large form previous-payments-form' )) }}	
			<div class="ui large left icon input">
				<i class="ui red minus icon"></i>
				{{ Form::text('previous_payment',$default = NULL, array('placeholder'=>'- _ _._ _','class' => 'money','style' => 'color:red')) }}
			</div>	
			<div class="ui divider"></div>
			<h3>TOTAL: <span class="adjusted-payment-total" style="color: #3D9E54;font-size: 22px;">&nbsp;</span></h3>	
			<br><br>
			<div class="ui left floated create-full-invoice-button green button">NO ADJUSTMENTS</div>
			<div class="ui previous-payment-create-invoice-button red button">SUBMIT ADJUSTMENTS</div>
		{{ Form::close() }}
	</div>			
</div>


<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {

        	$('.create-full-invoice-button').click(function(event) {	

				event.preventDefault();  	

				// Set Hidden Fields on Create Invoice Form
	           	$('input[name=invoice_previous_payment]').val(0);
	           	$('input[name=invoice_partial_payment_amount]').val(0);
	           	$('input[name=invoice_subtotal').val($('input[name=bid_sub_total').val());
	           	$('input[name=invoice_tax_rate]').val($('input[name=bid_tax_amount]').val());
	           	$('input[name=invoice_sales_tax_total]').val($('input[name=sales_tax_total]').val());
	           	$('input[name=invoice_grand_total]').val($('input[name=bid_grand_total]').val());

				// Set Amount Totaled by Entered Percent to Create Invoice Balance Display
				$('.invoice-final-balance').text($('input[name=bid_grand_total]').val());

				// Once the Create Invoice Form Is Setup Show It :)
				$('.ui.modal.create-invoice').modal('show');

        	});


        	$('.previous-payment-create-invoice-button').click(function(event) {	

				event.preventDefault();  	

				// Set Hidden Fields on Create Invoice Form
	           	$('input[name=invoice_partial_payment_amount]').val(0);

	           	previousPayment = parseFloat($('input[name=previous_payment]').val());

	           	bidSubTotal = $('input[name=bid_sub_total').val();
				
				//Get Sub Total by Removing the Previous Payment from the Bid's Subtotal
	           	subTotal = bidSubTotal - previousPayment;

				//Get Tax Rate
				taxRate = parseFloat($('input[name=bid_tax_amount]').val()/100);

				//Only Calculate Taxes if they exist
				if (taxRate) {

					taxAmount = subTotal * taxRate;
			 		grandTotal = subTotal + taxAmount;
				}
				else{
					taxAmount = 0;
					grandTotal = subTotal;
				}

				// Set Hidden Inputs
				$('input[name=invoice_previous_payment').val(previousPayment.toFixed(2));
				$('input[name=invoice_subtotal').val(subTotal.toFixed(2));
				$('input[name=invoice_sales_tax_total]').val(taxAmount.toFixed(2));
		 		$('input[name=invoice_grand_total]').val(grandTotal.toFixed(2));
				$('input[name=invoice_tax_rate]').val($('input[name=bid_tax_amount]').val());

				// Set Amount Totaled by Entered Percent to Create Invoice Balance Display
				$('.invoice-final-balance').text(grandTotal.toFixed(2));

				// Once the Create Invoice Form Is Setup Show It :)
				$('.ui.modal.create-invoice').modal('show');

        	}); 


			$('body').on('input propertychange','input[name=previous_payment]', function () {

				alert(partialAmount);
				// Set and Calculate Variables
				bidSubTotal = $('#bid_sub_total').val();
				adjustment = parseFloat($(this).val());
				partialAmount = bidSubTotal - adjustment;
				alert(partialAmount);
				// Show Amount Totaled by Entered Percent
			 	$('.adjusted-payment-total').text('$ ' + partialAmount.toFixed(2));

			 });        	      	

        });
    })(jQuery);
</script>


