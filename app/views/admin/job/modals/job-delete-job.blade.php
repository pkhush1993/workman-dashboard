<!-- Delete Job Modal -->
<div class="ui small modal delete-job">
	<i class="ui close icon"></i>
	<h2 class="ui header">Are you sure you want to delete this job?</h2>
	<div class="content">
		<a href="{{ route('delete.job', [$job->id]) }}" class="ui positive button">Delete</a>
		<div class="ui negative cancel button">Cancel</div>
	</div>
</div>

<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {
            $('.ui.modal.delete-job ui.close.icon, .ui.modal.delete-job .ui.cancel.button').click(function(event) {
                $('.ui.modal.delete-job').modal("hide");
            });

        });
    })(jQuery);
</script>       