<!-- Invoice Type Modal -->
<div class="ui invoice-type basic modal">
	<div class="content">
		<div class="ui center aligned basic segment">
			<div class="ui  huge buttons">
				<div class="ui invoice-partial-payment-amount-button teal button">PARTIAL PAYMENT</div>
				<div class="or"></div>
				<div class="ui previous-payments-button blue button">FULL INVOICE</div>
			</div>
		</div>			
	</div>
</div>