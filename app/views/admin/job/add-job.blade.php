@extends('layouts.default')

@section('content')

    <div id="add-job">

        <h1><i class="file text icon"></i>Add New Job</h1>

		<div class="ui ordered steps">
			<div class="active step set-account">
				<div class="content">
					<div class="title">
						Set Account
					</div>
					<div class="description">
						Select New or Existing Account
					</div>
				</div>
			</div>
			<div class="step enter-job-info">
				<div class="content">
					<div class="title">
						Enter Job Info
					</div>
					<div class="description">
						Fill out the job form
					</div>
				</div>
			</div>
			<div class="step create-job">
				<div class="content">
					<div class="title">
						Create Job
					</div>
					<div class="description">
						Submit your new job
					</div>
				</div>
			</div>
			<div class="step setup-bid">
				<div class="content">
					<div class="title">
						Setup Bid
					</div>
					<div class="description">
						Set Bid Up on the New Job
					</div>
				</div>
			</div>
		</div>

    </div>
    <div class="ui divider"></div>
	{{ Form::open(array('class' => 'ui form','id' => 'create-job','route' => ['create.job'] )) }}

		<!-- ================== Hidden Form Fields ================== -->
		<!-- Dates/Priorities -->
		{{ Form::hidden('bid_date', null) }}
		{{ Form::hidden('estimator_deadline', null) }}
		{{ Form::hidden('estimator_priority', null) }}
		{{ Form::hidden('production_deadline', null) }}
		{{ Form::hidden('production_priority', null) }}
		{{ Form::hidden('completed_date', null) }}

		<!-- Job Contacts -->
		{{ Form::hidden('job_contacts',$value = null) }}

		<!-- New Account? -->
		{{ Form::hidden('new_account',$value = null) }}

		<!-- New Account Fields -->
		{{ Form::hidden('new_account_name',$value = null) }}
		{{ Form::hidden('new_account_email',$value = null) }}
		{{ Form::hidden('new_account_phone',$value = null) }}
		{{ Form::hidden('new_account_mobile',$value = null) }}
		{{ Form::hidden('new_account_fax',$value = null) }}
		{{ Form::hidden('new_account_address_1',$value = null) }}
		{{ Form::hidden('new_account_address_2',$value = null) }}
		{{ Form::hidden('new_account_city',$value = null) }}
		{{ Form::hidden('new_account_state', $value = null) }}
		{{ Form::hidden('new_account_zip',$value = null) }}

		<!-- New Contact Fields -->
		{{ Form::hidden('new_contact_title',$value = null) }}
		{{ Form::hidden('new_contact_first_name',$value = null) }}
		{{ Form::hidden('new_contact_last_name',$value = null) }}
		{{ Form::hidden('new_contact_email',$value = null) }}
		{{ Form::hidden('new_contact_phone',$value = null) }}
		{{ Form::hidden('new_contact_mobile',$value = null) }}
		{{ Form::hidden('new_contact_fax',$value = null) }}
		{{ Form::hidden('new_contact_address_1',$value = null) }}
		{{ Form::hidden('new_contact_address_2',$value = null) }}
		{{ Form::hidden('new_contact_city',$value = null) }}
		{{ Form::hidden('new_contact_state', $value = null) }}
		{{ Form::hidden('new_contact_zip',$value = null) }}
		<!-- ======================================================= -->

		<div class="ui stackable grid">
			<div class="row">
				<!-- Left Column -->
				<div class="ten wide column ">
					<h3 style="margin-top:0"><i class="book icon"></i>Set Account</h3>
					<div class="ui basic segment">
						<div class="field">
							<div class="ui fluid large blue button job-account">Set Account</div>
						</div>
						<div class="two fields account-contacts" style="display:none">
							<div class="field">
								{{ Form::label('account', 'Account') }}
								<div class="assigned_job_account" style="display:none">
									{{ Form::select('assigned_job_account', array(''=>'Select Account'), $value = null , array('class'=>'ui large fluid search icon selected_job_account','autocomplete' =>'off')) }}
								</div>
								<div class="new-account-name" style="display:none">
									{{ Form::text('new_account_name','',array('readonly' => 'readonly')) }}
								</div>
							</div>
							<div class="field actions">
								<div class="ui small button orange new-account" style="display:none">Edit</div>
								<div class="ui labeled green icon button add-contacts-button" style="display:none"><i class="ui icon add"></i>Assign Contacts To Job</div>
								<div class="ui small button existing-account" style="display:none">Choose Different Account</div>
							</div>
						</div>
						<div class="field primary-contact-field" style="display:none">
							{{ Form::label('name', 'Primary Contact') }}
							{{ Form::select('primary_contact', ['' => 'Select a Primary Contact'], '', array('class'=>'ui primary-contact dropdown','autocomplete' =>'off')) }}
						</div>
					</div>
					<h3><i class="circle info icon"></i>Job Info</h3>
					<div class="ui blue segment">
						<div class="required field">
							{{ Form::label('job_name', 'Job Name/Number') }}
							<div class="two fields">
								<div class="field">
									{{ Form::text('name',$value = null, array('placeholder'=>'Enter Job Name')) }}
								</div>
								<div class="field">
									{{ Form::text('job_number',$company->getNextJob(), array('placeholder'=>'Enter Job Number')) }}
								</div>
							</div>
						</div>
						<div class="required field">
							{{ Form::label('bid_date', 'Bid Date') }}
							{{ Form::text('bid_date',date('m/d/Y'), array('placeholder'=>'Enter Date','class'=>'date')) }}
						</div>
						<div class="required field">
							{{ Form::label('current_status', 'Current Status') }}
							{{ Form::select('status', array(
								''     		  => 'Select Status',
								'request'     => 'Request',
								'outstanding' => 'Outstanding',
								'contracted'  => 'Contracted',
								'proceed'     => 'Proceed',
								'scheduled'   => 'Scheduled',
								'completed'   => 'Completed',
								'invoicing'   => 'Invoicing',
								'paid'        => 'Paid',
								'rejected'    => 'Rejected',
							), 'request' , array('class'=>'ui dropdown','autocomplete' =>'off')) }}
						</div>
						<div class="field">
							<div class="ui small fluid orange icon button view-dates-priorities"><i class="calendar icon"></i> View Dates/Priorities</div>
						</div>
					</div>
					<h3><i class="users icon"></i> Project Roles</h3>
					<div class="ui green segment">
						@if ($estimators)
							<div class="required field">
								{{ Form::label('estimator', 'Estimator') }}
								{{ Form::select('estimator', $estimators, array ('' => 'asdasdsa')  , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
							</div>
						@endif
						@if ($project_managers)
							<div class="required field">
								{{ Form::label('project_manager', 'Project Manager') }}
								{{ Form::select('project_manager', $project_managers, $default = null , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
							</div>
						@endif
						@if ($foremen)
							<div class="required field">
								{{ Form::label('foreman', 'Foreman') }}
								{{ Form::select('foreman', $foremen, $default = null , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
							</div>
						@endif
						@if ($accountants)
							<div class="required field">
								{{ Form::label('accounting', 'Accountant') }}
								{{ Form::select('accounting', $accountants, $default = null , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
							</div>
						@endif
						<?php /* Removed Salesman
						@if ($salesman)
							<div class="field">
								{{ Form::label('salesman', 'Salesman') }}
								{{ Form::select('salesman', $salesman, $job->salesman , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
							</div>
						@endif
						*/ ?>
					</div>
					<div class="ui divider"></div>
					<div class="field">
						{{ Form::label('lead_source', 'Lead Source') }}
						{{ Form::select('lead_source', $lead_sources, $default = null , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
					</div>
					<div class="field">
						{{ Form::label('prime_contract', 'Enter Prime Contract') }}
						{{ Form::text('prime_contract','') }}
					</div>
				</div>
				<!-- Right Column -->
				<div class="six wide column ">
					<h3 style="margin-top:0"><i class="marker info icon"></i>Addresses</h3>
					<!-- Site Address -->
					<div class="ui segment">
						<h4 class="ui header" >Site Address</h4>
						{{ Form::label('address_1', 'Address') }}
						<div class="two fields">
							<div class="field">
								{{ Form::text('site_address_1',$value = null, array('placeholder'=>'Address')) }}
							</div>
							<div class="field">
								{{ Form::text('site_address_2',$value = null, array('placeholder'=>'Address 2')) }}
							</div>
						</div>
						<div class="fields">
							<div class="seven wide field">
								{{ Form::label('city', 'City') }}
								{{ Form::text('site_city',$value = null, array('placeholder'=>'City')) }}
							</div>
							<div class="five wide field">
								{{ Form::label('state', 'State') }}
								{{ Form::select('site_state', array(
									'' => 'State',
									'AL' => 'AL',
									'AK' => 'AK',
									'AZ' => 'AZ',
									'AR' => 'AR',
									'CA' => 'CA',
									'CO' => 'CO',
									'CT' => 'CT',
									'DE' => 'DE',
									'DC' => 'DC',
									'FL' => 'FL',
									'GA' => 'GA',
									'HI' => 'HI',
									'ID' => 'ID',
									'IL' => 'IL',
									'IN' => 'IN',
									'IA' => 'IA',
									'KS' => 'KS',
									'KY' => 'KY',
									'LA' => 'LA',
									'ME' => 'ME',
									'MD' => 'MD',
									'MA' => 'MA',
									'MI' => 'MI',
									'MN' => 'MN',
									'MS' => 'MS',
									'MO' => 'MO',
									'MT' => 'MT',
									'NE' => 'NE',
									'NV' => 'NV',
									'NH' => 'NH',
									'NJ' => 'NJ',
									'NM' => 'NM',
									'NY' => 'NY',
									'NC' => 'NC',
									'ND' => 'ND',
									'OH' => 'OH',
									'OK' => 'OK',
									'OR' => 'OR',
									'PA' => 'PA',
									'RI' => 'RI',
									'SC' => 'SC',
									'SD' => 'SD',
									'TN' => 'TN',
									'TX' => 'TX',
									'UT' => 'UT',
									'VT' => 'VT',
									'VA' => 'VA',
									'WA' => 'WA',
									'WV' => 'WV',
									'WI' => 'WI',
									'WY' => 'WY',
								), $default = null , array('class'=>'ui dropdown search site-state fluid','autocomplete' =>'off')) }}
							</div>
							<div class="four wide field">
								{{ Form::label('zip', 'Zip') }}
								{{ Form::text('site_zip',$value = null, array('placeholder'=>'Zip Code')) }}
							</div>
						</div>
						<div class="two fields">
							<div class="field">
								<div class="ui fluid tiny use-billing_address button">Use Billing</div>
							</div>
							<div class="field">
								<div class="ui fluid tiny use-account-address-site button" style="display:none">Use Account</div>
							</div>
						</div>
					</div>
					<!-- Billing Address -->
					<div class="ui segment">
						<h4 class="ui header" >Billing Address</h4>
						{{ Form::label('address_1', 'Address') }}
						<div class="two fields">
							<div class="field">
								{{ Form::text('billing_address_1',$value = null, array('placeholder'=>'Address')) }}
							</div>
							<div class="field">
								{{ Form::text('billing_address_2',$value = null, array('placeholder'=>'Address 2')) }}
							</div>
						</div>
						<div class="fields">
							<div class="seven wide field">
								{{ Form::label('city', 'City') }}
								{{ Form::text('billing_city',$value = null, array('placeholder'=>'City')) }}
							</div>
							<div class="five wide field">
								{{ Form::label('state', 'State') }}
								{{ Form::select('billing_state', array(
									'' => 'State',
									'AL' => 'AL',
									'AK' => 'AK',
									'AZ' => 'AZ',
									'AR' => 'AR',
									'CA' => 'CA',
									'CO' => 'CO',
									'CT' => 'CT',
									'DE' => 'DE',
									'DC' => 'DC',
									'FL' => 'FL',
									'GA' => 'GA',
									'HI' => 'HI',
									'ID' => 'ID',
									'IL' => 'IL',
									'IN' => 'IN',
									'IA' => 'IA',
									'KS' => 'KS',
									'KY' => 'KY',
									'LA' => 'LA',
									'ME' => 'ME',
									'MD' => 'MD',
									'MA' => 'MA',
									'MI' => 'MI',
									'MN' => 'MN',
									'MS' => 'MS',
									'MO' => 'MO',
									'MT' => 'MT',
									'NE' => 'NE',
									'NV' => 'NV',
									'NH' => 'NH',
									'NJ' => 'NJ',
									'NM' => 'NM',
									'NY' => 'NY',
									'NC' => 'NC',
									'ND' => 'ND',
									'OH' => 'OH',
									'OK' => 'OK',
									'OR' => 'OR',
									'PA' => 'PA',
									'RI' => 'RI',
									'SC' => 'SC',
									'SD' => 'SD',
									'TN' => 'TN',
									'TX' => 'TX',
									'UT' => 'UT',
									'VT' => 'VT',
									'VA' => 'VA',
									'WA' => 'WA',
									'WV' => 'WV',
									'WI' => 'WI',
									'WY' => 'WY',
								), $default = null , array('class'=>'ui dropdown search billing-state fluid','autocomplete' =>'off')) }}
							</div>
							<div class="four wide field">
								{{ Form::label('billing_zip', 'Zip') }}
								{{ Form::text('billing_zip',$value = null, array('placeholder'=>'Zip Code')) }}
							</div>
						</div>
						<div class="two fields">
							<div class="field">
								<div class="ui fluid tiny use-site_address button">Use Site</div>
							</div>
							<div class="field">
								<div class="ui fluid tiny use-account-address-billing button" style="display:none">Use Account</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="ui divider"></div>
			<!-- Custom Fields -->
			<?php
				// Select Default
				$default = ['' => 'Select Field'];
			?>
			<div class="row">
				<div class="column">

					@if ($company->getCustomDrop1())
						<div class="field">
							{{ Form::label('custom_drop_1', key($company->getCustomDrop1())) }}
							{{ Form::select('custom_drop_1', $custom_fields_dropdowns['custom_drop_1'], $default = null , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
						</div>
					@endif

					@if ($company->getCustomDrop2())
						<div class="field">
							{{ Form::label('custom_drop_2', key($company->getCustomDrop2())) }}
							{{ Form::select('custom_drop_2', $custom_fields_dropdowns['custom_drop_2'], $default = null , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
						</div>
					@endif

					@if ($company->getCustomDrop3())
						<div class="field">
							{{ Form::label('custom_drop_3', key($company->getCustomDrop3())) }}
							{{ Form::select('custom_drop_3', $custom_fields_dropdowns['custom_drop_3'], $default = null , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
						</div>
					@endif

					@if ($company->getCustomText1())
						<div class="field">
							{{ Form::label('custom_text_1', $company->getCustomText1()) }}
							{{ Form::text('custom_text_1',$value = null, array('placeholder'=>'Enter Value')) }}
						</div>
					@endif
					@if ($company->getCustomText2())
						<div class="field">
							{{ Form::label('custom_text_2', $company->getCustomText2()) }}
							{{ Form::text('custom_text_2',$value = null, array('placeholder'=>'Enter Value')) }}
						</div>
					@endif
					@if ($company->getCustomText3())
					<div class="field">
						{{ Form::label('custom_text_3', $company->getCustomText3()) }}
						{{ Form::text('custom_text_3',$value = null, array('placeholder'=>'Enter Value')) }}
					</div>
					@endif
				</div>
			</div>
		</div>
		<br>
		<div class="actions">
			<div class="ui huge submit fluid green button">CREATE NEW JOB</div>
		</div>
		<div class="ui error message"></div>

	{{ Form::close() }}

	{{-- Account Addresses for AutoFill --}}
	{{ Form::hidden('set_account_address_1',$value = null) }}
	{{ Form::hidden('set_account_address_2',$value = null) }}
	{{ Form::hidden('set_account_city',$value = null) }}
	{{ Form::hidden('set_account_state', $value = null) }}
	{{ Form::hidden('set_account_zip',$value = null) }}



	<!-- ====================== MODALS ====================== -->

	<!-- Add Account Modal -->
	@include('admin.job.modals.job-add-account')

	<!-- Add New Contact Modal -->
	<?php /* @include('admin.job.modals.job-add-contact') */ ?>

	<!-- Add Contacts To Job Modal -->
 	@include('admin.job.modals.job-add-contacts')

	<!-- View Contacts Modal -->
	<?php /* @include('admin.job.modals.job-contacts') */ ?>

	<!-- Default Modal (a blank modal used with AJAX calls) -->
	@include('admin.job.modals.default')

	<!-- Set Account Modal -->
	@include('admin.job.modals.set-account')

	<!-- Choose Account Modal -->
	@include('admin.job.modals.choose-account')

	<!-- View Account No Selected Modal -->
	@include('admin.job.modals.job-account-not-selected')

	@include('admin.job.modals.view-dates-priorities')

	<!-- =================================================== -->

	<script>
		jQuery.noConflict();
		(function( $ ) {
			$(function() {
				$('input.search').attr('autocomplete', false);
				// Dates/Priorities
				$('.view-dates-priorities').pop('.ui.modal.dates-priorities',true,false);

				// Add Account Modal
				$('.new-account').pop('.ui.modal.add-account');

				// Set Account Modal
				$('.job-account').pop('.ui.modal.set-account');

				// Set Existing Account Modal
				$('.existing-account').pop('.ui.modal.choose-account',false);

				// Add Contacts to Job Modal
				$('.add-contacts-button	').pop('.ui.modal.add-contacts');

				// Add New Contact Modal
				$('.add-contact').pop('.ui.modal.add-contact');

				//View Contact Modal
				$('.view-contact-button').pop('.ui.modal.view-contacts');

				//Semantic UI Form Validation =========================
				$('#create-job').form(
					{
					    jobName: {
							identifier  : 'name',
							rules: [
								{
									type   : 'empty',
									prompt : 'Job Name is required'
								}
							]
					    },
					    jobAccount: {
							identifier  : 'assigned_job_account',
							rules: [
								{
									type   : 'empty',
									prompt : 'Job Account is required',
								}
							]
					    },
					    jobNumber: {
							identifier  : 'job_number',
							rules: [
								{
									type   : 'empty',
									prompt : 'Job Number is required'
								}
							]
					    },
					    jobStatus: {
							identifier  : 'status',
							rules: [
								{
									type   : 'empty',
									prompt : 'Status is required'
								}
							]
					    },
					    bidDate: {
							identifier  : 'bid_date',
							rules: [
								{
									type   : 'empty',
									prompt : 'Bid Date is required'
								}
							]
					    },
					    workCompleted: {
							identifier  : 'completion_deadline',
							rules: [
								{
									type   : 'empty',
									prompt : 'Completed Date is required'
								}
							]
					    },
					    jobEstimator: {
							identifier  : 'estimator',
							rules: [
								{
									type   : 'empty',
									prompt : 'Job Estimator is required',
								}
							]
					    },
					    jobPM: {
							identifier  : 'project_manager',
							rules: [
								{
									type   : 'empty',
									prompt : 'Job Project Manager is required',
								}
							]
					    },
					    jobForeman: {
							identifier  : 'foreman',
							rules: [
								{
									type   : 'empty',
									prompt : 'Job Foreman is required',
								}
							]
					    },
					    jobAccountant: {
							identifier  : 'accounting',
							rules: [
								{
									type   : 'empty',
									prompt : 'Job Accountant is required',
								}
							]
					    }
					},
					{
					    inline : true,
					    on     : 'blur',
						onInvalid: function (data) {
							// Iterate through each form error
							$.each(data, function(index, val) {
								// If errors contain No Job Account error pop validation modal
								if(val === "Job Account is required"){
									//View Contact Modal
									$('.no-account-selected.modal')
									  	.modal({
									    closable  : false,
									    onApprove : function() {
									     	$('.ui.modal.set-account').modal('show');
									    }
									}).modal('show');
								}
							});


							// alert('Account Must Be Attached to Job for Creation');
						}
					}
				);

				// Copy Site Address
				$('.use-site_address').click(function(event) {
					$('input[name=billing_address_1]').val($('input[name=site_address_1]').val());
					$('input[name=billing_address_2]').val($('input[name=site_address_2]').val());
					$('input[name=billing_city]').val($('input[name=site_city]').val());
					$('.billing-state').dropdown('set selected', $('.site-state').dropdown('get value') );
					$('input[name=billing_zip]').val($('input[name=site_zip]').val());
				});

				// Copy Billing Address
				$('.use-billing_address').click(function(event) {
					$('input[name=site_address_1]').val($('input[name=billing_address_1]').val());
					$('input[name=site_address_2]').val($('input[name=billing_address_2]').val());
					$('input[name=site_city]').val($('input[name=billing_city]').val());
					$('.site-state').dropdown('set selected', $('.billing-state').dropdown('get value') );
					$('input[name=site_zip]').val($('input[name=billing_zip]').val());
				});

				// Copy Account Address for Site Address
				$('.use-account-address-site').click(function(event) {
					var account_id = $('.selected_job_account').dropdown('get value');
					var isNewAccount = $('input[name="new_account"').val();

					if ( isNewAccount == 1 ) {
						$('input[name=sitethpa_address_1]').val($('input[name="new_account_address_1"]').val());
						$('input[name=sitethpa_address_2]').val($('input[name="new_account_address_2"]').val());
						$('input[name=sitethpa_city]').val($('input[name="new_account_city"]').val());
						$('.sitethpa-state').dropdown('set selected', $('input[name="new_account_state"]').val());
						$('input[name=sitethpa_zip]').val($('input[name="new_account_zip"]').val());
					}
					else {
						$.ajax({
							type: "POST",
							url: "/job/account/address/ajax/" + account_id,
						}).done(function (data) {
							$('input[name=site_address_1]').val(data.address_1);
							$('input[name=site_address_2]').val(data.address_2);
							$('input[name=site_city]').val(data.city);
							$('.site-state').dropdown('set selected', data.state);
							$('input[name=site_zip]').val(data.zip);
						});
					}
				});

				// Copy Account Address for Billing Address
				$('.use-account-address-billing').click(function() {
					var account_id = $('.selected_job_account').dropdown('get value');
					var isNewAccount = $('input[name="new_account"').val();

					if ( isNewAccount == 1 ) {
						$('input[name=billing_address_1]').val($('input[name="new_account_address_1"]').val());
						$('input[name=billing_address_2]').val($('input[name="new_account_address_2"]').val());
						$('input[name=billing_city]').val($('input[name="new_account_city"]').val());
						$('.billing-state').dropdown('set selected', $('input[name="new_account_state"]').val());
						$('input[name=billing_zip]').val($('input[name="new_account_zip"]').val());
					}
					else {
						$.ajax({
							type: "POST",
							url: "/job/account/address/ajax/" + account_id,
						}).done(function (data) {
							$('input[name=billing_address_1]').val(data.address_1);
							$('input[name=billing_address_2]').val(data.address_2);
							$('input[name=billing_city]').val(data.city);
							$('.billing-state').dropdown('set selected', data.state);
							$('input[name=billing_zip]').val(data.zip);
						});
					}
				});


				// Set Job Contacts Depending on Specified Account in Dropdown
				$("select[name=assigned_job_account]").on('change',function(){

					if ( $(this).val() == "new_account"){
						$('.ui.modal.add-account').modal('show');
					}
					else{
						// Get Request Contacts Id
						var accountId = $(this).val();

						// Build URL using Route
						contactsUrl = "{{route('job.account.contacts','') }}/" + accountId;
						// Set Contacts depending on Account Chosen - AJAX
						$.ajax({
							type: "GET",
							url: contactsUrl,
						})
						.done(function( data ) {
							//Reset Job Contacts Hidden Input to prep for new contacts from a different account
							$('input[name=job_contacts]').val('');
							//Reload Form in Modal with New Accounts Contacts
							$('.ui.modal.add-contacts .add-contacts-table').html(data);
							$('.ui.modal.add-contacts .ui.checkbox').checkbox();
						});
					}

				});

				// Close Action on View Contact Form
				$('.modal.view-contact .close').click(function(event) {
					$('.ui.modal.add-contact').modal('show');
				});

				// Display the Primary Contact options after contacts have been selected
				$('body').on('click', '.add-contacts-table button', function() {
					if( $('.primary-contact-field').css('display') == 'none') {
						$('.primary-contact-field').toggle();
					}
					console.log('clicked');
					var primaryContact = $('select[name="primary_contact"]');
					primaryContact.empty();
					primaryContact.append("<option value=''>Select a Primary Contact</option>");
					$(this).parent().find('input[type="checkbox"]:checked').each(function() {
						primaryContact.append("<option value='" + $(this).val()+ "'>" + $(this).data('name') + "</option>");
					});
				});
			});
		})(jQuery);
	</script>

@stop
