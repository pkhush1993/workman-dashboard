@extends('layouts.default')

@section('content')

    <div id="view-jobs">
        <h1>
            <i class="file text icon"></i>
            Jobs
            <a href="{{ route('add.job') }}" class="ui labeled small icon green button" style="margin-left: 20px;top: -4px;">
                <i class="plus icon"></i>
                ADD NEW JOB
            </a>
        </h1>
        <table class="ui padded celled table view-jobs-table">
            <thead>
                  <tr>
                      <th></th>
                      <th>Number</th>
                      <th>Name</th>
                      <th>Account</th>
                      <th>Status</th>
                      <th>Last Update</th>
                  </tr>
            </thead>
            {{-- <tbody>
                @foreach ($jobs as $job)
                    <tr>
                        <td class="center aligned"><a href="{{ route('view.job',$job->id); }}"><i class="large unhide icon"></i></a></td>
                        <td>{{ $job->job_number }}</td>
                        <td>{{ $job->name }}</td>
                        <td>{{ $job->account()->name }}</td>
                        <td>{{ $job->status }}</td>
                        <td>{{ date('Y-m-d',strtotime($job->updated_at)) }}</td>
                    </tr>
                @endforeach
            </tbody> --}}
        </table>
        {{-- <a href="{{ route('add.job') }}" class="ui labeled icon green button">
            <i class="plus icon"></i>
            ADD NEW JOB
        </a>  --}}
    </div>

<script>
    jQuery.noConflict();
    (function( $ ) {
        $(function() {

            $(document).ready(function(){
                $('.view-jobs-table').dataTable( {
                    "info": false,
                    "lengthChange": false,
                    "dom": '<"table-search"<f> > t <"F"ip>',
                    "aoColumnDefs": [
                      { 'bSortable': false, 'aTargets': [ 0 ] }
                    ],
                    "order": [[ 1, "desc" ]],
                    "iDisplayLength": 50,
                    "processing": true,
    		        "serverSide": true,
    		        "ajax": "{{route('view.jobs.ajax')}}",
                })
            });

        });
    })(jQuery);
</script>

@stop
