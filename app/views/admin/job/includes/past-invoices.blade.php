<div class="ui grid">
    <div class="eight wide column">
	<h2>Past Invoices</h2>

	<div class="ui divided list">
		@foreach ($invoices as $invoice)
			<a target="_blank" href="{{ route('job.invoice', [$invoice->invoice_type,$invoice->job_id,$invoice->down_payment,$invoice->id, $invoice->view_type]) }}" class="item">
				<i class="ui dollar icon"></i>
				<div class="content">
					<div class="header">Invoice #{{ $invoice->invoice_number }} - <small>Created On: {{ date('m-d-Y',strtotime($invoice->created_at)) }} by {{ $invoice->getCreatedByProfile()->getFullName() }}</small></div>
				</div>
			</a>
		@endforeach
	</div>
	</div>
</div>