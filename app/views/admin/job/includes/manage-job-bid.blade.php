<h3 class="ui header">
	<div style="float:right">
		<a href="{{ route('import.bid.csv', ['job_id' => $job->id]) }}" class="ui green basic button">Import From CSV</a>
	</div>
	<div class="content">
		Bid Line Items
		<div class="sub header">Manage your Line Items for the Job's Bid.</div>
	</div>

</h3>

{{ Form::open(array('class' => 'ui form Job_Bids_Items','id' => 'job_bid','route' => ['job.bid',$job_id] )) }}
<input type="hidden" name="job_id" value="{{ $job->id }}">
<!-- Line Items -->
<table class="ui small compact striped table">
	<thead>
	<tr>
		<th class="one wide center aligned collapsing">
			<i class="ui large outline trash icon"></i>
			{{-- Just using a blank form for semantic markup of DOM jQuery select all Delete checkboxes toggle --}}
			<form id="delete_select_all">
				<div class="ui checkbox header_checkbox">

					<input id="delete_all" type="checkbox" name="delete_select_all" value="" />
				</div>
			</form>
			{{-- End check all trigger  --}}
		</th>
		<th class="three wide">Category</th>
		<th class="three wide">Product/Service</th>
		<th class="two wide">Quantity</th>
		<th class="two wide">Units</th>
		<th class="two wide">Rate</th>
		<th class="two wide">Amount</th>
		<th class="one wide center aligned">
			Tax

			{{-- Just using a blank form for semantic markup of DOM jQuery select all Tax checkboxes toggle --}}
			<form id="tax_select_all">
				<div class="ui checkbox header_checkbox">

					<input id="tax_all" type="checkbox" name="tax_select_all" value="" />
				</div>
			</form>
			{{-- End check all trigger  --}}
		</th>
	</tr>
	</thead>
	<tbody class="line_items_repeater">

	@if ($bid_line_items )
		{{-- Signal Controller Know Bid Exists --}}
		{{ Form::hidden('bid_id',$value = $bid->id) }}

		<tr class="line_item hidden">
			<td class="center aligned">
				<div class="ui checkbox delete_line_item">{{ Form::checkbox('delete_line_item[]',0, false,array('tabindex' => '-1')) }}</div>
			</td>
			<td class="bid-category">{{ Form::select('category[]', $bid_fields, $default = null , array('class'=>'ui bid_field fluid ','autocomplete' =>'off','disabled' => 'disabled')) }}</td>
			<td class="bid-item">{{ Form::select('product[]', array('' => 'Enter Product/Service'), $default = null , array('autocomplete' =>'off','disabled' => 'disabled','style' =>'display:none')) }}</td>
			<td class="item-quantity">{{ Form::number('item_quantity[]', $default = null,array('class'=>'task', 'placeholder' => 'Quantity','disabled' => 'disabled','style' =>'display:none', ) ) }}</td>
			<td class="item-units">{{ Form::text('item_units[]', $default = null,array('placeholder' => 'Units','disabled' => 'disabled','style' =>'display:none') ) }}</td>
			<td class="item-rate">{{ Form::number('item_rate[]', $default = null,array('placeholder' => 'Rate','disabled' => 'disabled','style' =>'display:none', 'step' => ".01") ) }}</td>
			<td class="item-amount">{{ Form::text('item_amount[]', 0,array('placeholder' => 'Amount','readonly' => 'readonly','disabled' => 'disabled','style' =>'display:none') ) }}</td>
			<td class="tax-ident center aligned"><div class="ui checkbox">{{ Form::checkbox('tax[]',$value = null, false, array('tabindex' => '-1')) }}</div></td>
		</tr>
		{{--*/ $line_item_count = 1 /*--}}
		@foreach ($bid_line_items as $line_item)
			<tr class="line_item existing">
				{{ Form::hidden('line_item_id[]', $line_item->id) }}
				<td class="center aligned">
					<div class="ui checkbox delete_line_item" data-id="{{ $line_item->id }}">{{ Form::checkbox('delete_line_item[]',$line_item->id, false,array('tabindex' => '-1')) }}</div>
				</td>

				{{-- Add Saved Drop Down Options from the job_bid_line_items to the Preset Bid Categories and Line Items --}}
				<?php
				// If Line Item Category is Custom (marked by a category_id of 0) Set It Up by its Name as the Value in the Dropdown
				if ($line_item->category_id == 0) {
					$bid_fields_dropdown = [$line_item->category => $line_item->category] + $bid_fields;
				}
				else{
					$bid_fields_dropdown = [$line_item->category_id => $line_item->category] + $bid_fields;
				}
				// If Line Item Product/Service is Custom (marked by a product_id of 0) Set It Up by its Name as the Value in the Dropdown
				if ($line_item->product_id == 0) {
					$bid_field_items_dropdown = [$line_item->product => $line_item->product] + $line_item->items_input;
				}
				else{
					$bid_field_items_dropdown = [$line_item->product_id => $line_item->product] + $line_item->items_input;
				}
				$selected = $line_item->product_id == 0 ? $line_item->product : $line_item->product_id;
				?>
				<td class="bid-category">{{ Form::select('category[]', $bid_fields_dropdown, ($line_item->category_id == 0) ? $line_item->category : $line_item->category_id , array('class'=>'ui search dropdown bid_field fluid','autocomplete' =>'off')) }}</td>
				<td class="bid-item">
					@if ($line_item->product_id == 0)
						<div class="ui search dropdown bid_item fluid selection" autocomplete="off">
							<input type="hidden" name="product[]" value="<?php echo e($line_item->product) ?>"/>
							<i class="dropdown icon"></i>
							<div class="default text">Select Product/Service</div>
							<div class="menu">
								<?php foreach($bid_field_items_dropdown as $key => $value) :?>
								<?php if ( $value != 'Select Product/Service') :?>
								<div class="item" data-value="<?php echo e($key) ?>" data-text="<?php echo e($value) ?>"><?php echo e($value) ?></div>
								<?php endif ?>
								<?php endforeach ?>
							</div>
						</div>
						</div>
					@else
						{{ Form::select('product[]', $bid_field_items_dropdown, ($line_item->product_id == 0) ? e($line_item->product) : $line_item->product_id, array('class'=>'ui search dropdown bid_item fluid','autocomplete' =>'off')) }}
					@endif
				</td>
				<td class="item-quantity">{{ Form::number('item_quantity[]', $line_item->quantity,array('class'=>'task', 'placeholder' => 'Quantity', ) ) }}</td>
				<td class="item-units">{{ Form::text('item_units[]', $line_item->unit,array('placeholder' => 'Units') ) }}</td>
				<td class="item-rate">{{ Form::text('item_rate[]', $line_item->rate,array('placeholder' => 'Rate', 'step' => ".01") ) }}</td>
				<td class="item-amount">{{ Form::text('item_amount[]', $line_item->amount,array('placeholder' => 'Amount','readonly' => 'readonly') ) }}</td>
				<td class="tax-ident center aligned"><div class="ui checkbox">{{ Form::checkbox('tax[]',$value = null, (bool)$line_item->getAttribute('taxable'), array('tabindex' => '-1', 'class' => 'checkbox_individual_toggle')) }}</div></td>
				<input class="is_taxable_toggle" type="hidden" name="is_taxable[]" value="0" />

			</tr>
			<?php $line_item_count++; ?>
		@endforeach
		<tr class="line_item existing NewItems">
			{{ Form::hidden('line_item_id[]', 0, ['disabled' => 'disabled']) }}
			<td class="center aligned">
				<div class="ui checkbox delete_line_item new_delete_box">{{ Form::checkbox('delete_line_item[]',$value = null, false, array('tabindex' => '-1')) }}</div>
			</td>
			<td class="bid-category">{{ Form::select('category[]', $bid_fields, $default = null , array('class'=>'ui search dropdown bid_field fluid','autocomplete' =>'off','disabled' => 'disabled')) }}</td>
			<td class="bid-item">{{ Form::select('product[]', array('' => 'Enter Product/Service'), $default = null , array('autocomplete' =>'off','disabled' => 'disabled','style' =>'display:none')) }}</td>
			<td class="item-quantity">{{ Form::text('item_quantity[]', $default = null,array('class'=>'ui task','placeholder' => 'Quantity','style' =>'display:none','disabled' => 'disabled', ) ) }}</td>
			<td class="item-units">{{ Form::text('item_units[]', $default = null,array('placeholder' => 'Units','style' =>'display:none','disabled' => 'disabled') ) }}</td>
			<td class="item-rate">{{ Form::text('item_rate[]', $default = null,array('placeholder' => 'Rate','style' =>'display:none','disabled' => 'disabled', 'step' => ".01") ) }}</td>
			<td class="item-amount">{{ Form::text('item_amount[]', $default = null,array('placeholder' => 'Amount','readonly' => 'readonly','style' =>'display:none','disabled' => 'disabled') ) }}</td>
			<td class="tax-ident center aligned"><div class="ui checkbox">{{ Form::checkbox('tax[]',$value = null, false, array('tabindex' => '-1', 'class' => 'new_checkbox_item checkbox_individual_toggle')) }}</div></td>
			<input class="is_taxable_toggle" type="hidden" disabled="disabled" name="is_taxable[]" value="0" />
		</tr>
	@else
		{{-- Signal Controller that this is a new bid on the job --}}
		{{ Form::hidden('bid_id',$value = false) }}
		<tr class="line_item hidden">
			{{ Form::hidden('line_item_id[]', 0, ['disabled' => 'disabled']) }}
			<td class="center aligned">
				<div class="ui checkbox delete_line_item">{{ Form::checkbox('delete_line_item[]',$value = null, false,array('tabindex' => '-1')) }}</div>
			</td>
			<td class="bid-category">{{ Form::select('category[]', $bid_fields, $default = null , array('class'=>'ui bid_field fluid','autocomplete' =>'off','disabled' => 'disabled')) }}</td>
			<td class="bid-item">{{ Form::select('product[]', array('' => 'Enter Product/Service'), $default = null , array('autocomplete' =>'off','disabled' => 'disabled','style' =>'display:none')) }}</td>
			<td class="item-quantity">{{ Form::number('item_quantity[]', $default = null,array('class'=>'task', 'placeholder' => 'Quantity','disabled' => 'disabled','style' =>'display:none') ) }}</td>
			<td class="item-units">{{ Form::text('item_units[]', $default = null,array('placeholder' => 'Units','disabled' => 'disabled','style' =>'display:none') ) }}</td>
			<td class="item-rate">{{ Form::text('item_rate[]', $default = null,array('placeholder' => 'Rate','disabled' => 'disabled','style' =>'display:none') ) }}</td>
			<td class="item-amount">{{ Form::text('item_amount[]', 0,array('placeholder' => 'Amount','readonly' => 'readonly','disabled' => 'disabled','style' =>'display:none') ) }}</td>
			<td class="center aligned"><div class="ui checkbox">{{ Form::checkbox('tax[]',$value = null, false, array('tabindex' => '-1')) }}</div></td>
			<input type="hidden" name="is_taxable[]" value="0" />
		</tr>
		<tr class="line_item existing">
			{{ Form::hidden('line_item_id[]', 0) }}
			<td class="center aligned">
				<div class="ui checkbox delete_line_item">{{ Form::checkbox('delete_line_item[]',$value = null, false, array('tabindex' => '-1')) }}</div>
			</td>
			<td class="bid-category">{{ Form::select('category[]', $bid_fields, $default = null , array('class'=>'ui search dropdown bid_field fluid','autocomplete' =>'off','disabled' => 'disabled')) }}</td>
			<td class="bid-item">{{ Form::select('product[]', array('' => 'Enter Product/Service'), $default = null , array('autocomplete' =>'off','disabled' => 'disabled','style' =>'display:none')) }}</td>
			<td class="item-quantity">{{ Form::text('item_quantity[]', $default = null,array('class'=>'ui task','placeholder' => 'Quantity','style' =>'display:none','disabled' => 'disabled', ) ) }}</td>
			<td class="item-units">{{ Form::text('item_units[]', $default = null,array('placeholder' => 'Units','style' =>'display:none','disabled' => 'disabled') ) }}</td>
			<td class="item-rate">{{ Form::text('item_rate[]', $default = null,array('placeholder' => 'Rate','style' =>'display:none','disabled' => 'disabled', 'step' => ".01") ) }}</td>
			<td class="item-amount">{{ Form::text('item_amount[]', $default = null,array('placeholder' => 'Amount','readonly' => 'readonly','style' =>'display:none','disabled' => 'disabled') ) }}</td>
			<td class="center aligned"><div class="ui checkbox ">{{ Form::checkbox('tax[]',$value = null, false, array('tabindex' => '-1')) }}</div></td>
			<input type="hidden" name="is_taxable[]" value="0" />
		</tr>
		<tr class="line_item existing">
			{{ Form::hidden('line_item_id[]', 0) }}
			<td class="center aligned">
				<div class="ui checkbox delete_line_item">{{ Form::checkbox('delete_line_item[]',$value = null, false, array('tabindex' => '-1')) }}</div>
			</td>
			<td class="bid-category">{{ Form::select('category[]', $bid_fields, $default = null , array('class'=>'ui search dropdown bid_field fluid','autocomplete' =>'off','disabled' => 'disabled')) }}</td>
			<td class="bid-item">{{ Form::select('product[]', array('' => 'Enter Product/Service'), $default = null , array('autocomplete' =>'off','disabled' => 'disabled','style' =>'display:none')) }}</td>
			<td class="item-quantity">{{ Form::text('item_quantity[]', $default = null,array('class'=>'ui task','placeholder' => 'Quantity','style' =>'display:none','disabled' => 'disabled', ) ) }}</td>
			<td class="item-units">{{ Form::text('item_units[]', $default = null,array('placeholder' => 'Units','style' =>'display:none','disabled' => 'disabled') ) }}</td>
			<td class="item-rate">{{ Form::text('item_rate[]', $default = null,array('placeholder' => 'Rate','style' =>'display:none','disabled' => 'disabled', 'step' => ".01") ) }}</td>
			<td class="item-amount">{{ Form::text('item_amount[]', $default = null,array('placeholder' => 'Amount','readonly' => 'readonly','style' =>'display:none','disabled' => 'disabled') ) }}</td>
			<td class="center aligned"><div class="ui checkbox ">{{ Form::checkbox('tax[]',$value = null, false, array('tabindex' => '-1')) }}</div></td>
			<input type="hidden" name="is_taxable[]" value="0" />
		</tr>
		<tr class="line_item existing">
			{{ Form::hidden('line_item_id[]', 0) }}
			<td class="center aligned">
				<div class="ui checkbox delete_line_item">{{ Form::checkbox('delete_line_item[]',$value = null, false, array('tabindex' => '-1')) }}</div>
			</td>
			<td class="bid-category">{{ Form::select('category[]', $bid_fields, $default = null , array('class'=>'ui search dropdown bid_field fluid','autocomplete' =>'off','disabled' => 'disabled')) }}</td>
			<td class="bid-item">{{ Form::select('product[]', array('' => 'Enter Product/Service'), $default = null , array('autocomplete' =>'off','disabled' => 'disabled','style' =>'display:none')) }}</td>
			<td class="item-quantity">{{ Form::text('item_quantity[]', $default = null,array('class'=>'ui task','placeholder' => 'Quantity','style' =>'display:none','disabled' => 'disabled', ) ) }}</td>
			<td class="item-units">{{ Form::text('item_units[]', $default = null,array('placeholder' => 'Units','style' =>'display:none','disabled' => 'disabled') ) }}</td>
			<td class="item-rate">{{ Form::text('item_rate[]', $default = null,array('placeholder' => 'Rate','style' =>'display:none','disabled' => 'disabled', 'step' => ".01") ) }}</td>
			<td class="item-amount">{{ Form::text('item_amount[]', $default = null,array('placeholder' => 'Amount','readonly' => 'readonly','style' =>'display:none','disabled' => 'disabled') ) }}</td>
			<td class="center aligned"><div class="ui checkbox ">{{ Form::checkbox('tax[]',$value = null, false, array('tabindex' => '-1')) }}</div></td>
			<input type="hidden" name="is_taxable[]" value="0" />
		</tr>
		<tr class="line_item existing">
			{{ Form::hidden('line_item_id[]', 0) }}
			<td class="center aligned">
				<div class="ui checkbox delete_line_item">{{ Form::checkbox('delete_line_item[]',$value = null, false, array('tabindex' => '-1')) }}</div>
			</td>
			<td class="bid-category">{{ Form::select('category[]', $bid_fields, $default = null , array('class'=>'ui search dropdown bid_field fluid','autocomplete' =>'off','disabled' => 'disabled')) }}</td>
			<td class="bid-item">{{ Form::select('product[]', array('' => 'Enter Product/Service'), $default = null , array('autocomplete' =>'off','disabled' => 'disabled','style' =>'display:none')) }}</td>
			<td class="item-quantity">{{ Form::text('item_quantity[]', $default = null,array('class'=>'ui task','placeholder' => 'Quantity','style' =>'display:none','disabled' => 'disabled', ) ) }}</td>
			<td class="item-units">{{ Form::text('item_units[]', $default = null,array('placeholder' => 'Units','style' =>'display:none','disabled' => 'disabled') ) }}</td>
			<td class="item-rate">{{ Form::text('item_rate[]', $default = null,array('placeholder' => 'Rate','style' =>'display:none','disabled' => 'disabled', 'step' => ".01") ) }}</td>
			<td class="item-amount">{{ Form::text('item_amount[]', $default = null,array('placeholder' => 'Amount','readonly' => 'readonly','style' =>'display:none','disabled' => 'disabled') ) }}</td>
			<td class="center aligned"><div class="ui checkbox ">{{ Form::checkbox('tax[]',$value = null, false, array('tabindex' => '-1')) }}</div></td>
			<input type="hidden" name="is_taxable[]" value="0" />
		</tr>
		<tr class="line_item existing">
			{{ Form::hidden('line_item_id[]', 0) }}
			<td class="center aligned">
				<div class="ui checkbox delete_line_item">{{ Form::checkbox('delete_line_item[]',$value = null, false, array('tabindex' => '-1')) }}</div>
			</td>
			<td class="bid-category">{{ Form::select('category[]', $bid_fields, $default = null , array('class'=>'ui search dropdown bid_field fluid','autocomplete' =>'off','disabled' => 'disabled')) }}</td>
			<td class="bid-item">{{ Form::select('product[]', array('' => 'Enter Product/Service'), $default = null , array('autocomplete' =>'off','disabled' => 'disabled','style' =>'display:none')) }}</td>
			<td class="item-quantity">{{ Form::text('item_quantity[]', $default = null,array('class'=>'ui task','placeholder' => 'Quantity','style' =>'display:none','disabled' => 'disabled') ) }}</td>
			<td class="item-units">{{ Form::text('item_units[]', $default = null,array('placeholder' => 'Units','style' =>'display:none','disabled' => 'disabled') ) }}</td>
			<td class="item-rate">{{ Form::text('item_rate[]', $default = null,array('placeholder' => 'Rate','style' =>'display:none','disabled' => 'disabled', 'step' => ".01") ) }}</td>
			<td class="item-amount">{{ Form::text('item_amount[]', $default = null,array('placeholder' => 'Amount','readonly' => 'readonly','style' =>'display:none','disabled' => 'disabled') ) }}</td>
			<td class="center aligned"><div class="ui checkbox ">{{ Form::checkbox('tax[]',$value = null, false, array('tabindex' => '-1')) }}</div></td>
			<input type="hidden" name="is_taxable[]" value="0" />
		</tr>
	@endif
	</tbody>
	<tfoot>
	<tr>
		<th colspan="4" style="vertical-align: middle">
			<div class="ui tiny green button add-line-item">ADD ROWS</div>
			<div class="ui tiny red button remove-line-item">DELETE SELECTED</div>

		</th>
		<th style="vertical-align: middle;text-align:right;">
			<b>{{ Form::label('bid_sub_total', 'Bid Subtotal') }}</b>
		</th>
		<th colspan="3">
			<div class="ui field">
				<div class="ui large left icon input">
					<i class="ui dollar icon"></i>
					@if ($bid_line_items )
						{{ Form::text('bid_sub_total', $bid->subtotal,array('placeholder' => '0.00','readonly' => 'readonly','style' => 'border: none;font-weight: bold;text-align:right') ) }}
					@else
						{{ Form::text('bid_sub_total', '',array('placeholder' => '0.00','readonly' => 'readonly','style' => 'border: none;font-weight: bold;text-align:right') ) }}
					@endif
				</div>
			</div>
		</th>
	</tr>
	<tr>
		<th colspan="4" style="vertical-align: top;border: none;">

			<div class="UpdatejobsBidsAlert"></div>
		</th>
		<th style="vertical-align: middle;text-align:right;">
			<b>{{ Form::label('bid_tax_amount', 'Enter Sales Tax Rate') }}</b>
		</th>
		<th colspan="3" style="padding: 0.4em .8em;">
			<div class="ui two fields">
				<div class="field">
					{{-- Check if Bid Exists --}}
					@if ($bid_line_items)
						@if ($bid->tax_description)
							<?php $tax_rates_dropdown = [$bid->tax_description => $bid->tax_description] + $tax_rates_select; ?>
						@else
							<?php $tax_rates_dropdown = $tax_rates_select; ?>
						@endif
						{{ Form::select('tax_description', $tax_rates_dropdown, $bid->tax_description  , array('class'=>'ui search dropdown tax_description','autocomplete' =>'off')) }}
					@else
						<?php $tax_rates_dropdown = $tax_rates_select; ?>
						{{ Form::select('tax_description', $tax_rates_dropdown, NULL  , array('class'=>'ui search dropdown tax_description','autocomplete' =>'off')) }}
					@endif
				</div>
				<div class="field">
					<div class="ui mini left labeled input">
						{{-- Check if Bid Has been Created --}}
						<div class="ui label">%</div>
						@if ($bid_line_items )
							{{ Form::text('bid_tax_amount', $bid->tax_rate,array('placeholder' => '0.00','style' => 'text-align: right;margin: 0;padding: 0 11px !important;font-size: 14px;font-weight: bold;') ) }}
						@else
							{{ Form::text('bid_tax_amount', '',array('placeholder' => '0.00') ) }}
						@endif
					</div>
				</div>
			</div>
		</th>
	</tr>
	<tr>
		<th colspan="4" style="vertical-align: top;border: none;">
		</th>
		<th style="vertical-align: middle;text-align:right;">
			<b>{{ Form::label('sales_tax_total', 'Calculated Sales Tax') }}</b>
		</th>
		<th colspan="3">
			<div class="ui field">
				<div class="ui large left icon input">
					{{-- Check if Bid Has been Created --}}
					@if ($bid_line_items )
						{{ Form::text('sales_tax_total', $bid->sales_tax_total,array('placeholder' => '0.00','readonly' => 'readonly','style' => 'border: none;font-weight: bold;text-align:right') ) }}
					@else
						{{ Form::text('sales_tax_total', '',array('placeholder' => '0.00','readonly' => 'readonly','style' => 'border: none;font-weight: bold;text-align:right') ) }}
					@endif
					<i class="ui dollar icon"></i>
				</div>
			</div>
		</th>
	</tr>
	<tr>
		<th colspan="4" style="vertical-align: top;border: none;">
		</th>
		<th style="vertical-align: middle;text-align:right;">
			<b>{{ Form::label('bid_grand_total', 'Grand Total') }}</b>
		</th>
		<th colspan="3">
			<div class="ui field">
				<div class="ui large left icon input">
					{{-- Check if Bid Has been Created --}}
					@if ($bid_line_items )
						{{ Form::text('bid_grand_total', $bid->grand_total,array('placeholder' => '0.00','readonly' => 'readonly','style' => 'border: none;font-weight: bold;color:green;font-size: 17px;text-align:right') ) }}
					@else
						{{ Form::text('bid_grand_total', '',array('placeholder' => '0.00','readonly' => 'readonly','style' => 'border: none;font-weight: bold;color:green;font-size: 17px;text-align:right') ) }}
					@endif
					<i class="ui dollar icon"></i>
				</div>
			</div>
		</th>
	</tr>
	</tfoot>
</table>
{{--<button class="ui large blue button SaveJobsBidBtn">SAVE JOB BID</button>--}}
<span class="ui yellow message save-bid-message" style="display:none">You must Save your Bid Before Creating an Invoice or Refresh the page to revert Changes</span>
<div class="ui right floated green bid-type-button button">VIEW/PRINT BID</div>
<button class="ui right floated invoice-type-button green button">CREATE INVOICE</button>
{{ Form::close() }}

<script>
	jQuery.noConflict();
	(function( $ ) {
		$(function() {

			$(document).ready(function(){
				var check_if_all_checked = $('.checkbox_individual_toggle:not(.new_checkbox_item)');

				if(check_if_all_checked.length == check_if_all_checked.filter(':checked').length)
				{
					$( "#tax_all" ).prop( "checked", true );
				}
			});

			// Superficially check for checkbox values to match with is_taxable input.
			// more of a just in case scenario
			$('input[name="tax[]"]').each(function() {
				if ( $(this).prop('checked') ) {
					$(this).parent().parent().parent().find('input[name="is_taxable[]"]').val(1);

				} else {
					$(this).parent().parent().parent().find('input[name="is_taxable[]"]').val(0);
				}
			});

			//add select all tax checkbox
			$('body').on('change', '#tax_all', function(event){
				if ( $(this).is(':checked') ) {

					$('.tax-ident').each(function(){
						var check_for_service = $(this).parent().find('.bid-item div.text');
						var check_for_bid = $(this).parent().find('.bid-category div.text');
						if(
								!check_for_service.hasClass('default') &&
								!check_for_service.find('input').is(':disabled') &&
								!check_for_bid.hasClass('default')
						) {

							$(this).find('.ui.checkbox').addClass('checked');
							$(this).find('.ui.checkbox .checkbox_individual_toggle').prop('checked', true).removeClass('new_checkbox_item');
							$(this).parent().find('.is_taxable_toggle').val(1);
							// necessary at this stage
							// loop through to re calculate tax totals based on checkboxes
							$(this).parent().find('.item-amount input').trigger('change');
						}

					});


				} else {
					$('.tax-ident').each(function(){
						$(this).find('.ui.checkbox').removeClass('checked');
						$(this).find('.ui.checkbox .checkbox_individual_toggle').prop('checked', false);
						$(this).parent().find('.is_taxable_toggle').val(0);
						// necessary at this stage
						// loop through to re calculate tax totals based on checkboxes
						$(this).parent().find('.item-amount input').trigger('change');
					});

				}

			});

			$('body').on('change', '.checkbox_individual_toggle', function(event){
				var check_if_all_checked = $('.checkbox_individual_toggle:not(.new_checkbox_item)');
				if(check_if_all_checked.length == check_if_all_checked.filter(':checked').length)
				{
					$( "#tax_all" ).prop( "checked", true );
				}
				else {
					$( "#tax_all" ).prop( "checked", false );
				}
			});

			$('body').on('change', '.delete_line_item input', function(event){
				var check_if_all_checked = $('.line_item:not(.hidden) .delete_line_item:not(.new_delete_box) input[name*="delete_line_item"]');

				if(check_if_all_checked.length == check_if_all_checked.filter(':checked').length)
				{
					$( "#delete_all" ).prop( "checked", true );
				}
				else {
					$( "#delete_all" ).prop( "checked", false );
				}
			});



			$('body').on('click', '.tax-ident .ui.checkbox', function(){

				var current_line_item = $(this).parent().parent();
				var quantity_input = current_line_item.find('.item-quantity > input');
				var quantity_val = quantity_input.val();


				if ( $(this).hasClass('checked') ) {
					current_line_item.find('input[name="is_taxable[]"]').val(1);
				} else {
					current_line_item.find('input[name="is_taxable[]"]').val(0);
				}
				//loop through to trigger updates
				current_line_item.find('.item-amount input').trigger('change');
			});

			// delete all
			//add select all tax checkbox
			$('body').on('change', '#delete_all', function(event){
				if ( $(this).is(':checked') ) {

					$('.checkbox.delete_line_item').each(function(){
						// var check_for_service = $(this).parent().parent().find('.bid-item div.text');
						// var check_for_bid = $(this).parent().parent().find('.bid-category div.text');
						// if(
						// 	!check_for_service.hasClass('default') &&
						// 	!check_for_service.find('input').is(':disabled') &&
						// 	!check_for_bid.hasClass('default')
						// ) {
						//if(!$(this).hasClass('new_delete_box')) {
						$(this).find('input').prop('checked', true);
						$(this).addClass('checked');
						$('.delete_line_item').removeClass('new_delete_box');
						// }
					});

				} else {
					$('.checkbox.delete_line_item').each(function(){
						$(this).find('input').prop('checked', false);
						$(this).removeClass('checked');
					});
				}
			});

			$('body').on('change', '.NewItems', function(){

				if(!$(this).find('.bid-category text').hasClass('default'))
				{
					$(this).find('.delete_line_item').removeClass('new_delete_box');
				}

			});

			// Disable Invoice Button Until Bid has Been Saved
			$('body').on('change', '#job_bid table', function(e){
//				$('.invoice-type-button').addClass('disabled');
				$('.save-bid-message').show().addClass("yellow").removeClass("green").text("You must Save your Bid Before Creating an Invoice or Refresh the page to revert Changes");
//				$(".SaveJobsBidBtn").removeAttr("disabled");
			});

			// Delete Line Items
			$('.delete_line_item').click(function(event) {
				event.preventDefault();
			});

			// BID LINE ITEM REPEATER FUNCTION
			// ----------------------------------------------------

			$.fn.bidLineRepeater = function(fieldClass,increment) {

				var fieldWrapper = $(this);
				var fieldCount ={{ (isset($line_item_count)) ? $line_item_count : 5 }}; //Track the field amounts
				var increment = true; //Option to Increment Input Names

				$('body').on('click', '.add-line-item', function(event) {

					// Prompt User to Save Bid Before Creating an Invoice

					var clonedObject = $(fieldClass+'.hidden', fieldWrapper).clone(true);
					clonedObject.removeClass('hidden');
					clonedObject.addClass('existing');
					clonedObject.find('.delete_line_item').addClass('new_delete_box');
					clonedObject.find('select[name=category]').addClass('dropdown search bid_field');
					clonedObject.appendTo(fieldWrapper).show();
					clonedObject.find('.bid_field').dropdown({fullTextSearch:true});
					$('.ui.checkbox').checkbox();
					clonedObject.find('.ui.checkbox').checkbox('uncheck');
					fieldCount++;//text box added increment

					if($('#delete_all').is(':checked') ) {

						$( "#delete_all" ).prop( "checked", false );
					}

//					if(increment == true) {
//						clonedObject.find('input').each(function () {
//							//Make Sure not to add a name to Semantic UI's Hidden Input for DropDowns
//							if(!$(this).hasClass('search')){
//								var originalName = $(this).attr('name');
//								$(this).attr('name', originalName + "["+fieldCount+"]" );
//								// Handle Radio Inputs
//								if ( $(this).attr('type') == 'radio' ) {
//									var originalId = $(this).attr('id');
//									$(this).attr('id', originalId + "_" + fieldCount );
//								};
//							}
//						});
//						clonedObject.find('select').each(function () {
//							var originalName = $(this).attr('name');
//							if(originalName){
//								$(this).attr('name', originalName + "["+fieldCount+"]" );
//							}
//						});

//					}
					//find last item, add tax class
					$(".line_item:last-child").addClass("NewItems").find('td:last').addClass('tax-ident');

					$(".line_item:last-child").find('.tax-ident .checkbox input[type="checkbox"]').addClass('checkbox_individual_toggle');

					$(".line_item:last-child").find('.tax-ident').append('<input type="hidden" class="line_item_id_field" disabled="disabled" name="line_item_id[]" value="0" />');

					$(".line_item:last-child").find('.tax-ident').append('<input type="hidden" class="is_taxable_toggle is_taxable_field " disabled="disabled" name="is_taxable[]" value="0" />');
				});

				$('.line_item').on('focus', '.task', $.proxy(function (e) {
					var task = $(e.currentTarget);
					var row = task.closest('.line_item');
					var lastRow = row.parent().find(".line_item").last();
					var isLast = row[0] === lastRow[0];
					var code = e.keyCode || e.which;
					//console.log("code", isLast)
					// Code 9  is the Tab Key
					if (isLast /*&& code == '9'*/)
					{
						$('.add-line-item').trigger('click');
					}
				}, this));

				$('.remove-line-item').click(function() {

					$('.ui.checkbox.delete_line_item.checked').each(function() {
						$(this).closest('.line_item').hide().find('.bid-category select').attr('disabled', 'disabled');
						$(this).closest('.line_item').find('.bid-category input').attr('disabled', 'disabled');
						var lineItem = $(this).closest('.line_item');
						var remove_id = $(this).data('id');
						console.log(remove_id+' IS BEING REMOVED');
						if ( !lineItem.hasClass('hidden') ) {
							$('#job_bid').append('<input type="hidden" name="remove_line_item[]" value="'+remove_id+'" />');
							lineItem.remove();
						}
					});

					if($('#delete_all').is(':checked') ) {

						$( "#delete_all" ).prop( "checked", false );
					}

					// Trigger Change to Update Totals
					$('.item-amount input').trigger('change');

				});

				return this;

			};

			$('.line_items_repeater').bidLineRepeater('.line_item', true);
			// ------------------------------------------------------------


			//Get Bid Items when Bid Field is Selected
			$('body').on('change', '.bid_field', function(e){
				//Get Parent
				lineItem = $(this).closest('.line_item');

				//Enabled Field by Removing Disabled Attribute
				lineItem.find('.bid_field select').removeAttr('disabled');
				lineItem.find('.is_taxable_field').removeAttr('disabled');
				lineItem.find('.line_item_id_field').removeAttr('disabled');

				//Get the Current Bid Items Input ID
				bidItemInputName = lineItem.find('td.bid-item select').attr('name');

				bidFieldId = $(this).dropdown('get value');


				// Store Url via Laravel Routes
				getFieldItemsUrl = "{{route('job.bid.items.drop','') }}/" + bidFieldId;


				//Make Sure its not a Custom Input
				if (bidFieldId == "custom") {

					//Store Field Name
					fieldName = $(this).find('select').attr('name');
					itemName = lineItem.find('.bid-item select').attr('name');

					//Reset Info Inputs
					$(this).closest('td').html('<input placeholder="Enter Category" name="'+fieldName+'" type="text">');
					lineItem.find('td.item-units > input').val('');
					lineItem.find('td.item-rate > input').val('');
					lineItem.find('td.item-amount > input').val('');
					lineItem.find('td.bid-item').html('<input placeholder="Enter Product/Service" name="product[]" type="text">');

					//Reveal The rest of the Inputs and Remove Disabled Attribute
					lineItem.find('input').each(function() {
						$(this).slideDown();
						$(this).removeAttr('disabled');
					});

				}
				else{
					//Set Contacts depending on Account Chosen - AJAX
					$.ajax({
						type: "GET",
						url: getFieldItemsUrl,
					})
							.done(function( data ) {
								//Replace old Html in Table with New Dropdown
								lineItem.find('td.bid-item').html(data);
								//Set Name with Corrent ID
								lineItem.find('td.bid-item select').attr('name',bidItemInputName )
								// Initiate Dropdown
								lineItem.find('.ui.dropdown.bid_item').dropdown({fullTextSearch:true});
								//Reveal THe rest of the Inputs
								lineItem.find('input').each(function() {
									$(this).slideDown();
									$(this).removeAttr('disabled');
								});
							});
				}


			});

			//Get Bid Item Info when Bid Item is Selected

			$('body').on('change', '.bid_item', function(e){


				//Get Parent
				lineItem = $(this).closest('.line_item');

				// Get Bid Item Id
				bidItemId = $(this).dropdown('get value');

				// Store Url via Laravel Routes
				getFieldItemsUrl = "{{route('job.bid.item.info','') }}/" + bidItemId;

				// Reset Quantity on Product/Service Change
				lineItem.find('.item-quantity > input').val('');

				//Enable Inputs
				lineItem.find('input').removeAttr('disabled');


				if (bidItemId == "custom") {

					//Store Field Name
					fieldName = $(this).find('select').attr('name');

					//Reset Info Inputs
					lineItem.find('td.item-units > input').val('');
					lineItem.find('td.item-rate > input').val('');
					lineItem.find('td.item-amount > input').val('');

					$(this).closest('td').append('<input placeholder="Enter Product/Service" name="product[]" type="text">');
					$(this).toggle();
					$(this).find('select').attr('disabled', 'disabled');
				}
				else{
					$.ajax({
						type: "GET",
						url: getFieldItemsUrl,
					})
							.done(function(data) {
								//Set Data to Input Fields
								lineItem.find('td.item-units > input').val(data.units);
								lineItem.find('td.item-rate > input').val(data.price);
								lineItem.find('td.item-amount > input').val(data.price).trigger('change');
							});
				}

			});


			// Update Total Upon Quantity Change
			$('body').on('input propertychange keyup', '.item-quantity input', function(e){


				var closest_line_item = $(this).closest('.line_item');

				var checkbox = closest_line_item.find('.tax-ident .checkbox_individual_toggle');
				var is_taxable = closest_line_item.find('.is_taxable_toggle').val();

				var tax_this_is_checked = checkbox.is(':checked');


				// Get Line Item Quantity
				quantity  = $(this).val();

				// Get Line Item Rate
				rate = closest_line_item.find('.item-rate > input').val();

				// Calculate Amount
				amount = quantity * rate;

				// Update Amount
				//$(this).closest('.line_item').find('.item-amount > input').val(amount.toFixed(2)).trigger('change');

				closest_line_item.find('.item-amount > input').val(amount.toFixed(2));
				closest_line_item.find('.item-amount > input').trigger('change');


			});

			// Update Total Upon Rate Change
			$('body').on('input propertychange keyup', '.item-rate input', function(e){


				var closest_line_item = $(this).closest('.line_item');

				var checkbox = closest_line_item.find('.tax-ident .checkbox_individual_toggle');
				var is_taxable = closest_line_item.find('.is_taxable_toggle').val();

				var tax_this_is_checked = checkbox.is(':checked');

				// Get Line Item Quantity
				quantity  = closest_line_item.find('.item-quantity > input').val();

				// Get Line Item Rate
				rate = $(this).val();

				// Calculate Amount
				amount = quantity * rate;

				// Update Amount
//				closest_line_item.find('.item-amount > input').val(amount.toFixed(2));

				closest_line_item.find('.item-amount > input').val(amount.toFixed(2));
				closest_line_item.find('.item-amount > input').trigger('change')

			});

			// Update Sub Total Upon Any Amount Change
			$('body').on('change', '.item-amount input', function(e){


				//Set Integer to Build off of
				subTotal = parseFloat(0);

				//Iterate Through each Amount Input Val and Add Up
				$('.item-amount input').each(function() {

					var closest_line_item = $(this).closest('.line_item');
					var checkbox = closest_line_item.find('.tax-ident .checkbox_individual_toggle');
					var is_taxable = closest_line_item.find('.is_taxable_toggle').val();
					var tax_this_is_checked = checkbox.is(':checked');


					// MAKE SURE TO SKIP LINE ITEMS THAT HAVE BEEN DELETED SO OUR TOTALS ARE CORRECT
					if (! closest_line_item.find('.delete_line_item input').is(':checked')) {

						amount = parseFloat($(this).val());

						qty = $(this).closest('.line_item').find('.item-quantity > input').val();

						if(amount){


							subTotal += parseFloat($(this).val());

						}

					}

				});



				$('input[name=bid_sub_total]').val(subTotal.toFixed(2)).trigger('change');
			});

			// Update Grand Total Upon Sub Total Update
			$('body').on('change', 'input[name=bid_sub_total]', function(e){

				var calc_total_tax_per_checkbox = 0;

				//Get Tax Rate
				taxRate = parseFloat($('input[name=bid_tax_amount]').val()/100);

				//Get Sub Total
				subTotal = parseFloat($(this).val());


				//Only Calculate Taxes if they exist
				// per checkbox that is selected
				if (taxRate) {

					$('.line_item:not(.hidden)').each(function(){

						var checkbox = $(this).find('.tax-ident .checkbox_individual_toggle');
						var is_taxable = $(this).find('.is_taxable_toggle').val();
						var tax_this_is_checked = checkbox.is(':checked');


						//originally a string
						var calc_amount = parseFloat($(this).find('.item-amount input').val());


						var calc_tax = 0;


						if(tax_this_is_checked && is_taxable != 0 && calc_amount > 0 && calc_amount !== null)
						{
							calc_tax = calc_amount * taxRate;
							calc_total_tax_per_checkbox += calc_tax;
						}

					});

					//taxAmount = subTotal * taxRate;
					taxAmount = calc_total_tax_per_checkbox;
					grandTotal = subTotal + taxAmount;

					$('input[name=sales_tax_total]').val(taxAmount.toFixed(2));
					$('input[name=bid_grand_total]').val(grandTotal.toFixed(2));
				}
				//If not just copy over the sub total
				else{
					$('input[name=bid_grand_total]').val(subTotal.toFixed(2));
				};

			});

			// Update Grand Total and Calculated Tax Upon Tax Rate Update
			$('body').on('input propertychange keyup', 'input[name=bid_tax_amount]', function(e){


				//Get Tax Rate
				taxRate = parseFloat($(this).val()/100);

				//Get Sub Total
				subTotal = parseFloat($('input[name=bid_sub_total]').val());

				//Only Calculate Taxes if they exist
				if ($(this).val()) {

					//Update Calculated Sales Tax
					taxAmount = subTotal * taxRate;
					$('input[name=sales_tax_total]').val(taxAmount.toFixed(2));

					//Update Grand Total
					grandTotal = subTotal + taxAmount;
					$('input[name=bid_grand_total]').val(grandTotal.toFixed(2));
				}
				//If not just copy over the sub total
				else{
					$('input[name=bid_grand_total]').val(subTotal.toFixed(2));
				};
			});


			$('body').on('change', '.tax_description', function(e){

				//Set Contacts depending on Account Chosen - AJAX
				var taxRateId = $(this).dropdown('get value');


				if (taxRateId == "custom") {

					//Store Field Name
					fieldName = $(this).find('select').attr('name');

					$(this).closest('.field').html('<input placeholder="Enter Enter Tax Rate"name="'+fieldName+'"type="text">');
				}
				else{
					$.ajax({
						type: "GET",
						url: "{{route('job.tax.rate','') }}/" + taxRateId,
					})
							.done(function( data ) {
								$('#bid_tax_amount').val(data);
								$('#bid_tax_amount').trigger('propertychange');
							});
				}
			});
			// ************
			function AlertMsg(Type,  Msg1, Msg2){
				var Data =  '<div class="ui ' + Type + ' message">'+
						'<i class="close icon"></i>'+
						'<div class="header">'+
						Msg1 +
						'</div>'+
						'<p>' + Msg2 + '</p>'+
						'</div>';
				return Data;
			}
			//********
			// Updating job bids
			$('.invoice-type-button').click(function() {

				$(".Job_Bids_Items").submit(function (e) {
					e.preventDefault();
					var Url = $(this).attr("action");
//				$(".SaveJobsBidBtn").attr("disabled", true);
					$(".invoice-type-button").attr("disabled", true);

					// reset check all tax toggle for UI
					//$('#tax_all').removeClass('checked');
					//$('#tax_all').prop('checked', false);

					var request = $.ajax({
						url: Url,
						method: "POST",
						data: $(this).serialize(),
						dataType: "json"
					});

					request.done(function (msg) {
						if (msg.detail == "success") {

							var reloadRequest = $.ajax({
								url: '/job/bid/reload',
								method: "POST",
								data: $('#job_bid').serialize(),
								dataType: "json"
							});

							reloadRequest.done(function (data) {
								$('input[name="remove_line_item[]"]').each(function (index) {
									console.log($(this));
									$(this).remove();
								});

								var lineItemIds = data.ids;
								$('.line_item.existing input[name="line_item_id[]"]').each(function (index) {
									$(this).val(lineItemIds[index]);
								})
							});

							$(".save-bid-message").removeClass("yellow").addClass("green").show().text(msg.message);
							//$(".SaveJobsBidBtn").removeAttr("disabled");
							$(".invoice-type-button").removeAttr("disabled");

							$('.ui.invoice-type').modal('show');

						} else {
							$(".save-bid-message").removeClass("yellow").addClass("green").show().text(msg.message);
							//$(".SaveJobsBidBtn").removeAttr("disabled");
							$(".invoice-type-button").removeAttr("disabled");
						}
					});

					request.fail(function (msg) {
						$(".save-bid-message").removeClass("yellow").addClass("red").show().text(msg.message);
						//$(".SaveJobsBidBtn").removeAttr("disabled");
						$(".invoice-type-button").removeAttr("disabled");
					});
				});
				//****
			});

		});
	})(jQuery);
</script>