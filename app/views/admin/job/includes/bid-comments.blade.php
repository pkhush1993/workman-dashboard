{{--============== Bid Comment ==============--}}
<h2>Bid Comments</h2>

{{ Form::open(array('class' => 'ui form activity-form Bid_CommentsForm','route' => ['job.bid.comments', $job_id, $bid ? $bid->id : ''])) }}
	<div class="bid-comments-repeater">
		{{ Form::hidden('removed_ids', $value=NULL) }}
		<div class="two fields bid-comment hidden">
			<div class="fourteen wide field">
				{{ Form::label('bid_comment', 'New Bid Comment') }}
				{{ Form::select('bid_comment[]', $default_comments, $value=NULL, array('class' => 'ui bid_comment fluid', 'autocomplete' => 'off', 'placeholder' => 'Enter Bid Comment','data-name' => 'bid_comment')) }}
			</div>
			<div class="two wide field">
				{{ Form::label('', '&nbsp;') }}
				<i class="large green add circle icon add-field"></i>
				<i class="large red minus circle icon remove-field"></i>
			</div>		
		</div>
		{{--============ Bid Comments ============--}}
		@if ($job->getBid() && !$job->getBid()->getBidComments()->isEmpty() )
			@foreach ($job->getBid()->getBidComments() as $comment)
			<div class="two fields bid-comment">
				<div class="fourteen wide field">
					{{ Form::text('bid_comment['.$comment->id.']', $comment->comment , array('placeholder' => 'Enter Bid Comment','data-id' => $comment->id,'class' => 'bid-comment-input ui bid_comment search dropdown')) }}
				</div>
				<div class="two wide field">
					<i class="large red minus circle icon remove-bid-comment remove-field"></i>
				</div>		
			</div>	
			@endforeach
		@endif
		<div class="two fields bid-comment">
			<div class="fourteen wide field">
				{{ Form::label('bid_comment', 'New Bid Comment') }}
				{{ Form::select('bid_comment[]', $default_comments, $value=NULL, array('class' => 'ui bid_comment search dropdown', 'autocomplete' => 'off', 'placeholder' => 'Enter Bid Comment','data-name' => 'bid_comment')) }}
			</div>
			<div class="two wide field">
				{{ Form::label('', '&nbsp;') }}
				<i class="large green add circle icon add-field"></i>
				<i class="large red minus circle icon remove-field"></i>
			</div>		
		</div>		
	</div>
	{{-- Form::submit('UPDATE BID COMMENTS',array('class' => 'ui submit green button UpdateBidCommentsBtn')); --}}
	<span class="ui yellow message save-bidComments-message" style="display:none"></span>
{{ Form::close() }}

<script>
	jQuery.noConflict();
	(function( $ ) {
		$(function() {

			$('.activity-field-repeater').repeater('.activity-field',false);

			$('.bid-comments-repeater').repeater('.bid-comment',false);

			$('body').on('change', '.bid_comment', function(e) {
				var comment_val = $(this).dropdown('get value');
				if ( comment_val == 'custom') {
					$(this).closest('.fourteen.wide.field').append('<input class="bid_comment_custom" type="text" name="bid_comment[]" >');
					$(this).closest('.bid_comment_customer').focus();
					var comment = $(this).closest('.fourteen.wide.field').find('.bid_comment');
					comment.toggle();
				}
			});
			
			// Updating Bid comments
			$(".Bid_CommentsForm").submit(function(e){
				e.preventDefault();
				var Url = $(this).attr("action");

				var request = $.ajax({
					url: Url,
					method: "POST",
					data: $(this).serialize(),
					dataType: "json"
				});

				request.done(function( msg ) {
					console.log(msg);
					if (msg.detail == "success") {
						$(".save-bidComments-message").removeClass("yellow").addClass("green").show().text(msg.message);
					}else{
						$(".save-bidComments-message").removeClass("yellow").addClass("red").show().text(msg.message);
					}
				});

				request.fail(function( msg, textStatus ) {
					console.log(msg);
					$(".save-bidComments-message").removeClass("yellow").addClass("red").show().text(msg.message);
				});

				setTimeout(function(){
					$(".UpdateBidCommentsBtn").removeAttr("disabled");
					$(".Job_Bids_Items").find("button").removeAttr("disabled");
				}, 700);
			});
			//****


		});
	})(jQuery);
</script>
