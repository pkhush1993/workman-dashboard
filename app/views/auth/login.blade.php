@extends('layouts.blank')

@section('content')

<div id="login">
	<h1><i class="user outline icon"></i>Login</h1>
	<div class="ui blue segment">
		{{ Form::open(array('class' => 'ui form','route' => 'auth.login')) }}
			<div class="field">
				{{ Form::label('email', 'Email') }}
				{{ Form::text('email',$value = null, array('placeholder'=>'Enter Email','id' => 'email')) }}
			</div>
			<div class="field">
				{{ Form::label('password', 'Password') }}
				{{ Form::password('password',$value = null, array('placeholder'=>'Password','id' => 'password')) }}
			</div>

			<div class="inline field">
			    <div class="ui checkbox">
			    	{{ Form::label('remember', 'Remember Me') }}
			    	{{ Form::checkbox('remember') }}
			    </div>
			</div>
			{{ Form::submit('LOGIN', array('class' => 'ui submit green button')) }}
			<small class="forgot-pass"><em>{{ HTML::link('auth/forgot', 'Forgot Password') }}</em></small>
			<div class="ui error message"></div>
		{{ Form::close() }}
	</div>
	{{-- REMOVED link to auto register for security --}}
	{{--<p><small><em>Not Registered Yet? {{ HTML::link('auth/register', 'Create an Account') }}</em></small></p>--}}
</div>

@stop
