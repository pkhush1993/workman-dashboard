@extends('layouts.blank')

@section('content')

<div id="register">
	<h1><i class="user outline icon"></i>Setup Your Account</h1>
	<div class="ui blue segment">
		{{ Form::open(array('class' => 'ui form','route' => 'auth.post.setup.account')) }}
				{{ Form::hidden('activation_code',$activationCode) }}
				{{ Form::hidden('email',$user->email) }}
				<div class="required field">
					<label for="first-name">Name</label>
					<div class="two fields">
			        <div class="field">
			        	{{ Form::text('first_name',$user->first_name, array('placeholder'=>'First Name')) }}
			        </div>
					<div class="field">
						{{ Form::text('last_name',$user->last_name, array('placeholder'=>'Last Name')) }}
					</div>
 	 			</div>
				</div>
			<div class="two fields">
				<div class="required field">
					<label>Password</label>
					{{ Form::password('password',$value = null, array('placeholder'=>'Password')) }}
				</div>
				<div class="required field">
					<label>Repeat Password</label>
					{{ Form::password('password_check',$value = null, array('placeholder'=>'Re-Enter Password')) }}
				</div>				
			</div>
			<label>Address</label>
			<div class="two fields">
				<div class="field">
					{{ Form::text('address_1',$value = null, array('placeholder'=>'Address')) }}
				</div>
				<div class="field">
					{{ Form::text('address_2',$value = null, array('placeholder'=>'Address 2')) }}
				</div>
			</div>
			<div class="fields">
				<div class="seven wide field">
					<label>City</label>
					{{ Form::text('city',$value = null, array('placeholder'=>'City')) }}
				</div>
				<div class="five wide field">
					<label>State</label>
					{{ Form::select('state', array(
						'' => 'Select State',
						'AL' => 'Alabama',
						'AK' => 'Alaska',
						'AZ' => 'Arizona',
						'AR' => 'Arkansas',
						'CA' => 'California',
						'CO' => 'Colorado',
						'CT' => 'Connecticut',
						'DE' => 'Delaware',
						'DC' => 'District Of Columbia',
						'FL' => 'Florida',
						'GA' => 'Georgia',
						'HI' => 'Hawaii',
						'ID' => 'Idaho',
						'IL' => 'Illinois',
						'IN' => 'Indiana',
						'IA' => 'Iowa',
						'KS' => 'Kansas',
						'KY' => 'Kentucky',
						'LA' => 'Louisiana',
						'ME' => 'Maine',
						'MD' => 'Maryland',
						'MA' => 'Massachusetts',
						'MI' => 'Michigan',
						'MN' => 'Minnesota',
						'MS' => 'Mississippi',
						'MO' => 'Missouri',
						'MT' => 'Montana',
						'NE' => 'Nebraska',
						'NV' => 'Nevada',
						'NH' => 'New Hampshire',
						'NJ' => 'New Jersey',
						'NM' => 'New Mexico',
						'NY' => 'New York',
						'NC' => 'North Carolina',
						'ND' => 'North Dakota',
						'OH' => 'Ohio',
						'OK' => 'Oklahoma',
						'OR' => 'Oregon',
						'PA' => 'Pennsylvania',
						'RI' => 'Rhode Island',
						'SC' => 'South Carolina',
						'SD' => 'South Dakota',
						'TN' => 'Tennessee',
						'TX' => 'Texas',
						'UT' => 'Utah',
						'VT' => 'Vermont',
						'VA' => 'Virginia',
						'WA' => 'Washington',
						'WV' => 'West Virginia',
						'WI' => 'Wisconsin',
						'WY' => 'Wyoming',
					), '' , array('class'=>'ui search dropdown','autocomplete' =>'off')) }}
				</div>
				<div class="four wide field">
					<label>Zip</label>
					{{ Form::text('zip',$value = null, array('placeholder'=>'Zip Code')) }}
				</div>
			</div>
			<input type="submit" class="ui submit green button" value="SUBMIT">
			<div class="ui error message"></div>
		{{ Form::close() }}
	</div>
</div>


<script>
	jQuery.noConflict();
	(function( $ ) {
		$(function() {

			//Semantic UI Register Form Validation
			$('.ui.form').form({
			    firstName: {
					identifier  : 'first-name',
					rules: [{
							type   : 'empty',
							prompt : 'Please enter the First Name'
					}]
			    },
			    lastName: {
					identifier  : 'last-name',
					rules: [{
							type   : 'empty',
							prompt : 'Please enter the Last Name'
					}]
			    },
			    userName: {
					identifier  : 'email',
					rules: [
						{
							type   : 'empty',
							prompt : 'Please enter your User Name'
						},
						{
							type   : 'email',
							prompt : 'You must enter a valid email address'
						}						
					]
			    },
			    password: {
					identifier  : 'password',
					rules: [
						{
							type   : 'empty',
							prompt : 'Please enter a password'
						},
						{
							type   : 'length[6]',
							prompt : 'Your password must be at least 6 characters'
						}
					]
			    }		    			    	    
			});

		});
	})(jQuery);
</script>

@stop