@extends('layouts.blank')

@section('content')

<div id="reset-password">
	<h1><i class="help circle icon"></i>Reset Password</h1>
	<div class="ui blue segment">
		{{ Form::open(array('class' => 'ui form')) }}

			<div class="field">
				{{ Form::label('password', 'New Password') }}
				{{ Form::password('password', array('placeholder' => 'New Password')) }}
			</div>

			<div class="field">
				{{ Form::label('confirm_password', 'Confirm Password') }}
				{{ Form::password('confirm_password', array('placeholder' => 'Confirm Password')) }}
			</div>
			
			<div class="field">
				{{ Form::submit('Reset Password', array('class' => 'ui green button')) }}
			</div>
			<div class="ui error message"></div>
		{{ Form::close()}}
		<br>
		<p><small><em>{{ HTML::link('auth/login', 'Remeber your password?') }}</em></small></p>
	</div>
</div>

@stop