<div class="LoginBoxModal" style="display: none">
    <div id="login">
        <h1><i class="user outline icon"></i>Login</h1>
        <div class="LoginMessages"></div>

        <div class="ui blue segment">
            {{ Form::open(array('class' => 'ui form AjaxLoginForm','route' => 'auth.ajax.login')) }}
            <div class="field">
                {{ Form::label('email', 'Email') }}
                {{ Form::text('email',$value = null, array('placeholder'=>'Enter Email','id' => 'email')) }}
            </div>
            <div class="field">
                {{ Form::label('password', 'Password') }}
                {{ Form::password('password',$value = null, array('placeholder'=>'Password','id' => 'password')) }}
            </div>

            <div class="inline field">
                <div class="ui checkbox">
                    {{ Form::label('remember', 'Remember Me') }}
                    {{ Form::checkbox('remember') }}
                </div>
            </div>
            {{ Form::submit('LOGIN', array('class' => 'ui submit green button LoginSubmit')) }}
            <small class="forgot-pass"><em>{{ HTML::link('auth/forgot', 'Forgot Password') }}</em></small>
            <div class="ui error message"></div>
            {{ Form::close() }}
        </div>
        <p><small><em>Not Registered Yet? {{ HTML::link('auth/register', 'Create an Account') }}</em></small></p>
    </div>
</div>
<style>
    .LoginBoxModal{
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        z-index: 9999999999;
        background: #fff;
    }
</style>

<script type="text/javascript">
    jQuery.noConflict();
    (function( $ ) {
        $(function() {

//            setTimeout(function(){
//                $(".LoginBoxModal").show();
//            }, 1500000); // 25Min = 1500000 and 20 Min = 1500000 and 15 Min = 1500000 and 10 Min = 1500000

            $(".AjaxLoginForm").submit(function(e){
                e.preventDefault();
                var Url = $(this).attr("action");
                var ErroMsg = '<div class="ui negative message ErrorMessage">'+
                        '<i class="close icon"></i>'+
                        '<div class="header">'+
                        'Error!'+
                        '</div>'+
                        '<p>Please check your login username and password and try again.'+
                        '</p>'+
                        '</div>';
                var SuccessMsg = '<div class="ui success message SuccessMessage">'+
                        '<i class="close icon"></i>'+
                        '<div class="header">'+
                        'Success!'+
                        '</div>'+
                        '<p>Welcome back, You have successfully logged in.</p>'+
                        '</div>';

                var jqxhr = $.post( Url, $(this).serialize(),  function(data) {

                    if (data.details == "Success"){
                        $(".LoginMessages").html(SuccessMsg);
                        setTimeout(function(){
                            $(".LoginBoxModal").hide();
                            $(".LoginSubmit").removeAttr("disabled");
                        }, 1000);
                    }else{
                        $(".LoginMessages").html(ErroMsg);
                        $(".LoginSubmit").removeAttr("disabled");
                    }

                })
                .fail(function(data) {
                    $(".LoginMessages").html(ErroMsg);
                    $(".LoginSubmit").removeAttr("disabled");
                });


            });


            setInterval(function(){

                var jqxhr = $.get( "/users/login-status/", function(data) {

                    if (data.details == "login"){
                        $(".LoginBoxModal").css("display", "none");
                    }else{
                        $(".LoginBoxModal").show();
                    }

                });
            }, 5000);

        });
    })(jQuery);
</script>