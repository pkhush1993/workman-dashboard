@extends('layouts.blank')

@section('content')

<div id="forgot-password">
	<h1><i class="help circle icon"></i>Reset Password</h1>
	<div class="ui blue segment">
		{{ Form::open(array('class' => 'ui form')) }}
			<div class="field">
				{{ Form::email('email', $value = null, array('placeholder' => 'Email')) }}
			</div>

			<div class="field">
				{{ Form::submit('Send Reset Code', array('class' => 'ui blue button')) }}
			</div>
		{{ Form::close()}}

		<p><em>Enter your email and reset instructions will be sent</em></p>
		<p><small><em>{{ HTML::link('auth/login', 'Remember your password?') }}</em></small></p>
	</div>
</div>
@stop
