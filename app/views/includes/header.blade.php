<div @if(App::environment('staging')) style="margin-top: 20px" @endif class="ui menu">
	<div class="item">
		<a href="{{ url('/') }}">
			<img class="logo" src="{{ url('img/logo.png') }}" />
		</a>
  	</div>
	{{-- Serve a Restricted Menu if the Users Role is Time Entry --}}
	@if ( Sentinel::inRole('time_entry') )
		<div class="ui dropdown item">
			<i class="dropdown icon"></i>
			TimeKeeping
			<div class="menu">
				@if ($current_timesheet = $current_user_profile->getCurrentTimesheet($current_user_profile->id))
					<a href="{{ route('view.timesheet',$current_timesheet->id) }}" class="item">Edit Current Timesheet</a>
				@else
					<a href="{{ route('add.timesheet') }}" class="item">Add New Timesheet</a>
				@endif
	    		<a href="{{ route('view.timesheets') }}" class="item">View Past Timesheets</a>
	  		</div>
		</div>
		<div class="right menu">
	    	<div class="ui dropdown item">
	    		<i class="dropdown icon"></i>
				<img class="ui avatar image" src="{{$current_user_profile->getAvatar()}}">
				&nbsp;<span>{{$current_user_profile->getFirstName()}}</span>
				<a style="display:none" class="ui primary button log-out" href="{{ URL::to('auth/logout') }}">Log Out</a>
	      		<div class="menu">
	        		<a href="{{ URL::to('admin/settings/profile/'.$current_user->id) }}" class="item"><i class="user icon"></i>Edit Profile</a>
	        		<a href="{{ URL::to('auth/logout') }}" class="item">Logout</a>
	      		</div>
	    	</div>
		</div>
	{{-- If the Users Does Not have a Role of Time Entry Serve the Full Menu --}}
	@else
		<div class="ui dropdown item">
			<i class="dropdown icon"></i>
			<a href="{{ URL::to('admin') }}">
				<i class="ui icon dashboard"></i>
				Dashboard
			</a>
	  		<div class="menu">
				@if (Sentinel::inRole('admin') OR Sentinel::inRole('executive'))
					<a href="{{ route('dashboard.executive') }}" class="item">Executive Dashboard</a>
				@endif
				@if (Sentinel::inRole('admin') OR Sentinel::inRole('executive') OR Sentinel::inRole('foreman') )
					<a href="{{ route('dashboard.foreman',$current_user->id) }}" class="item">Foreman Dashboard</a>
				@endif
				@if (Sentinel::inRole('admin') OR Sentinel::inRole('executive') OR Sentinel::inRole('project_manager') )
					<a href="{{ route('dashboard.project-manager',$current_user->id) }}" class="item">Project Manager Dashboard</a>
				@endif
				@if (Sentinel::inRole('admin') OR Sentinel::inRole('executive') OR Sentinel::inRole('estimator') )
					<a href="{{ route('dashboard.estimator',$current_user->id) }}" class="item">Estimator Dashboard</a>
				@endif
				@if (Sentinel::inRole('admin') OR Sentinel::inRole('executive') OR Sentinel::inRole('accounting') )
					<a href="{{ route('dashboard.accounting',$current_user->id) }}" class="item">Accounting Dashboard</a>
				@endif
	  		</div>
		</div>
		@if ($current_user->hasAccess('navigation.jobs'))
			<div class="ui dropdown item">
				<i class="dropdown icon"></i>
				Jobs
				<div class="menu">
					<a href="{{ route('view.jobs') }}" class="item"><i class="unhide icon"></i>View Jobs</a>
					<a tabindex="-1" href="{{ route('add.job') }}" class="item"><i class="plus icon"></i>Add Job</a>
				</div>
			</div>
		@endif
		@if ($current_user->hasAccess('navigation.accounts'))
			<div class="ui dropdown item">
				<i class="dropdown icon"></i>
				Accounts &amp; Contacts
				<div class="menu">
					<a href="{{ route('company.accounts') }}" class="item"><i class="book icon"></i>Accounts</a>
					<a href="{{ route('company.contacts') }}" class="item"><i class="child icon"></i>Contacts</a>
				</div>
			</div>
		@endif
		@if ($current_user->hasAccess('navigation.timekeeping'))
			<div class="ui dropdown item">
				<i class="dropdown icon"></i>
				TimeKeeping
				<div class="menu">
					@if ($current_timesheet = $current_user_profile->getCurrentTimesheet($current_user_profile->id))
						<a href="{{ route('view.timesheet',$current_timesheet->id) }}" class="item">Edit Current Timesheet</a>
					@else
						<a href="{{ route('add.timesheet') }}" class="item">Add New Timesheet</a>
					@endif
					<a href="{{ route('view.timesheets') }}" class="item">View Past Timesheets</a>
					@if (Sentinel::inRole('admin') OR Sentinel::inRole('executive') OR Sentinel::inRole('project_manager') )
						<a href="{{ route('view.approve.timesheets') }}" class="item">Timesheets Review</a>
					@endif
					@if (Sentinel::inRole('admin') OR Sentinel::inRole('project_manager') )
						<a href="{{ route('search-time') }}" class="item">
							Search Time
						</a>
					@endif
				</div>
			</div>
		@endif
		@if ($current_user->hasAccess('navigation.reports'))
			<a href="{{ route('reports') }}" class="item">
				Reports
			</a>
		@endif

		<div class="right menu">
			@if ($current_user->hasAccess('navigation.jobs'))
			<div class="item">
				{{ Form::open( array('class' => 'ui form job-input-ajax-header-form','route' => 'go.to.job') ) }}
					<div class="ui search job-input-ajax-header" style="float: left;margin-right: 8px;">
						<div class="ui mini icon input">
							{{ Form::text('',$default = NULL, array('placeholder'=>'Search for Job...','class'=>'prompt')) }}
						</div>
						{{ Form::hidden('job_id',$default = NULL, array('class'=>'header-job')) }}
						<div class="results"></div>
					</div>
					{{ Form::submit('GO',array('class' => 'ui submit mini blue button','style' => 'float: left;')); }}
				{{ Form::close() }}
			</div>
				<div class="item">
					<a href="{{ route('add.job') }}" class="ui labeled small icon blue button">
						<i class="plus icon"></i>
						ADD NEW JOB
					</a>
				</div>
			@endif
			{{-- <div class="item">
				<div class="ui icon input">
					<input type="text" placeholder="Search...">
					<i class="search link icon"></i>
				</div>
			</div> --}}
	    	<div class="ui dropdown item">
	    		<i class="dropdown icon"></i>
				<img class="ui avatar image" src="{{$current_user_profile->getAvatar()}}">
				&nbsp;<span>{{$current_user_profile->getFirstName()}}</span>
				<a style="display:none" class="ui primary button log-out" href="{{ URL::to('auth/logout') }}">Log Out</a>
	      		<div class="menu">
	        		<a href="{{ URL::to('admin/settings/profile/'.$current_user->id) }}" class="item"><i class="user icon"></i>Edit Profile</a>
					@if (Sentinel::inRole('admin'))
						<a href="{{ URL::to('admin/settings/company/1') }}" class="item"><i class="setting icon"></i>Admin Panel</a>
					@endif
					@if (Sentinel::inRole('admin'))
						<a href="{{ route('admin.users') }}" class="item"><i class="users icon"></i>Edit Users</a>
					@endif
	        		<a href="{{ URL::to('auth/logout') }}" class="item">Logout</a>
	      		</div>
	    	</div>
		</div>
	@endif;
</div>

<script>
    jQuery.noConflict();
    (function( $ ) {
		$(function() {
			//Semantic UI AJAX Search =========================
			$('.ui.job-input-ajax-header.search')
			  .search({
				apiSettings: {
					url: '{{route("jobs.input.ajax")}}/' + '{query}',
				},
				maxResults: 50,
				onSelect(result) {
					$(this).closest('.job-input-ajax-header').find('input[name=job_id]').val(result.job_id);
					console.log(result);
					window.location.href = "/job/"+result.job_id;
				}
			});
		});
    })(jQuery);
</script>
