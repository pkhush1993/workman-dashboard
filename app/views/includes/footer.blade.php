<div class="ui footer vertical segment">
    <div class="ui stackable center aligned page grid">
        <div class="four wide column">
            <a href="{{ url('/') }}">
                <img src="{{ url('img/logo.png')}}" class="ui fluid image" alt="Workman's Dashboard Job Management System Software for Specialty Contractors"><br><br>
            </a>
            <p>
                <strong>Support (Toll Free): </strong><a href="tel:(800) 261-9142">(800) 261-9142</a><br>
                <strong>Email: </strong><a href="mailto:info@workmansdashboard.com">info@workmansdashboard.com</a><br>
            </p>
            <addr>
              <strong>Workman’s Dashboard</strong><br>
              P.O. Box 1284<br>
              North Bend, WA 98045<br>
            </addr>
      </div>
    </div>
    <div class="ui stackable center aligned page grid">
        @if(\Sentinel::check())
            <br>
        @endif
        <div class="six wide column"><p class="copyright">2016 - Copyright © Workman’s Dashboard. All Rights Reserved.</p></div>
    </div>
</div>

<script>
    jQuery.noConflict();
    (function(){
        var originalAddClassMethod = jQuery.fn.addClass;

        jQuery.fn.addClass = function(){
            var result = originalAddClassMethod.apply( this, arguments );
            jQuery(this).trigger('errorAdded');
            return result;
        }
    })();
    (function( $ ) {
        $(function() {
            // Globally Handles Auto Pop for Modals via Controller Return
            @if ( Session::get('pop') )
                $('.ui.modal.{{Session::get("pop")}}').modal({autofocus : false}).modal('show');
            @endif
            // Globally Handle Message Transitions
            setTimeout(function(){
                $('.ui.message.positive').transition({
                    animation  : 'fade',
                    duration   : '3s',
                });
            },1500);

            $('.ui.form').submit(function(e){
                if( $(this).hasClass('form-submitted') ){
                    e.preventDefault();
                    return;
                }

                $(this).find('select').each(function() {
                    if ($(this).val() == 'custom' ) {
                        $(this).remove();
                    }
                });

                $(this).addClass('form-submitted');

                $(this).find(':button').each(function(e) {
                    $(this).attr('disabled', 'disabled');
                })
            });

            $('.ui.form').bind('errorAdded', function() {
                if ( $(this).hasClass('error') ) {
                    $('body').find('.ui.form.form-submitted').each(function() {
                        $(this).removeClass('form-submitted');
                    });
                    $('body').find(':button').each(function() {
                        $(this).removeAttr('disabled');
                    });
                    $('body').find('input[type="submit"]').each(function() {
                        $(this).removeAttr('disabled');
                    });
                }
            });

            $('input[type=submit]').click(function() {
                $(this).attr('disabled', 'disabled');
                $(this).parents('form').submit()
            });
        });
    })(jQuery);
</script>





