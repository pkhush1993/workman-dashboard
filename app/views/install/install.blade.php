@extends('layouts.blank')

@section('content')


<div id="installer">


	<h1 class="ui icon header">
		<i class="settings icon"></i>
		<div class="content">
			Welcome to the Installer
			<div class="sub header">Manage your account settings and set e-mail preferences.</div>
		</div>
	</h1>

	<div class="ui selection list">
		<div class="item">
			<i class="large users icon"></i>
			<div class="content">
				<h3 class="header">
					<a href="{{ URL::to('install/users') }}">Install Users</a>
				</h3>
				<div class="description">Install the Default Users for the App</div>
			</div>
		</div>
		<div class="item">
			<i class="large rebel icon"></i>
			<div class="content">
				<h3 class="header">
					<a href="{{ URL::to('install/roles') }}">Install Roles</a>
				</h3>
				<div class="description">Install the Default User Roles for the App</div>
			</div>
		</div>		
	</div>

</div>


@stop