<!doctype html>
<html>
<head>
	@include('includes.head')
</head>
<body class="{{app('request')->segment(2)}}" style="overflow:visible;background:#fff;color:#000">

	@if ( $errors->count() )
		<div class="ui error message">
			<i class="close icon"></i>
			<div class="header">
				{{ $errors->first() }}
			</div>
		</div>		
	@endif


	@if ( Session::get('success') )
		<div class="ui positive message">
			<i class="close icon"></i>
			<div class="header">
				{{ Session::get('success') }}
			</div>
		</div>		
	@endif

	@yield('content')

</body>
</html>