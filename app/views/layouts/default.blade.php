<!doctype html>
<html>
<head>
	@include('includes.head')
</head>
<body class="{{app('request')->segment(1)}}">
	<!-- Loading Visual for Extended Page Loads the script to control this is included in local.js -->
	<div class="ui active inverted dimmer">
	  <div class="ui text loader">Loading</div>
	</div>
	@if( App::environment('staging') )
		<div class="staging-warning" style="color: #fff; background-color: red; text-align: center;">WARNING: You are using the Workman Dashboard staging site. Any data saved here will NOT reflect on production.</div>
	@endif
	<header>
		@include('includes.header')
	</header>

	<div id="page-content" style="opacity:0">
		<div class="ui page grid" style="margin:0">
			<div class="column">

				@if ( $errors->count() )
					<br>
					<div class="ui error message">
						<i class="close icon"></i>
						<div class="header">
							{{ $errors->first() }}
						</div>
					</div>
				@endif


				@if ( Session::get('success') AND !Session::get('pop'))
					<br>
					<div class="ui positive message">
						<i class="close icon"></i>
						<div class="header">
							{{ Session::get('success') }}
						</div>
					</div>
				@endif

				@yield('content')
			</div>
		</div>
	</div>

	<footer>
		@include('includes.footer')
	</footer>
	{{--User login modal--}}
	@include('auth.ajax-login')
</body>
</html>
