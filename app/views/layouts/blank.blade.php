<!doctype html>
<html>
<head>
	@include('includes.head')
</head>
<body>

	<div id="page-content">
		<div class="ui page grid">
			<div class="column">

				@if ( $errors->count() )
					<div class="ui error message">
						<i class="close icon"></i>
						<div class="header">
							{{ $errors->first() }}
						</div>
					</div>		
				@endif


				@if ( Session::get('success') )
					<div class="ui positive message">
						<i class="close icon"></i>
						<div class="header">
							{{ Session::get('success') }}
						</div>
					</div>		
				@endif

				@yield('content')
			</div>
		</div>
	</div>

	<footer>
		@include('includes.footer')
	</footer>

</body>
</html>