<?php namespace App\Controllers;
/**
 *	Copyright 2015 CoolBlueWeb
 *
 *	Licensed under the Apache License, Version 2.0 (the "License");
 *	you may not use this file except in compliance with the License.
 *	You may obtain a copy of the License at
 *
 *			http://www.apache.org/licenses/LICENSE-2.0
 *
 *	Unless required by applicable law or agreed to in writing, software
 *	distributed under the License is distributed on an "AS IS" BASIS,
 *	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *	See the License for the specific language governing permissions and
 *	limitations under the License.
 *
 *	@package 	Laravel
 *	@subpackage coolblueweb Custom
 *	@copyright  2015 CoolBlueWeb
 *	@author 	CoolBlueWeb <bryan@coolblueweb.com>
 */

use BaseController, Sentinel, View, Input, Response, Redirect, Session, Mail, Lang, Config,App,Excel;
use App\Models\Report;
use App\Models\Company;
use App\Models\JobRework;
use App\Models\JobNote;
use App\Models\JobBid;
use App\Models\Job;
use App\Models\Contact;
use App\Models\User;
use App\Models\Profile;
use App\Models\Invoice;
use App\Models\InvoiceLineItems;
use App\Models\Timesheet;
use App\Models\TimesheetDetail;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;


/**
 * Class TimeSheetSearchController for new time sheet reporting edits section
 *
 * @package App\Controllers
 */
class TimeSheetSearchController extends BaseController {


    /**
     * View Reports
     *
     * @return View
     */
    public function viewReports()
    {

        if (Sentinel::inRole('admin') OR Sentinel::inRole('project_manager') ) {
            return View::make('admin.timesheet-search.run-timesheet-search');
        }
        else {
            return Redirect::back()->withErrors('No Actions Available');
        };
    }

    /**
     * View Run Report
     *
     * @return View
     */
    public function showReports()
    {

        //Redirect::route('show.timesheet.search-results');
        if (Sentinel::inRole('admin') OR Sentinel::inRole('project_manager') ) {
            return Redirect::route('show.timesheet.search-results',[
                date('Y-m-d',strtotime(Input::get('start_date'))),
                date('Y-m-d',strtotime(Input::get('end_date'))),
                Input::get('job_id'),
                Input::get('user_id') ]);
        } else {
            return Redirect::back()->withErrors('No Actions Available');
        };

    }


    public function viewTimeSearchResults($start_date, $end_date, $job_id = 'all', $user_id = 'all') {

        // $data = [
        // 		'start_date'           => $start_date,
        // 		'end_date'             => $end_date,
        // 		'job_id'     => $job_id,
        // 		'user_id'      => $user_id,
        // ];

        //dd($data);

        // if empty query all
        if(!isset($job_id) || $job_id == 'all') {
            $job_id = '%';
        }

        // if empty query all
        if(!isset($user_id) || $user_id == 'all') {
            $user_id = '%';
        }
        // $query = \DB::select("SELECT CONCAT(users.first_name,' ',users.last_name), jobs.job_number, jobs.name, date_in, date_out, break, total, task
        // FROM timesheet_details
        // LEFT JOIN users ON timesheet_details.user_id=users.id
        // LEFT JOIN jobs ON timesheet_details.job_id=jobs.id
        // WHERE date_in BETWEEN '2016-01-01' AND '2016-12-30'
        // AND DATE(date_out) BETWEEN '2016-01-01' AND '2016-12-30'
        // AND timesheet_details.job_id = 13370
        // AND users.id= 172
        // ORDER BY users.last_name, users.first_name, timesheet_details.date_in");


        $query = \DB::select("SELECT timesheet_details.id, users.first_name,users.last_name, jobs.job_number, jobs.name, date_in, date_out, break, total, task, job_id, user_id
		FROM timesheet_details
		LEFT JOIN users ON timesheet_details.user_id=users.id
		LEFT JOIN jobs ON timesheet_details.job_id=jobs.id
		WHERE date_in BETWEEN '$start_date' AND '$end_date'
		AND DATE(date_out) BETWEEN '$start_date' AND '$end_date'
		AND timesheet_details.job_id LIKE '$job_id'
		AND users.id LIKE '$user_id'
		ORDER BY users.last_name, users.first_name, timesheet_details.date_in");

        // echo '<pre>';
        // dd($query);

        if ($query) {

            $count = count($query);

            return View::make('admin.timesheet-search.results')->with([
                'data' => $query,
                'start_date' => $start_date,
                'end_date'  => $end_date,
                'job_id' =>	$job_id,
                'user_id' => $user_id,
                'result_count' => $count

            ]);
        }
        else {
            return Redirect::back()->withErrors(Lang::get('company.no_report_results'));
        }



    }

    /**
     * Get Employees Input AJAX
     *
     * @param null $q
     * @return mixed
     */
    public function viewUserInputAJAX($q = null)
    {

        $_results = $users = User::where('first_name', 'like', '%'.$q.'%')->orWhere('last_name', 'like', '%'.$q.'%')->orderBy('last_name', 'desc')->get();
        $returned_results = [];

        foreach ($_results as $result) {
            $returned_results[] = ["first_name" =>$result->first_name,"last_name" => $result->last_name, "user_id" => $result->id];
        }

        return Response::json([ 'results' => $returned_results]);

        die();
    }


    public function updateTimeSheets()
    {

        // echo '<pre>';
        // die(var_dump($_REQUEST));

        $time_sheet_details = Input::except('_token');


        foreach( $time_sheet_details['time_sheet_id'] as $key => $time_sheet_detail)
        {

            $date_in  = $time_sheet_details['date_in'][$key];
            $date_out = $time_sheet_details['date_out'][$key];
            $break    = $time_sheet_details['break'][$key];
            $total    = $time_sheet_details['total'][$key];
            $job_id   = $time_sheet_details['job_id'][$key];
            $task     = $time_sheet_details['task'][$key];
            $user_id  = $time_sheet_details['user_id'][$key];


            $timesheed_id = $time_sheet_details['time_sheet_id'][$key];

            //Field
            $time_sheet = TimesheetDetail::find($timesheed_id);



            // echo '<pre>';
            // dd($time_sheet);
            //Ensure Bid Field Item Exists
            if ($time_sheet) {
                //get originals for project manager permissions testing
                //$original_date_in = date('Y-m-d H:i:s',strtotime($time_sheet->pluck('date_in')));
                $original_date_in = date('Y-m-d H:i:s',strtotime($time_sheet->date_in));
                $original_date_out = date('Y-m-d H:i:s',strtotime($time_sheet->date_out));
                $original_break = $time_sheet->break;



                // project managers can't edit time in, time out, or break time.
                if(Sentinel::inRole('project_manager'))
                {
                    // admins are also project managers
                    if(!Sentinel::inRole('admin'))
                    {
                        if($original_date_in != date('Y-m-d H:i:s',strtotime($date_in)))
                        {
                            return Redirect::back()->withErrors('Sorry, you do not have permission to edit this field.');
                        }
                        if($original_date_out != date('Y-m-d H:i:s',strtotime($date_out)))
                        {
                            return Redirect::back()->withErrors('Sorry, you do not have permission to edit this field.');
                        }
                        if($original_break != $break)
                        {
                            return Redirect::back()->withErrors('Sorry, you do not have permission to edit this field.');
                        }
                    }
                }


                try {
                    $time_sheet->fill([
                        'date_in'       => date('Y-m-d H:i:s',strtotime($date_in)),
                        'date_out'      => date('Y-m-d H:i:s',strtotime($date_out)),
                        'break' 	 	=>	$break,
                        'total'   	 	=>	$total,
                        'job_id' 		=>	$job_id,
                        'task' 			=>  $task,
                        'user_id'		=>  $user_id,
                    ])->save();
                } catch ( \Exception $e ) {
                    return Redirect::back()->withErrors($e->getMessage());
                }
            }
        }

        return Redirect::back()->with('success', Lang::get('Timesheet details have been updated'));

    }

    public function exportTimeSheetstoCSV($start_date, $end_date, $job_id = 'all', $user_id = 'all') {

        //$start_date = '2016-06-01';
        //$end_date = '2016-06-02';


        if(!isset($job_id) || $job_id == 'all') {
            $job_id = '%';
        }

        // if empty query all
        if(!isset($user_id) || $user_id == 'all') {
            $user_id = '%';
        }

        $query = \DB::select("SELECT timesheet_details.id, users.first_name,users.last_name, jobs.job_number, jobs.name, date_in, date_out, break, total, task, job_id, user_id
		FROM timesheet_details
		LEFT JOIN users ON timesheet_details.user_id=users.id
		LEFT JOIN jobs ON timesheet_details.job_id=jobs.id
		WHERE date_in BETWEEN '$start_date' AND '$end_date'
		AND DATE(date_out) BETWEEN '$start_date' AND '$end_date'
		AND timesheet_details.job_id LIKE '$job_id'
		AND users.id LIKE '$user_id'
		ORDER BY users.last_name, users.first_name, timesheet_details.date_in");

        //die(var_dump($query));

        //$query = array('foo' => 'bar');

        if($query) {

            //$this->object_to_array($query);

            $foo = json_encode($query);
            $bar = json_decode($foo, true);
            // echo '<pre>';
            // dd($bar);

            $query = $bar;

            try {
                Excel::create('Time Sheet Export', function($excel) use ($query) {

                    $excel->sheet('Excel sheet', function($sheet) use ($query) {

                        $sheet->fromArray($query);
                    });

                })->export('csv');
            } catch ( \Exception $e ) {
                // echo '<pre>';
                // die($e);
                return $e;
            }


        }

    }


}