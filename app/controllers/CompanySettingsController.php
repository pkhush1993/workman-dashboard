<?php namespace App\Controllers;
/**
 *	Copyright 2014 CoolBlueWeb
 *
 *	Licensed under the Apache License, Version 2.0 (the "License");
 *	you may not use this file except in compliance with the License.
 *	You may obtain a copy of the License at
 *
 *			http://www.apache.org/licenses/LICENSE-2.0
 *
 *	Unless required by applicable law or agreed to in writing, software
 *	distributed under the License is distributed on an "AS IS" BASIS,
 *	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *	See the License for the specific language governing permissions and
 *	limitations under the License.
 *
 *	@package 	Laravel
 *	@subpackage Panviso App
 *	@copyright  2014 CoolBlueWeb
 *	@author 	CoolBlueWeb <bryan@coolblueweb.com>
 */

use BaseController, Sentinel, Activation, Reminder, View, Input, Response, Redirect, Session, Mail, Lang, Config,App,DB,ProfileController;
use App\Models\Company;
use App\Models\Profile;
use App\Models\BidField;
use App\Models\BidFieldItem;
use App\Models\Activity;
use App\Models\TaxRate;
use App\Models\TimesheetTask;

class CompanySettingsController extends BaseController {

	/**
	 * View Company
	 *
	 * @param $company_id
	 * @return View
	 */
	public function viewCompany($company_id)
	{
		// Get Company
		$company = Company::find($company_id);
		//Get All Admins
		$all_admins = Sentinel::findRoleByName('Admin')->users()->get();
		// Get All Users
		$all_users = Sentinel::getUser()->all();
		//Get Bid Fields and Items (children)
		$bid_fields = BidField::getBidFieldsAndItems();
		// Get Default Activities
		$activities = Activity::all();
		// Get Default Activities
		$tax_rates = TaxRate::all();
		// Get Default Bid Comments
		$comments = App\Models\Comment::all();

		$leads = App\Models\Lead::all();

		$timesheet_tasks = TimesheetTask::all();

		//Admins
		foreach ($all_admins as $admin){

			$admin->profile =  $this->getProfile($admin->id);
		}


		//Invited Users
		$invited_users = array();
		foreach($all_users as $user){
			if (Activation::exists($user)){

				$user->profile =  $this->getProfile($user->id);

				$invited_users[] = $user;

			}
		}

		//Make Sure User is an Admin
		if ( Sentinel::inRole('admin') ) {
			return View::make('admin.company.company')->with(['company'=> $company,
															  	'all_admins'	=>	$all_admins,
																'invited_users' =>	$invited_users,
																'bid_fields'	=>  $bid_fields,
																'comments' 		=> 	$comments,
																'activities'	=>	$activities,
																'tax_rates'		=>	$tax_rates,
																'leads' 		=> 	$leads,
																'timesheet_tasks' => $timesheet_tasks]);
		}

		return Redirect::back()->withErrors('Must be an Administrator to View this Page');
	}


	/**
	 * Update Company
	 *
	 * @param $company_id
	 * @param $action
	 *
	 * @return Update
     */
	public function updateCompany($company_id, $action)
	{

		if ( $action == 'info' ) {
			return $this->updateCompanyInfo($company_id);
		}
		elseif ($action == 'settings') {
			return $this->updateCompanySettings($company_id);
		}
		elseif ($action == 'job_defaults') {
			return $this->updateJobDefaults($company_id);
		}
		elseif ($action == 'custom_fields') {
			return $this->updateCustomFields($company_id);
		}
		elseif ($action == 'invite_users') {
			return $this->postInviteUsers();
		}
		elseif ($action == 'send_invite') {
			return $this->resendInvite();
		}
		elseif ($action == 'remove_admin') {
			return $this->removeAdmin();
		}
		elseif ($action == 'contract_language') {
			return $this->updateContractLanguage($company_id);
		}
		elseif ($action == 'activities') {
			return $this->updateDefaultActivities($company_id);
		}
		elseif ($action == 'leads') {
			return $this->updateDefaultLeadSources($company_id);
		}
		elseif ($action == 'comments') {
			return $this->updateDefaultBidComments($company_id);
		}
		elseif ($action == 'tax-rates') {
			return $this->updateDefaultTaxRates($company_id);
		}
		elseif ($action == 'tasks') {
			return $this->updateTimesheetTasks();
		}
		else {
			return Redirect::back()->withErrors('No Actions Available (See CompanySettingsController)');
		}

	}


	/**
	 * Update Company Profile
	 *
	 * @param $company_id
	 *
	 * @return mixed
     */
    private function updateCompanyInfo($company_id)
	{

		$company = Company::find($company_id);

		//Ensure Company Exists
		if ($company) {
			$company->name = Input::get('company_name');
			$company->email = Input::get('email');
			$company->website = Input::get('website');
			$company->phone = Input::get('phone');
			$company->fax = Input::get('fax');
			$company->address_1 = Input::get('address_1');
			$company->address_2 = Input::get('address_2');
			$company->city = Input::get('city');
			$company->state = Input::get('state');
			$company->zip = Input::get('zip');

			try {
				$company->save();
//				return Redirect::to(tab_redirect('company.profile', 'personal'))->with('success', Lang::get('company.company_update'));
				$data = array(
					'detail' => 'success',
					'message' => Lang::get('company.company_update')
				);
				return $data;

			} catch ( \Exception $e ) {
//				return Redirect::back()->withErrors($e);
				return $e;
			}

		} else {

			$ErrData = array(
				'detail' => 'error',
				'message' => Lang::get('company.no_profile')
			);
			return $ErrData;
//			return Redirect::back()->withErrors(Lang::get('company.no_profile'));
		}

	}


	/**
	 * Update Company Settings
	 *
	 * @param $company_id
	 *
	 * @return mixed
     */
    private function updateCompanySettings($company_id)
	{
		$company = Company::find($company_id);

		//Ensure Company Exists
		if ($company) {
			$company->next_invoice_number   	=  Input::get('next_invoice_number');
			$company->next_job_number   		=  Input::get('next_job_number');
			$company->bid_header   				=  Input::get('bid_header');
			$company->contract_language			=  Input::get('contract_language');
			$company->avatar 					=  Input::get('avatar');
			$company->pay_period_type   		=  Input::get('pay_period_type');
			$company->pay_period_start 			=  date('Y-m-d',strtotime(Input::get('pay_period_start')));
			$company->pay_period_start_1 		=  Input::get('pay_period_start_1');
			$company->pay_period_start_2 		=  Input::get('pay_period_start_2');
			$company->time_entry_grace_period	=  Input::get('time_entry_grace_period');
			$company->overtime_pay 				=  Input::get('overtime_pay');
			$company->weekly_hours 				=  Input::get('weekly_hours');
			try {
				$company->save();
				$data = array(
					'detail' => 'success',
					'message' => Lang::get('company.company_update')
				);
				return $data;
//				return Redirect::back()->with('success', Lang::get('company.company_update'));
			} catch ( \Exception $e ) {
				$data = array(
					'detail' => 'error',
					'message' => $e
				);
				return $data;

//				return Redirect::back()->withErrors($e);
			}

		}
		else{
			$data = array(
				'detail' => 'error',
				'message' => Lang::get('company.no_profile')
			);
			return $data;
//			return Redirect::back()->withErrors(Lang::get('company.no_profile'));
		}

	}


	/**
	 * Update Job Defaults
	 *
	 * @param $company_id
	 *
	 * @return mixed
     */
    private function updateJobDefaults($company_id)
	{
		$company = Company::find($company_id);

		//Ensure Company Exists
		if ($company) {

			$defaults = implode(',',Input::except('_token'));
			$company->job_defaults   =  $defaults;

			try {
				$company->save();
				return Redirect::back()->with('success', Lang::get('company.company_update'));
			} catch ( \Exception $e ) {
				return Redirect::back()->withErrors($e);
			}

		}
		else{
			return Redirect::back()->withErrors(Lang::get('company.no_profile'));
		}

	}


	/**
	 * Update Job Defaults
	 *
	 * @param $company_id
	 *
	 * @return mixed
     */
    private function updateContractLanguage($company_id)
	{
		$company = Company::find($company_id);

		//Ensure Company Exists
		if ($company) {

			$contract_language = Input::get('contract_language');
			$company->contract_language   =  $contract_language;

			try {
				$company->save();
				return Redirect::back()->with('success', Lang::get('company.company_update'));
			} catch ( \Exception $e ) {
				return Redirect::back()->withErrors($e);
			}

		}
		else{
			return Redirect::back()->withErrors(Lang::get('company.no_profile'));
		}
	}

	/**
	 * Update Job Defaults
	 *
	 * @param $company_id
	 * @return Update
	 */
	private function updateCustomFields($company_id)
	{
		$company = Company::find($company_id);

		//Ensure Company Exists
		if ($company) {

			// Custom Drop Field 1
			$custom_drop_1 = Input::get('custom_drop_1');
			$custom_drop_1_items = Input::get('custom_drop_1_item');
			if ($custom_drop_1) {
				$company->custom_drop_1  = serialize(array( $custom_drop_1 => $custom_drop_1_items));
			} else {
				$company->custom_drop_1  = '';
			}
			// Custom Drop Field 2
			$custom_drop_2 = Input::get('custom_drop_2');
			$custom_drop_2_items = Input::get('custom_drop_2_item');
			if ($custom_drop_2) {
				$company->custom_drop_2  = serialize(array( $custom_drop_2 => $custom_drop_2_items));
			} else {
				$company->custom_drop_2  = '';
			}
			// Custom Drop Field 3
			$custom_drop_3 = Input::get('custom_drop_3');
			$custom_drop_3_items = Input::get('custom_drop_3_item');
			if ($custom_drop_3) {
				$company->custom_drop_3  = serialize(array( $custom_drop_3 => $custom_drop_3_items));
			} else {
				$company->custom_drop_3  = '';
			}


			// Custom Text Field 1
			$company->custom_text_1 = Input::get('custom_text_1');

			// Custom Text Field 2
			$company->custom_text_2 = Input::get('custom_text_2');

			// Custom Text Field 3
			$company->custom_text_3 = Input::get('custom_text_3');


			try {
				$company->save();
				$Data = array(
					'detail' => 'success',
					'message' => Lang::get('company.company_update')
				);
				return $Data;
//				return Redirect::back()->with('success', Lang::get('company.company_update'));
			} catch ( \Exception $e ) {
				$Data = array(
					'detail' => 'error',
					'message' => $e
				);
				return $Data;
//				return Redirect::back()->withErrors($e);
			}

		}
		else{
			$Data = array(
				'detail' => 'error',
				'message' => Lang::get('company.no_profile')
			);
			return $Data;
//			return Redirect::back()->withErrors(Lang::get('company.no_profile'));
		}

	}


	/**
	 * Update Default Activities
	 *
	 * @param $company_id
	 * @return Update
	 */
	private function updateDefaultActivities($company_id)
	{
		$company = Company::find($company_id);

		//Ensure Company Exists
		if ($company) {

			//Iterate Through Activities
			foreach(Input::get('activity') as $activity_id => $activity_val) {

				// Skip Empty Input Values
				if ($activity_val) {
					//Get Activity
					$activity = Activity::find($activity_id);

					// If the Activity already Exists Update It
					if ($activity) {

						$activity->activity = $activity_val;

						// Update the Activity
						try {
							$activity->save();
						} catch ( \Exception $e ) {
							$Data = array(
								'detail' => 'error',
								'message' => $e
							);
							return $Data;
//							return Redirect::back()->withErrors($e);
						}
					}

					// If the Activity does NOT exist Create A New One
					else{

						$_activity = array(
								'activity'   =>	$activity_val,
								'sort_order' =>	1
						);

						// Create Activity
						try {
							Activity::create($_activity);
						} catch ( \Exception $e ) {
							$Data = array(
								'detail' => 'error',
								'message' => $e
							);
							return $Data;
//							return Redirect::to(tab_redirect('company.profile', 'activities'))->withErrors($e);
						}
					}
				}
			}

			// Check For Deleted Fields
			if (Input::get('removed_ids')) {

				$removed_fields = explode(',', Input::get('removed_ids'));

				foreach ($removed_fields as $remove_field) {
					$removed_field = Activity::find($remove_field);

					// Remove Contact
					try {
						$removed_field->delete();
					} catch ( \Exception $e ) {
						$Data = array(
							'detail' => 'error',
							'message' => $e
						);
						return $Data;
//						return Redirect::to(tab_redirect('company.profile', 'activities'))->withErrors($e);
					}
				}
			}

			// SUCCESS!!! Fields Updated
			$Data = array(
				'detail' => 'success',
				'message' => Lang::get('company.activity_update')
			);
			return $Data;
//			return Redirect::to(tab_redirect('company.profile', 'activities'))->with('success', Lang::get('company.activity_update'));
		}
	}

	/**
	 * Update Default Lead Sources
	 *
	 * @param $company_id
     */
	private function updateDefaultLeadSources($company_id)
	{
		if ( $company = Company::find($company_id) ) {

			// Update and Create Lead Sources
			foreach ( Input::get('lead') as $lead_id => $lead_source ) {
				try {
					if ( strlen($lead_source) ) {
						App\Models\Lead::findOrNew($lead_id)
								->fill(['lead' => $lead_source])
								->save();
					}
				} catch ( \Exception $e ) {
					$Data = array(
						'detail' => 'error',
						'message' => $e
					);
					return $Data;
//					return Redirect::to(tab_redirect('company.profile', 'lead-sources'))->withErrors($e);
				}
			}

			// Remove Lead Sources
			if ( Input::get('removed_ids') ) {
				$lead_ids = explode(',', Input::get('removed_ids'));

				foreach ( $lead_ids as $lead_id ) {
					try {
						App\Models\Lead::find($lead_id)
								->delete();
					} catch ( \Exception $e ) {
						$Data = array(
							'detail' => 'error',
							'message' => $e
						);
						return $Data;
//						return Redirect::to(tab_redirect('company.profile', 'lead-sources'))->withErrors($e);
					}
				}
			}
		}

		$Data = array(
			'detail' => 'success',
			'message' => Lang::get('company.activity_update')
		);
		return $Data;
//		return Redirect::to(tab_redirect('company.profile', 'lead-sources'))->with('success', Lang::get('company.activity_update'));
	}

	/**
	 * Update Default Tax Rates
	 *
	 * @param $company_id
	 * @return Update
	 */
	private function updateDefaultTaxRates($company_id)
	{

		$company = Company::find($company_id);

		//Ensure Company Exists
		if ($company) {

			$tax_rates = Input::except('_token');

			//Iterate Through Activities
			foreach(Input::get('tax_rate') as $tax_rate_id => $tax_rate) {

				//Field
				$tax_description = $tax_rates['tax_description'][$tax_rate_id];




				// Skip Empty Input Values
				if ($tax_rate) {

					//Get Activity
					$_tax_rate = TaxRate::find($tax_rate_id);

					// If the Activity already Exists Update It
					if ($_tax_rate) {

						$_tax_rate->tax_rate = $tax_rate;
						$_tax_rate->tax_description = $tax_description;

						// Update the Activity
						try {
							$_tax_rate->save();
						} catch ( \Exception $e ) {
							$Data = array(
								'detail' => 'success',
								'message' => $e
							);
							return $Data;
//							return Redirect::to(tab_redirect('company.profile', 'tax-rates'))->withErrors($e);
						}
					}

					// // If the Tax Rate does NOT exist Create A New One
					else{

						$_tax_rate = array(
								'tax_rate'        => $tax_rate,
								'tax_description' => $tax_description
						);

						// Create Activity
						try {
							TaxRate::create($_tax_rate);
						} catch ( \Exception $e ) {
							$Data = array(
								'detail' => 'success',
								'message' => $e
							);
							return $Data;
//							return Redirect::to(tab_redirect('company.profile', 'tax-rates'))->withErrors($e);
						}
					}

				}
			}

			// Check For Deleted Fields
			if (Input::get('removed_ids')) {

				$removed_fields = explode(',', Input::get('removed_ids'));

				foreach ($removed_fields as $remove_field) {
					$removed_field = TaxRate::find($remove_field);

					// Remove Contact
					try {
						$removed_field->delete();
					} catch ( \Exception $e ) {
						$Data = array(
							'detail' => 'success',
							'message' => $e
						);
						return $Data;
//						return Redirect::to(tab_redirect('company.profile', 'tax-rates'))->withErrors($e);
					}
				}
			}

			// SUCCESS!!! Fields Updated
			$Data = array(
				'detail' => 'success',
				'message' => Lang::get('company.tax_rate_update')
			);
			return $Data;
//			return Redirect::to(tab_redirect('company.profile', 'tax-rates'))->with('success', Lang::get('company.tax_rate_update'));
		}
	}

	/**
	 * Invite Users
	 *
	 * @return Update
	 */
	private function postInviteUsers()
	{

		$user = Sentinel::findByCredentials(['login' => Input::get('invite_email')]);
		$roles = Input::get('invite_user_role');


		// Does User Already Exist?
		if ($user) {
			$UsrData = array(
				'detail' => 'error',
				'message' => Lang::get('authorization.user_exists')
			);
			return $UsrData;
//			return Redirect::to(tab_redirect('company.profile', 'users'))->withErrors(Lang::get('authorization.user_exists'));
		}
		// Create User
		try {

			//If Account Activation is Required
			if ( Config::get('admin.require_activation') ) {

				// Register User
				$user = Sentinel::create(array(
						'email'     => Input::get('invite_email'),
					//'password'  => Input::get('password'),
						'first_name'=>	Input::get('invite_first_name'),
						'last_name' =>	Input::get('invite_last_name')
				));

				// Create Profile
				if($user){
					$profile = array(
							'id'		=>	$user->id,
							'first_name'=>	Input::get('invite_first_name'),
							'last_name' =>	Input::get('invite_last_name'),
							'email'   	=>	Input::get('invite_email'),
							'avatar'   	=>	'/img/default_avatar.jpg'
					);
					//Profile::create($profile);
					DB::table('profiles')->insert($profile);
				}

				//Setup Activation
				$data['activationCode'] = Activation::create($user)->code;
				$data['email']			= base64_encode($user->email);
				Mail::send('admin.profile.emails.auth.invited_user', $data, function($message) use($user) {
					$message->to($user->email, '')->subject(Lang::get('authorization.email_activate_account_subject', array('url' => Config::get('app.url'))));
				});

				//Assign Role
				foreach ($roles as $role) {
					$user_role = Sentinel::findRoleBySlug($role);
					$user_role->users()->attach($user);
				}
				$SuccessData = array(
					'detail' => 'success',
					'message' => Lang::get('company.company_invite')
				);
				return $SuccessData;
//				return Redirect::to(tab_redirect('company.profile', 'users'))->with('success', Lang::get('company.company_invite'));
			}
			else{
				$user = Sentinel::registerAndActivate(array(
						'email'     => Input::get('email'),
						'password'  => Input::get('password'),
				));
				$SuccessData = array(
					'detail' => 'success',
					'message' => Lang::get('company.company_invite')
				);
				return $SuccessData;

//				return Redirect::to(tab_redirect('company.profile', 'users'))->with('success', Lang::get('company.company_invite'));
			}

		} catch (\Exception $e) {
			$ErrMessage = array(
				'detail' => 'error',
				'message' => $e->getMessage()
			);
//			return $ErrMessage;
			return $e->getMessage();

//			return Redirect::to(tab_redirect('company.profile', 'users'))->withErrors($e->getMessage());
		}

	}

	/**
	 * Resend Invite to User
	 *
	 * @return Update
	 */
	private function resendInvite()
	{
		if ( Input::get('action_type') == 'Revoke') {
			return $this->revokeInvitation();
		}
		
		$user = Sentinel::findByCredentials(['login' => Input::get('invite_email')]);

		//Send Email
		try {

			$data['activationCode'] = Activation::exists($user)->code;
			$data['email']			= base64_encode($user->email);

			Mail::send('admin.profile.emails.auth.invited_user', $data, function($message) use($user) {
				$message->to($user->email, '')->subject(Lang::get('authorization.email_activate_account_subject', array('url' => Config::get('app.url'))));
			});

			return Redirect::to('/admin/settings/company/1')->with('success', Lang::get('company.company_invite'));

		} catch (\Exception $e) {
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

	/**
	 * Revoke User Invitation
	 * @return mixed
	 */
	private function revokeInvitation()
	{
		try {
			$user = Sentinel::findByCredentials(['login' => Input::get('invite_email')]);
			Activation::remove($user);
			\App\Models\User::find($user->id)->profile()->delete();
			$user->delete();
			return Redirect::back()->with('success', Lang::get('company.revoke_invite_success'));

		} catch (\Exception $e) {
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

	/**
	 * Remove Admin
	 *
	 * @return Update
	 */
	private function removeAdmin()
	{

		$user = Sentinel::findByCredentials(['login' => Input::get('admin_email')]);

		//Send Email
		try {

			$role = Sentinel::findRoleByName('Admin');
			$role->users()->detach($user);

			return Redirect::to(tab_redirect('company.profile', 'users'))->with('success', Lang::get('company.admin_removed'));

		} catch (\Exception $e) {
			return Redirect::to(tab_redirect('company.profile', 'users'))->withErrors($e->getMessage());
		}
	}

	/**
	 * Update, Create or Delete Bid Comments
	 *
	 * @param $company_id
	 * @return mixed
     */
	private function updateDefaultBidComments($company_id)
	{
		if ( $company = Company::find($company_id) ) {

			// Update and Create Lead Sources
			foreach ( Input::get('comment') as $comment_id => $comment_comment ) {
				try {
					if ( strlen($comment_comment) ) {
						App\Models\Comment::findOrNew($comment_id)
								->fill(['comment' => $comment_comment])
								->save();
					}
				} catch ( \Exception $e ) {
					$data =  array(
						'detail' => 'error',
						'message' => $e
					);
					return $data;
//					return Redirect::back()->withErrors($e);
				}
			}

			// Remove Lead Sources
			if ( Input::get('removed_ids') ) {
				$comment_ids = explode(',', Input::get('removed_ids'));

				foreach ( $comment_ids as $comment_id ) {
					try {
						App\Models\Comment::find($comment_id)
								->delete();
					} catch ( \Exception $e ) {
						$data =  array(
							'detail' => 'error',
							'message' => $e
						);
						return $data;
//						return Redirect::to(tab_redirect('company.profile', 'comments'))->withErrors($e);
					}
				}
			}
		}

		$data =  array(
			'detail' => 'success',
			'message' => Lang::get('company.activity_update')
		);
		return $data;
//		return Redirect::to(tab_redirect('company.profile', 'comments'))->with('success', Lang::get('company.activity_update'));
	}

	private function updateTimesheetTasks()
	{
			// Update and Create Lead Sources
			foreach ( Input::get('task') as $task_id => $task ) {
				try {
					if ( strlen($task) ) {
						TimesheetTask::findOrNew($task_id)
							->fill(['task' => $task])
							->save();
					}
				} catch ( \Exception $e ) {
					$data =  array(
						'detail' => 'error',
						'message' => $e
					);
					return $data;
//					return Redirect::back()->withErrors($e);
				}
			}

			// Remove Lead Sources
			if ( Input::get('removed_ids') ) {
				$task_ids = explode(',', Input::get('removed_ids'));

				foreach ( $task_ids as $task_id ) {
					try {
						TimesheetTask::find($task_id)
							->delete();
					} catch ( \Exception $e ) {
						$data =  array(
							'detail' => 'error',
							'message' => $e
						);
						return $data;
//						return Redirect::to(tab_redirect('company.profile', 'comments'))->withErrors($e);
					}
				}
			}

		$data =  array(
			'detail' => 'success',
			'message' => Lang::get('company.activity_update')
		);
//		return $data;
		return Redirect::to(tab_redirect('company.profile', 'timesheet-task'))->with('success', Lang::get('company.activity_update'));
	}

	/**
	 * Get Profile
	 *
	 * @param $profile_id
	 * @return View
	 */
	public static function getProfile($profile_id)
	{
		return Profile::find($profile_id);
	}


}
