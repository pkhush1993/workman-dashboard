<?php namespace App\Controllers;
/**
*	Copyright 2014 CoolBlueWeb
*	
*	Licensed under the Apache License, Version 2.0 (the "License");
*	you may not use this file except in compliance with the License.
*	You may obtain a copy of the License at
*	
*			http://www.apache.org/licenses/LICENSE-2.0
*	
*	Unless required by applicable law or agreed to in writing, software
*	distributed under the License is distributed on an "AS IS" BASIS,
*	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*	See the License for the specific language governing permissions and
*	limitations under the License.
*
*	@package 	Laravel
*	@subpackage CoolBlueWeb Custom
*	@copyright  2014 CoolBlueWeb
*	@author 	CoolBlueWeb <bryan@coolblueweb.com>
*/

use BaseController, Sentinel, View, Input, Response, Redirect,Request, Session, Mail, Lang, Config,App;
use App\Models\JobRework;
use App\Models\JobNote;
use App\Models\Job;
use App\Models\User;
use App\Models\FollowUpNote;
use App\Models\Activity;
use App\Models\Contact;


class JobReworkController extends BaseController {

	/**
	 * Add Rework to Job
	 *
	 * @return View
	 */
	public function createJobRework($job_id)
	{
		$job = Job::find($job_id);

		// Ensure that Job exists
		if ($job ) {
			// Setup Job Rework Array
			$job_rework = array(
				'job_id'      => $job_id,
				'number'      => Input::get('number'),
				'description' => Input::get('description'),
				'deadline'    => date('Y-m-d',strtotime(Input::get('deadline'))),
				'status'      => 'active'
				);
			// Create Job Rework
			try {
				$job_rework = JobRework::create($job_rework);
				return Redirect::back()->with('success', Lang::get('company.job_rework_created'))->with('pop','job-rework');
			} catch ( \Exception $e ) {
				return Redirect::back()->withErrors($e->getMessage());
			}
		}
		// If Job is not found return with error
		else{
			return Redirect::back()->withErrors(Lang::get('company.no_job'));
		}

	}

	/**
	 * Delete Job Rework
	 *
	 * @return View
	 */
	public function deleteJobRework()
	{

		//Get Contact Ids
		$job_rework_ids = Input::get('selected_rework');

		// Ensure that Job Rework exists
		if ($job_rework_ids ) {

			// Run Through Each Job Contact ID
			foreach ($job_rework_ids as $job_rework_id) {

				// Find Job Contact
				$job_rework = JobRework::find($job_rework_id);
				
				//Soft Delete Job Contact
				try {
					$job_rework->delete();
				} catch ( \Exception $e ) {
					return Redirect::back()->withErrors($e->getMessage());
				}					
			}	
			return Redirect::back()->with('success', Lang::get('company.job_rework_deleted'))->with('pop','job-rework');
		}
		// If Job is not found return with error
		else{
			return Redirect::back()->withErrors(Lang::get('company.no_job_rework'));
		}

	}

	/**
	 * Complete Job Rework
	 *
	 * @return View
	 */
	public function completeJobRework()
	{

		//Get Contact Ids
		$job_rework_ids = Input::get('selected_rework');

		// Ensure that Job Rework exists
		if ($job_rework_ids ) {

			// Run Through Each Job Contact ID
			foreach ($job_rework_ids as $job_rework_id) {

				// Find Job Contact and Set Status
				$job_rework = JobRework::find($job_rework_id);

				$job_rework->status = 'closed';
				
				//Update Status to Closed
				try {
					$job_rework->save();
				} catch ( \Exception $e ) {
					return Redirect::back()->withErrors($e->getMessage());
				}
			}

			return Redirect::back()->with('success', Lang::get('company.job_rework_closed'))->with('pop','job-rework');
		}
		// If Job is not found return with error
		else{
			return Redirect::back()->withErrors(Lang::get('company.no_job_rework'));
		}

	}


	/**
	 * Re-Open Job Rework
	 *
	 * @return View
	 */
	public function openJobRework()
	{

		//Get Contact Ids
		$job_rework_ids = Input::get('selected_rework');

		// Ensure that Job Rework exists
		if ($job_rework_ids ) {

			// Run Through Each Job Contact ID
			foreach ($job_rework_ids as $job_rework_id) {

				// Find Job Contact and Set Status
				$job_rework = JobRework::find($job_rework_id);

				$job_rework->status = 'active';
				
				//Update Status to Closed
				try {
					$job_rework->save();
				} catch ( \Exception $e ) {
					return Redirect::back()->withErrors($e->getMessage());
				}
			}

			return Redirect::back()->with('success', Lang::get('company.job_rework_opened'))->with('pop','job-rework');
		}
		// If Job is not found return with error
		else{
			return Redirect::back()->withErrors(Lang::get('company.no_job_rework'));
		}

	}	


}	