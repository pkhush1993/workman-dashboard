<?php namespace App\Controllers;
/**
*	Copyright 2014 CoolBlueWeb
*
*	Licensed under the Apache License, Version 2.0 (the "License");
*	you may not use this file except in compliance with the License.
*	You may obtain a copy of the License at
*
*			http://www.apache.org/licenses/LICENSE-2.0
*
*	Unless required by applicable law or agreed to in writing, software
*	distributed under the License is distributed on an "AS IS" BASIS,
*	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*	See the License for the specific language governing permissions and
*	limitations under the License.
*
*	@package 	Laravel
*	@subpackage CoolBlueWeb Custom
*	@copyright  2014 CoolBlueWeb
*	@author 	CoolBlueWeb <bryan@coolblueweb.com>
*/

use BaseController, Sentinel, View, Input, Response, Redirect,Request, Session, Mail, Lang, Config,App;
use App\Models\JobRework;
use App\Models\JobNote;
use App\Models\Job;
use App\Models\User;
use App\Models\FollowUpNote;
use App\Models\Activity;
use App\Models\Contact;


class NotesController extends BaseController {

	/**
	 * Add Activity/Note to Job
	 *
	 * @return View
	 */
	public function createJobNote()
	{
		$job = Job::find(Input::get('job_id'));
		$withPop = true;
		// Since this could be coming from the dashboard, search
		// by name if finding by id fails.
		if ( !$job ) {
			$job = Job::where('name', Input::get('job_id'))->first();
			$withPop = false;
		}

		// Ensure that Job exists
		if ( $job ) {
			// Setup Job Note Array
			$job_note = array(
				'job_id'          => $job->getAttribute('id'),
				'activity'        => ( Input::get('activity_custom') ) ? Input::get('activity_custom') : Input::get('activity') ,
				'type'            => Input::get('type'),
				'note'            => Input::get('note'),
				'job_contacts_id' => Input::get('contact')
				);
			//Check If the Activity is a Follow Up, If it is attach assign and deadlines to the job note array
			if (Input::get('follow-up')) {
				$job_note['follow_up'] = 1;
				$job_note['deadline']  = date('Y-m-d',strtotime(Input::get('deadline')));
				$job_note['assigned']  = Input::get('assigned');
			}
			// Create Job Note
			try {
				$job_note = JobNote::create($job_note);
				if ( $withPop ) {
					return Redirect::back()->with('success', Lang::get('company.job_note_created'))->with('pop','job-notes');
				}
				return Redirect::back()->with('success', Lang::get('company.job_note_created'));
			} catch ( \Exception $e ) {
				return Redirect::back()->withErrors($e);
			}
		}
		// If Job is not found return with error
		else{
			return Redirect::back()->withErrors(Lang::get('company.no_job'));
		}

	}

	/**
	 * Update Job Note
	 *
	 * @return View
	 */
	public function updateNote($note_id)
	{
		$note = JobNote::find($note_id);

		// Ensure that Job exists
		if ( $note ) {

			$note->activity        = Input::get('activity');
			$note->type            = Input::get('type');
			$note->note            = Input::get('note');
			$note->job_contacts_id = Input::get('contact');
			$note->deadline        = date('Y-m-d',strtotime(Input::get('deadline')));
			$note->assigned        = Input::get('assigned');

			// Update Job Note
			try {
				$note->save();
				return Redirect::back()->with('success', Lang::get('company.job_note_updated'))->with('pop','job-notes');
			} catch ( \Exception $e ) {
				return Redirect::back()->withErrors($e);
			}
		}
		// If Note is not found return with error
		else{
			return Redirect::back()->withErrors(Lang::get('company.no_note'));
		}

	}


	/**
	 * Delete Job Note
	 *
	 * @return View
	 */
	public function removeNote()
	{

		//Get Contact Ids
		$note_ids = Input::get('selected_notes');

		// Ensure that Job Rework exists
		if ($note_ids ) {
			// Run Through Each Note ID
			foreach ($note_ids as $note_id) {

				// Find Note
				$job_note = JobNote::find($note_id);

				//Soft Delete Job Note
				try {
					$job_note->delete();
				} catch ( \Exception $e ) {
					return Redirect::back()->withErrors($e);
				}
			}
			return Redirect::back()->with('success', Lang::get('company.job_note_deleted'))->with('pop','job-notes');
		}
		// If Note is not found return with error
		else{
			return Redirect::back()->withErrors(Lang::get('company.no_note'));
		}

	}

	/**
	 * Restore Job Note
	 *
	 * @return View
	 */
	public function restoreNote()
	{

		//Get Note Ids
		$note_ids = Input::get('selected_notes');

		foreach ($note_ids as $note) {

			// Restore Contacts
			try {
				JobNote::withTrashed()->where('id', $note)->restore();
			} catch ( \Exception $e ) {
				return Redirect::back()->withErrors($e);
			}
		}

		return Redirect::back()->with('success', Lang::get('company.job_note_restored'))->with('pop','job-notes');
	}

	/**
	 * Create Follow Up Note
	 *
	 * @return
	 */
	public function createFollowUpNote()
	{

		$job = Job::find(Input::get('job_id'));

		// Ensure that Job exists
		if ( $job ) {

			// Setup Job Note Array
			$follow_up_note = array(
				'job_id'          => Input::get('job_id'),
				'job_note_id'     => Input::get('job_note_id'),
				'note'            => Input::get('note'),
				'new_deadline'    => ( Input::get('deadline') ) ? date('Y-m-d',strtotime(Input::get('deadline'))) : NULL
			);
			// Create Follow Up Note
			try {
				$follow_up_note = FollowUpNote::create($follow_up_note);
			} catch ( \Exception $e ) {
				return Redirect::back()->withErrors($e);
			}

			//Check If the User Updated the Deadline Date if So Update the Parent Job Note
			if (Input::get('new_deadline')) {

				$job_note = JobNote::find(Input::get('job_note_id'));

				$job_note->deadline = date('Y-m-d',strtotime(Input::get('deadline')));

				//Update the Associated Follow Up with the New Deadline Date
				try {
					$job_note->save();
				} catch ( \Exception $e ) {
					return Redirect::back()->withErrors($e);
				}
			}
			//Check If the User Re-Assigned Follow Up if So Update the Parent Job Note
			if (Input::get('reassign')) {

				$job_note = JobNote::find(Input::get('job_note_id'));

				$job_note->assigned = Input::get('assigned');

				//Update the Associated Follow Up with the New Deadline Date
				try {
					$job_note->save();
				} catch ( \Exception $e ) {
					return Redirect::back()->withErrors($e);
				}
			}

			// Success !!!
			return Redirect::back()->with('success', Lang::get('company.job_note_created'));
		}
		// If Job is not found return with error
		else{
			return Redirect::back()->withErrors(Lang::get('company.no_job'));
		}

	}

	/**
	 * Resolve Follow Up
	 *
	 * @return Message
	 */
	public function resolveFollowUp()
	{
		// Find Job Contact
		$job_note = JobNote::find(Input::get('job_note_id'));

		//Soft Delete Job Contact
		try {
			$job_note->delete();
			return Redirect::back()->with('success', Lang::get('company.follow_up_resolved'));
		} catch ( \Exception $e ) {
			return Redirect::back()->withErrors($e);
		}
	}


	/**
	 * View Note - Job
	 *
	 * @return View
	 */
	public function editJobNote($job_id,$note_id)
	{

		$data = [
			'note'                  => JobNote::find($note_id),
			'users'                 => User::getUsersSelect(),
			'activities_select'     => Activity::getActivitiesSelect(),
			'jobs_select'           => Job::getJobsSelect(),
			'contacts'              => Contact::getContactsInputOptions(),
			'job_contacts_dropdown' => Job::find($job_id)->getActiveJobContactsSelect(),
		];

		return View::make('admin.job.modals.job-edit-note-form',$data);
	}




}
