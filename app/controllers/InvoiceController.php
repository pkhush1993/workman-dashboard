<?php namespace App\Controllers;
/**
 *	Copyright 2014 CoolBlueWeb
 *
 *	Licensed under the Apache License, Version 2.0 (the "License");
 *	you may not use this file except in compliance with the License.
 *	You may obtain a copy of the License at
 *
 *			http://www.apache.org/licenses/LICENSE-2.0
 *
 *	Unless required by applicable law or agreed to in writing, software
 *	distributed under the License is distributed on an "AS IS" BASIS,
 *	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *	See the License for the specific language governing permissions and
 *	limitations under the License.
 *
 *	@package 	Laravel
 *	@subpackage Panviso App
 *	@copyright  2014 CoolBlueWeb
 *	@author 	CoolBlueWeb <bryan@coolblueweb.com>xw
 */

use BaseController, Sentinel, View, Input, Response, Redirect, Session, Mail, Lang, Config,App;
use App\Models\Company;
use App\Models\Invoice;
use App\Models\InvoiceLineItem;
use App\Models\Job;
use App\Models\JobBid;

class InvoiceController extends BaseController {


	/**
	 * Create an Invoice from a Job
	 *
	 * @param $job_id
	 *
	 * @return mixed
     */
    public function createInvoice($job_id)
	{

		$job = Job::find($job_id);

		if ( !$job->getBid() ) {
			$this->createJobBid($job);
		}

		$bid             = Job::find($job_id)->getBid();
		$line_items      = JobBid::find($bid->id)->getLineItems();
		$invoice_type    = Input::get('is_down_payment') == 1 ? 'down-payment' : Input::get('invoice_type');
		$down_payment    = Input::get('invoice_partial_payment_amount');
		$view_type 		 = null;
		$subtotal		 = Input::get('invoice_subtotal');
		$sales_tax_total = Input::get('invoice_sales_tax_total');
		$grand_total 	 = Input::get('invoice_grand_total');

		if ( $invoice_type == 'down-payment' ) {
			$view_type = Input::get('invoice_type');
			$subtotal = $bid->getAttribute('subtotal') * ($down_payment / 100);
			$sales_tax_total = $bid->getAttribute('sales_tax_total') * ($down_payment / 100);
			$grand_total 	 = $bid->getAttribute('grand_total') * ($down_payment / 100);
		}

		// ==== Created Invoice ====
		$invoice = array(
				'invoice_number'  => Input::get('invoice_number'),
				'job_id'          => $job_id,
				'bid_id'          => $bid->id,
				'invoice_type'    => $invoice_type,
				'down_payment'    => $down_payment,
				'status'          => '',
				'subtotal'        => $subtotal,
				'tax_rate'        => Input::get('invoice_tax_rate'),
				'sales_tax_total' => $sales_tax_total,
				'tax_description' => '',
				'grand_total'     => $grand_total,
				'view_type'		  => $view_type,
		);

		try {
			$invoice = Invoice::create($invoice);

		} catch ( \Exception $e ) {
			dd($e->getMessage());
			return Redirect::back()->withErrors($e->getMessage());
		}

		// ==== Create Invoice Line Items ====

		foreach ($line_items as $line_item) {
			$invoice_line_item = array(
					'invoice_id' => $invoice->id,
					'job_id' => $job_id,
					'bid_id' => $bid->id,
					'category' => $line_item->category,
					'product' => $line_item->product,
					'unit' => $line_item->unit,
					'rate' => $line_item->rate,
					'quantity' => $line_item->quantity,
					'taxable' => $line_item->taxable,
					'amount' => $line_item->amount,
					'sort_order' => $line_item->sort_order,
			);

			// Create Line Item for Invoice
			try {
				InvoiceLineItem::create($invoice_line_item);

			} catch ( \Exception $e ) {
				return Redirect::back()->withErrors($e->getMessage());
			}
		}

		// ----- Update Next Invoice Number -----

		// Get Company
		$company = Company::find(1);
		$last_invoice_number = $invoice->invoice_number;

		//Set Job Number (+1 More than the Job just created)
		$company->next_invoice_number   = $last_invoice_number+1;

		//Save Company Setting
		try {
			$company->save();
		} catch ( \Exception $e ) {
			return Redirect::back()->withErrors($e->getMessage());
		}

		return Redirect::route('job.invoice',[$invoice_type,$job_id,$down_payment,$invoice->id, $view_type]);
	}

	/**
	 * @param $job
	 *
	 * @return $this
     */
    protected function createJobBid($job)
	{
		JobBid::create([
				'job_id'          => $job->getKey(),
				'status'          => 'open',
				'subtotal'        => 0,
				'grand_total'     => 0,
				'tax_rate'        => 0,
				'tax_description' => 0,
				'sales_tax_total' => 0,
		]);

		return $this;
	}
}