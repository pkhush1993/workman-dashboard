<?php namespace App\Controllers;
/**
*	Copyright 2015 CoolBlueWeb
*
*	Licensed under the Apache License, Version 2.0 (the "License");
*	you may not use this file except in compliance with the License.
*	You may obtain a copy of the License at
*
*			http://www.apache.org/licenses/LICENSE-2.0
*
*	Unless required by applicable law or agreed to in writing, software
*	distributed under the License is distributed on an "AS IS" BASIS,
*	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*	See the License for the specific language governing permissions and
*	limitations under the License.
*
*	@package 	Laravel
*	@subpackage CoolBlueWeb Custom
*	@copyright  2015 CoolBlueWeb
*	@author 	CoolBlueWeb <bryan@coolblueweb.com>
*/

use BaseController, Sentinel, View, Input, Response, Redirect, Session, Mail, Lang, Config,App,Form;
use App\Models\User;
use App\Models\Company;
use App\Models\Profile;



class UserController extends BaseController {


	/**
	 * View Users
	 *
	 * @return View
	 */
	public function viewUsers()
	{
		$data = [
			'users' => User::all(),
			'_users' => Sentinel::getUser()->all(),
			'company' => Company::find(1),
		];

		return View::make('admin.users.users',$data);
	}


	/**
	 * Edit User
	 *
	 * @return View
	 */
	public function editUser($user_id)
	{

		$data = [
			'profile' => Profile::find($user_id),
			'user' => Sentinel::findById($user_id)
		];

		return View::make('admin.users.user',$data);
	}

	/**
	 * Update Profile
	 *
	 * @return View
	 */
	public function updateUser($user_id)
	{
		$user_profile = Profile::find($user_id);
		$UserDetail = User::find($user_id);
		//Ensure Profile Exisits
		if ($user_profile) {

			$user_profile->first_name = ( Input::get('first_name') )? Input::get('first_name') : $user_profile->first_name;
			$user_profile->last_name  = ( Input::get('last_name') ) ? Input::get('last_name')  : $user_profile->last_name;
			$user_profile->email      = ( Input::get('email') ) 	? Input::get('email')      : $user_profile->email;
			$user_profile->website    = ( Input::get('website') ) 	? Input::get('website')    : $user_profile->website;
			$user_profile->phone      = ( Input::get('phone') ) 	? Input::get('phone')      : $user_profile->phone;
			$user_profile->mobile     = ( Input::get('mobile') ) 	? Input::get('mobile')     : $user_profile->mobile;
			$user_profile->fax        = ( Input::get('fax') ) 		? Input::get('fax')        : $user_profile->fax;
			$user_profile->address_1  = ( Input::get('address_1') ) ? Input::get('address_1')  : $user_profile->address_1;
			$user_profile->address_2  = ( Input::get('address_2') ) ? Input::get('address_2')  : $user_profile->address_2;
			$user_profile->city       = ( Input::get('city') ) 		? Input::get('city')       : $user_profile->city;
			$user_profile->state      = ( Input::get('state') ) 	? Input::get('state')      : $user_profile->state;
			$user_profile->zip        = ( Input::get('zip') ) 		? Input::get('zip')        : $user_profile->zip;
			$user_profile->avatar     = ( Input::get('avatar') ) 	? Input::get('avatar')     : $user_profile->avatar;
			$user_profile->wage_rate  = ( Input::get('wage_rate') ) ? Input::get('wage_rate')  : $user_profile->wage_rate;

			$UserDetail->email = ( Input::get('email') ) ? Input::get('email') : $user_profile->email;
			try {
				$UserDetail->save();
				$user_profile->save();
				return Redirect::back()->with('success', Lang::get('profile.profile_update'));
			} catch ( \Exception $e ) {
				return Redirect::back();
			}

		}
		else{
			return Redirect::back()->withErrors(Lang::get('profile.no_profile'));
		}
	}


	/**
	 * Update Profile Roles
	 *
	 * @return View Redirect
	 */
	public static function updateUserRoles($user_id)
	{

		// Get User
		$user = Sentinel::findById($user_id);

		// Does User Exist?
		if ($user) {

			// Remove All Roles
			$roles = Sentinel::getRoleRepository()->all();

			foreach ($roles as $role) {

				$role->users()->detach($user);
			}

			//Assign New Roles
			$roles = Input::get('user_role');


			foreach ($roles as $role) {
				$user_role = Sentinel::findRoleBySlug($role);
				$user_role->users()->attach($user);
			}

			return Redirect::back()->with('success', Lang::get('authorization.roles_updated'));
		}
		else{
			return Redirect::to('auth/login')->withErrors(Lang::get('authorization.user_exists'));
		}

	}


	public function viewUsersInputAJAX($q = null)
	{
		$_results = $jobs = User::where('last_name', 'like', '%'.$q.'%')->orWhere('first_name', 'like', '%'.$q.'%')->orderBy('last_name', 'asc')->orderBy('first_name', 'asc')->get();
		$returned_results = [];

		foreach ($_results as $result) {
			$returned_results[] = ["title" =>$result->getFullName(),"description" => "# " . $result->id,"user_id" => $result->id];
		}

		return Response::json([ 'results' => $returned_results]);

		die();
	}


}
