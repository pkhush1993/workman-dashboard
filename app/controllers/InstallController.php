<?php namespace App\Controllers;
/**
*	Copyright 2014 CoolBlueWeb
*	
*	Licensed under the Apache License, Version 2.0 (the "License");
*	you may not use this file except in compliance with the License.
*	You may obtain a copy of the License at
*	
*			http://www.apache.org/licenses/LICENSE-2.0
*	
*	Unless required by applicable law or agreed to in writing, software
*	distributed under the License is distributed on an "AS IS" BASIS,
*	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*	See the License for the specific language governing permissions and
*	limitations under the License.
*
*	@package 	Laravel
*	@subpackage CoolBlueWeb Custom
*	@copyright  2014 CoolBlueWeb
*	@author 	CoolBlueWeb <michael@coolblueweb.com>
*/

use BaseController, Sentinel, View, Input, Response, Redirect, Session, Mail, Lang, Config,App;

class InstallController extends BaseController {

	/**
	 * Create User Roles
	 *
	 * @return View
	 */
	public function getRoles()
	{

		// Create Executive Role if it does not exist create it
		if (!Sentinel::findRoleBySlug('executive')) {
			$executive = Sentinel::getRoleRepository()->createModel()->create([
			    'name' => 'Executive',
			    'slug' => 'executive',
			]);
		}
		else{
			$executive = 'executive has already been created';
		}
		
		// Create Account Role if it does not exist create it
		if (!Sentinel::findRoleBySlug('accounting')) {
			$accounting = Sentinel::getRoleRepository()->createModel()->create([
			    'name' => 'Accounting',
			    'slug' => 'accounting',
			]);
		}
		else{
			$accounting = 'accounting has already been created';
		}
		
		// Create Project Manager Role if it does not exist create it
		if (!Sentinel::findRoleBySlug('project_manager')) {
			$project_manager = Sentinel::getRoleRepository()->createModel()->create([
			    'name' => 'Project Manager',
			    'slug' => 'project_manager',
			]);
		}
		else{
			$project_manager = 'project_manager has already been created';
		}
		
		// Create Estimator Role if it does not exist create it
		if (!Sentinel::findRoleBySlug('estimator')) {
			$estimator = Sentinel::getRoleRepository()->createModel()->create([
			    'name' => 'Estimator',
			    'slug' => 'estimator',
			]);
		}
		else{
			$estimator = 'estimator has already been created';
		}
		
		// Create Foreman Role if it does not exist create it
		if (!Sentinel::findRoleBySlug('foreman')) {
			$foreman = Sentinel::getRoleRepository()->createModel()->create([
			    'name' => 'Foreman',
			    'slug' => 'foreman',
			]);
		}
		else{
			$foreman = 'foreman has already been created';
		}
		
		// Create Salesman Role if it does not exist create it
		if (!Sentinel::findRoleBySlug('salesman')) {
			$salesman = Sentinel::getRoleRepository()->createModel()->create([
			    'name' => 'Salesman',
			    'slug' => 'salesman',
			]);		
		}
		else{
			$salesman = 'salesman has already been created';
		}
		
		// Create Foreman Role if it does not exist create it
		if (!Sentinel::findRoleBySlug('time_entry')) {
			$time_entry = Sentinel::getRoleRepository()->createModel()->create([
			    'name' => 'Time Entry',
			    'slug' => 'time_entry',
			]);
		}
		else{
			$time_entry = 'time_entry has already been created';
		}

		// Create Foreman Role if it does not exist create it
		if (!Sentinel::findRoleBySlug('admin')) {
			$time_entry = Sentinel::getRoleRepository()->createModel()->create([
			    'name' => 'Admin',
			    'slug' => 'admin',
			]);						
		}
		else{
			$admin = 'admin has already been created';
		}

		return View::make('install.roles',compact('executive','accounting','project_manager','estimator','foreman','time_entry','salesman'));

	}

	/**
	 * Create User Roles
	 *
	 * @return View
	 */
	public function getInstall()
	{
		return View::make('install.install');
	}
	
}