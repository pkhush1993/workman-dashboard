<?php namespace App\Controllers;
/**
*	Copyright 2014 CoolBlueWeb
*
*	Licensed under the Apache License, Version 2.0 (the "License");
*	you may not use this file except in compliance with the License.
*	You may obtain a copy of the License at
*
*			http://www.apache.org/licenses/LICENSE-2.0
*
*	Unless required by applicable law or agreed to in writing, software
*	distributed under the License is distributed on an "AS IS" BASIS,
*	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*	See the License for the specific language governing permissions and
*	limitations under the License.
*
*	@package 	Laravel
*	@subpackage Panviso App
*	@copyright  2014 CoolBlueWeb
*	@author 	CoolBlueWeb <bryan@coolblueweb.com>
*/

use BaseController, Sentinel, View, Input, Response, Redirect, Session, Mail, Lang, Config,App;
use App\Models\Profile;
use App\Models\User;

class ProfileController extends BaseController {

	/**
	 * View Profile
	 *
	 * @return View
	 */
	public function viewProfile($profile_id)
	{
		$profile = Profile::find($profile_id);

		return View::make('admin.profile.profile')->with('profile',$profile);
	}

	/**
	 * Get Profile
	 *
	 * @return View
	 */
	public static function getProfile($profile_id)
	{
		$profile = Profile::find($profile_id);
		return $profile;
	}


	/**
	 * Update Profile
	 *
	 * @return View
	 */
	public static function updateProfile($profile_id)
	{
		$profile = Profile::find($profile_id);
		$UserDetail = User::find($profile_id);

		//Ensure Profile Exisits
		if ($profile) {
			$profile->first_name = ( Input::get('first_name') ) ? Input::get('first_name') : $profile->first_name;
			$profile->last_name  = ( Input::get('last_name') ) 	? Input::get('last_name')  : $profile->last_name;
			$profile->email      = ( Input::get('email') ) 		? Input::get('email')      : $profile->email;
			$profile->website    = ( Input::get('website') ) 	? Input::get('website')    : $profile->website;
			$profile->phone      = ( Input::get('phone') ) 		? Input::get('phone')      : $profile->phone;
			$profile->mobile     = ( Input::get('mobile') ) 	? Input::get('mobile')     : $profile->mobile;
			$profile->fax        = ( Input::get('fax') ) 		? Input::get('fax')        : $profile->fax;
			$profile->address_1  = ( Input::get('address_1') ) 	? Input::get('address_1')  : $profile->address_1;
			$profile->address_2  = ( Input::get('address_2') ) 	? Input::get('address_2')  : $profile->address_2;
			$profile->city       = ( Input::get('city') ) 		? Input::get('city')       : $profile->city;
			$profile->state      = ( Input::get('state') ) 		? Input::get('state')      : $profile->state;
			$profile->zip        = ( Input::get('zip') ) 		? Input::get('zip')        : $profile->zip;
			$profile->avatar     = ( Input::get('avatar') ) 	? Input::get('avatar')     : $profile->avatar;
			$profile->wage_rate  = ( Input::get('wage_rate') ) 	? Input::get('wage_rate')  : $profile->wage_rate;

			$UserDetail->email = ( Input::get('email') ) ? Input::get('email') : $profile->email;
			try {
				$profile->save();
				$UserDetail->save();
				return Redirect::back()->with('success', Lang::get('profile.profile_update'));
			} catch ( \Exception $e ) {
				return Redirect::back();
			}

		}
		else{
			return Redirect::back()->withErrors(Lang::get('profile.no_profile'));
		}
	}


	/**
	 * Update Profile Roles
	 *
	 * @return View Redirect
	 */
	public static function updateProfileRoles($profile_id)
	{

		// Get User
		$user = Sentinel::findById($profile_id);

		// Does User Exist?
		if ($user) {

			// Remove All Roles
			$roles = Sentinel::getRoleRepository()->all();

			foreach ($roles as $role) {

				$role->users()->detach($user);
			}

			//Assign New Roles
			$roles = Input::get('user_role');

			if ( $roles ) {
				foreach ($roles as $role) {
					$user_role = Sentinel::findRoleBySlug($role);
					$user_role->users()->attach($user);
				}
			}


			return Redirect::back()->with('success', Lang::get('authorization.roles_updated'));
		}
		else{
			return Redirect::to('auth/login')->withErrors(Lang::get('authorization.user_exists'));
		}

	}




}
