<?php namespace App\Controllers;
/**
*	Copyright 2014 CoolBlueWeb
*
*	Licensed under the Apache License, Version 2.0 (the "License");
*	you may not use this file except in compliance with the License.
*	You may obtain a copy of the License at
*
*			http://www.apache.org/licenses/LICENSE-2.0
*
*	Unless required by applicable law or agreed to in writing, software
*	distributed under the License is distributed on an "AS IS" BASIS,
*	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*	See the License for the specific language governing permissions and
*	limitations under the License.
*
*	@package 	Laravel
*	@subpackage Panviso App
*	@copyright  2014 CoolBlueWeb
*	@author 	CoolBlueWeb <bryan@coolblueweb.com>
*/
use BaseController, Sentinel, Activation,Reminder,ProfileController, View, Input, Response, Redirect, Session, Mail, Lang, Config, App,DB,Route;
use App\Models\Profile;
use Illuminate\Support\Facades\Auth;

class AuthController extends BaseController {

	public function __construct()
	{
		View::share('errorTitles', array('Bloody Hell!', 'Balls!', 'Whoops!'));
	}

	/**
	 * Get Login Screen
	 *
	 * @return View
	 */
	public function getLogin()
	{
		return View::make('auth.login');
	}

	/**
	 *	Get Logout
	 *
	 * 	@return Redirect
	 */
	public function getLogout()
	{
		try {
			Sentinel::logout();
			return Redirect::to('auth/login')->with('success', Lang::get('authorization.logout_success'));
		} catch (\Exception $e) {
			return Redirect::back()->withErrors($e->getMessage());
		}
	}


	/**
	 *	Post Login
	 *
	 *	@return Response
	 */
	public function postLogin($decoy = null, $returnUrl = null)
	{

		if ( $user = Sentinel::authenticate( Input::all() ) ) {

			// Check for a referring URL. This is set in the Auth filter (filter.php)
			// if ( !is_null($returnUrl) && $redirect = base64_decode($returnUrl)) {
			// 	return Redirect::to($redirect)->with('success', Lang::get('authorization.login_success', array('email' => Input::get('email'))));
			// }

			return Redirect::route('dashboard')->with('success', Lang::get('authorization.login_success', array('email' => $user->email)));
		}

		return Redirect::back()->withErrors('Invalid username or password')->withInput();
	}


	/**
	 *	Ajax Post Login
	 *
	 *	@return Response
	 */
	public function ajaxPostLogin()
	{

		if ( $user = Sentinel::authenticate( Input::all() ) ) {

			// Check for a referring URL. This is set in the Auth filter (filter.php)
			// if ( !is_null($returnUrl) && $redirect = base64_decode($returnUrl)) {
			// 	return Redirect::to($redirect)->with('success', Lang::get('authorization.login_success', array('email' => Input::get('email'))));
			// }
			$data = array(
				'details' => 'Success',
				'message' => 'You have loggedin successfully!',
				'email' => $user->email
			);

			return $data;
		}

		$data = array(
			'details' => 'Error',
			'message' => 'Invalid username or password!'
		);
//		return Redirect::back()->withErrors('Invalid username or password')->withInput();
		return $data;
	}


	/**
	 *	Check user status
	 *
	 *	@return Response
	 */
	public function ajaxLoginStatus()
	{

		if ( Sentinel::check()->email ) {

			$data = array(
				'details' => 'login',
				'email' => Sentinel::check()->email
			);

			return $data;
		}

		$data = array(
			'details' => 'logout',
			'email' => ''
		);
		return $data;
	}

	/**
	 * Get Login Screen
	 *
	 * @return View
	 */
	public function getForgot()
	{
		return View::make('auth/forgot');
	}

	/**
	 *	Post Forgot
	 *
	 *	@return Response
	 */
	public function postForgot()
	{
		$user = Sentinel::findByCredentials(['login' => Input::get('email')]);

		// Does User Not Exist?
		if (!$user) {
			return Redirect::to('auth/forgot')->withErrors(Lang::get('authorization.no_user'));
		}
		// User Exists! Create Reset
		try {
    		// Get the password reset code
		    if ( $resetCode = Reminder::create($user)->code ) {

		    	$data['token'] = $resetCode;
		    	$data['email'] = base64_encode(trim($user->email));

				Mail::send('admin.profile.emails.auth.reminder', $data, function($message) use($user) {
    				$message->to($user->email, '')->subject(Lang::get('authorization.email_reset_instructions_subject', array('url' => Config::get('app.url'))));
				});

				return Redirect::to('auth/login')->with('success', Lang::get('authorization.reset_request_success'));
		    }

		} catch (\Exception $e) {
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

	/**
	 * Get Reset Screen
	 *
	 * @return View
	 */
	public function getReset($resetCode = NULL, $email = NULL)
	{
		if ( is_null($resetCode) OR is_null($email) ) {
			return Redirect::to('auth/forgot')->withErrors(Lang::get('authorization.reset_code_not_provided'));
		}
		$data['resetCode'] = $resetCode;
		return View::make('auth/reset')->with($data);
	}

	/**
	 *	Post Reset
	 *
	 *	@return Redirect/Response
	 */
	public function postReset($resetCode = NULL, $email = NULL)
	{
		if ( is_null($resetCode) || is_null($email) ) {
			return Redirect::to('auth/forgot')->withErrors(Lang::get('authorization.reset_code_not_provided'));
		}

		try {
			$user = Sentinel::findByCredentials( ['login' => base64_decode($email)] );

			// Check if the reset password code exists
		    if ( Reminder::exists($user) ) {

		        if ( Reminder::complete($user, $resetCode, Input::get('password')) ) {
		        	return Redirect::to('auth/login')->with('success', Lang::get('authorization.reset_success'));
		        } else {
		    		$errorMessage = Lang::get('authorization.reset_failed');
		        }
		    }
		    else {
		    	$errorMessage = Lang::get('authorization.reset_code_not_provided');
		    }

			return Redirect::to('auth/forgot')->withErrors($errorMessage);

		} catch (\Exception $e) {
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

	/**
	 *	Get Register
	 *
	 *	@return View
	 */
	public function getRegister()
	{
		return View::make('auth.register');
	}

	/**
	 *	Post Register
	 *
	 *	@return View
	 */
	public function postRegister()
	{

		$user = Sentinel::findByCredentials(['login' => Input::get('email')]);

		// Does User Already Exist?
		if ($user) {
			return Redirect::to('auth/login')->withErrors(Lang::get('authorization.user_exists'));
		}
			// Create User
			try {

				//If Account Activation is Required
	    		if ( Config::get('admin.require_activation') ) {

	    			// Register User
					$user = Sentinel::register(array(
		        		'email'     => Input::get('email'),
		        		'password'  => Input::get('password'),
						'first_name'=>	Input::get('first_name'),
						'last_name' =>	Input::get('last_name')
		    		));

					// Create Profile
					if($user){
			    		$profile = array(
			    			'id'		=>	$user->id,
							'first_name'=>	Input::get('first_name'),
							'last_name' =>	Input::get('last_name'),
							'email'   	=>	Input::get('email'),
							'address_1' =>	Input::get('address_1'),
							'address_2' =>	Input::get('address_2'),
							'city'   	=>	Input::get('city'),
							'state'   	=>	Input::get('state'),
							'zip'		=>	Input::get('zip'),
							'avatar' 	=>  Config::get('admin.default_avatar')
			    		);
			    		//Profile::create($profile);
			    		DB::table('profiles')->insert($profile);
					}

		    		//Setup Activation
	    			$data['activationCode'] = Activation::create($user)->code;
	    			$data['email']			= base64_encode($user->email);
					Mail::send('admin.profile.emails.auth.activation', $data, function($message) use($user) {
	    				$message->to($user->email, '')->subject(Lang::get('authorization.email_activate_account_subject', array('url' => Config::get('app.url'))));
					});

					//Assign Role
					$user = Sentinel::findById($user->id);
					$role = Sentinel::findRoleByName('Admin');
					$role->users()->attach($user);
					return Redirect::to('auth/login')->with('success', Lang::get('authorization.account_activation_required'));
	    		}
	    		else{
		    		$user = Sentinel::registerAndActivate(array(
			        	'email'     => Input::get('email'),
			        	'password'  => Input::get('password'),
			    	));

		    		return Redirect::to('auth/login')->with('success', Lang::get('authorization.registration_complete'));
	    		}

			} catch (\Exception $e) {
				return Redirect::back()->withErrors($e->getMessage());
			}
	}

	/**
	 *	Get Activation
	 *
	 *	@return Response/Redirect
	 */
	public function getActivate($activationCode = NULL, $email = NULL)
	{

		$user = Sentinel::findByCredentials( array('login' => trim(base64_decode($email))) );

		if ($user) {
			if ( is_null($activationCode) || is_null($email) ) {
				return Redirect::to('auth/register')->withErrors(Lang::get('authorization.missing_activation_code'));
			}
			elseif ($user->password == false ){

				return Redirect::route('auth.setup_account', array('activationCode' => $activationCode,'email' => $email ));
			}

			try {

				$user = Sentinel::findByCredentials( array('login' => trim(base64_decode($email))) );

			    if ( Activation::complete($user, $activationCode) ) {
			    	return Redirect::to('auth/login')->with('success', Lang::get('authorization.registration_complete'));
			    } else {
			        return Redirect::to('auth/register')->withErrors(Lang::get('authorization.missing_activation_code'));
			    }

			} catch (\Exception $e) {
				return Redirect::to('auth/register')->withErrors($e->getMessage());
			}
		}
		else {
			return Redirect::to('auth/login')->withErrors('User not in the system, please contact your administrator');
		}


	}

	/**
	 * Get Account Setup View
	 *
	 * @return View
	 */
	public function getSetup($activationCode = NULL, $email = NULL)
	{
		$email = trim(base64_decode($email));

		$user = Sentinel::findByCredentials( array('login' => $email) );

		return View::make('auth.setup_account')->with(array('activationCode' => $activationCode,'user' => $user ));
	}

	/**
	 * Post Account Setup
	 *
	 * @return View
	 */
	public function postSetup()
	{
		$email = Input::get('email');
		$activationCode = Input::get('activation_code');
		$user = Sentinel::findByCredentials( array('login' => $email) );

		// Register User
		Sentinel::update($user,array(
    		'password'  => Input::get('password'),
			'first_name'=>	Input::get('first_name'),
			'last_name' =>	Input::get('last_name')
		));

		// Update Profile
		if($user){
    		$profile = array(
				'first_name'=>	Input::get('first_name'),
				'last_name' =>	Input::get('last_name'),
				'address_1' =>	Input::get('address_1'),
				'address_2' =>	Input::get('address_2'),
				'city'   	=>	Input::get('city'),
				'state'   	=>	Input::get('state'),
				'zip'		=>	Input::get('zip'),
    		);
    		DB::table('profiles')->where('id', $user->id)->update($profile);
		}

		// Activate User
		if ( is_null($activationCode) ) {
			return Redirect::to('auth/register')->withErrors(Lang::get('authorization.missing_activation_code'));
		}
		try {

		    if ( Activation::complete($user, $activationCode) ) {
		    	return Redirect::to('auth/login')->with('success', Lang::get('authorization.registration_complete'));
		    } else {
		        return Redirect::to('auth/register')->withErrors(Lang::get('authorization.missing_activation_code'));
		    }

		} catch (\Exception $e) {
			return Redirect::to('auth/register')->withErrors($e->getMessage());
		}
	}


}
