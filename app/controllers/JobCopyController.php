<?php namespace App\Controllers;


use App\Models\Company;
use App\Models\Job;

class JobCopyController extends \BaseController
{
    /**
     * @var Job
     */
    protected $job;

    /**
     * JobCopyController constructor.
     *
     * @param $job Job
     */
    public function __construct(Job $job)
    {
        parent::__construct();

        $this->job = $job;
    }

    /**
     * Create a duplicate of the supplied job
     *
     * @param $job_id
     */
    public function createCopy($job_id)
    {
        $this->job = $this->job->newQuery()->find($job_id);
        $firstJobId = null;
        $quantity = \Input::get('quantity') > 0 ? \Input::get('quantity') : 1;

        for ( $i = 1; $i <= $quantity; $i++ ) {

            $isEverything = (bool)\Input::get('copy') == 'everything';

            // Replicate Job, but ignore these columns. Since PK
            // and the timestamps are set via observer, You only need
            // to set the number job number, since it has a UNIQUE Index
            $except = [
                $this->job->getKeyName(),
                $this->job->getCreatedAtColumn(),
                $this->job->getUpdatedAtColumn(),
                'job_number'
            ];

            // Here is some odd logic to help create a unique job number
            // without showing the user an error, in the case that a job
            // number has already been reserved
            $job_number = Company::find(1)->getAttribute('next_job_number')+1;
            while ( Job::where('job_number', $job_number)->withTrashed()->count() ) {
                $job_number++;
            }
            // Create and save a duplicate the supplied job
            // This could probably be cleaned up, since COPY
            // and the format of job is hard-coded.
            $instance = $this->job->replicate($except);
            $instance->setAttribute('name',$this->job->getAttribute('name').'-COPY-'.$i);
            $instance->setAttribute('job_number', $job_number);
            $instance->save();

            // Assuming we're successful, we'll redirect to this job
            if ( is_null($firstJobId) ) {
                $firstJobId = $instance->getAttribute('id');
            }

            // List of attributes that can be copied
            $attributes = [
                  'account',
                  'contacts',
                  'primary_contact',
                  'bid_date',
                  'estimator',
                  'project_manager',
                  'accounting',
                  'lead_source',
                  'estimator_deadline',
                  'tax_rate',
                  'completed_date',
                  'production_priority',
                  'production_deadline',
                  'estimator_priority',
                  'status',
                  'site_address',
                  'billing_address',
                  'bid_line_items',
                  'bid_comments',
                  'notes_estimating',
                  'notes_project',
                  'notes_accounting',
                  'foreman',
                  'custom_drop_1',
                  'custom_drop_2',
                  'custom_drop_3',
                  'custom_text_1',
                  'custom_text_2',
                  'custom_text_3',
                  'quantity'
            ];

            // Reset these attributes if the user didn't select
            // to copy them over. Since we replicate the job, we need
            // to clear out any information that the user wants to exclude
            foreach ( $attributes as $attribute ) {
                if ( !\Input::get($attribute) && !$isEverything) {
                    $instance->setAttribute($attribute, '');
                }
            }

            $instance->save();

            // Duplicate Bid Comments
            if ( \Input::get('bid_comments') || $isEverything ) {
                if ( !$instance->getBid() ) {
                    $this->createBid($instance);
                }

                $this->copyBidComments($instance);
            }

            // Duplicate Bid Line Items
            if ( \Input::get('bid_line_items') || $isEverything ) {

                if ( !$instance->getBid() ) {
                    $this->createBid($instance);
                }

                $this->copyBidLineItems($instance);
            }

            // Copy Associated Contacts
            if ( \Input::get('primary_contact') || $isEverything ) {
                $this->copyJobContacts($instance);
            }
        }

        
        return \Redirect::route('view.job', ['job_id' => $firstJobId])->with('success', 'Job Copied');
    }

    /**
     * @param $instance
     * @return mixed
     */
    protected function createBid($instance)
    {
        if ( $this->job->getBid() ) {
            $newBid = $this->job->getBid()->replicate(['job_id', 'id']);
            $newBid->setAttribute('job_id', $instance->getKey());

            return $newBid->save();
        }

        return $instance->attachNewBid();
    }

    protected function copyJobContacts($instance)
    {
        if ( $contacts = $this->job->getContacts()->get() ) {
            foreach ( $contacts as $contact ) {
                $newContact = $contact->replicate(['id', 'job_id']);
                $newContact->setAttribute('job_id', $instance->getKey());
                $newContact->save();
            }
        }
    }

    /**
     * @param $instance
     */
    protected function copyBidLineItems($instance)
    {
        if ( $this->job->getBid() && $this->job->getBid()->getLineItems()->count() ) {
            foreach (  $this->job->getBid()->getLineItems() as $lineItem ) {
                $newLine = $lineItem->replicate(['bid_id', 'id']);
                $newLine->setAttribute('bid_id', $instance->getBid()->getKey());
                $newLine->save();
            }
        }
    }

    /**
     * @param $instance
     */
    protected function copyBidComments($instance)
    {
        if ( $this->job->getBid() && $this->job->getBid()->getBidComments()->count() ) {
            foreach (  $this->job->getBid()->getBidComments() as $comment ) {
                $newComment = $comment->replicate(['bid_id', 'id']);
                $newComment->setAttribute('bid_id', $instance->getBid()->getKey());
                $newComment->save();
            }
        }
    }
}