<?php namespace App\Controllers;
/**
*	Copyright 2014 CoolBlueWeb
*
*	Licensed under the Apache License, Version 2.0 (the "License");
*	you may not use this file except in compliance with the License.
*	You may obtain a copy of the License at
*
*			http://www.apache.org/licenses/LICENSE-2.0
*
*	Unless required by applicable law or agreed to in writing, software
*	distributed under the License is distributed on an "AS IS" BASIS,
*	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*	See the License for the specific language governing permissions and
*	limitations under the License.
*
*	@package 	Laravel
*	@subpackage CoolBlueWeb Custom
*	@copyright  2014 CoolBlueWeb
*	@author 	CoolBlueWeb <bryan@coolblueweb.com>
*/

use BaseController, Sentinel, View, Input, Response, Redirect, Session, Mail, Lang, Config,App,Form;
use App\Models\BidField;
use App\Models\BidFieldItem;

class BidFieldController extends BaseController {

	/**
	 * Update Bid Fields
	 *
	 * @return Update
	 */
	public function createBidFields()
	{
		$bid_fields = Input::except('_token');

		foreach($bid_fields['bid_field'] as $key => $bid_service_val) {

			//Field
			$bid_field = $bid_fields['bid_field'][$key];
			//Field ID
			$bid_field_id = $bid_fields['bid_field_id'][$key];
			//Item Descriptions
			$bid_descriptions = $bid_fields['bid_description'][$key];
			//Item Units
			$bid_units = $bid_fields['bid_units'][$key];
			//Item Units
			$bid_price = $bid_fields['bid_price'][$key];


			// Save Field if New
			if ($bid_field_id == 0) {

				// Create BidField
				try {
					$new_bid_field = BidField::create(['name'=>$bid_field]);
				} catch ( \Exception $e ) {
					$data = array(
						'detail' => 'error',
						'message' => $e
					);
					return $data;
//					return Redirect::back()->withErrors($e);
				}

				$sort_order = 1;

				//Iterate Through Bid Field Items
				foreach ($bid_descriptions as $key => $description) {
					try {
						BidFieldItem::create([
							'bid_field_id'	=>	$new_bid_field->id,
							'description'	=>	$description,
							'units' 	 	=>	$bid_units[$key],
							'price'   	 	=>	$bid_price[$key],
							'sort_order' 	=>	$sort_order
						]);

						$sort_order++;

					} catch ( \Exception $e ) {
						$data = array(
							'detail' => 'error',
							'message' => $e
						);
						return $data;
//						return Redirect::back()->withErrors($e);
					}
				}
			}

		}
		//No Errors! Send Back With Success
		$data = array(
			'detail' => 'success',
			'message' => Lang::get('company.bid_fields_created')
		);
		return $data;
//		return Redirect::back()->with('success', Lang::get('company.bid_fields_created'));

	}

	/**
	 * @return mixed
     */
	public function updateBidFields()
	{
		$bid_fields = Input::except('_token');

		 if ( array_key_exists('remove_bid_field', $bid_fields) ) {
			foreach ( $bid_fields['remove_bid_field'] as $key => $value ) {
				try {
					BidField::find($value)->delete();
				} catch ( \Exception $e ) {
					return Redirect::to(tab_redirect('company.profile', 'bid'))->withErrors($e);
				}
			}
		}

		if ( array_key_exists('remove_bid_line_field', $bid_fields) ) {
			foreach ( $bid_fields['remove_bid_line_field'] as $key => $value ) {
				try {
					BidFieldItem::find($value)->delete();
				} catch ( \Exception $e ) {
					return Redirect::to(tab_redirect('company.profile', 'bid'))->withErrors($e);
				}
			}
		}

		foreach($bid_fields['bid_field'] as $key => $bid_service_val) {

			//Field
			$bid_field = BidField::find($key);
			// Field Name
			$bid_field_name = $bid_fields['bid_field'][$key];
			//Field ID
			$bid_field_id = $bid_fields['bid_field_id'][$key];
			//Field Item ID
			$bid_field_item_id = $bid_fields['bid_field_item_id'][$key];
			//Item Descriptions
			$bid_descriptions = $bid_fields['bid_description'][$key];
			//Item Units
			$bid_units = $bid_fields['bid_units'][$key];
			//Item Units
			$bid_price = $bid_fields['bid_price'][$key];

			// Save Bid Field Name
			if ($bid_field) {

				try {
					$bid_field->fill(['name' => $bid_field_name])->save();
				} catch ( \Exception $e ) {
					return Redirect::to(tab_redirect('company.profile', 'bid'))->withErrors($e);
				}

			}

			//Set Sort Order
			$sort_order = 1;


			//Iterate Through Bid Field Items
			foreach ($bid_descriptions as $key => $description) {

				$bid_field_item = BidFieldItem::find($bid_field_item_id[$key]);

				//Ensure Bid Field Item Exists
				if ($bid_field_item) {

					try {
						$bid_field_item->fill([
								'bid_field_id'		=>	$bid_field_id,
								'description'		=>	$description,
								'units' 	 		=>	$bid_units[$key],
								'price'   	 		=>	$bid_price[$key],
								'sort_order' 		=>	$sort_order,
						])->save();
					} catch ( \Exception $e ) {
						return Redirect::to(tab_redirect('company.profile', 'bid'))->withErrors($e);
					}
				}
				else {

					$bid_field_item = array(
		    			'bid_field_id'	=>	$bid_field_id,
						'description'	=>	$description,
						'units' 	 	=>	$bid_units[$key],
						'price'   	 	=>	$bid_price[$key],
						'sort_order' 	=>	$sort_order
		    		);

					// Create BidField Items
					try {
						BidFieldItem::create($bid_field_item);
					} catch ( \Exception $e ) {
						return Redirect::to(tab_redirect('company.profile', 'bid'))->withErrors($e);
					}
				}

	    		$sort_order++;

			}

		}

		//No Errors! Send Back With Success
		return Redirect::to(tab_redirect('company.profile', 'bid'))->with('success', Lang::get('company.bid_fields_updated'));

	}

	/**
	 * Get Bid Items Dropdown
	 *
	 * @param integer $bid_field_id
	 * @return mixed
     */
    public function getBidItemsDrop($bid_field_id)
	{
		// Get Bid Field Items
		$bid_field_items = BidFieldItem::getBidFieldItemsInputOptions($bid_field_id);
		return  Form::select('product', $bid_field_items, $default = null , ['class'=>'ui dropdown search bid_item','autocomplete' =>'off']);

	}


	/**
	 * Get Bid Item Information
	 *
	 * @param integer $bid_item_id
	 * @return mixed
     */
    public function getBidItemInfo($bid_item_id)
	{
		$bid_item = BidFieldItem::find($bid_item_id);

		return Response::json([
				'id' 			=> $bid_item->id,
				'description' 	=> $bid_item->description,
				'units' 		=> $bid_item->units,
				'price' 		=> $bid_item->price]);
	}
}
