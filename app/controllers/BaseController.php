<?php 
/**
*	Copyright 2014 CoolBlueWeb
*	
*	Licensed under the Apache License, Version 2.0 (the "License");
*	you may not use this file except in compliance with the License.
*	You may obtain a copy of the License at
*	
*			http://www.apache.org/licenses/LICENSE-2.0
*	
*	Unless required by applicable law or agreed to in writing, software
*	distributed under the License is distributed on an "AS IS" BASIS,
*	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*	See the License for the specific language governing permissions and
*	limitations under the License.
*
*	@package 	Laravel
*	@subpackage Panviso App
*	@copyright  2014 CoolBlueWeb
*	@author 	CoolBlueWeb <bryan@coolblueweb.com>
*/
use App\Controllers\ProfileController;
use App\Models\Profile;

class BaseController extends Controller {

	/**
	 * The layout used by the controller.
	 *
	 * @var \Illuminate\View\View
	 */
	protected $layout;

	/**
	 * BaseController constructor.
	 */
	public function __construct()
	{
		$this->layout = $this->layout ?: null;
	}


	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}

		// Store Current User
		$currentUser = Sentinel::getUser();

		//If User is Logged In Globally use $user and $profile
		if ($currentUser) {
		    View::share([
		        'current_user' => $currentUser,
		        'current_user_profile' => Profile::find($currentUser->id)
		    ]);
		}
	}



}
