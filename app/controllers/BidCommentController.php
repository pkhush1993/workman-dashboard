<?php namespace App\Controllers;
/**
*	Copyright 2015 CoolBlueWeb
*
*	Licensed under the Apache License, Version 2.0 (the "License");
*	you may not use this file except in compliance with the License.
*	You may obtain a copy of the License at
*
*			http://www.apache.org/licenses/LICENSE-2.0
*
*	Unless required by applicable law or agreed to in writing, software
*	distributed under the License is distributed on an "AS IS" BASIS,
*	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*	See the License for the specific language governing permissions and
*	limitations under the License.
*
*	@package 	Laravel
*	@subpackage CoolBlueWeb Custom
*	@copyright  2015 CoolBlueWeb
*	@author 	CoolBlueWeb <bryan@coolblueweb.com>
*/

use BaseController, Sentinel, View, Input, Response, Redirect, Session, Mail, Lang, Config,App;
use App\Models\Dashboard;
use App\Models\Activity;
use App\Models\Job;
use App\Models\JobBid;
use App\Models\BidComment;

class BidCommentController extends BaseController {


	/**
	 * Update Bid Comment
	 *
	 * @param $job_id
	 * @param $bid_id
	 *
	 * @return mixed
     */
    public function updateBidComments($job_id, $bid_id)
	{
		if ( $bid_id == 'new' ) {
			$bid_id = Job::find($job_id)->attachNewBid()->getAttribute('id');
		}

		$bid = JobBid::find($bid_id);

		//Ensure Bid Exists Exists
		if ($bid) {

			//Iterate Through Bid Comments
			foreach(Input::get('bid_comment') as $bid_comment_id => $bid_comment_val) {
				// Skip Empty Input Values
				if ($bid_comment_val) {
					//Get Bid Comment
					$bid_comment = BidComment::where('id', $bid_comment_id)->where('bid_id', $bid->getKey())->first();

					// If the Bid Comment already Exists Update It
					if (isset($bid_comment->comment) && strlen($bid_comment_id) > 3) {

						$bid_comment->comment = $bid_comment_val;

						// Update the Bid Comment
						try {
							$bid_comment->save();
						} catch ( \Exception $e ) {
							$data = array(
								'detail' => 'success',
								'message' => $e->getMessage()
							);
							return $data;
//							return Redirect::back()->withErrors($e);
						}
					}

					// If the Bid Comment does NOT exist Create A New One
					elseif($bid_comment_val != 'custom') {
			    		$_bid_comment = array(
							'job_id'   =>	$job_id,
							'bid_id'   =>	$bid_id,
							'comment'   =>	$bid_comment_val
			    		);

			    		// Create Bid Comment
						try {
							BidComment::create($_bid_comment);
						} catch ( \Exception $e ) {
//							return Redirect::back()->withErrors($e);
							$data = array(
								'detail' => 'success',
								'message' => $e->getMessage()
							);
							return $data;
						}
					}
				}
			}

			// Check For Deleted Fields
			if (Input::get('removed_ids')) {

				$removed_fields = explode(',', Input::get('removed_ids'));

				foreach ($removed_fields as $remove_field) {
					$removed_field = BidComment::find($remove_field);

					// Remove Contact
					try {
						$removed_field->delete();
					} catch ( \Exception $e ) {
						$data = array(
							'detail' => 'success',
							'message' => $e->getMessage()
						);
						return $data;
//						return Redirect::back()->withErrors($e);
					}
				}
			}

			// SUCCESS!!! Bid Comments Updated
			$data = array(
				'detail' => 'success',
				'message' => Lang::get('company.job_bid_comments_updated')
			);
			return $data;
//			return Redirect::back()->with('success', Lang::get('company.job_bid_comments_updated'))->with('pop','bid-fields');
		}
	}

}
