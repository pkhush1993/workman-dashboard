<?php namespace App\Controllers;
/**
 *	Copyright 2015 CoolBlueWeb
 *
 *	Licensed under the Apache License, Version 2.0 (the "License");
 *	you may not use this file except in compliance with the License.
 *	You may obtain a copy of the License at
 *
 *			http://www.apache.org/licenses/LICENSE-2.0
 *
 *	Unless required by applicable law or agreed to in writing, software
 *	distributed under the License is distributed on an "AS IS" BASIS,
 *	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *	See the License for the specific language governing permissions and
 *	limitations under the License.
 *
 *	@package 	Laravel
 *	@subpackage coolblueweb Custom
 *	@copyright  2015 CoolBlueWeb
 *	@author 	CoolBlueWeb <bryan@coolblueweb.com>
 */

use BaseController, Sentinel, View, Input, Response, Redirect, Session, Mail, Lang, Config,App;
use App\Models\Report;
use App\Models\Company;
use App\Models\JobRework;
use App\Models\JobNote;
use App\Models\JobBid;
use App\Models\Job;
use App\Models\Contact;
use App\Models\User;
use App\Models\Profile;
use App\Models\Invoice;
use App\Models\InvoiceLineItems;
use App\Models\Timesheet;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

/**
 * Class ReportController
 *
 * @package App\Controllers
 */
class ReportController extends BaseController {

	/**
	 * View Job Invoice
	 *
	 * @param $invoice_type
	 * @param $job_id
	 * @param null $down_payment
	 * @param null $invoice_id
	 * @param mixed $view_type
	 *
	 * @return mixed
     */
    public function viewInvoice($invoice_type, $job_id, $down_payment = NULL, $invoice_id = NULL, $view_type = NULL)
	{
		$job = Job::find($job_id);

		if ( !$job->getBid() ) {
			$this->createJobBid($job);
		}

		// If Invoice Type is an Invoice Grab Invoice Line Items by Category
		if ($invoice_id) {
			$data = [
				'view_type'		  => $view_type,
				'invoice_type'    => $invoice_type,
				'down_payment'    => $down_payment,
				'job'             => $job,
				'company'         => Company::find(1),
				'job_account' 	  => Job::find($job_id)->getAccount(),
				'primary_contact' => Job::find($job_id)->getPrimaryContact(),
				'bid'             => Invoice::find($invoice_id),
				'line_items'      => Report::getGroupedLineItems(Job::find($job_id)->getBid()),
				'invoice_id' 	  => $invoice_id
			];
		}
		// If Invoice Type is Bid Grab Bid Line Items by Category
		else{
			$data = [
				'view_type'		  => $view_type,
				'invoice_type'    => $invoice_type,
				'down_payment'    => $down_payment,
				'job'             => $job,
				'company'         => Company::find(1),
				'job_account' 	  => Job::find($job_id)->getAccount(),
				'primary_contact' => Job::find($job_id)->getPrimaryContact(),
				'bid'             => Job::find($job_id)->getBid(),
				'line_items'      => Report::getGroupedLineItems(Job::find($job_id)->getBid()),
				'invoice_id' 	  => $invoice_id
			];
		}

		return View::make('admin.report.invoice',$data);
	}

	/**
	 * View Reports
	 *
	 * @return View
	 */
	public function viewReports()
	{

		$data = [
				'employees_select'    => User::getEmployeeSelect('all'),
				'project_manager_select'    => User::getEmployeeSelect('project_manager'),
				'estimator_select'    => User::getEmployeeSelect('estimator'),
				'foreman_select'    => User::getEmployeeSelect('foreman'),
				'accounting_select'    => User::getEmployeeSelect('accounting'),
		];

		return View::make('admin.report.run-report',$data);
	}

	/**
	 * View Run Report
	 *
	 * @return View
	 */
	public function runReport()
	{

		if (Input::get('report_type') == 'employee_revenue') {
			return Redirect::route('user.revenue.report',[Input::get('report_view'),date('Y-m-d',strtotime(Input::get('start_date'))),date('Y-m-d',strtotime(Input::get('end_date'))),Input::get('user_id')]);
		}
		elseif (Input::get('report_type') == 'booked_work') {
			return Redirect::route('booked.work.report',[Input::get('user_id'),Input::get('job_status')]);
		}
		elseif (Input::get('report_type') == 'production_pipeline') {
			return Redirect::route('production.pipeline.report',[Input::get('user_id')]);
		}
		elseif (Input::get('report_type') == 'bid_requests') {
			return Redirect::route('bid.requests.report',[Input::get('user_id')]);
		}
		elseif (Input::get('report_type') == 'bid_history') {
			return Redirect::route('bid.history.report',[ Input::get('report_view'),date('Y-m-d',strtotime(Input::get('start_date'))),date('Y-m-d',strtotime(Input::get('end_date'))), Input::get('user_id') ]);
		}
		elseif (Input::get('report_type') == 'completed_jobs') {
			return Redirect::route('jobs.completed.report',[ date('Y-m-d',strtotime(Input::get('start_date'))),date('Y-m-d',strtotime(Input::get('end_date'))),Input::get('user_id') ]);
		}
		elseif (Input::get('report_type') == 'follow_ups') {
			return Redirect::route('follow.ups.report',[ date('Y-m-d',strtotime(Input::get('start_date'))),date('Y-m-d',strtotime(Input::get('end_date'))),Input::get('user_id') ]);
		}
		elseif (Input::get('report_type') == 'invoices') {
			return Redirect::route('invoices.report',[ date('Y-m-d',strtotime(Input::get('start_date'))),date('Y-m-d',strtotime(Input::get('end_date'))) ]);
		}
		elseif (Input::get('report_type') == 'jobs-margin') {
			return Redirect::route('jobs.margin.report',[ date('Y-m-d',strtotime(Input::get('start_date'))),date('Y-m-d',strtotime(Input::get('end_date'))) ]);
		}
		elseif (Input::get('report_type') == 'uninvoiced-jobs-labor') {
			return Redirect::route('uninvoiced.jobs.labor.report',[ date('Y-m-d',strtotime(Input::get('start_date'))),date('Y-m-d',strtotime(Input::get('end_date'))) ]);
		}
		elseif (Input::get('report_type') == 'employee-hours') {
			return Redirect::route('employee.hours.report',[ date('Y-m-d',strtotime(Input::get('start_date_limited'))),date('Y-m-d',strtotime(Input::get('end_date_limited'))) ]);
		}
		elseif ( Input::get('report_type') == 'labor-cost-by-job' ) {
			return Redirect::route('labor.cost.report',[ date('Y-m-d',strtotime(Input::get('start_date'))),date('Y-m-d',strtotime(Input::get('end_date')))]);
		}
		elseif ( Input::get('report_type') == 'account_hst_performance' ) {
			return Redirect::route('account.history.performance',[ date('Y-m-d',strtotime(Input::get('start_date'))),date('Y-m-d',strtotime(Input::get('end_date')))]);
		}
		else {
			return Redirect::back()->withErrors('No Actions Available (See ReportController)');
		};

	}

	/**
	 * View Job Work Order
	 *
	 * @param int $job_id
	 *
	 * @return View
	 */
	public function viewJobWorkOrder($job_id)
	{

		$data = [
				'invoice_type'    => NULL,
				'invoice_id' 	  => NULL,
				'job'             => Job::find($job_id),
				'company'         => Company::find(1),
				'job_account' 	  => Job::find($job_id)->getAccount(),
				'primary_contact' => Job::find($job_id)->getPrimaryContact(),
				'bid'             => Job::find($job_id)->getBid(),
				'line_items'      => ( Job::find($job_id)->getBid() ) ?  Report::getGroupedLineItems(Job::find($job_id)->getBid()) : NULL
		];

		return View::make('admin.report.job-work-order',$data);
	}


	/**
	 * View Production/Revenue History by User
	 *
	 * @param $report_view
	 * @param $start_date
	 * @param $end_date
	 * @param null $user_id
	 *
	 * @return mixed
     */
    public function viewUserRevenue($report_view, $start_date, $end_date, $user_id = NULL)
	{
		$data = [
				'report_view'          => $report_view,
				'start_date'           => $start_date,
				'end_date'             => $end_date,
				'project_managers'     => User::getUsersByRoleSlug('project_manager'),
				'project_manager'      => User::find($user_id),
				'user_id'              => $user_id,
		];
		// If a User is Defined Check if that Users Has Jobs within the Given Queried Date Ranges
		if ( User::find($user_id) ) {
			if ( count($project_manager_jobs = User::find($user_id)->getUserJobsByRole('project_manager',['completed','paid','invoicing'],$start_date,$end_date))  ) {
				$data['project_manager_jobs'] = $project_manager_jobs;
				return View::make('admin.report.employee-revenue-report',$data);
			}
			else{
				return Redirect::back()->withErrors(Lang::get('company.no_report_results'));
			}
		}
		else{
			return View::make('admin.report.employee-revenue-report',$data);
		}
	}


	/**
	 * View Bid History Within Date Range
	 *
	 * @param $report_view
	 * @param $start_date
	 * @param $end_date
	 * @param string $estimator
	 * @return mixed
	 */
	public function viewBidHistory($report_view, $start_date, $end_date, $estimator = 'all')
	{
		$bids = Job::where('jobs.bid_date','>=',date('Y-m-d', strtotime($start_date)))
				->where('jobs.bid_date','<=',date('Y-m-d', strtotime($end_date)))
				->join('job_bids', 'jobs.id', '=', 'job_bids.job_id');

		if ( $estimator != 'all' ) {
				$bids->where('jobs.estimator', $estimator);
		}

		$bids = $bids->orderBy('jobs.bid_date', 'asc')->get();
		$data = [
				'report_view' => $report_view,
				'start_date'  => $start_date,
				'end_date'    => $end_date,
				'bids'        => $bids,
				'estimator'	=> User::find($estimator),
		];
		
		if ( $bids->count() ) {
			return View::make('admin.report.bid-history',$data);
		}

		return Redirect::back()->withErrors(Lang::get('company.no_report_results'));
	}

	/**
	 * View Jobs Completed by Project Manager
	 *
	 * @param $start_date
	 * @param $end_date
	 * @param null $user_id
	 * @return mixed
	 */
	public function viewJobsCompleted($start_date, $end_date, $user_id = NULL)
	{
		$jobs = Job::where('completed_date', '>=', date('Y-m-d', strtotime($start_date)))
				->where('completed_date', '<=', date('Y-m-d', strtotime($end_date)))
				->whereIn('status', ['completed', 'rejected', 'paid', 'invoicing']);

		if ( !is_null($user_id) && $user_id != 'all' ) {
			$jobs->where('project_manager', $user_id);
		}

		$jobs = $jobs->orderBy('completed_date', 'asc')->get();

		$data = [
				'start_date'       => $start_date,
				'end_date'         => $end_date,
				'project_managers' => User::getUsersByRoleSlug('project_manager'),
				'project_manager'  => User::find($user_id),
				'jobs'             => $jobs
		];

		if ( $jobs->count() ) {
			return View::make('admin.report.completed-jobs',$data);
		}

		return Redirect::back()->withErrors(Lang::get('company.no_report_results'));
	}


	/**
	 * View Jobs Completed Margins
	 * @param $start_date
	 * @param $end_date
	 *
	 * @return mixed
     */
    public function viewJobsMargin($start_date, $end_date)
	{
		$jobs = Job::where(function($query) {
			$query->where('status','completed')
					->orWhere('status', 'paid')->orWhere('status', 'invoicing');
		})->where('completed_date','>=',date('Y-m-d', strtotime($start_date)))
		->where('completed_date','<=',date('Y-m-d', strtotime($end_date)))
		->orderBy('completed_date', 'asc')
		->get();

		$data = [
			'start_date'       => $start_date,
			'end_date'         => $end_date,
			'jobs'             => $jobs,
		];

		if ($jobs->count()) {
			return View::make('admin.report.jobs-margin',$data);
		}

		return Redirect::back()->withErrors(Lang::get('company.no_report_results'));

	}


	/**
	 * View Univoiced Jobs Labor
	 *
	 * @param $start_date
	 * @param $end_date
	 * @return mixed
	 */
	public function viewUninvoicedJobsLabor($start_date, $end_date)
	{
		$jobs = \DB::select("SELECT jobs.job_number, jobs.name, jobs.status, FORMAT(IFNULL(job_bids.subtotal,0),2) AS 'bid_total',
						FORMAT(SUM(timesheet_details.total*profiles.wage_rate),2) AS 'labor',
						FORMAT(SUM(timesheet_details.total),2) as hours
						FROM timesheet_details, profiles, jobs LEFT JOIN job_bids ON jobs.id=job_bids.job_id
						WHERE jobs.id=timesheet_details.job_id AND timesheet_details.user_id=profiles.id AND jobs.status NOT IN ('completed', 'invoicing', 'paid') AND date_in BETWEEN ? AND ?
						GROUP BY jobs.job_number, jobs.name, jobs.status", [$start_date, $end_date]);

		if (!count($jobs)) {
			return Redirect::back()->withErrors(Lang::get('company.no_report_results'));
		}

		$data = [
				'start_date'       => $start_date,
				'end_date'         => $end_date,
				'jobs'             => $jobs,

		];

		return View::make('admin.report.uninvoiced-jobs-labor',$data);
	}


	/**
	 *  View Univoiced Jobs Labor
	 *
	 * @param $start_date
	 * @param $end_date
	 * @return mixed
     */
	public function viewEmployeeHours($start_date, $end_date)
	{

		$isOneWeek = true;

		$sql1 = "SELECT profiles.id, first_name, profiles.wage_rate, last_name, IFNULL(ST_WK1,0) AS week_1_st,
                    IFNULL(OT_WK1,0) AS week_1_ot,
                    IFNULL(ST_WK1,0) AS total_st,
                    IFNULL(OT_WK1,0) AS total_ot,
                    ROUND(wage_rate*(IFNULL(ST_WK1,0)),2) AS st_pay,
                    ROUND(wage_rate*1.5*(IFNULL(OT_WK1,0)),2) AS ot_pay
                FROM profiles LEFT JOIN
                    (SELECT profiles.id AS wk1_uid,
                        if(sum(total)>40,40,sum(total)) AS ST_WK1,
                        if(sum(total)>40,sum(total)-40,0) AS OT_WK1
                    FROM profiles, timesheet_details
                    WHERE profiles.id=timesheet_details.user_id
                    AND date_in BETWEEN ? AND ?
                    GROUP BY profiles.id) week_1 ON profiles.id=week_1.wk1_uid
                WHERE ST_WK1 IS NOT NULL
                ORDER BY last_name, first_name";

		$sql2 = "SELECT profiles.id, profiles.wage_rate, first_name, last_name, IFNULL(ST_WK1,0) AS Week_1_ST,
					IFNULL(OT_WK1,0) AS Week_1_OT,
					IFNULL(ST_WK2,0) AS Week_2_ST,
					IFNULL(OT_WK2,0) AS Week_2_OT,
					IFNULL(ST_WK1,0)+IFNULL(ST_WK2,0) AS Total_ST,
					IFNULL(OT_WK1,0)+IFNULL(OT_WK2,0) AS Total_OT,
					ROUND(wage_rate*(IFNULL(ST_WK1,0)+IFNULL(ST_WK2,0)),2) AS ST_Pay,
					ROUND(wage_rate*1.5*(IFNULL(OT_WK1,0)+IFNULL(OT_WK2,0)),2) AS OT_pay
				FROM profiles LEFT JOIN
					(SELECT profiles.id AS wk1_uid,
						if(sum(total)>40,40,sum(total)) AS ST_WK1,
						if(sum(total)>40,sum(total)-40,0) AS OT_WK1
					FROM profiles, timesheet_details
					WHERE profiles.id=timesheet_details.user_id
					AND date_in BETWEEN ? AND ?
					GROUP BY profiles.id) week_1 ON profiles.id=week_1.wk1_uid
				LEFT JOIN (SELECT profiles.id AS wk2_uid, if(sum(total)>40,40,sum(total)) AS ST_WK2,
					if(sum(total)>40,sum(total)-40,0) AS OT_WK2
				FROM profiles, timesheet_details
				WHERE profiles.id=timesheet_details.user_id
				AND date_in BETWEEN ? AND ?
				GROUP BY profiles.id) week_2 ON profiles.id=week_2.wk2_uid WHERE ST_WK1 IS NOT NULL OR ST_WK2 IS NOT NULL
				ORDER BY last_name, first_name";

		$employees = \DB::select($sql1, [date('Y-m-d 00:00:00', strtotime($start_date)), date('Y-m-d 23:59:59', strtotime($end_date))]);

		if ( !count($employees) ) {
			return Redirect::back()->withErrors(Lang::get('company.no_report_results'));
		}

		return View::make('admin.report.employee-hours', compact('start_date', 'end_date', 'employees', 'isOneWeek'));
	}


	/**
	 * View Booked Work By Project Manager or All Project Managers
	 *
	 * @param string $user_id
	 * @param string $job_status
	 *
	 * @return mixed
     */
    public function viewBookedWork($user_id = NULL, $job_status = NULL)
	{
		$data = [
				'project_managers' => User::getUsersByRoleSlug('project_manager'),
				'project_manager'  => User::find($user_id),
				'user_id'          => $user_id,
				'job_status'       => $job_status,
		];
		return View::make('admin.report.booked-work',$data);
	}

	/**
	 * View Production Pipeline By Project Manager or All Project Managers
	 *
	 * @param $user_id
	 * @return View
	 */
	public function viewProductionPipeline($user_id = 'all')
	{
		$jobs = Job::whereIn('status',['contracted', 'proceed', 'scheduled']);

		if ( $user_id != 'all' ) {
			$jobs->where('project_manager', $user_id);
		}

		$jobs = $jobs->get();
		$data = [
				'project_managers' => User::getUsersByRoleSlug('project_manager'),
				'project_manager'  => User::find($user_id),
				'user_id'          => $user_id,
				'jobs'             => $jobs,
		];
		return View::make('admin.report.production-pipeline',$data);
	}


	/**
	 * View Bid Requests By Estimator or All Estimators
	 *
	 * @param string|int $user_id
	 * @return mixed
     */
	public function viewBidRequests($user_id = 'all')
	{
		$requests = Job::where('status','request');

		if ( $user_id != 'all' ) {
			$requests->where('estimator', $user_id);
		}

		$requests = $requests->get();

		$data = [
			'estimators' => User::getUsersByRoleSlug('estimator'),
			'estimator'  => User::find($user_id),
			'user_id'    => $user_id,
			'jobs'       => $requests,
		];
		return View::make('admin.report.bid-requests',$data);
	}


	/**
	 * View Follow Ups By Users/Date or All Project Managers
	 *
	 * @param $start_date
	 * @param $end_date
	 * @param mixed $user_id
	 *
	 * @return mixed
     */
    public function viewFollowUps($start_date, $end_date, $user_id = 'all')
	{
		$follow_ups = JobNote::where('follow_up',1)
			->where('created_at','>=',date('Y-m-d', strtotime($start_date)))
			->where('created_at','<=',date('Y-m-d', strtotime($end_date)));

		if ( $user_id && $user_id != 'all' ) {
			$follow_ups->where('assigned', $user_id);
		}

		$follow_ups = $follow_ups->orderBy('created_at', 'asc')->get();

		$data = [
			'start_date' 	=> $start_date,
			'end_date'   	=> $end_date,
			'users'      	=> User::getUsersByRoleSlug('estimator'),
			'user'       	=> User::find($user_id),
			'user_id'    	=> $user_id,
			'follow_ups'	=> $follow_ups,
		];

		// Check to Make Sure Follow Ups Exists in the Given Query
		if ( $follow_ups->count() ) {
			return View::make('admin.report.follow-ups',$data);
		}

		return Redirect::back()->withErrors(Lang::get('company.no_report_results'));

	}


	/**
	 * View Invoices By Users/Date
	 * @param $start_date
	 * @param $end_date
	 *
	 * @return mixed
     */
    public function viewInvoices($start_date, $end_date)
	{

		$data = [
				'start_date' => $start_date,
				'end_date'   => $end_date,
				'invoices'   => Invoice::where('created_at','>=',date('Y-m-d', strtotime($start_date)))->where('created_at','<=',date('Y-m-d', strtotime($end_date)))->orderBy('created_at', 'asc')->get()
		];

		// Check to Make Sure Follow Ups Exists in the Given Query
		if (count($data['invoices'])) {
			return View::make('admin.report.invoices',$data);
		}

		return Redirect::back()->withErrors(Lang::get('company.no_report_results'));
	}


	/**
	 * POST Action for Bid History Chart Within Date Range
	 *
	 * @return mixed
	 */
	public function postBidDollars()
	{
		return Redirect::route('bid.dollars.chart',$this->getChartPostData());
	}

	public function viewAccountHistoryPerformance($start, $end)
	{
		$data = \DB::select('SELECT accounts.name, jobs_bid_count, IFNULL(ROUND(jobs_bid_sum,0),0) 
			AS jobs_bid_sum, IFNULL(jobs_secured_count,0) 
			AS jobs_secure_count, IFNULL(ROUND(jobs_secured_sum,0),0) 
			AS jobs_secure_sum, ROUND(IFNULL(jobs_secured_count,0)/jobs_bid_count,2) 
			AS jobs_secure_pct, IFNULL(ROUND(IFNULL(jobs_secured_sum,0)/IFNULL(jobs_bid_sum,0),2),"N/A") 
			AS dollars_secure_pct FROM accounts LEFT JOIN (SELECT account, COUNT(jobs.id) 
			AS jobs_bid_count, SUM(job_bids.subtotal) 
			AS jobs_bid_sum FROM jobs, job_bids WHERE jobs.id=job_bids.job_id 
			AND jobs.bid_date BETWEEN ?
			AND ? GROUP BY account) qry_jobs_bid ON accounts.id=qry_jobs_bid.account LEFT JOIN (SELECT account, COUNT(jobs.id) 
			AS jobs_secured_count, SUM(job_bids.subtotal) 
			AS jobs_secured_sum FROM jobs, job_bids WHERE jobs.id=job_bids.job_id 
			AND jobs.bid_date BETWEEN ? AND ?
			AND jobs.status IN (\'contracted\', \'proceed\', \'scheduled\', \'invoicing\', \'paid\', \'completed\') GROUP BY account) qry_jobs_secured ON accounts.id=qry_jobs_secured.account 
			WHERE qry_jobs_bid.account IS NOT NULL OR qry_jobs_secured.account IS NOT NULL ORDER BY accounts.name ASC', [$start, $end, $start, $end]);

		if (!count($data)) {
			return Redirect::back()->withErrors('No Results for the period of time you\'ve been selected! ');
		}
		return view('admin.report.account-performance', compact('data'));
	}

	/**
	 * View Bid History Chart Within Date Range
	 * @param $report_view
	 * @param $start_date
	 * @param $end_date
	 * @param string $project_manager
	 * @param string $estimator
	 *
	 * @return mixed
     */
    public function viewBidDollars($report_view, $start_date, $end_date, $project_manager = 'all', $estimator = 'all')
	{
		$bids = JobBid::where('jobs.bid_date','>=',date('Y-m-01', strtotime($start_date)))
						->where('jobs.bid_date','<=',date('Y-m-t', strtotime($end_date)))
						->join('jobs', 'jobs.id', '=', 'job_bids.job_id');

		if ( $project_manager != 'all' ) {
			$bids->where('jobs.project_manager', $project_manager);
		}

		if ( $estimator != 'all' ) {
			$bids->where('jobs.estimator', $estimator);
		}

		$bids = $bids->orderBy('jobs.bid_date', 'asc')->get(['job_bids.*']);

		$data = [
			'start_date'  => $start_date,
			'end_date'    => $end_date,
			'bids'        => $bids,
			'report_view' => $report_view,
		];

		// Check to See that here are bid available if not throw error
		if ( !$bids->count() ) {
			return Redirect::back()->withErrors(Lang::get('company.no_report_results'));
		}


		$start = strtotime($start_date);
		$end = strtotime($end_date);

		while($start < $end) {
			$months_array[date('Y-m', $start)] = [0];
			$start = strtotime("+1 month", $start);
		}

		// ====== Build Array of Data to Pass to HighCharts ======

		// First Build Array and Group by Date
		foreach ($data['bids'] as $bid){
			// Filter To Get Only Paid Jobs
			$grouped_data[date('Y-m',strtotime($bid->created_at))][] = $bid->subtotal;
		}


		//Combine the Two Arrays
		$grouped_data = $grouped_data + $months_array;

		// Sum Up Bids that Fall on the Same Day and Setup Data for Highcharts ex [ ['date',dollars],['date',dollars] ]
		foreach ($grouped_data as $date => $dollars) {

			// MONTHLY
			if ( $data['report_view'] == 'monthly' ) {
				$bid_data[] = [$date,array_sum($dollars)];
			}
			// 12 MONTH TRAILING
			elseif ( $data['report_view'] == '12-month' ) {
				$bid_data[] = [$date,Report::getTrailingTwelveMonthBidDollars($date)];
			}
			else{
				return Redirect::back()->withErrors(Lang::get('company.no_report_results'));
			}
			$orderByDate[$date]  = strtotime($date);
		}

		array_multisort($bid_data, SORT_ASC, $orderByDate);

		//Encode Data to JSON
		$bid_data = json_encode($bid_data,JSON_NUMERIC_CHECK);


		// Add the Bid Data to the Views Data Variable
		$data['bid_data'] = $bid_data;

		// Create Chart View! :)
		return View::make('admin.report.total-dollars-bid',$data);
	}


	/**
	 * POST Action for Getting Revenue by Month Chart Within Date Range
	 *
	 * @return View
	 */
	public function postRevenueByMonth()
	{
		return Redirect::route('revenue', $this->getChartPostData());
	}


	/**
	 * View  Revenue by Month Chart Within Date Range
	 *
	 * @param $report_view
	 * @param $start_date
	 * @param $end_date
	 * @param string $project_manager
	 * @param string $estimator
	 *
	 * @return mixed
     */
    public function viewRevenueByMonth($report_view, $start_date, $end_date, $project_manager = 'all', $estimator = 'all')
	{
		$bids = JobBid::where('jobs.bid_date','>=',date('Y-m-d', strtotime($start_date)))
				->where('jobs.bid_date','<=',date('Y-m-d', strtotime($end_date)))
				->join('jobs', 'jobs.id', '=', 'job_bids.job_id');

		if ( $project_manager != 'all' ) {
			$bids->where('jobs.project_manager', $project_manager);
		}

		if ( $estimator != 'all' ) {
			$bids->where('jobs.estimator', $estimator);
		}

		$bids = $bids->orderBy('jobs.bid_date', 'asc')->get(['job_bids.*']);

		$data = [
			'start_date'  => $start_date,
			'end_date'    => $end_date,
			'bids'        => $bids,
			'report_view' => $report_view
		];

		// Check to See that here are bid available if not throw error
		if ( !$bids->count() ) {
			return Redirect::back()->withErrors(Lang::get('company.no_report_results'));
		} else {

			// Build Array of All the Months Between the Date Range Queried
			$start = strtotime($start_date);
			$end = strtotime($end_date);
			while($start < $end)
			{
				$months_array[date('Y-m', $start)] = [0];
				$start = strtotime("+1 month", $start);
			}

			// ====== Build Array of Data to Pass to HighCharts ======

			// First Build Array and Group by Date
			foreach ($data['bids'] as $bid){
				// Filter To Get Only Paid Jobs
				if ( in_array($bid->job()->status,['completed','invoicing','paid']) ) {
					$grouped_data[date('Y-m',strtotime($bid->created_at))][] = $bid->subtotal;
				}
			}

			//Combine the Two Arrays
			$grouped_data = $grouped_data + $months_array;

			// Sum Up Bids that Fall on the Same Day and Setup Data for Highcharts ex [ ['date',dollars],['date',dollars] ]
			foreach ($grouped_data as $date => $dollars) {

				// MONTHLY
				if ( $data['report_view'] == 'monthly' ) {
					$bid_data[] = [$date,array_sum($dollars)];
				}
				// 12 MONTH TRAILING
				elseif ( $data['report_view'] == '12-month' ) {
					$bid_data[] = [$date,Report::getTrailingTwelveMonthBidDollarsByJobStatus($date,['completed','invoicing','paid'])];
				}
				else{
					return Redirect::back()->withErrors(Lang::get('company.no_report_results'));
				}
				$orderByDate[$date]  = strtotime($date);
			}

			// Sort the Data by Month Ascending
			array_multisort($bid_data, SORT_ASC, $orderByDate);

			//Encode Data to JSON
			$bid_data = json_encode($bid_data,JSON_NUMERIC_CHECK);

			// Add the Bid Data to the Views Data Variable
			$data['bid_data'] = $bid_data;

			// Create Chart View! :)
			return View::make('admin.report.revenue-by-month',$data);
		}
	}


	/**
	 * POST Action for Getting Dollars Bid and Performed Within Date Range
	 *
	 * @return View
	 */
	public function postDollarsBidPerformed()
	{

		return Redirect::route('dollars.bid.and.performed',$this->getChartPostData());
	}


	/**
	 * @param $report_view
	 * @param $start_date
	 * @param $end_date
	 * @param string $project_manager
	 * @param string $estimator
	 * @return mixed
     */
	public function viewDollarsBidPerformed($report_view, $start_date, $end_date, $project_manager = 'all', $estimator = 'all')
	{

		$bids = JobBid::where('jobs.bid_date','>=',date('Y-m-d', strtotime($start_date)))
				->where('jobs.bid_date','<=',date('Y-m-d', strtotime($end_date)))
				->join('jobs', 'jobs.id', '=', 'job_bids.job_id');

		if ( $project_manager != 'all' ) {
			$bids->where('jobs.project_manager', $project_manager);
		}

		if ( $estimator != 'all' ) {
			$bids->where('jobs.estimator', $estimator);
		}

		$bids = $bids->orderBy('jobs.bid_date', 'asc')->get(['job_bids.*']);

		$data = [
				'start_date'  => $start_date,
				'end_date'    => $end_date,
				'bids'        => $bids,
				'report_view' => $report_view,

		];


		// Check to See that here are bid available if not throw error
		if (!$bids->count()) {
			return Redirect::back()->withErrors(Lang::get('company.no_report_results'));
		}
		else{

			// Build Array of All the Months Between the Date Range Queried
			$start = strtotime($start_date);
			$end = strtotime($end_date);
			while($start < $end)
			{
				$months_array[date('Y-m', $start)] = [0];
				$start = strtotime("+1 month", $start);
			}


			// ====== Build Array of Bid Data to Pass to HighCharts ======

			// First Build Array and Group by Date
			foreach ($data['bids'] as $bid){
				// Filter To Get Only Paid Jobs
				$grouped_data[date('Y-m',strtotime($bid->bid_date))][] = $bid->subtotal;
			}

			//Combine the Two Arrays
			$grouped_data = $grouped_data + $months_array;

			// Sum Up Bids that Fall on the Same Day and Setup Data for Highcharts ex [ ['date',dollars],['date',dollars] ]
			foreach ($grouped_data as $date => $dollars) {
				// MONTHLY
				if ( $data['report_view'] == 'monthly' ) {
					$bid_data[] = [$date,array_sum($dollars)];
				}
				// 12 MONTH TRAILING
				elseif ( $data['report_view'] == '12-month' ) {
					$bid_data[] = [$date,Report::getTrailingTwelveMonthBidDollars($date)];
				}
				else{
					return Redirect::back()->withErrors(Lang::get('company.no_report_results'));
				}
				$orderByDate[$date]  = strtotime($date);
			}

			// Sort the Array By Date
			array_multisort($bid_data, SORT_ASC, $orderByDate);

			// ====== Build Array of Performed Data to Pass to HighCharts ======

			// First Build Array and Group by Date
			foreach ($data['bids'] as $bid){
				// Filter To Get Only Completed, Invoicing, and Paid Jobs
				if ( in_array($bid->job()->status,['completed','invoicing','paid']) ) {
					$grouped_performed_data[date('Y-m',strtotime($bid->bid_date))][] = $bid->subtotal;
				}
				else{
					$grouped_performed_data[date('Y-m',strtotime($bid->bid_date))][] = 0;
				}
			}

			//Combine the Two Arrays
			$grouped_performed_data = $grouped_performed_data + $months_array;


			// Sum Up Bids that Fall on the Same Day and Setup Data for Highcharts ex [ ['date',dollars],['date',dollars] ]
			foreach ($grouped_performed_data as $date => $dollars) {
				if ( $data['report_view'] == 'monthly' ) {
					$performed_data[] = [$date,array_sum($dollars)];
				}
				// 12 MONTH TRAILING
				elseif ( $data['report_view'] == '12-month' ) {
					$performed_data[] = [$date,Report::getTrailingTwelveMonthBidDollarsByJobStatus($date,['completed','invoicing','paid'])];
				}
				else{
					return Redirect::back()->withErrors(Lang::get('company.no_report_results'));
				}
				$orderByDate[$date]  = strtotime($date);
			}

			array_multisort($performed_data, SORT_ASC, $orderByDate);

			//Encode Data to JSON
			$performed_data = json_encode($performed_data,JSON_NUMERIC_CHECK);

			// Add the Bid Data to the Views Data Variable
			$data['performed_data'] = $performed_data;

			//Encode Data to JSON
			$bid_data = json_encode($bid_data,JSON_NUMERIC_CHECK);

			// Add the Bid Data to the Views Data Variable
			$data['bid_data'] = $bid_data;


			// Create Chart View! :)
			return View::make('admin.report.dollars-bid-and-performed',$data);
		}
	}

	/**
	 * POST Action for Getting Jobs Bid and Performed Within Date Range
	 *
	 * @return View
	 */
	public function postJobsBidPerformed()
	{

		return Redirect::route('jobs.bid.and.performed',$this->getChartPostData());
	}


	/**
	 * View Jobs Bid and Performed Chart Within Date Range
	 *
	 * @param $report_view
	 * @param $start_date
	 * @param $end_date
	 * @param string $project_manager
	 * @param string $estimator
	 * @return mixed
     */
	public function viewJobsBidPerformed($report_view, $start_date, $end_date, $project_manager = 'all', $estimator = 'all')
	{

		$bids = JobBid::where('job_bids.bid_date','>=',date('Y-m-01', strtotime($start_date)))
				->where('job_bids.bid_date','<=',date('Y-m-t', strtotime($end_date)))
				->join('jobs', 'jobs.id', '=', 'job_bids.job_id');

		if ( $project_manager != 'all' ) {
			$bids->where('jobs.project_manager', $project_manager);
		}

		if ( $estimator != 'all' ) {
			$bids->where('jobs.estimator', $estimator);
		}

		$bids = $bids->orderBy('job_bids.bid_date', 'asc')->get(['job_bids.*']);

		$data = [
				'start_date'  => $start_date,
				'end_date'    => $end_date,
				'bids'        => $bids,
				'report_view' => $report_view
		];

		// Check to See that here are bid available if not throw error
		if (!$bids->count()) {
			return Redirect::back()->withErrors(Lang::get('company.no_report_results'));
		}
		else{

			// Build Array of All the Months Between the Date Range Queried
			$start = strtotime($start_date);
			$end = strtotime($end_date);
			while($start < $end)
			{
				$months_array[date('Y-m', $start)] = [0];
				$start = strtotime("+1 month", $start);
			}


			// ====== Build Array of Bid Data to Pass to HighCharts ======

			// First Build Array and Group by Date
			foreach ($data['bids'] as $bid){
				$grouped_data[date('Y-m',strtotime($bid->bid_date))][] = 1;
			}

			//Combine the Two Arrays
			$grouped_data = $grouped_data + $months_array;


			// Sum Up Bids that Fall on the Same Day and Setup Data for Highcharts ex [ ['date',jobs],['date',jobs] ]
			foreach ($grouped_data as $date => $jobs) {
				// MONTHLY
				if ( $data['report_view'] == 'monthly' ) {
					$bid_data[] = [$date,array_sum($jobs)];
				}
				// 12 MONTH TRAILING
				elseif ( $data['report_view'] == '12-month' ) {
					$bid_data[] = [$date,Report::getTrailingTwelveMonthJobsBid($date)];
				}
				else{
					return Redirect::back()->withErrors(Lang::get('company.no_report_results'));
				}
				$orderByDate[$date]  = strtotime($date);
			}

			// Sort the Array By Date
			array_multisort($bid_data, SORT_ASC, $orderByDate);

			//Encode Data to JSON
			$bid_data = json_encode($bid_data,JSON_NUMERIC_CHECK);

			// Add the Bid Data to the Views Data Variable
			$data['bid_data'] = $bid_data;



			// ====== Build Array of Performed Data to Pass to HighCharts ======

			// First Build Array and Group by Date
			foreach ($data['bids'] as $bid){
				// Filter To Get Only Paid Jobs
				if ( in_array($bid->job()->status,['completed','invoicing','paid']) ) {
					$grouped_performed_data[date('Y-m',strtotime($bid->bid_date))][] = 1;
				}
				else{
					$grouped_performed_data[date('Y-m',strtotime($bid->bid_date))][] = 0;
				}
			}

			//Combine the Two Arrays
			$grouped_performed_data = $grouped_performed_data + $months_array;


			// Sum Up Bids that Fall on the Same Day and Setup Data for Highcharts ex [ ['date',jobs],['date',jobs] ]
			foreach ($grouped_performed_data as $date => $jobs) {
				// MONTHLY
				if ( $data['report_view'] == 'monthly' ) {
					$performed_data[] = [$date,array_sum($jobs)];
				}
				// 12 MONTH TRAILING
				elseif ( $data['report_view'] == '12-month' ) {
					$performed_data[] = [$date,Report::getTrailingTwelveMonthJobsBidByJobStatus($date,['completed','invoicing','paid'])];
				}
				else{
					return Redirect::back()->withErrors(Lang::get('company.no_report_results'));
				}
				$orderByDate[$date]  = strtotime($date);
			}

			array_multisort($performed_data, SORT_ASC, $orderByDate);

			//Encode Data to JSON
			$performed_data = json_encode($performed_data,JSON_NUMERIC_CHECK);

			// Add the Bid Data to the Views Data Variable
			$data['performed_data'] = $performed_data;

			// Create Chart View! :)
			return View::make('admin.report.jobs-bid-and-performed',$data);
		}
	}

	/**
	 * POST Action for Getting Gross Revenue Within Date Range
	 *
	 * @return View
	 */
	public function postGrossRevenue()
	{

		return Redirect::route('gross.revenue',$this->getChartPostData());
	}


	/**
	 *  View Gross Revenue Chart Within Date Range
	 *
	 * @param $start_date
	 * @param $end_date
	 * @param string $project_manager
	 * @param string $estimator
	 * @return mixed
     */
	public function viewGrossRevenue($start_date, $end_date, $project_manager = 'all', $estimator = 'all')
	{

		$bids = JobBid::where('job_bids.bid_date','>=',date('Y-m-d', strtotime($start_date)))
				->where('job_bids.bid_date','<=',date('Y-m-d', strtotime($end_date)))
				->join('jobs', 'jobs.id', '=', 'job_bids.job_id');

		if ( $project_manager != 'all' ) {
			$bids->where('jobs.project_manager', $project_manager);
		}

		if ( $estimator != 'all' ) {
			$bids->where('jobs.estimator', $estimator);
		}

		$bids = $bids->orderBy('job_bids.bid_date', 'asc')->get(['job_bids.*']);

		$data = [
				'start_date'  => $start_date,
				'end_date'    => $end_date,
				'bids'        => $bids,
		];


		// Check to See that here are bid available if not throw error
		if (!$bids->count()) {
			return Redirect::back()->withErrors(Lang::get('company.no_report_results'));
		}
		else{

			// Build Array of All the Months Between the Date Range Queried
			$start = strtotime($start_date);
			$end = strtotime($end_date);
			while($start < $end)
			{
				$months_array[date('Y-m', $start)] = [0];
				$start = strtotime("+1 month", $start);
			}


			// ====== Build Array of Revenue Data to Pass to HighCharts ======

			// First Build Array and Group by Date
			foreach ($data['bids'] as $bid){
				// Filter To Get Only Paid Jobs
				if ($bid->job()->status == "paid") {
					$grouped_revenue_data[date('Y-m',strtotime($bid->bid_date))][] = $bid->grand_total;
				}
			}

			//Combine the Two Arrays
			$grouped_revenue_data = $grouped_revenue_data + $months_array;


			// Sum Up Bids that Fall on the Same Day and Setup Data for Highcharts ex [ ['date',dollars],['date',dollars] ]
			foreach ($grouped_revenue_data as $date => $dollars) {
				$revenue_data[] = [$date,array_sum($dollars)];
				$orderByDate[$date]  = strtotime($date);
			}

			array_multisort($revenue_data, SORT_ASC, $orderByDate);

			//Encode Data to JSON
			$revenue_data = json_encode($revenue_data,JSON_NUMERIC_CHECK);

			// Add the Bid Data to the Views Data Variable
			$data['revenue_data'] = $revenue_data;

			// Create Chart View! :)
			return View::make('admin.report.gross-revenue',$data);
		}
	}




	/**
	 * POST Action for Getting Bid and Completion Trend Within Date Range
	 *
	 * @return View
	 */
	public function postBidCompletionByDollars()
	{

		return Redirect::route('bid.completion.by.dollars',$this->getChartPostData());
	}


	/**
	 *  View Bid and Completion Trend Chart Within Date Range
	 *
	 * @param $report_view
	 * @param $start_date
	 * @param $end_date
	 * @param string $project_manager
	 * @param string $estimator
	 * @return mixed
     */
	public function viewBidCompletionByDollars($report_view, $start_date, $end_date, $project_manager = 'all', $estimator = 'all')
	{
		$bids = JobBid::where('job_bids.bid_date','>=',date('Y-m-d', strtotime($start_date)))
				->where('job_bids.bid_date','<=',date('Y-m-d', strtotime($end_date)))
				->join('jobs', 'jobs.id', '=', 'job_bids.job_id');

		if ( $project_manager != 'all' ) {
			$bids->where('jobs.project_manager', $project_manager);
		}

		if ( $estimator != 'all' ) {
			$bids->where('jobs.estimator', $estimator);
		}

		$bids = $bids->orderBy('job_bids.bid_date', 'asc')->get(['job_bids.*']);

		$data = [
				'start_date'  => $start_date,
				'end_date'    => $end_date,
				'bids'        => $bids,
				'report_view' => $report_view,

		];


		// Check to See that here are bid available if not throw error
		if (!count($data['bids'])) {
			return Redirect::back()->withErrors(Lang::get('company.no_report_results'));
		}
		else{

			// Build Array of All the Months Between the Date Range Queried
			$start = strtotime($start_date);
			$end = strtotime($end_date);
			while($start < $end)
			{
				$months_array[date('Y-m', $start)] = [0];
				$start = strtotime("+1 month", $start);
			}


			// ====== Build Array of Bid Data to Pass to HighCharts ======

			// First Build Array and Group by Date
			foreach ($data['bids'] as $bid){
				// Get Total Job Dollars
				$job_dollars[date('Y-m',strtotime($bid->bid_date))][] = $bid->subtotal;
				// Filter To Get Jobs with Specified Statuses
				if ( in_array($bid->job()->status,['completed','invoicing','paid','scheduled','contracted','proceed']) ) {
					$performed_job_dollars[date('Y-m',strtotime($bid->bid_date))][] = $bid->subtotal;
				}
				else{
					$performed_job_dollars[date('Y-m',strtotime($bid->bid_date))][] = 0;
				}
			}

			//Combine the Two Arrays
			$job_dollars  = $job_dollars + $months_array;
			$performed_job_dollars = $performed_job_dollars + $months_array;

			// Sort Arrays by Date(key)
			ksort($job_dollars);
			ksort($performed_job_dollars);

			// ====== Build Array of Percent of Jobs Succeeded/Won to Pass to HighCharts ======
			if ($report_view == 'monthly') {
				foreach ($job_dollars as $date => $count) {

					$performed_dollars = array_sum($performed_job_dollars[date('Y-m',strtotime($date))]);

					// Check if the Month has 0 Jobs Bid
					if ($performed_dollars OR $count == 0) {
						$percent = number_format($performed_dollars / array_sum($count),2) * 100;
					}
					else{
						$percent = 0;
					}
					// /Setup Array to Highcharts Accepted Format
					$percent_data[] = [$date,$percent];
				}
			}
			elseif ($report_view == '12-month') {
				foreach ($job_dollars as $date => $count) {

					$bid_dollars = Report::getTrailingTwelveMonthBidDollars($date,'paid');
					$performed_dollars = Report::getTrailingTwelveMonthBidDollarsByJobStatus($date,['completed','invoicing','paid','scheduled','contracted','proceed']);

					// Check if the Month has 0 Jobs Bid
					if ($performed_dollars OR $count == 0) {
						$percent = number_format($performed_dollars / $bid_dollars,2) * 100;
					}
					else{
						$percent = 0;
					}
					// /Setup Array to Highcharts Accepted Format
					$percent_data[] = [$date,$percent];
				}

			}

			//Encode Data to JSON
			$percent_data = json_encode($percent_data,JSON_NUMERIC_CHECK);

			// Add the Bid Data to the Views Data Variable
			$data['percent_data'] = $percent_data;

			// Create Chart View! :)
			return View::make('admin.report.bid-completion-by-dollars',$data);
		}
	}

	/**
	 * POST Action for Getting Bid and Completion Trend Within Date Range
	 *
	 * @return View
	 */
	public function postBidCompletionByJobs()
	{

		return Redirect::route('bid.completion.by.jobs',$this->getChartPostData());
	}


	/**
	 * View Bid and Completion Trend Chart Within Date Range
	 *
	 * @param $report_view
	 * @param $start_date
	 * @param $end_date
	 * @param string $project_manager
	 * @param string $estimator
	 * @return mixed
     */
	public function viewBidCompletionByJobs($report_view, $start_date, $end_date, $project_manager = 'all', $estimator = 'all')
	{
		$bids = JobBid::where('job_bids.bid_date','>=',date('Y-m-d', strtotime($start_date)))
				->where('job_bids.bid_date','<=',date('Y-m-d', strtotime($end_date)))
				->join('jobs', 'jobs.id', '=', 'job_bids.job_id');

		if ( $project_manager != 'all' ) {
			$bids->where('jobs.project_manager', $project_manager);
		}

		if ( $estimator != 'all' ) {
			$bids->where('jobs.estimator', $estimator);
		}

		$bids = $bids->orderBy('job_bids.bid_date', 'asc')->get(['job_bids.*']);

		$data = [
				'start_date'  => $start_date,
				'end_date'    => $end_date,
				'bids'        => $bids,
				'report_view' => $report_view,

		];

		// Check to See that here are bid available if not throw error
		if (!count($data['bids'])) {
			return Redirect::back()->withErrors(Lang::get('company.no_report_results'));
		}
		else{

			// Build Array of All the Months Between the Date Range Queried
			$start = strtotime($start_date);
			$end = strtotime($end_date);
			while($start < $end)
			{
				$months_array[date('Y-m', $start)] = [0];
				$start = strtotime("+1 month", $start);
			}

			// ====== Build Array of Bid Data to Pass to HighCharts ======

			// First Build Array and Group by Date
			foreach ($data['bids'] as $bid){
				// Get Total Job Dollars
				$jobs_bid[date('Y-m',strtotime($bid->bid_date))][] = 1;
				// Filter To Get Only Paid Jobs
				if ( in_array($bid->job()->status,['completed','invoicing','paid','scheduled','contracted','proceed']) ) {
					$performed_jobs_bid[date('Y-m',strtotime($bid->bid_date))][] = 1;
				}
				else{
					$performed_jobs_bid[date('Y-m',strtotime($bid->bid_date))][] = 0;
				}
			}

			//Combine the Two Arrays
			$jobs_bid  = $jobs_bid + $months_array;
			$performed_jobs_bid = $performed_jobs_bid + $months_array;

			// Sort Arrays by Date(key)
			ksort($jobs_bid);
			ksort($performed_jobs_bid);

			// ====== Build Array of Percent of Jobs Succeeded/Won to Pass to HighCharts ======
			if ($report_view == 'monthly') {
				foreach ($jobs_bid as $date => $count) {

					$performed_jobs = array_sum($performed_jobs_bid[date('Y-m',strtotime($date))]);

					// Check if the Month has 0 Jobs Bid
					if ($performed_jobs OR $count == 0) {
						$percent = number_format($performed_jobs / array_sum($count),2) * 100;
					}
					else{
						$percent = 0;
					}
					// /Setup Array to Highcharts Accepted Format
					$percent_data[] = [$date,$percent];
				}
			}
			elseif ($report_view == '12-month') {
				foreach ($jobs_bid as $date => $count) {

					$job_bid = Report::getTrailingTwelveMonthBidDollars($date,['completed','invoicing','paid','scheduled','contracted','proceed']);
					$performed_jobs = Report::getTrailingTwelveMonthBidDollarsByJobStatus($date,['completed','invoicing','paid','scheduled','contracted','proceed']);

					// Check if the Month has 0 Jobs Bid
					if ($performed_jobs OR $count == 0) {
						$percent = number_format($performed_jobs / $job_bid,2) * 100;
					}
					else{
						$percent = 0;
					}
					// /Setup Array to Highcharts Accepted Format
					$percent_data[] = [$date,$percent];
				}

			}

			//Encode Data to JSON
			$percent_data = json_encode($percent_data,JSON_NUMERIC_CHECK);

			// Add the Bid Data to the Views Data Variable
			$data['percent_data'] = $percent_data;

			// Create Chart View! :)
			return View::make('admin.report.bid-completion-by-jobs',$data);
		}
	}

	public function viewLaborCosts($start, $end)
	{
		$data = \DB::select('SELECT job_number, jobs.name, task, round(SUM(total*wage_rate),2) as cost
								FROM jobs, profiles, timesheet_details
								WHERE jobs.id=timesheet_details.job_id AND timesheet_details.user_id=profiles.id AND date_in BETWEEN ? AND ? GROUP BY job_number, jobs.name, task
								ORDER BY job_number ASC, task ASC', [$start, $end]);
		if (!count($data)) {
			return Redirect::back()->withErrors(Lang::get('company.no_report_results'));
		}

		return view('admin.report.labor-cost', compact('data'));
	}

	/**
	 * POST Action for Getting Bid and Completion Trend Within Date Range
	 *
	 * @return View
	 */
	public function postBidCompletionHistogram()
	{
		return Redirect::route('bid.completion.histogram', [
			'start_date' 		=> date('Y-m-d',strtotime(Input::get('start_date'))),
			'end_date' 			=> date('Y-m-d',strtotime(Input::get('end_date'))),
			'project_manager' 	=> Input::get('project_manager'),
			'estimator'			=> Input::get('estimator'),
		]);
	}


	/**
	 * View Bid and Completion Trend Chart Within Date Range
	 *
	 * @param $start_date
	 * @param $end_date
	 * @param string $project_manager
	 * @param string $estimator
	 * @return mixed
     */
	public function viewBidCompletionHistogram($start_date, $end_date, $project_manager = 'all', $estimator = 'all')
	{
		$data = JobBid::where('jobs.bid_date','>=',date('Y-m-d', strtotime($start_date)))
			->where('jobs.bid_date','<=',date('Y-m-d', strtotime($end_date)))
			->join('jobs', 'jobs.id', '=', 'job_bids.job_id');

		if ( $project_manager != 'all' ) {
			$data->where('jobs.project_manager', $project_manager);
		}

		if ( $estimator != 'all' ) {
			$data->where('jobs.estimator', $estimator);
		}

		$data = $data->get();

		if ( !$data->count() ) {
			return Redirect::back()->withErrors(Lang::get('company.no_report_results'));
		}

		$ranges = $this->getHistogramRangeCategories($data);

		foreach ( $ranges as $key => $parameters ) {
			$labels[] = "<strong>".$parameters['label']
				."</strong><br>".$parameters['percentage']
				."<br><strong>Won: </strong>".$parameters['wins']
				."<br><strong>Lost: </strong>".$parameters['loss'];

			$won[] = $parameters['wins'];
			$lost[] = $parameters['loss'];
		}

		$start = date('F d, Y', strtotime($start_date));
		$end = date('F d, Y', strtotime($end_date));

		$title = sprintf('Winning Percentage by Range (%s - %s)', $start, $end);
		return View::make('admin.report.bid-completion-histogram', compact('data', 'ranges', 'labels', 'won', 'lost', 'start', 'end', 'title'));
	}

	/**
	 * Create a job bid
	 *
	 * @param $job
	 *
	 * @return $this
     */
    protected function createJobBid($job)
	{
		JobBid::create([
				'job_id'          => $job->getKey(),
				'status'          => 'open',
				'subtotal'        => 0,
				'grand_total'     => 0,
				'tax_rate'        => 0,
				'tax_description' => 0,
				'sales_tax_total' => 0,
		]);

		return $this;
	}

	/**
	 * Get Chart Post Data
	 *
	 * @return array
     */
	protected function getChartPostData()
	{
		return [
			'report_view' 		=> Input::get('report_view'),
			'start_date' 		=> date('Y-m-d',strtotime(Input::get('start_date'))),
			'end_date' 			=> date('Y-m-d',strtotime(Input::get('end_date'))),
			'project_manager' 	=> Input::get('project_manager'),
			'estimator'			=> Input::get('estimator'),
		];
	}

	/**
	 * Creates an associative array of categories with corresponding
	 * data, to be used in the bid histogram
	 *
	 * @param Collection $data
	 *
	 * @return array
     */
    protected function getHistogramRangeCategories(Collection $data)
	{
		// Number of columns to display on the chart
		$columns = 5;

		$ranges = [];

		// Create an array of grand totals and determine the highest
		// bid amount. Use the highest amount to create an iterator
		$totals = $data->map(function($key) {
			return $key->getAttribute('grand_total');
		})->toArray();

		$highest = round(max($totals));
		$highest = round($highest, -(strlen($highest)-2));
		$iterator = $highest/$columns;

		// An array of statuses that are considered a "win"
		$win_statuses = ['paid', 'contracted', 'proceed', 'scheduled', 'completed', 'invoicing'];

		// Create categories with a dynamic range based on the column number
		// and the maximum range of the bid grand totals
		for ( $i=0; $i<= ($columns-1); $i++ ) {
			$low = $i*$iterator;
			$high = $i == ($columns-1) ? $highest : (($i+1)*($iterator)-1);

			// Filter any bids with a grand total that matches
			// the given high - low range.
			$matches = $data->filter(function($key) use($low, $high) {
				$grand = $key->getAttribute('grand_total');
				return (bool)($grand >= $low && $grand <= $high);
			});

			// Wins are based on status. Anything matching the $win_statuses array
			// is considered a win.
			$wins = $matches->filter(function($key) use($win_statuses) {
				return (bool)in_array($key->getAttribute('status'), $win_statuses);
			})->count();

			// Literally not a win
			$loss = $matches->filter(function($key) use($win_statuses) {
				return (bool)!in_array($key->getAttribute('status'), $win_statuses);
			})->count();

			// Create a formatted category array to be used with
			// High Charts Histogram
			$ranges[] = [
				'label'			=>	'$'.number_format($low).' - '.'$'.number_format($high),
				'total'			=>	$matches->count(),
				'wins'			=>	$wins,
				'loss'			=>  $loss,
				'percentage'	=>	$wins > 0 ? round($wins / $matches->count() * 100, 1).'%' : '0%'
			];
		}

		return $ranges;
	}
}
