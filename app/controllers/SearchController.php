<?php namespace App\Controllers;
/**
*	Copyright 2015 CoolBlueWeb
*	
*	Licensed under the Apache License, Version 2.0 (the "License");
*	you may not use this file except in compliance with the License.
*	You may obtain a copy of the License at
*	
*			http://www.apache.org/licenses/LICENSE-2.0
*	
*	Unless required by applicable law or agreed to in writing, software
*	distributed under the License is distributed on an "AS IS" BASIS,
*	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.routes
*	See the License for the specific language governing permissions and
*	limitations under the License.
*
*	@package 	Laravel
*	@subpackage coolblueweb Custom
*	@copyright  2015 CoolBlueWeb
*	@author 	CoolBlueWeb <bryan@coolblueweb.com>
*/

use BaseController, Sentinel, View, Input, Response, Redirect, Session, Mail, Lang, Config,App;
use App\Models\Report;
use App\Models\Company;
use App\Models\JobRework;
use App\Models\JobNote;
use App\Models\Job;
use App\Models\Contact;
use App\Models\User;
use App\Models\Profile;
use App\Models\Invoice;
use App\Models\InvoiceLineItems;

class SearchController extends BaseController {

	/**
	 * View Search
	 *
	 * @return View
	 */
	public function viewSearch()
	{

		$data = [
			'var'    => 'value',
		];
		
		return View::make('admin.search.search',$data);
	}

}
