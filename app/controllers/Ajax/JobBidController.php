<?php

namespace App\Controllers\Ajax;

use App\Models\Job;

class JobBidController extends \BaseController
{
    public function reload()
    {
        $job = Job::find(\Input::get('job_id'));
        $bid = $job->bid()->first();

        $lineItemIds = [];

        if ( $bid->exists() ) {
            foreach ( $bid->getLineItems() as $lineItem ) {
                $lineItemIds[] = $lineItem->getAttribute('id');
            }
        }

        return \Response::json(['ids' => $lineItemIds]);

    }
}