<?php namespace App\Controllers\Ajax;
use App\Models\Account;
use Illuminate\Support\Facades\Response;

/**
 * This file is part of the App\Controllers\Ajax package
 *
 * @package App\Controllers\Ajax
 * @subpackage AccountController
 * @copyright 2010-2014 CoolBlueWeb
 * @author Michael Kyle Martin <michael@coolblueweb.com>
 */


class AccountController extends \BaseController
{
    public function address($account_id)
    {
        $account = Account::findOrFail($account_id);
        return Response::json($account);
    }
}