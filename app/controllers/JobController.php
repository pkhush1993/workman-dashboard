<?php namespace App\Controllers;
/**
 *	Copyright 2014 CoolBlueWeb
 *
 *	Licensed under the Apache License, Version 2.0 (the "License");
 *	you may not use this file except in compliance with the License.
 *	You may obtain a copy of the License at
 *
 *			http://www.apache.org/licenses/LICENSE-2.0
 *
 *	Unless required by applicable law or agreed to in writing, software
 *	distributed under the License is distributed on an "AS IS" BASIS,
 *	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *	See the License for the specific language governing permissions and
 *	limitations under the License.
 *
 *	@package 	Laravel
 *	@subpackage CoolBlueWeb Custom
 *	@copyright  2014 CoolBlueWeb
 *	@author 	CoolBlueWeb <bryan@coolblueweb.com>
 */

use BaseController, Sentinel, View, Input, Response, Redirect,Request, Session, Mail, Lang, Config,App,AccountContactsController;
use App\Models\Job;
use App\Models\Company;
use App\Models\Contact;
use App\Models\Account;
use App\Models\Activity;
use App\Models\BidField;
use App\Models\BidFieldItem;
use App\Models\JobBid;
use App\Models\JobBidLineItems;
use App\Models\JobContact;
use App\Models\JobRework;
use App\Models\JobNote;
use App\Models\User;
use App\Models\TaxRate;
use App\Classes\DataTablesSSP;

class JobController extends BaseController {

	/**
	 * Get Jobs
	 *
	 * @return View
	 */
	public function viewJobs()
	{

		$jobs = Job::all();
		return view('admin.job.jobs', compact('jobs'));
	}

	/**
	 * Get Jobs AJAX
	 *
	 * @return View
	 */
	public function viewJobsAJAX()
	{
		/*
         * DataTables server-side processing script for Subscriptions Tables.
         *
         */

		// DB table to use
		$table = 'jobs';

		// Table's primary key
		$primaryKey = 'j`.`id';

		$columns = array(
				array(
						'db'        => '`j`.`job_number`',
						'dt'        => 0,
						'field'		=> 'job_number',
						'formatter' => function( $d) {
							return'<a href="'.route("view.job",['job_number' => $d, 'type' => 'job_number']).'"><i class="large unhide icon"></i></a>';
						}
				),
				array(
						'db' 		=> '`j`.`job_number`',
						'dt' 		=> 1,
						'ds'		=> 1,
						'orderable' => 'true',
					'cast'	=>	'unsigned',
						'field'		=> 'job_number',
				),
				array(
						'db' => '`j`.`name`',
						'dt' => 2,
						'ds' => 2,
						'field'		=> 'job_name',
						'as'		=>	'job_name'
				),
				array(
						'db' => '`a`.`name`',
						'dt' => 3 ,
						'ds' => 3 ,
						'field'		=> 'account_name',
						'as'		=>	'account_name'
				),
				array(
						'db' => '`j`.`status`',
						'dt' => 4 ,
						'ds' => 4 ,
						'field' => 'status',
						'formatter' => function( $d ) {
							return ucwords($d);
						}
				),
				array(
						'db' => '`j`.`updated_at`',
						'dt' => 5 ,
						'ds' => 5 ,
						'field'	=> 'updated_at',
					'orderable' => true,
					'order'		=> 'DESC',
						'formatter' => function( $d ) {
							return date('m/d/Y',strtotime($d));
						}
				)
		);

		// SQL server connection information
		$sql_details = array(
				'user' => $_ENV['DBUSERNAME'],
				'pass' => $_ENV['PASSWD'],
				'db'   => $_ENV['DBNAME'],
				'host' => $_ENV['HOST']
		);

		$joinQuery =  "FROM `{$table}` AS `j` LEFT JOIN `accounts` AS `a` ON (`a`.`id` = `j`.`account`)";

		// Since we're searching trashed jobs, this will filter them on render of DataTables
		$trashFilter = '`j`.`deleted_at` is null';

		return Response::json( \SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $trashFilter ) );
	}

	/**
	 * Get Jobs Input AJAX
	 *
	 * @param null $q
	 * @return mixed
     */
	public function viewJobsInputAJAX($q = null)
	{
		$_results = $jobs = Job::where('name', 'like', '%'.$q.'%')->orWhere('job_number', '=', $q)->where('deleted_at', null)->orderBy('job_number', 'desc')->get();
		$returned_results = [];

		foreach ($_results as $result) {
			$returned_results[] = ["title" =>$result->name,"description" => "# ".$result->job_number,"job_id" => $result->id];
		}

		return Response::json([ 'results' => $returned_results]);

		die();
	}
	/**
	 * Send User from Input AJAX Job Serach to View A Job (Job AJAX Search Located in the header)
	 *
	 * @return view
     */
	public function goToJob()
	{
		$job_id = Input::get('job_id');

		return Redirect::route('view.job',$job_id);
	}

	/**
	 * Get a job
	 *
	 * @param $job_id
	 * @param $type
	 *
	 * @return mixed
     */
	public function viewJob($job_id, $type = 'job_id')
	{
		$fromImport = Input::get('fromImport');

		if ( $type == 'job_number' ) {
			$job = Job::where('job_number', $job_id)->first();
			return Redirect::route('view.job', $job->getAttribute('id'));
		}
		// Get Job
		$job = Job::find($job_id);

		// Get Bid
		$bid = $job->getBid();

		//If Bid Exists on the Job Get The Bid Line Items
		if ($bid) {
			$bid_line_items = JobBid::find($bid->id)->getLineItems();

			// Attach Products to Line Items
			foreach ($bid_line_items as $bid_line_item) {
				$bid_line_item->items_input = BidFieldItem::getBidFieldItemsInputOptions($bid_line_item->category_id);
			}
		}
		//If Not Signal the View that there is not any Bid Items Attached to the Job
		else{
			$bid_line_items = false;
		}
		$lead_sources = ['' => 'Select a Lead Source']+ App\Models\Lead::lists('lead', 'lead');

		$lead_sources = array_merge($lead_sources, [$job->getAttribute('lead_source') => $job->getAttribute('lead_source')]);
		$data = [
				'estimators'              => Job::getEstimatorsSelect(),
				'project_managers'        => Job::getProjectManagersSelect(),
				'foremen'                 => Job::getForemenSelect(),
				'salesman'                => Job::getSalesmanSelect(),
				'accountants'             => Job::getAccountingSelect(),
				'company'                 => Company::find(1),
				'custom_fields_dropdowns' => Job::getCustomDropOptions(),
				'contacts_dropdowns'      => ['' => 'Select a Primary Contact'] + Contact::getContactsInputOptions($job->account,[$job->primary_contact]),
				'accounts_dropdowns'      => Account::getAccountsInputOptions(),
				'account_name'            => Account::find($job->account)->getName(),
				'account'            	  => Account::find($job->account),
				'contacts'                => Contact::getContactsInAccount($job->account),
				'availible_contacts'      => Contact::getAvailibleContactsInAccount($job->account,$job_id),
				'job_contacts'            => Job::find($job_id)->getActiveJobContacts(),
				'job_contacts_dropdown'   => Job::find($job_id)->getActiveJobContactsSelect(),
				'inactive_job_contacts'   => Job::find($job_id)->getInactiveJobContacts(),
				'lead_sources'			  => $lead_sources,
				'job'                     => $job,
				'job_id'                  => $job->id,
				'invoice_number' 		  => Company::find(1)->next_invoice_number,
				'bid_fields'              => empty(BidField::getBidFieldsInputOptions()) ? [] : BidField::getBidFieldsInputOptions(),
				'bid'                     => $bid,
				'bid_line_items'          => $bid_line_items,
				'invoices'          	  => Job::find($job_id)->getInvoices(),
				'rework_items'            => Job::find($job_id)->getRework(),
				'tax_rates_select' 		  => TaxRate::getTaxRateSelect(),
				'activities_select'       => !empty(Activity::getActivitiesSelect()) ? Activity::getActivitiesSelect() : [],
				'job_notes'            	  => Job::find($job_id)->getNotes(),
				'hidden_notes'   		  => Job::find($job_id)->getHiddenNotes(),
				'jobs_select' 		 	  => Job::getJobsSelect(),
				'users' 				  => User::getUsersSelect(),
				'next_rework_number' 	  => JobRework::getNextReworkNumber($job_id),
				'default_comments'		  =>  ['' => 'Select a Comment', 'custom' => 'Enter Your Own Comment'] + App\Models\Comment::lists('comment', 'comment'),
				'fromImport'			  => $fromImport
		];

		return View::make('admin.job.edit-job',$data);
	}

	/**
	 * Add Job
	 *
	 * @return View
	 */
	public function viewAddJob()
	{

		$company = Company::find(1);

		$data = [
				'estimators'                 => Job::getEstimatorsSelect(),
				'project_managers'           => Job::getProjectManagersSelect(),
				'foremen'                    => Job::getForemenSelect(),
				'salesman'                   => Job::getSalesmanSelect(),
				'accountants'             	 => Job::getAccountingSelect(),
				'company'                    => Company::find(1),
				'custom_fields_dropdowns'    => Job::getCustomDropOptions(),
				'contacts_dropdowns' 		 => ['' => 'Select a Primary Contact'] + Contact::getContactsInputOptions(),
				'accounts_dropdowns'		 => Account::getAccountsInputOptions(),
				'contacts' 					 => NULL,
				'lead_sources'			  	 => ['' => 'Select a Lead Source']+ App\Models\Lead::lists('lead', 'lead'),
				'job_id' 					 => $company->next_job_number,
				'bid_fields' 				 => BidField::getBidFieldsInputOptions(),
				'bid'                     	 => false,
				'bid_line_items'             => false
		];

		return View::make('admin.job.add-job',$data);
	}

	/**
	 * Create Job
	 *
	 * @return View
	 */
	public function createJob()
	{
		$job_number = \App\Models\Job::where('job_number', Input::get('job_number'))->count() ? Company::find(1)->getAttribute('next_job_number')+1 : Input::get('job_number');
		/** Check if the job number is valid */
		while ( \App\Models\Job::where('job_number', $job_number)->withTrashed()->count() ) {
			$job_number++;
		}

		// Save Posted Data to Job Table
		$job = array(
				'name'                 => Input::get('name'),
				'status'               => Input::get('status'),
				'job_number'           => $job_number,
				'company' 			   => 1,
				'account' 			   => Input::get('assigned_job_account'),
				'contacts' 	   		   => Input::get('job_contacts'),
				'bid_date'             => ( Input::get('bid_date') ) ? date('Y-m-d',strtotime(Input::get('bid_date'))) : NULL,
				'completed_date' 	   => ( Input::get('completed_date') ) ? date('Y-m-d',strtotime(Input::get('completed_date'))) : NULL,
				'prime_contract'       => Input::get('prime_contract'),
				'salesman'             => Input::get('salesman'),
				'accounting'           => Input::get('accounting'),
				'estimator'            => Input::get('estimator'),
				'project_manager'      => Input::get('project_manager'),
				'foreman'              => Input::get('foreman'),
				'lead_source'          => Input::get('lead_source'),
				'custom_drop_1'        => Input::get('custom_drop_1'),
				'custom_drop_2'        => Input::get('custom_drop_2'),
				'custom_drop_3'        => Input::get('custom_drop_3'),
				'custom_text_1'        => Input::get('custom_text_1'),
				'custom_text_2'        => Input::get('custom_text_2'),
				'custom_text_3'        => Input::get('custom_text_3'),
				'site_address_1'       => Input::get('site_address_1'),
				'site_address_2'       => Input::get('site_address_2'),
				'site_city'            => Input::get('site_city'),
				'site_state'           => Input::get('site_state'),
				'site_zip'             => strlen(Input::get('site_zip')) ? Input::get('site_zip') : NULL,
				'billing_address_1'    => Input::get('billing_address_1'),
				'billing_address_2'    => Input::get('billing_address_2'),
				'billing_city'         => Input::get('billing_city'),
				'billing_state'        => Input::get('billing_state'),
				'billing_zip'          => strlen(Input::get('billing_zip')) ? Input::get('billing_zip') : NULL,
				'primary_contact'	   => Input::get('primary_contact')
		);

		try {
			$job = Job::create($job);

		} catch ( \Exception $e ) {
			return Redirect::back()->withErrors($e->getMessage())->withInput();
		}

		//If There Is A New Account Setup Creation of Account and Primary Contact Then Assign To Job
		if (Input::get('new_account')) {

			// Create Account
			$new_account = array(
					'company_id' => 1,
					'name'       => Input::get('new_account_name'),
					'email'       => Input::get('new_account_email'),
					'phone'      => Input::get('new_account_phone'),
					'mobile'     => Input::get('new_account_mobile'),
					'fax'        => Input::get('new_account_fax'),
					'address_1'  => Input::get('new_account_address_1'),
					'address_2'  => Input::get('new_account_address_2'),
					'city'       => Input::get('new_account_city'),
					'state'      => Input::get('new_account_state'),
					'zip'        => strlen(Input::get('new_account_zip')) ? Input::get('new_account_zip') : NULL,
					'status'     => 'active'
			);
			try {
				$account = Account::create($new_account);
			} catch ( \Exception $e ) {
				return Redirect::back()->withErrors($e->getMessage());
			}

			// Create Contact
			$new_contact = array(
					'company_id' => 1,
					'account_id' => $account->id,
					'title'      => Input::get('new_contact_title'),
					'first_name' => Input::get('new_contact_first_name'),
					'last_name'  => Input::get('new_contact_last_name'),
					'email'      => Input::get('new_contact_email'),
					'phone'      => Input::get('new_contact_phone'),
					'mobile'     => Input::get('new_contact_mobile'),
					'fax'        => Input::get('new_contact_fax'),
					'address_1'  => Input::get('new_contact_address_1'),
					'address_2'  => Input::get('new_contact_address_2'),
					'city'       => Input::get('new_contact_city'),
					'state'      => Input::get('new_contact_state'),
					'zip'        => strlen(Input::get('new_contact_zip')) ? Input::get('new_contact_zip') : NULL,
					'status'     => 'active'
			);
			try {
				$contact = Contact::create($new_contact);
			} catch ( \Exception $e ) {
				return Redirect::back()->withErrors($e->getMessage());
			}

			// Assign Primary Contact and Account to Job
			if ($job) {

				$job->account             = $account->id;
				$account->primary_contact = $contact->id;

				try {
					$job->save();
					$account->save();
				} catch ( \Exception $e ) {
					return Redirect::back()->withErrors($e->getMessage());
				}
			}

			//Create Job Contact Entry
			$job_contact = array(
					'job_id'     => $job->id,
					'account_id' => $account->id,
					'contact_id' => $contact->id,
					'status'     => 'active'
			);

			try {
				JobContact::create($job_contact);
			} catch ( \Exception $e ) {
				return Redirect::back()->withErrors($e->getMessage());
			}
		}

		//Since There Is Not A New Account Use Existing and Attach Contacts to the Job
		else{
			//Store Job Contacts
			$job_contacts = explode(',', Input::get('job_contacts'));

			// Run through each contact by ID and create a Job Contact
			foreach ($job_contacts as $job_contact) {

				$job_contact = array(
						'job_id'     => $job->id,
						'account_id' => $job->account,
						'contact_id' => $job_contact,
						'status'     => 'active'
				);

				// Create New Job Contact
				try {
					JobContact::create($job_contact);
				} catch ( \Exception $e ) {
					return Redirect::back()->withErrors($e->getMessage());
				}
			}

			// Assign Primary Contact To Job by The Primary Contact on the Job's Account
			if ($job) {

				// Create the Primary Contact
				$job->primary_contact = Account::find($job->account)->primary_contact;

				try {
					$job->save();
				} catch ( \Exception $e ) {
					return Redirect::back()->withErrors($e->getMessage());
				}

				// Add Primary Contact to the Job
				$job_contact = array(
						'job_id'     => $job->id,
						'account_id' => $job->account,
						'contact_id' => $job->primary_contact,
						'status'     => 'active'
				);

				try {
					JobContact::create($job_contact);
				} catch ( \Exception $e ) {
					return Redirect::back()->withErrors($e->getMessage());
				}
			}

		}

		// ----- Update Next Job Number -----

		// Get Company
		$company = Company::find(1);
		$last_job_id = $job->job_number;

		//Set Job Number (+1 More than the Job just created)
		$company->next_job_number   = $last_job_id+1;

		//Save Company Setting
		try {
			$company->save();
		} catch ( \Exception $e ) {
			return Redirect::back()->withErrors($e->getMessage());
		}

		// Success!! Open New Job in Edit Screen
		return Redirect::route('view.job',$job->id)->with('success', Lang::get('company.job_create'));
	}


	/**
	 * Update an existing job
	 *
	 * @param $job_id
	 * @return mixed
     */
	public function updateJob($job_id)
	{
		$job = Job::find($job_id);

		//Ensure Job Exists
		if ($job) {

			// If The Job Status is Updated Create a Note
			if ( Input::get('status') != $job->status ) {
				// Setup Job Note Array
				$job_note = array(
						'job_id'          => $job->id,
						'activity'        => 'Job Status Updated To '.ucwords(Input::get('status')),
						'type'            => 'system',
						'note'            => NULL,
						'job_contacts_id' => NULL
				);
				// Create Job Note
				try {
					$job_note = JobNote::create($job_note);
				} catch ( \Exception $e ) {
					return Redirect::back()->withErrors($e);
				}
			}

			// ---------------- Update Job Table ----------------
			$job->name                = Input::get('name');
			$job->status              = Input::get('status');
			$job->job_number          = Input::get('job_number');
			$job->company             = 1;
			$job->account             = Input::get('assigned_job_account');
			$job->primary_contact     = Input::get('primary_contact');
			$job->contacts            = Input::get('job_contacts');
			$job->bid_date            = (Input::get('bid_date')) ? date('Y-m-d',strtotime(Input::get('bid_date'))) : NULL;
			$job->completed_date      = (Input::get('completed_date')) ? date('Y-m-d',strtotime(Input::get('completed_date'))) : NULL;
			$job->estimator_deadline  = (Input::get('estimator_deadline')) ? date('Y-m-d',strtotime(Input::get('estimator_deadline'))) : NULL;
			$job->production_deadline = (Input::get('production_deadline')) ? date('Y-m-d',strtotime(Input::get('production_deadline'))) : NULL;
			$job->estimator_priority  = Input::get('estimator_priority');
			$job->production_priority = Input::get('production_priority');
			$job->prime_contract      = Input::get('prime_contract');
			$job->salesman            = Input::get('salesman');
			$job->estimator           = Input::get('estimator');
			$job->project_manager     = Input::get('project_manager');
			$job->foreman             = Input::get('foreman');
			$job->accounting          = Input::get('accounting');
			$job->lead_source         = Input::get('lead_source');
			$job->custom_drop_1       = Input::get('custom_drop_1');
			$job->custom_drop_2       = Input::get('custom_drop_2');
			$job->custom_drop_3       = Input::get('custom_drop_3');
			$job->custom_text_1       = Input::get('custom_text_1');
			$job->custom_text_2       = Input::get('custom_text_2');
			$job->custom_text_3       = Input::get('custom_text_3');
			$job->site_address_1      = Input::get('site_address_1');
			$job->site_address_2      = Input::get('site_address_2');
			$job->site_city           = Input::get('site_city');
			$job->site_state          = Input::get('site_state');
			$job->site_zip            = Input::get('site_zip');
			$job->billing_address_1   = Input::get('billing_address_1');
			$job->billing_address_2   = Input::get('billing_address_2');
			$job->billing_city        = Input::get('billing_city');
			$job->billing_state       = Input::get('billing_state');
			$job->billing_zip         = Input::get('billing_zip');


			try {
				$job->save();
			} catch ( \Exception $e ) {
				return Redirect::back()->withErrors($e->getMessage());
			}

			// If Job has a Bid Update its Bid Date in the job_bids table to match the job's bid_date
			if ($job_bid = $job->getBid()) {
				// $job_bid->bid_date = date('Y-m-d',strtotime(Input::get('bid_date')));

				//Update Bid with the Jobs Corresponding Bid Date
				try {
					$job_bid->save();
				} catch ( \Exception $e ) {
					return Redirect::back()->withErrors($e->getMessage());
				}
			}

			// All Data is Good and Updated Retrun with a Success
			return Redirect::back()->with('success', Lang::get('company.job_update'));

		}
		else{
			return Redirect::back()->withErrors(Lang::get('company.no_job'));
		}

	}


    /**
     * Update Job Bid
     *
     * @param int $job_id
     *
     * @return mixed
     */
    public function updateJobBid($job_id)
    {
        // echo '<pre>';
        // dd(\Input::all());
        $bid = Job::find($job_id)->getBid();

        $bid_id = $bid ? $bid->id : false;
        $tax_description = NULL;

        // Setup Tax Description
        if ( TaxRate::find(Input::get('tax_description')) ) {
            $tax_description = TaxRate::find(Input::get('tax_description'))->tax_description;
        }
        elseif (Input::get('tax_description')) {
            $tax_description = Input::get('tax_description');
        }
        //If Bid ID Does Not Exists Create a New Bid
        if (!$bid_id) {
            // Setup Bid Array
            $bid = array(
                'job_id'          => $job_id,
                'status'          => 'open',
                'subtotal'        => Input::get('bid_sub_total'),
                'grand_total'     => Input::get('bid_grand_total'),
                'tax_rate'        => Input::get('bid_tax_amount'),
                'tax_description' => $tax_description,
                'sales_tax_total' => Input::get('sales_tax_total')
            );

            // Create New Bid
            try {
                $bid = JobBid::create($bid);
            } catch ( \Exception $e ) {
                $data = array(
                    'detail' => 'error',
                    'message' => $e->getMessage()
                );
                return $data;
//				return Redirect::back()->withErrors($e->getMessage());
            }
            //Set Bid Id To Be Passed to New Line Items
            $bid_id = $bid->id;
        }
        // If Bid Does Exist Already Update the Bid
        else{
            // Get Bid
            $job_bid = JobBid::find($bid_id);

            // Setup Totals
            $job_bid->subtotal        = Input::get('bid_sub_total');
            $job_bid->grand_total     = Input::get('bid_grand_total');
            $job_bid->tax_rate        = Input::get('bid_tax_amount');
            $job_bid->tax_description = $tax_description;
            $job_bid->sales_tax_total = Input::get('sales_tax_total');

            //Update Bid with New Totals
            try {
                $job_bid->save();
            } catch ( \Exception $e ) {
                $data = array(
                    'detail' => 'error',
                    'message' => $e->getMessage()
                );
                return $data;
//				return Redirect::back()->withErrors($e->getMessage());
            }
        }


        // ---------------- Setup Line Items  ----------------

        if ( $remove_line_items = Input::get('remove_line_item') ) {
            foreach ( $remove_line_items as $key => $line_item_id ) {
                if ( $line_item = JobBidLineItems::find($line_item_id) ) {
                    try {
                        $line_item->delete();
                    } catch ( \Exception $e ) {
                        // Continue
                    }
                }
            }
        }
        // Get Categories to Build off Of (Every Line Item Should At the least have this)
        $categories = Input::get('category');


        // Ensure New Line Items Exist in POST
        if ($categories) {

            //Setup Arrays
            $products      = Input::get('product');
            $quantities    = Input::get('item_quantity');
            $units         = Input::get('item_units');
            $rates         = Input::get('item_rate');
            $amounts       = Input::get('item_amount');
            $line_item_ids = Input::get('line_item_id');
            $taxes = Input::get('is_taxable');


            // Get Bid
            //$job_bid = JobBid::find($bid_id);
            $tax_rate = Input::get('bid_tax_amount');

            // Set Sort Order
            $sort_order = 1;

            // Iterate Through each Field POSTED
            foreach($categories as $key => $val) {

                $products = is_array($products) ? $products : [$key => $products];

                // -- Store Category,Product,Line Item IDs --
                // if ( array_key_exists($key, $products) ) {


                $category_id = $categories[$key];
                $product_id = array_key_exists($key, $products) ? $products[$key] : 0;
                $line_item_id = array_key_exists($key, $line_item_ids) ? $line_item_ids[$key] : false;

                if ($line_item_id == 0) {
                    $line_item_id = false;
                }

                // Check If a Line Item Already Exists
                if ($line_item_id && $line_item = JobBidLineItems::find($line_item_id)) {
                    // Get Line Item

                    // Setup Object
                    $line_item->bid_id = $bid_id;
                    $line_item->category_id = (BidField::find($category_id)) ? $category_id : 0;
                    $line_item->category = (BidField::find($category_id)) ? BidField::find($category_id)->name : $categories[$key];
                    $line_item->product_id = (BidFieldItem::find($product_id)) ? $product_id : 0;
                    if ( array_key_exists($key, $products) ) {
                        $line_item->product = (BidFieldItem::find($product_id)) ? BidFieldItem::find($products[$key])->description : $products[$key];
                    }
                    $line_item->quantity = round($quantities[$key]);
                    $line_item->rate = round($rates[$key], 2);
                    $line_item->unit = $units[$key];

                    // if is_taxable = 1, we need to calculate taxes based on Sales Tax Rate
                    // amount * tax rate / 100
                    //$line_item->amount = $amounts[$key];
                    $line_item->amount = round($line_item->quantity * $line_item->rate, 2);
                    if($taxes[$key] == 1 && $tax_rate != 0 && $tax_rate !== NULL) {
//                        $line_item->amount = (($amounts[$key] * $tax_rate) / 100) + $amounts[$key];
                    }

                    $line_item->taxable = $taxes[$key];
                    $line_item->sort_order = $sort_order;

                    //Update Line Item
                    try {
                        $line_item->save();
                    } catch (\Exception $e) {
                        $data = array(
                            'detail' => 'error',
                            'message' => $e->getMessage()
                        );
                        return $data;
//							return Redirect::back()->withErrors($e->getMessage());
                    }

                } // If It Doesn't Exists Create New Line Item(s)
                else {

                    $calculated_amount = 0;
                    // if is_taxable = 1, we need to calculate taxes based on Sales Tax Rate
                    // amount * tax rate / 100
					$amounts[$key] = round((round($quantities[$key]) * $rates[$key]), 2);
                    if($taxes[$key] == 1 && $tax_rate != 0 && $tax_rate !== NULL)
                    {
//                        $calculated_amount = (($amounts[$key] * $tax_rate) / 100) + $amounts[$key];
						$calculated_amount = $amounts[$key];
					}
                    else {
                        $calculated_amount = $amounts[$key];
                    }

                    // Setup New Line Items Array
                    $new_line_items = array(
                        'bid_id' => $bid_id,
                        'category_id' => $category_id,
                        'category' => (BidField::find($category_id)) ? BidField::find($category_id)->name : $categories[$key],
                        'product_id' => $product_id,
                        'product' => (BidFieldItem::find($products[$key])) ? BidFieldItem::find($products[$key])->description : $products[$key],
                        'quantity' => round($quantities[$key]),
                        'rate' => round($rates[$key], 2),
                        'unit' => $units[$key],
                        'amount' => $calculated_amount,
                        'taxable' => $taxes[$key],
                        'sort_order' => $sort_order
                    );

                    // Create New Line Items
                    try {
                        JobBidLineItems::create($new_line_items);
                    } catch (\Exception $e) {
                        $data = array(
                            'detail' => 'error',
                            'message' => $e->getMessage()
                        );
                        return $data;
//							return Redirect::back()->withErrors($e->getMessage());
                    }
                }
            }

            //Increment Sort Order
            $sort_order++;
        }
        // }

        // ---------------- Delete Line Items ----------------
//		die();
        // Get Deleted Line Items
        $delete_line_items = Input::get('delete_line_item');

        // Check if Deleted Calues Exsist
        if ($delete_line_items && isset($job_bid)) {

            foreach ($delete_line_items as $delete_line_item) {
                // Get Line Item by Id and Check If it Exists
                $line_item = JobBidLineItems::where('id', $delete_line_item)->where('bid_id', $job_bid->getKey())->first();

                if ( $line_item ) {
                    try {
                        $line_item->delete();
                    } catch ( \Exception $e ) {
                        $data = array(
                            'detail' => 'error',
                            'message' => $e->getMessage()
                        );
                        return $data;
//						return Redirect::back()->withErrors($e->getMessage());
                    }
                }
            }
        }

        if ( isset($job_bid) ) {
            $job_bid->recalculateTotals();
        }
        //Check if the Request is coming from AJAX or Not
        if (Request::ajax()){
            $response = array(
                'status'       => 'success',
                'message'      => 'Job Bid Saved Successfully',
                'bid_id'       => $bid_id,
                'redirect_url' => route('view.job', $job_id),
            );

//			Session::flash('success', Lang::get('company.job_bid_saved'));
            $data = array(
                'detail' => 'success',
                'message' => Lang::get('company.job_bid_saved')
            );
            return $data;

//			return Response::json( $response );
        }
        // If not AJAX use Redirect
        else{
            $data = array(
                'detail' => 'success',
                'message' => Lang::get('company.job_bid_saved')
            );
            return $data;
//			return Redirect::back()->with('success', Lang::get('company.job_bid_saved'))->with('pop','bid-fields');
        }

    }


	/**
	 * Attach Contacts to a Job
	 *
	 * @return View
	 */
	public function attachContactsToJob($job_id,$account_id)
	{

		//Get Contact Ids
		$job_contacts = Input::get('attach_contact');

		//If Contacts Exists
		if ($job_contacts) {

			foreach ($job_contacts as $job_contact) {

				//Check if Job Contact Exists
				$existing_job_contact = JobContact::withTrashed()->where('job_id','=',$job_id)->where('contact_id','=',$job_contact)->first();

				// If Job Contact Exists Restore Job Contact
				if ($existing_job_contact) {

					try {
						$existing_job_contact->restore();
					} catch ( \Exception $e ) {
						return Redirect::back()->withErrors($e->getMessage());
					}

				}
				// If it doesn't exists create a new job contact record
				else{
					//Create Job Contact Entry
					$new_job_contact = array(
							'job_id'     => $job_id,
							'account_id' => $account_id,
							'contact_id' => $job_contact,
							'status'     => 'active'
					);

					//Create Job Contact
					try {
						JobContact::create($new_job_contact);
					} catch ( \Exception $e ) {
						return Redirect::back()->withErrors($e->getMessage());
					}
				}
			}
			// Success!!
			return Redirect::back()->with('success', Lang::get('company.job_contacts_added'))->with('pop','view-contacts');
		}
	}

	/**
	 * Remove Contacts On a Job
	 *
	 * @return View
	 */
	public function removeContactsOnJob()
	{

		//Get Contact Ids
		$job_contact_ids = Input::get('remove_job_contact');

		//If Contacts Exists
		if ($job_contact_ids) {

			// Run Through Each Job Contact ID
			foreach ($job_contact_ids as $job_contact_id) {

				// Find Job Contact
				$job_contact = JobContact::find($job_contact_id);

				//Soft Delete Job Contact
				try {
					$job_contact->delete();
				} catch ( \Exception $e ) {
					return Redirect::back()->withErrors($e->getMessage());
				}
			}

			// Success!!
			return Redirect::back()->with('success', Lang::get('company.job_contacts_removed'));
		}

		// Job Contact Not Selected
		return Redirect::back()->withErrors(Lang::get('company.job_contacts_removed'));
	}


	/**
	 * Restore Contacts
	 *
	 * @return View
	 */
	public function restoreContactsOnJob()
	{

		// Get Accounts
		$restore_contacts = Input::get('restore_job_contact');

		foreach ($restore_contacts as $contact) {

			// Restore Contacts
			try {
				JobContact::withTrashed()->where('id', $contact)->restore();
			} catch ( \Exception $e ) {
				return Redirect::back()->withErrors($e->getMessage());
			}
		}

		return Redirect::back()->with('success', Lang::get('company.contact_restored'));
	}

	/**
	 * Get Tax Rate For a Job
	 *
	 * @return View
	 */
	public function getTaxRate($tax_rate_id)
	{

		return TaxRate::find($tax_rate_id)->getRate();
	}


	/**
	 * Add Rework to Job
	 *
	 * @return View
	 */
	public function deleteJob($job_id)
	{
		$job = Job::find($job_id);

		//Ensure Job Exists
		if ($job) {
			try {
				$job->delete();
				return Redirect::route('view.jobs')->with('success', Lang::get('company.job_deleted', array('job_name' => $job->name)));
			} catch ( \Exception $e ) {
				return Redirect::back()->withErrors($e);
			}
		}
	}
}
