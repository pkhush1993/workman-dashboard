<?php
/*
  * This file is part of the  App\Controllers library.
 *
  * (c) Michael Kyle Martin <michael@tinyrocket.co>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace App\Controllers;

use App\Models\Job;
use App\Models\JobBid;
use App\Models\JobBidLineItems;
use Illuminate\Http\Request;

class ImportController extends \BaseController
{

    /**
     * @var \Illuminate\Http\Request $request
     */
    protected $request;

    /**
     * @var \App\Models\Job $job
     */
    protected $job;

    /**
     * @var \App\Models\Job $line
     */
    protected $line;

    /**
     * @var \App\Models\Job $bid
     */
    protected $bid;

    /**
     * ImportController constructor.
     *
     * @param Request $request
     * @param Job $job
     * @param JobBidLineItems $line
     * @param JobBid $bid
     */
    public function __construct(Request $request, Job $job, JobBidLineItems $line, JobBid $bid)
    {
        $this->request = $request;
        $this->job = $job;
        $this->line = $line;
        $this->bid = $bid;
    }


    /**
     * Import CSV Action for Bid Items
     *
     * @param int $jobId
     * @return mixed
     */
    public function bidCsvImport($jobId)
    {
        $this->job = $this->job->find($jobId);

        if ( $this->request->isMethod('post') )
        {
            try {
                $this->parseImport();

                return \Response::json([
                    'url' => route('view.job', ['job_id' => $jobId]),
                    'data' => $this->request->input()
                ]);
            } catch ( \Exception $e ) {
                return \Response::json(['message' => $e->getMessage(), 'data' => $this->request->input()]);
            }
        }

        return view('admin.import.job.bidImporter', [
            'job'   => $this->job,
        ]);
    }

    /**
     * Parse Import Post
     *
     * @return void
     */
    protected function parseImport()
    {
        if ( !$this->job->getBid() )
        {
            $this->createBid();
        }

        $this->parseLineItems();

        $this->job->getBid()->recalculateTotals();
    }

    /**
     * Create a new bid and associate it with the current job
     *
     * @return void
     */
    protected function createBid()
    {
        $this->bid->create([
            'job_id'          => $this->job->getKey(),
            'status'          => 'open',
            'subtotal'        => 0,
            'grand_total'     => 0,
            'tax_rate'        => 0,
            'tax_description' => '',
            'sales_tax_total' => 0
        ]);

    }

    /**
     * Parse individual line items and create respective bid lines
     *
     * @return void
     */
    protected function parseLineItems()
    {
        foreach ( current($this->request->input()) as $line )
        {
            if ( strlen($line[0]) )
            {
                $quantity = preg_replace('@[^0-9a-z\.]+@i', '', $line[2]);
                $rate = preg_replace('@[^0-9a-z\.]+@i', '',  $line[4]);
                $this->line->create([
                    'bid_id'      => $this->job->getBid()->getKey(),
                    'category_id' => 0,
                    'category'    => $line[0],
                    'product_id'  => 0,
                    'product'     => $line[1],
                    'quantity'    =>  $quantity,
                    'rate'        => $rate,
                    'unit'        => $line[3],
                    'amount'      => ((float)$quantity) * ((float)$rate),
                    'taxable'     => 0,
                    'sort_order'  => 0,
                ]);
            }
        }
    }
}