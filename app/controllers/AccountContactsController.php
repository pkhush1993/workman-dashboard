<?php namespace App\Controllers;
/**
*	Copyright 2014 CoolBlueWeb
*
*	Licensed under the Apache License, Version 2.0 (the "License");
*	you may not use this file except in compliance with the License.
*	You may obtain a copy of the License at
*
*			http://www.apache.org/licenses/LICENSE-2.0
*
*	Unless required by applicable law or agreed to in writing, software
*	distributed under the License is distributed on an "AS IS" BASIS,
*	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*	See the License for the specific language governing permissions and
*	limitations under the License.
*
*	@package 	Laravel
*	@subpackage CoolBlueWeb Custom
*	@copyright  2014 CoolBlueWeb
*	@author 	CoolBlueWeb <bryan@coolblueweb.com>
*/

use BaseController, Sentinel, View, Input, Response, Redirect, Session, Mail, Lang, Config,App,Validator;
use App\Models\Account;
use App\Models\Contact;
use App\Models\JobContact;

class AccountContactsController extends BaseController {

	/**
	 * @var array
     */
	protected $sql_details;

	/**
	 * AccountContactsController constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		$this->sql_details = [
			'user' => $_ENV['DBUSERNAME'],
			'pass' => $_ENV['PASSWD'],
			'db'   => $_ENV['DBNAME'],
			'host' => $_ENV['HOST']
		];

	}

	/**
	 * View Contacts
	 *
	 * @return View
	 */
	public function viewContacts()
	{

		$data = ['archive' => false];

		return View::make('admin.contact.contacts',$data);
	}

	/**
	 * View Archived Contacts
	 *
	 * @return View
	 */
	public function viewArchivedContacts()
	{
		$data = [
			'contacts' => Contact::onlyTrashed()->get(),
			'archive' => true
		];

		// Check if Archived Contacts Exist
		if (!$data['contacts']->isEmpty()) {

			// Attach Account Name to Contacts
			foreach ($data['contacts'] as $contact){

				$contact->account_name =  Account::withTrashed()->where('id', $contact->account_id)->first()->getName();

			}

			return View::make('admin.contact.archived',$data);
		}

		else{
			return Redirect::route('company.contacts')->withErrors(Lang::get('company.no_archived_contacts'));
		}
	}


	/**
	 * View Contact
	 *
	 * @param int $contact_id
	 *
	 * @return mixed
     */
    public function viewContact($contact_id)
	{
		$contact = Contact::find($contact_id);
		$accounts_dropdowns = Account::getAccountsInputOptions();

		return View::make('admin.contact.contact',compact(['contact','contact_id','accounts_dropdowns']));
	}

	/**
	 * View Contact - Job
	 *
	 * @param int $contact_id
	 *
	 * @return View
	 */
	public function viewJobContact($contact_id)
	{

		$data = [
			'accounts_dropdowns' => Account::getAccountsInputOptions(),
			'contact_id'         => $contact_id,
			'contact'            => Contact::withTrashed()->where('id', $contact_id)->first()
		];

		return View::make('admin.contact.forms.contact',$data);
	}


	/**
	 * View Contacts From Chosen Account - Add/Edit Job
	 *
	 * @param int $account_id
	 * @param int $job_id
	 *
	 * @return View
	 */
	public function viewAccountContacts($account_id,$job_id = NULL)
	{
		// Retrieve Contacts in Specified Account for Existing Job
		if ($job_id) {
			$data['contacts'] = Contact::getAvailibleContactsInAccount($account_id,$job_id);
		}
		// Retrieve Contacts in Specified Account for New Job
		else{
			$data['contacts'] = Contact::getContactsInAccount($account_id);
		}
		$data['archive']	=	false;

		// Attach Account Name to Contacts
		foreach ($data['contacts'] as $contact){

			$contact->account_name =  Account::find($contact->account_id)->getName();

		}
		$data['account_id'] = $account_id;
		//Render View with Contacts Retrieved
		return View::make('admin.contact.contacts',$data);
	}

	/**
	 * View Contacts From Chosen Account - Add/Edit Job
	 *
	 * @param int $account_id
	 * @param int $job_id
	 *
	 * @return View
	 */
	public function viewJobAccountContacts($account_id,$job_id = NULL)
	{
		// Retrieve Contacts in Specified Account for Existing Job
		if ($job_id) {
			$data['contacts'] = Contact::getAvailibleContactsInAccount($account_id,$job_id);
		}
		// Retrieve Contacts in Specified Account for New Job
		else{
			$data['contacts'] = Contact::getContactsInAccount($account_id);
		}
		// Attach Account Name to Contacts
		foreach ($data['contacts'] as $contact){
			$contact->account_name =  Account::find($contact->account_id)->getName();
		}
		//Render View with Contacts Retrieved
		return View::make('admin.contact.forms.add-contacts',$data);
	}


	/**
	 * Get Jobs Input AJAX
	 *
	 * @param $q string
	 * @return View
	 */
	public function viewAccountsInputAJAX($q = null)
	{

		$_results = Account::where('name', 'like', '%'.$q.'%')->get();
		$returned_results = [];
		foreach ($_results as $result) {
			$returned_results[] = ["title" =>$result->name,"description" => $result->city.', '.$result->state,"account_id" => $result->id];
		}

		return Response::json([ 'results' => $returned_results]);

	    die();
	}

	/**
	 * Get Jobs Input AJAX second
	 *
	 * @param $q string
	 * @return View
	 */
	public function viewAccountsInputAJAXsecond()
	{
		$text = Input::get('key');

		$_results = Account::where('name', 'like', '%'.$text.'%')->orderBy('name', 'DESC')->get();
		$returned_results = [];
		if (!is_null($_results)){
			foreach ($_results as $result) {
				$returned_results[] = ["title" =>$result->name,"description" => $result->city.', '.$result->state,"account_id" => $result->id];
			}

			return Response::json([ 'results' => $returned_results]);
		} else {
			$Error = array(
				"error" => "Result not found"
			);
			return $Error;
		}

	}

	/**
	 * Returning searched account ID
	 *
	 * @param $q string
	 * @return View
	 */
	public function ReturnSelectedAccountId()
	{
		$text = Input::get('key');

		$_results = Account::where('name', 'like', '%'.$text.'%')->get();

		if (!is_null($_results)){
			foreach ($_results as $ResultItm) {
				if ($ResultItm->name == $text){
					$returned_results = array("title" =>$ResultItm->name,"description" => $ResultItm->city.', '.$ResultItm->state,"account_id" => $ResultItm->id);
					return $returned_results;
				}
			}

		} else {
			$Error = array(
				"error" => "Result not found",
				"account_id" => null
			);
			return $Error;
		}

	}
	/**
	 * View Contacts From Chosen Account - Job
	 *
	 * @param int $account_id
	 *
	 * @return View
	 */
	public function getAccountContacts($account_id)
	{
		// Retrive Contacts in Specified Account
		$contacts = Contact::getContactsInAccount($account_id);

		// Attach Account Name to Contacts
		foreach ($contacts as $contact){

			$contact->account_name =  Account::find($contact->account_id)->getName();

		}

		return $contacts;
	}



	/**
	 * View Add Contact
	 *
	 * @return View
	 */
	public function viewAddContact()
	{
		$data['accounts_dropdowns'] = Account::getAccountsInputOptions();
		return View::make('admin.contact.add-contact',$data);
	}

	/**
	 * Update Contact
	 *
	 * @return Save -> View Redirect
	 */
	public function updateContact()
	{

		$contact = Contact::find(Input::get('contact_id'));

		//Ensure Company Exists
		if ($contact) {

			$contact->first_name = Input::get('first_name');
			$contact->last_name = Input::get('last_name');
			$contact->title = Input::get('title');
			$contact->account_id = Input::get('account_id');
			$contact->email = Input::get('email');
			$contact->phone = Input::get('phone');
			$contact->mobile = Input::get('mobile');
			$contact->fax = Input::get('fax');
			$contact->address_1 = Input::get('address_1');
			$contact->address_2 = Input::get('address_2');
			$contact->city = Input::get('city');
			$contact->state = Input::get('state');
			$contact->zip = strlen(Input::get('zip')) ? Input::get('zip') : NULL;
			$contact->notes = Input::get('notes');

			try {
				$contact->save();
				return Redirect::back()->with('success', Lang::get('company.contact_update'));
			} catch ( \Exception $e ) {
				return Redirect::back()->withErrors($e);
			}

		}
		else{
			return Redirect::back()->withErrors(Lang::get('company.no_contact'));
		}

	}


	/**
	 * Create Contact
	 *
	 * @param int $job_id
	 *
	 * @return Save -> View Redirect
	 */
	public function createContact($job_id = NULL)
	{
		$contact = array(
			'company_id'=> Input::get('company_id'),
			'first_name'=> Input::get('first_name'),
			'last_name' => Input::get('last_name'),
			'title' 	=> Input::get('title'),
			'account_id'=> Input::get('assigned_job_account'),
			'email' 	=> Input::get('email'),
			'phone' 	=> Input::get('phone'),
			'mobile' 	=> Input::get('mobile'),
			'fax' 		=> Input::get('fax'),
			'address_1' => Input::get('address_1'),
			'address_2' => Input::get('address_2'),
			'city' 		=> Input::get('city'),
			'state' 	=> Input::get('state'),
			'zip' 		=> strlen(Input::get('zip')) ? Input::get('zip') : NULL,
			'notes' 	=> Input::get('notes'),
		);

		// Create New contact
		try {
			$contact = Contact::create($contact);
		} catch ( \Exception $e ) {
			return Redirect::back()->withErrors($e);
		}

		if ($job_id) {

			//Create Job Contact Entry
			$job_contact = array(
				'job_id'     => $job_id,
				'account_id' => Input::get('account_id'),
				'contact_id' => $contact->id,
				'status'     => 'active'
  			);

			try {
				JobContact::create($job_contact);
			} catch ( \Exception $e ) {
				return Redirect::back()->withErrors($e);
			}

			// Return Back TO Job Screen if Contact was Created from Job
			return Redirect::back()->with('success', Lang::get('company.job_contacts_added'))->with('pop','view-contacts');
		}

		return Redirect::back()->with('success', Lang::get('company.contact_created'));

	}

	/**
	 * Remove Contact
	 *
	 * @param int $contact_id
	 *
	 * @return View
	 */
	public function removeContact($contact_id)
	{
		$contact = Contact::find($contact_id);

		// Remove Contact
		try {
			$contact->delete();
			return Redirect::route('company.contacts')->with('success', Lang::get('company.contact_deleted'));
		} catch ( \Exception $e ) {
			return Redirect::back()->withErrors($e);
		}
	}

	/**
	 * Remove Contacts
	 *
	 * @return View
	 */
	public function removeContacts()
	{

		// Get Contacts
		$contacts = Input::get('selected_contact');


		foreach ($contacts as $contact) {
			// Find Contact
			$contact = Contact::find($contact);

			// Remove Contact
			try {
				$contact->delete();
			} catch ( \Exception $e ) {
				return Redirect::back()->withErrors($e);
			}
		}

		return Redirect::route('company.contacts')->with('success', Lang::get('company.contact_deleted'));
	}


	/**
	 * Restore Contacts
	 *
	 * @return View
	 */
	public function restoreContacts()
	{

		// Get Accounts
		$contacts = Input::get('selected_contact');

		foreach ($contacts as $contact) {

			// Restore Contacts
			try {
				Contact::withTrashed()->where('id', $contact)->restore();
			} catch ( \Exception $e ) {
				return Redirect::back()->withErrors($e);
			}
		}

		return Redirect::route('company.contacts')->with('success', Lang::get('company.contact_restored'));
	}


	/**
	 * View Accounts
	 *
	 * @return View
	 */
	public function viewAccounts()
	{

		$data = [
			'archive' => false
		];

		return View::make('admin.account.accounts',$data);
	}


	/**
	 * View Archived Accounts
	 *
	 * @return View
	 */
	public function viewArchivedAccounts()
	{
		$data = [
			'accounts' => Account::onlyTrashed()->get(),
			'archive' => true
		];

		// Check if Archived Accounts Exist
		if (!$data['accounts']->isEmpty()) {

			//Attach Contact Count to Account
			foreach ($data['accounts'] as $account){

				$account->contact_count =  count(Contact::getArchivedContactsInAccount($account->id));
			}

			return View::make('admin.account.archived',$data);

		}
		else{
			return Redirect::route('company.accounts')->withErrors(Lang::get('company.no_archived_accounts'));
		}


	}

	/**
	 * View Account
	 *
	 * @return View
	 */
	public function viewAccount($account_id)
	{

		$data['account'] = Account::find($account_id);
		$data['account_id'] = $account_id;
		$data['primary_contact'] = Contact::find($data['account']->primary_contact);
		$data['contacts'] = Contact::getContactsInputOptions($account_id);

		return View::make('admin.account.account',$data);
	}

	/**
	 * View New Account
	 *
	 * @return View
	 */
	public function viewAddAccount()
	{
		$data['contacts_dropdowns'] = Contact::getContactsInputOptions();
		return View::make('admin.account.add-account',$data);
	}

	/**
	 * Update Account
	 *
	 * @return Save -> View Redirect
	 */

	public function updateAccount()
	{

		$account = Account::find(Input::get('account_id'));

		// Only if Name is Updated Validate to Make Sure Account with the Same Name doesnt Exists
		if ( $account->name != Input::get('name')) {
			$validation = Validator::make( ['name' => Input::get( 'name' )], ['name' => 'unique:accounts'] );

			if ( $validation->fails() ) {
			    $errors = $validation->messages();
			    return Redirect::back()->withErrors(Lang::get('company.account_name_exists', ['name' => Input::get( 'name' )]));
			}
		}

		//Ensure Company Exists
		if ($account) {

			$account->name = Input::get('name');
			$account->primary_contact = Input::get('primary_contact');
			$account->email = Input::get('email');
			$account->phone = Input::get('phone');
			$account->mobile = Input::get('mobile');
			$account->fax = Input::get('fax');
			$account->address_1 = Input::get('address_1');
			$account->address_2 = Input::get('address_2');
			$account->city = Input::get('city');
			$account->state = Input::get('state');
			$account->zip = strlen(Input::get('zip')) ? Input::get('zip') : NULL;

			try {
				$account->save();
				return Redirect::back()->with('success', Lang::get('company.account_update'));
			} catch ( \Exception $e ) {
				return Redirect::back()->withErrors($e);
			}

		}
		else{
			return Redirect::back()->withErrors(Lang::get('company.no_account'));
		}

	}

	/**
	 * Create Account
	 *
	 * @return Save -> View Redirect
	 */

	public function createAccount()
	{

		// Validate to Make Sure Account with the Same Name doesnt Exists
		$validation = Validator::make( ['name' => Input::get( 'account_name' )], ['name' => 'unique:accounts'] );

		if ( $validation->fails() ) {
		    $errors = $validation->messages();
		    return Redirect::back()->withErrors(Lang::get('company.account_name_exists', ['name' => Input::get( 'account_name' )]));
		}

		// Create New Account
		$account = array(
			'company_id'=> Input::get('company_id'),
			'name' 		=> Input::get('account_name'),
			'email' 	=> Input::get('account_email'),
			'phone' 	=> Input::get('account_phone'),
			'mobile' 	=> Input::get('account_mobile'),
			'fax' 		=> Input::get('account_fax'),
			'address_1' => Input::get('account_address_1'),
			'address_2' => Input::get('account_address_2'),
			'city' 		=> Input::get('account_city'),
			'state' 	=> Input::get('account_state'),
			'zip' 		=> strlen(Input::get('zip')) ? Input::get('zip') : NULL
		);

		try {
			$account = Account::create($account);
		} catch ( \Exception $e ) {
			return Redirect::back()->withErrors($e);
		}


		// Check If Contact Is New
		if (Input::get('account_contact_type') == 'new_contact') {

			// Setup New Contact
			$contact = array(
				'company_id'=> Input::get('company_id'),
				'first_name'=> Input::get('contact_first_name'),
				'last_name' => Input::get('contact_last_name'),
				'title' 	=> Input::get('contact_title'),
				'account_id'=> $account->id,
				'email' 	=> Input::get('contact_email'),
				'phone' 	=> Input::get('contact_phone'),
				'mobile' 	=> Input::get('contact_mobile'),
				'fax' 		=> Input::get('contact_fax'),
				'address_1' => Input::get('contact_address_1'),
				'address_2' => Input::get('contact_address_2'),
				'city' 		=> Input::get('contact_city'),
				'state' 	=> Input::get('contact_state'),
				'zip' 		=> Input::get('contact_zip')
			);


			// Create New Contact
			try {
				$contact = Contact::create($contact);
			} catch ( \Exception $e ) {
				return Redirect::back()->withErrors($e);
			}

			// Add Contact To Account As Primary Contact
			$account->primary_contact = $contact->id;
			try {
				$account->save();
			} catch ( \Exception $e ) {
				return Redirect::back()->withErrors($e);
			}

		}
		elseif ( Input::get('account_contact_type') == 'existing_contact' ){


			$contact = Contact::find(Input::get('assigned_contact'));

			//Ensure Company Exists
			if ($contact) {

				// Add Account ID To Existing Contact
				$contact->account_id = $account->id;
				// Add Contact To Account As Primary Contact
				$account->primary_contact = $contact->id;

				// Update Contact/Account
				try {
					$contact->save();
					$account->save();
					return Redirect::back()->with('success', Lang::get('company.account_created'));
				} catch ( \Exception $e ) {
					return Redirect::back()->withErrors($e);
				}

			}
			else{
				return Redirect::back()->withErrors(Lang::get('company.no_account'));
			}


		}

		return Redirect::back()->with('success', Lang::get('company.account_created'));

	}

	/**
	 * Remove Account
	 *
	 * @param int $account_id
	 *
	 * @return View
	 */
	public function removeAccount($account_id)
	{
		try {
			// Soft Delete Account and Contacts Attached
			Account::deleteAccount($account_id);
			return Redirect::route('company.accounts')->with('success', Lang::get('company.account_deleted'));
		} catch ( \Exception $e ) {
			return Redirect::back()->withErrors($e);
		}
	}


	/**
	 * Remove Accounts
	 *
	 * @return View
	 */
	public function removeAccounts()
	{

		// Get Accounts
		$accounts = Input::get('selected_account');


		foreach ($accounts as $account) {
			// Find Account
			$account = Account::find($account);

			// Remove Account
			try {
				$account->delete();
			} catch ( \Exception $e ) {
				return Redirect::back()->withErrors($e);
			}
		}

		return Redirect::back()->with('success', Lang::get('company.account_deleted'));
	}

	/**
	 * Restore Accounts
	 *
	 * @return View
	 */
	public function restoreAccounts()
	{

		// Get Accounts
		$accounts = Input::get('selected_account');

		foreach ($accounts as $account) {

			// Restore Account
			try {
				Account::withTrashed()->where('id', $account)->restore();
			} catch ( \Exception $e ) {
				return Redirect::back()->withErrors($e);
			}
		}

		return Redirect::route('company.accounts')->with('success', Lang::get('company.account_restored'));
	}

	/**
	 * Restore Accounts
	 *
	 * @return JSON
	 */
	public function getAccountAJAX($account_id)
	{
		// Get Bid Field Items
		$account = Account::find($account_id);

		return Response::json($account);
		//return Response::json(array('address_1' => $bid_item->id, 'address_2' => $bid_item->description,'city' => $bid_item->units, 'state' => $bid_item->price,'zip' => $bid_item->price));
	}

	/**
	 * Ajax route for populating contacts data table
	 *
	 * @param mixed $account_id
	 * @return json response
     */
	public function viewContactsAjax($account_id = null)
	{
		$contacts = \DB::table('contacts')->join('accounts', 'contacts.account_id', '=', 'accounts.id')->where('contacts.deleted_at', null);

		// Filter by Account ID. jQuery passes null as a string, hence
		// the double (somewhat redundant) conditional.
		if ( !is_null($account_id) && $account_id != 'null' ) {
			$contacts->where('contacts.account_id', $account_id);
		}

		$contacts->select([
			'contacts.id',
			\DB::raw("CONCAT(contacts.first_name, ' ',contacts.last_name) as name"),
			'contacts.phone', 'contacts.email',
			\DB::raw("CONCAT(contacts.address_1, '<br>',contacts.address_2, '<br>', contacts.city,' ', contacts.state, ' ', contacts.zip) as address"),
			'accounts.name as account_name',
			'contacts.account_id'
		]);

		$table = \Datatables::of($contacts)
			->editColumn('id', function($model) {
				return "<a href='".route('company.contact', ['contact_id' => $model->id])
									."'><i class='large unhide icon view-contact'></i></a>";
			})
			->editColumn('email', function($model) {
				if ( $model->email ) {
					return \HTML::mailto($model->email, $model->email);
				}
			})
			->editColumn('account_name', function($model) {
				return link_to_route('company.account', $model->account_name, ['account_id' => $model->account_id]);
			})
			->removeColumn('contacts.account_id');
		return $table->make(true);
	}

	/**
	 * Ajax route for populating accounts data table
	 *
	 * @return mixed
     */
	public function viewAccountsAjax()
	{
		$table = 'accounts';

		$primaryKey = 'id';

		$columns = array(
				array(
						'db'        => 'id',
						'dt'        => 0,
						'ds'        => 0,
						'field'		=> 'id',
						'formatter' => function( $d) {
							return '<a href="'.route('company.account', [$d]).'">'."<i class='large unhide icon view-contact'></i>".'</a>';
						}
				),
				array(
						'db'        => 'name',
						'dt'        => 1,
						'ds'        => 1,
						'field'		=> 'name',
						'orderable' => true,
						'order'		=> 'ASC',
				),
				array(
						'db' 		=> 'phone',
						'dt' 		=> 2,
						'ds'        => 2,
						'field'		=> 'phone',
						'formatter' => function($d) {
							return '<a href="tel:'.$d.'">'.$d.'</a>';
						}
				),
				array(
						'db' => 'email',
						'dt' => 3,
						'ds'        => 3,
						'field'		=> 'email',
						'as'		=>	'email',
						'formatter' => function($d) {
							return '<a href="mailto:'.$d.'">'.$d.'</a>';
						}
				),
				array(
						'db' => 'address_1',
						'dt' => 4 ,
						'ds'        => 4,
						'field'		=> 'address_1',
						'as'		=>	'address_1'
				),
				array(
						'db' => 'id',
						'dt' => 5 ,
						'ds'        => 5,
						'field'		=> 'id',
						'as'		=>	'id',
						'formatter' => function( $d) {
							return '<a href="'.route('company.account.contacts', [$d]).'"><i class="ui user icon"></i> '.Account::find($d)->getContactCount().'</a>';
						}
				),
		);

		return Response::json( \DataTablesSSP::simple( $_GET, $this->sql_details, $table, $primaryKey, $columns) );
	}
}
