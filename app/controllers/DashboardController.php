<?php namespace App\Controllers;
/**
*	Copyright 2015 CoolBlueWeb
*
*	Licensed under the Apache License, Version 2.0 (the "License");
*	you may not use this file except in compliance with the License.
*	You may obtain a copy of the License at
*
*			http://www.apache.org/licenses/LICENSE-2.0
*
*	Unless required by applicable law or agreed to in writing, software
*	distributed under the License is distributed on an "AS IS" BASIS,
*	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*	See the License for the specific language governing permissions and
*	limitations under the License.
*
*	@package 	Laravel
*	@subpackage CoolBlueWeb Custom
*	@copyright  2015 CoolBlueWeb
*	@author 	CoolBlueWeb <bryan@coolblueweb.com>
*/

use BaseController, Sentinel, View, Input, Response, Redirect, Session, Mail, Lang, Config,App;
use App\Models\Dashboard;
use App\Models\Activity;
use App\Models\JobRework;
use App\Models\JobNote;
use App\Models\Job;
use App\Models\Contact;
use App\Models\User;
use App\Models\Profile;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class DashboardController extends BaseController {

	/**
	 * View Dashboard
	 *
	 * @return View
	 */
	public function viewDashboard()
	{

		if ( Sentinel::inRole('executive') ){
			return Redirect::route('dashboard.executive');
		}
		elseif( Sentinel::inRole('foreman') ){
			return Redirect::route('dashboard.foreman',Sentinel::getUser()->id);
		}
		elseif( Sentinel::inRole('accounting') ){
			return Redirect::route('dashboard.accounting',Sentinel::getUser()->id);
		}
		elseif( Sentinel::inRole('project_manager') ){
			return Redirect::route('dashboard.project-manager',Sentinel::getUser()->id);
		}
		elseif( Sentinel::inRole('estimator') ){
			return Redirect::route('dashboard.estimator',Sentinel::getUser()->id);
		}
		elseif( Sentinel::inRole('time_entry') ){
			$current_user_profile = Profile::find(Sentinel::getUser()->id);
			if ($current_timesheet = $current_user_profile->getCurrentTimesheet($current_user_profile->id))
				return Redirect::route('view.timesheet',$current_timesheet->id);
			else{
				return Redirect::route('add.timesheet');
			}
		}
	}

	/**
	 * View Executive Dashboard
	 *
	 * @return View
	 */
	public function viewExecutiveDashboard()
	{
		Log::info('executive initialized '. Carbon::now()->toDateTimeString());

		$follow_ups = JobNote::where('follow_up',1)->has('jobData')->with(['jobData', 'user_profile_data', 'contact_profile_data', 'notesData'])->get();

		$activities = JobNote::where('updated_at','>=',date('Y-m-d', strtotime('-7 days')))->has('jobData')->with(['jobData', 'created_profile_data'])->orderBy('id', 'DESC')->get();

		$data = [
			'activities'         => $activities,
			'follow_ups'         => $follow_ups,
			'users' 			 => User::getUsersSelect(),
			'activities_select'  => Activity::getActivitiesSelect(),
			'contacts' 			 => Contact::getContactsInputOptions(),
			// 'jobs_select' 	   => Job::getJobsSelect(),
			//'request_totals'     => Job::getJobsBidTotalByStatus('request'),
			//'outstanding_totals' => Job::getJobsBidTotalByStatus('outstanding'),
			//'contracted_totals'  => Job::getJobsBidTotalByStatus('contracted'),
			//'proceed_totals'     => Job::getJobsBidTotalByStatus('proceed'),
			//'scheduled_totals'   => Job::getJobsBidTotalByStatus('scheduled'),
			//'completed_totals'   => Job::getJobsBidTotalByStatus('completed'),
			//'invoicing_totals'   => Job::getJobsBidTotalByStatus('invoicing'),
			//'paid_totals'        => Job::getJobsBidTotalByStatus('paid'),
			//'rejected_totals'    => Job::getJobsBidTotalByStatus('rejected'),
		];

		Log::info('executive view render initialized '. Carbon::now()->toDateTimeString());

		return View::make('admin.dashboard.executive',$data);
	}


	/**
	 * View Foreman Dashboard
	 *
	 * @param null|string $user_id
	 * @return mixed
     */
	public function viewForemanDashboard($user_id = NULL)
	{
		//$jobs = Job::where('status','scheduled');
		$jobs = Job::whereIn('status',['contracted', 'proceed', 'scheduled']);
//		$jobs = Job::orderBy('id', 'DESC');
		$notes = JobNote::where('follow_up',1);
		if ( $user_id != NULL && $user_id != 'all') {
//			$jobs->where('foreman',$user_id);
			$notes->where('assigned',$user_id);
		}

		$jobs = $jobs->orderBy('id', 'DESC')->get();
		$notes = $notes->get();

		$data = [
			'activities'        => JobNote::where('updated_at','>=',date('Y-m-d', strtotime('-7 days')))->get(),
			'follow_ups'        => $notes,
			'users'             => User::getUsersSelect(),
			'activities_select' => Activity::getActivitiesSelect(),
			// 'jobs_select'       => Job::getJobsSelect(),
			'jobs'              => $jobs,
			'foremen_select'    => Job::getForemenSelect(true),
			'contacts'          => Contact::getContactsInputOptions(),
			'user_id'           => $user_id,
			'reworks'           => JobRework::where('status','active')->get(),
		];

		return View::make('admin.dashboard.foreman',$data);

	}

	/**
	 * View Project Manager Dashboard
	 *
	 * @return View
	 */
	public function viewProjectManagerDashboard($user_id = NULL)
	{


		$data = [
			'activities'         	 => JobNote::where('updated_at','>=',date('Y-m-d', strtotime('-7 days')))->orderBy('id', 'DESC')->get(),
			'follow_ups'             => JobNote::where('follow_up',1)->where('assigned',$user_id)->orderBy('id', 'DESC')->get(),
			'users'                  => User::getUsersSelect(),
			'activities_select'      => Activity::getActivitiesSelect(),
			// 'jobs_select'            => Job::getJobsSelect(),
			'jobs'                   => Job::where('project_manager',$user_id)->where(function($query){$query->where('status','contracted')->orWhere('status', 'scheduled')->orWhere('status', 'proceed');})->get(),
			'reworks'          		 => JobRework::where('status','active')->get(),
			'project_manager_select' => Job::getProjectManagersSelect(1),
			'contacts'               => Contact::getContactsInputOptions(),
			'user_id'                => $user_id
		];

		// @ToDo: Add A All PM FollowUps Option
		// 'follow_ups' => JobNote::where('follow_up',1)->get(),

		// If Current User is Not A Project Manager Show All
		if (! User::checkForUserRole($user_id,'project_manager') ) {

			$data['jobs'] = Job::where(function($query){$query->where('status','contracted')->orWhere('status', 'scheduled')->orWhere('status', 'proceed');})->get();
			$data['user_id'] = 'all';
		}

		return View::make('admin.dashboard.project-manager',$data);

	}

	/**
	 * View Estimator Dashboard
	 *
	 * @return View
	 */
	public function viewEstimatorDashboard($user_id = NULL)
	{
		$jobs = Job::whereIn('status',['request', 'outstanding']);
		$notes = JobNote::where('follow_up',1);
		if ( $user_id != NULL && $user_id != 'all') {
			$jobs->where('estimator',$user_id);
			$notes->where('assigned',$user_id);
		}

		$jobs = $jobs->orderBy('id', 'DESC')->get();
		$notes = $notes->get();

		$data = [
			'activities'         => JobNote::where('updated_at','>=',date('Y-m-d', strtotime('-7 days')))->orderBy('id', 'DESC')->get(),
			'follow_ups'         => $notes,
			'users'              => User::getUsersSelect(),
			'activities_select'  => Activity::getActivitiesSelect(),
			'jobs'               => $jobs,
			'reworks'            => JobRework::all(),
			'estimators_select'  => Job::getEstimatorsSelect(true),
			'contacts'           => Contact::getContactsInputOptions(),
			'user_id'            => $user_id,
		];

		return View::make('admin.dashboard.estimator',$data);

	}


	/**
	 * View Accounting Dashboard
	 *
	 * @return View
	 */
	public function viewAccountingDashboard($user_id = NULL)
	{
		$jobs = Job::whereIn('status',['scheduled', 'invoicing', 'completed']);
		$outstanding = Job::whereIn('status', ['status', 'invoicing']);

		$notes = JobNote::where('follow_up',1);
		
		if ( $user_id != NULL && $user_id != 'all') {
			$jobs->where('accounting',$user_id);
			$outstanding->where('accounting',$user_id);
			$notes->where('assigned',$user_id);
		}

		$jobs = $jobs->get();
		$notes = $notes->get();
		$outstanding = $outstanding->get();

		$data = [
			'activities'        => JobNote::where('updated_at','>=',date('Y-m-d', strtotime('-7 days')))->orderBy('id', 'DESC')->get(),
			'follow_ups'        => $notes,
			'users'             => User::getUsersSelect(),
			'activities_select' => Activity::getActivitiesSelect(),
			'jobs'              => $jobs,
			'outstanding_jobs'  => $outstanding,
			'reworks'           => JobRework::where('status','active')->orderBy('id', 'DESC')->get(),
			'accounting_select' => Job::getAccountingSelect(true),
			'contacts'          => Contact::getContactsInputOptions(),
			'user_id'           => $user_id
		];

		return View::make('admin.dashboard.accounting',$data);

	}


}
