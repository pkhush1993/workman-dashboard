<?php namespace App\Controllers;

use BaseController, Sentinel, View, Input, Response, Redirect, Session, Mail, Lang, Config,App,Form;
use App\Models\Job;
use App\Models\Company;
use App\Models\Profiles;
use App\Models\Timesheet;
use App\Models\TimesheetDetail;
use App\Models\Profile;
use App\Models\TimesheetTask;
use Illuminate\Routing\Redirector;

class TimeSheetController extends BaseController {



	public function currentUserCanAccessTimesheet($timesheet_id){
		$current_user_id = Sentinel::getUser()->id;
		$timesheet = Timesheet::find($timesheet_id);
		$timesheet_user_id = $timesheet["user_id"];
		$current_user_is_admin = Sentinel::inRole('admin');

		if($current_user_id != $timesheet_user_id && !$current_user_is_admin){
			return false;
		}
		return true;
	}

	/**
	 * View a single Timesheet
	 *
	 * @param int $timesheet_id
	 *
	 * @return mixed
     */
    public function viewTimesheet($timesheet_id)
	{

		// Access Check
		if(!TimeSheetController::currentUserCanAccessTimesheet($timesheet_id)){
			App::abort(403, 'Access denied');
		}

		$timesheet = Timesheet::find($timesheet_id);

//		$current_user_id = Sentinel::getUser()->id;
//		$timesheet_user_id = $timesheet["user_id"];
//		$current_user_is_admin = Sentinel::inRole('admin');
//		$current_user_profile = Profile::find($current_user_id);
//		$current_timesheet = $current_user_profile->getCurrentTimesheet($current_user_profile->id);
//		if($current_timesheet){
//			route('view.timesheet',$current_timesheet->id)
//			return Redirect::route('view.timesheet',$current_timesheet->id);
//		}

		$data = [
			'timesheet' 		=> $timesheet,
			'timesheet_details' => $timesheet->details(),
			'jobs_select' 		=> Job::getJobsSelect(),
			'company' 			=> Company::find(1),
			'last_timesheet'	=> Timesheet::getPastTimeSheet(Sentinel::getUser()->id),
			'entry_grace_period'=> Timesheet::checkEntryGracePeriod(),
			'period_start'      => date('Y-m-d',strtotime(Company::find(1)->getCurrentPayPeriod()['period_start'])),
			'period_end'        => date('Y-m-d',strtotime(Company::find(1)->getCurrentPayPeriod()['period_end'])),
			'past_period_start'	=> date('Y-m-d',strtotime(Company::find(1)->getLastPayPeriod()['period_start'])),
			'past_period_end'   => date('Y-m-d',strtotime(Company::find(1)->getLastPayPeriod()['period_end'])),
			'task_select' 		=> TimesheetTask::getTaskSelect()
		];

		// Check if the Timesheet Requested falls in the Current Period, if so jsut serve it up yo
		if ($data['timesheet']->period_start == $data['period_start'] ) {
			return View::make('admin.timesheet.view-timesheet',$data);
		}
		// Check if we are in the Grace Period for Editing the Last Timesheet and That the Timesheet is from the Last Period
		elseif( (Timesheet::checkEntryGracePeriod()) AND ($data['timesheet']->period_start == $data['past_period_start']) ){
			return View::make('admin.timesheet.view-timesheet',$data);
		}

		return View::make('admin.timesheet.view-timesheet',$data)->withErrors(Lang::get('company.timesheet_no_edit'));

	}


	/**
	 * View Past Timesheets
	 *
	 * @return View
	 */
	public function viewTimesheets()
	{
		$data = [
			'current_timesheets'  => Timesheet::getUserTimesheets('current'),
			'pending_timesheets'  => Timesheet::getUserTimesheets('pending'),
			'approved_timesheets' => Timesheet::getUserTimesheets('approved'),
			'last_timesheet'	  => Timesheet::getPastTimeSheet(Sentinel::getUser()->id),
			'entry_grace_period'  => Timesheet::checkEntryGracePeriod(),
		];

		return View::make('admin.timesheet.view-timesheets',$data);
	}

	/**
	 * View Timesheets Needing Approval
	 *
	 * @return View
	 */
	public function viewPendingApprovalTimesheets()
	{
		$timesheets = Timesheet::where('timesheets.approval','1')
			->whereNull('timesheets.approval_date')->get();


		return View::make('admin.timesheet.approve-timesheets', compact('timesheets'));
	}

	public function viewPendingApprovalTimesheetsAjax()
	{
		$contacts = \DB::table('timesheets')->join('timesheet_details', 'timesheets.id', '=', 'timesheet_details.timesheets_id');

		$contacts->select([
			'timesheets.id',
			\DB::raw("CONCAT(contacts.first_name, ' ',contacts.last_name) as name"),
			'contacts.phone', 'contacts.email',
			\DB::raw("CONCAT(contacts.address_1, '<br>',contacts.address_2, '<br>', contacts.city,' ', contacts.state, ' ', contacts.zip) as address"),
			'accounts.name as account_name',
			'contacts.account_id'
		]);

		$table = \Datatables::of($contacts)
			->editColumn('id', function($model) {
				return "<a href='".route('company.contact', ['contact_id' => $model->id])
				."'><i class='large unhide icon view-contact'></i></a>";
			})
			->editColumn('email', function($model) {
				if ( $model->email ) {
					return \HTML::mailto($model->email, $model->email);
				}
			})
			->editColumn('account_name', function($model) {
				return link_to_route('company.account', $model->account_name, ['account_id' => $model->account_id]);
			})
			->removeColumn('contacts.account_id');
		return $table->make(true);
	}

	/**
	 * View Timesheets Needing Approval
	 *
	 * @return View
	 */
	public function viewApproveTimesheet($timesheet_id)
	{
		$data = [
			'timesheet' => Timesheet::find($timesheet_id),
			'timesheet_details' => Timesheet::find($timesheet_id)->details(),
			'period_start' => date('Y-m-d',strtotime(Company::find(1)->getCurrentPayPeriod()['period_start'])),
			'period_end'   => date('Y-m-d',strtotime(Company::find(1)->getCurrentPayPeriod()['period_end'])),
			'jobs_select' => Job::getJobsSelect(),
			'company' => Company::find(1),
		];

		return View::make('admin.timesheet.approve-timesheet',$data);
	}


	/**
	 * View Timesheets Needing Approval
	 *
	 * @return View
	 */
	public function viewApprovedTimesheet($timesheet_id)
	{
		$data = [
			'timesheet' => Timesheet::find($timesheet_id),
			'timesheet_details' => Timesheet::find($timesheet_id)->details(),
			'period_start' => date('Y-m-d',strtotime(Company::find(1)->getCurrentPayPeriod()['period_start'])),
			'period_end'   => date('Y-m-d',strtotime(Company::find(1)->getCurrentPayPeriod()['period_end'])),
			'jobs_select' => Job::getJobsSelect(),
			'company' => Company::find(1),
		];

		return View::make('admin.timesheet.view-approved-timesheet',$data);
	}


	/**
	 * Approve/Save Timesheet
	 *
	 * @return View
	 */
	public function approveTimesheet($timesheet_id,$approval = NULL)
	{
		$timesheet = Timesheet::find($timesheet_id);

		// If the Timesheet is Approved Tag it with an Approval Date
		if ($approval) {
			// ===== Approve Timesheet ====================
			$timesheet->approval = 1;
			$timesheet->approval_date = date('Y-m-d');

			//Update Timesheet with Approval Information
			try {
				$timesheet->save();
			} catch ( \Exception $e ) {
				return Redirect::back()->withErrors($e->getMessage());
			}
		}


		// ===== Update Timesheet Details Depending on the Role ====================

		// Get Timesheet Details Task Categories
		$date_ins      = Input::get('date_in');
		$date_outs     = Input::get('date_out');
		$breaks        = Input::get('break');
		$totals        = Input::get('total');
		$job_ids       = Input::get('job_id');
		$tasks         = Input::get('task');
		$descriptions  = Input::get('description');
		$time_entry_id = Input::get('time_entry_id');
		$user_id  	   = Sentinel::getUser()->id;

		if ( is_array($tasks) ) {
			// Attach Details To TimeSheet
			foreach ($tasks as $key => $task) {

				// Work around for dealing with existing timesheets whose
				// line items have been completely deleted
				if ( array_key_exists($key, $time_entry_id) ) {
					$pk =  is_array($time_entry_id[$key]) ? current($time_entry_id[$key]) : $time_entry_id[$key];
					$timesheet_detail = TimesheetDetail::find($pk);
				} else {
					$timesheet_detail = false;
				}

				// Check if this is a new Time Entry or an Existing One
				if ($timesheet_detail) {
					if (Sentinel::inRole('admin') OR Sentinel::inRole('executive')) {
						// Update Task Category
						$timesheet_detail->date_in = date('Y-m-d H:i:s', strtotime($date_ins[$key]));
						$timesheet_detail->date_out = date('Y-m-d H:i:s', strtotime($date_outs[$key]));
						$timesheet_detail->break = $breaks[$key];
						$timesheet_detail->total = $totals[$key];
						$timesheet_detail->job_id = $job_ids[$key];
						$timesheet_detail->task = $tasks[$key];
						$timesheet_detail->description = $descriptions[$key];
						$timesheet_detail->user_id = $user_id;
					} else {
						// Update Task Category
						$timesheet_detail->job_id = $job_ids[$key];
						$timesheet_detail->task = $task;
						$timesheet_detail->description = $descriptions[$key];
						$timesheet_detail->user_id = $user_id;
					}

					// Save Timesheet Detail
					try {
						$timesheet_detail->save();
					} catch (\Exception $e) {
						return Redirect::back()->withErrors($e->getMessage());
					}
				} // Create a New Time Entry
				else {
					// Setup New Time Entry Details
					$timesheet_details = [
						'timesheets_id' => $timesheet->id,
						'date_in' => date('Y-m-d H:i:s', strtotime($date_ins[$key])),
						'date_out' => date('Y-m-d H:i:s', strtotime($date_outs[$key])),
						'break' => $breaks[$key],
						'total' => $totals[$key],
						'job_id' => $job_ids[$key],
						'task' => $tasks[$key],
						'description' => $descriptions[$key],
						'user_id' => $user_id
					];

					try {
						$timesheet_details = TimesheetDetail::create($timesheet_details);

					} catch (\Exception $e) {
						return Redirect::back()->withErrors($e->getMessage());
					}
				}
			}
		}

		// Check For Deleted Fields
		if (Input::get('removed_ids')) {
			$removed_fields = explode(',', Input::get('removed_ids'));

			foreach ($removed_fields as $remove_field) {
				$removed_timesheet = TimesheetDetail::find($remove_field);

				// Remove Timesheet Detail
				if ( $removed_timesheet ) {
					try {
						$removed_timesheet->delete();
					} catch ( \Exception $e ) {
						return Redirect::back()->withErrors($e);
					}
				}
			}
		}

		// If Timesheet is Approved Send User back to Approval Screen, If it is just saved send the user back to the Timesheet
		if ($approval) {
			return Redirect::route('view.approve.timesheets')->with('success', Lang::get('company.timesheet_approved'));
		}
		else{
			return Redirect::back()->with('success', Lang::get('company.timesheet_saved'));
		}
	}


	/**
	 * Add Timesheet
	 *
	 * @return View
	 */
	public function addTimesheet()
	{

		// If current timesheet exists, redirect to it
		$current_user_profile = Profile::find(Sentinel::getUser()->id);
		$current_timesheet = $current_user_profile->getCurrentTimesheet($current_user_profile->id);
		if($current_timesheet){
			return Redirect::route('view.timesheet',$current_timesheet->id);
		}

		//Otherwise set up a new timesheet
		$data = [
			'jobs_select'       => Job::getJobsSelect(),
			'last_timesheet'	=> Timesheet::getPastTimeSheet(Sentinel::getUser()->id),
			'entry_grace_period'=> Timesheet::checkEntryGracePeriod(),
			'company'           => Company::find(1),
			'period_start'      => date('Y-m-d',strtotime(Company::find(1)->getCurrentPayPeriod()['period_start'])),
			'period_end'        => date('Y-m-d',strtotime(Company::find(1)->getCurrentPayPeriod()['period_end'])),
			'past_period_start'	=> date('Y-m-d',strtotime(Company::find(1)->getLastPayPeriod()['period_start'])),
			'past_period_end'   => date('Y-m-d',strtotime(Company::find(1)->getLastPayPeriod()['period_end'])),
			'task_select' 		=> TimesheetTask::getTaskSelect()
		];
		return View::make('admin.timesheet.add-timesheet',$data);
	}

	/**
	 * Create Timesheet
	 *
	 * @return View
	 */
	public function createTimesheet($approval = NULL)
	{
		// ==== Create New Timesheet ==========================
		$timesheet = [
			'user_id'    => Sentinel::getUser()->id,
			'period_start' => date('Y-m-d',strtotime(Company::find(1)->getCurrentPayPeriod()['period_start'])),
			'period_end' => date('Y-m-d',strtotime(Company::find(1)->getCurrentPayPeriod()['period_end']))
		];

		if ($approval) {
			$timesheet['approval'] = 1;
		}
		else{
			$timesheet['approval'] = 0;
		}

		try {
			$timesheet = Timesheet::create($timesheet);

		} catch ( \Exception $e ) {
			return Redirect::back()->withErrors($e->getMessage());
		}


		// ==== Attach Details To TimeSheet ==========================

		// Set Entry Details to Variables
		$date_ins  = Input::get('date_in');
		$date_outs = Input::get('date_out');
		$breaks    = Input::get('break');
		$totals    = Input::get('total');
		$job_ids   = Input::get('job_id');
		$tasks     = Input::get('task');
		$user_id   = Input::get('user_id');

		// Attach Details To TimeSheet
		foreach ($totals as $key => $value) {

			// Get Each Entries Details
			$date_in  = $date_ins[$key];
			$date_out = $date_outs[$key];
			$break    = $breaks[$key];
			$total    = $totals[$key];
			$job_id   = $job_ids[$key];
			$task     = $tasks[$key];
			$user_id  = $user_id;

			if ($total) {
				$timesheet_details = [
					'timesheets_id' => $timesheet->id,
					'date_in'       => date('Y-m-d H:i:s',strtotime($date_in)),
					'date_out'       => date('Y-m-d H:i:s',strtotime($date_out)),
					'break'         => $break,
					'total'         => $total,
					'job_id'        => $job_id,
					'task'          => $task,
					'user_id'       => $user_id
				];

				try {
					TimesheetDetail::create($timesheet_details);

				} catch ( \Exception $e ) {
					return Redirect::back()->withErrors($e->getMessage());
				}
			}

		}
		// If Submitted as Approval Change Success Language and Route
		if ($approval) {
			return Redirect::route('view.timesheets')->with('success', Lang::get('company.timesheet_submit'));
		}
		else{
			return Redirect::route('view.timesheets')->with('success', Lang::get('company.timesheet_saved'));
		}

	}

	/**
	 * Update Timesheet
	 *
	 * @return View
	 */

	public function updateTimesheet($timesheet_id,$approval = NULL)
	{
		// Access Check
		if(!TimeSheetController::currentUserCanAccessTimesheet($timesheet_id)){
			App::abort(403, 'Access denied');
		}

		// Get Timesheet
		$timesheet = Timesheet::find($timesheet_id);


		// ===== Mark Timesheet for Approval if Approval was submitted ====================
		if ($approval) {
			// ===== Approve Timesheet ====================
			$timesheet->approval = 1;

			//Update Timesheet with Approval Information
			try {
				$timesheet->save();
			} catch ( \Exception $e ) {
				return Redirect::back()->withErrors($e->getMessage());
			}
		}

		// ===== Update Timesheet Details Depending on the Role ====================

		// Get Timesheet Details Task Categories
		$date_ins      = Input::get('date_in');
		$date_outs     = Input::get('date_out');
		$breaks        = Input::get('break');
		$totals        = Input::get('total');
		$job_ids       = Input::get('job_id');
		$tasks         = Input::get('task');
		$time_entry_id = Input::get('time_entry_id');
		$user_id  	   = Sentinel::getUser()->id;

		if ( is_array($tasks) ) {
			// Attach Details To TimeSheet
			foreach ($tasks as $key => $task) {

				// Work around for dealing with existing timesheets whose
				// line items have been completely deleted
				if (array_key_exists($key, $time_entry_id)) {
					$pk = is_array($time_entry_id[$key]) ? current($time_entry_id[$key]) : $time_entry_id[$key];
					$timesheet_detail = TimesheetDetail::find($pk);
				} else {
					$timesheet_detail = false;
				}

				// Check if this is a new Time Entry or an Existing One
				if ($timesheet_detail) {
					if (Sentinel::inRole('admin') || Sentinel::inRole('executive') || Sentinel::inRole('time_entry')) {
						// Update Task Category
						$timesheet_detail->date_in = date('Y-m-d H:i:s', strtotime($date_ins[$key]));
						$timesheet_detail->date_out = date('Y-m-d H:i:s', strtotime($date_outs[$key]));
						$timesheet_detail->break = $breaks[$key];
						$timesheet_detail->total = $totals[$key];
						$timesheet_detail->job_id = $job_ids[$key];
						$timesheet_detail->task = $tasks[$key];
						$timesheet_detail->user_id = $user_id;
					} else {
						// Update Task Category
						$timesheet_detail->job_id = $job_ids[$key];
						$timesheet_detail->task = $task;
						$timesheet_detail->user_id = $user_id;
					}

					// Save Timesheet Detail
					try {
						$timesheet_detail->save();
					} catch (\Exception $e) {
						return Redirect::back()->withErrors($e->getMessage());
					}
				} // Create a New Time Entry
				else {
					// Setup New Time Entry Details
					if ( strlen($date_ins[$key]) && strlen($date_outs[$key]) ) {
						$timesheet_details = [
							'timesheets_id' => $timesheet->id,
							'date_in' => date('Y-m-d H:i:s', strtotime($date_ins[$key])),
							'date_out' => date('Y-m-d H:i:s', strtotime($date_outs[$key])),
							'break' => $breaks[$key],
							'total' => $totals[$key],
							'job_id' => $job_ids[$key],
							'task' => $tasks[$key],
							'user_id' => Sentinel::getUser()->id
						];

						try {
							TimesheetDetail::create($timesheet_details);
						} catch (\Exception $e) {
							return Redirect::back()->withErrors($e->getMessage());
						}
					}
				}
			}
		}

		// Check For Deleted Fields
		if (Input::get('removed_ids')) {
			$removed_fields = explode(',', Input::get('removed_ids'));

			foreach ($removed_fields as $remove_field) {
				$removed_timesheet = TimesheetDetail::find($remove_field);

				try {
					$removed_timesheet->delete();
				} catch ( \Exception $e ) {
					return Redirect::back()->withErrors($e);
				}
			}
		}

		// If Submitted as Approval Change Success Language and Route
		if ($approval) {
			return Redirect::route('view.timesheets')->with('success', Lang::get('company.timesheet_submit'));
		}

		return Redirect::back()->with('success', Lang::get('company.timesheet_saved'));


	}

}
