<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateWageRateOnProfile extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('profiles', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE profiles MODIFY COLUMN wage_rate decimal(18,2)');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('profiles', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE profiles MODIFY COLUMN wage_rate int(11)');
		});
	}

}
