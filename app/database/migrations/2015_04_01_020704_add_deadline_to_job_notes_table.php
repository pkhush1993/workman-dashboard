<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeadlineToJobNotesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('job_notes', function(Blueprint $table)
		{
			$table->dateTime('deadline')->after('view_foreman')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('job_notes', function(Blueprint $table)
		{
			Schema::drop('deadline');
		});
	}

}
