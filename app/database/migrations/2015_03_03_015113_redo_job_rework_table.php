<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RedoJobReworkTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		Schema::create('job_reworks', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('job_id')->nullable();
			$table->integer('number')->nullable();
			$table->string('description')->nullable();
			$table->string('status')->nullable();
			$table->integer('created_by')->nullable();
			$table->dateTime('deadline')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('job_reworks');
	}

}

