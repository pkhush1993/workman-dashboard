<?php

use Illuminate\Database\Migrations\Migration;

class AlterCompanySettingsDropdowns extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		\DB::statement('ALTER TABLE `company_settings` MODIFY COLUMN `custom_drop_1` LONGTEXT');
		\DB::statement('ALTER TABLE `company_settings` MODIFY COLUMN `custom_drop_2` LONGTEXT');
		\DB::statement('ALTER TABLE `company_settings` MODIFY COLUMN `custom_drop_3` LONGTEXT');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\DB::statement('ALTER TABLE `company_settings` MODIFY COLUMN `custom_drop_1` VARCHAR(255)');
		\DB::statement('ALTER TABLE `company_settings` MODIFY COLUMN `custom_drop_2` VARCHAR(255)');
		\DB::statement('ALTER TABLE `company_settings` MODIFY COLUMN `custom_drop_3` VARCHAR(255)');
	}

}
