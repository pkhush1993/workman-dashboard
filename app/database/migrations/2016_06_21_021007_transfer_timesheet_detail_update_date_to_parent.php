<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransferTimesheetDetailUpdateDateToParent extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		foreach ( \App\Models\TimesheetDetail::all() as $detail ) {
			if ($detail->getAttribute('updated_at') > $detail->timesheet()->getAttribute('updated_at')) {
				$detail->timesheet()->fill([
					'updated_at' => $detail->getAttribute('updated_at')
				])->save();
			}
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
