<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CompanySettingsUpdateFieldTypes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('ALTER TABLE company_settings MODIFY COLUMN contract_language TEXT');
		DB::statement('ALTER TABLE company_settings MODIFY COLUMN bid_header TEXT');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('ALTER TABLE company_settings MODIFY COLUMN contract_language VARCHAR(255)');
		DB::statement('ALTER TABLE company_settings MODIFY COLUMN bid_header VARCHAR(255)');
	}

}