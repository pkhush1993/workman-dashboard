<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateReworkNumber extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('job_reworks', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE job_reworks MODIFY COLUMN number varchar(10)');
		
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('job_reworks', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE job_reworks MODIFY COLUMN number int(11)');
		});
	}

}


