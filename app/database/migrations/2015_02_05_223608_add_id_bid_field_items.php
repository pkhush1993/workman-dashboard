<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdBidFieldItems extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('bid_field_items', function(Blueprint $table)
		{
			$table->increments('id')->before('bid_field_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('bid_field_items', function(Blueprint $table)
		{
			$table->dropColumn('id');
		});
	}

}
