<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTaxRateColumnInvoicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('invoices', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `invoices` MODIFY COLUMN `tax_rate` decimal(18,2)');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('invoices', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `invoices` MODIFY COLUMN `tax_rate` int(11)');
		});
	}

}
