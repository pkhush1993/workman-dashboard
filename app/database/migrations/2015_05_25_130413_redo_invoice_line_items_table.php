<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RedoInvoiceLineItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		if (Schema::hasTable('invoice_line_items')) {
			Schema::drop('invoice_line_items');
		}

		Schema::create('invoice_line_items', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('invoice_id')->nullable();
			$table->integer('job_id')->nullable();
			$table->integer('bid_id')->nullable();
			$table->string('category')->nullable();
			$table->string('product')->nullable();
			$table->string('unit')->nullable();
			$table->decimal('rate', 18, 2)->nullable();
			$table->integer('quantity')->nullable();
			$table->boolean('taxable')->nullable();
			$table->decimal('amount', 18, 2)->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('sort_order')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('invoice_line_items');
	}

}

