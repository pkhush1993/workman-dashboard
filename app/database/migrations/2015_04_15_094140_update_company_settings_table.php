<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCompanySettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		if (Schema::hasTable('company_settings')) {
			Schema::drop('company_settings');
		}
		
		Schema::create('company_settings', function(Blueprint $table)
		{

			$table->increments('id');
			$table->string('name')->nullable();
			$table->string('phone')->nullable();
			$table->string('fax')->nullable();
			$table->string('address_1')->nullable();
			$table->string('address_2')->nullable();
			$table->string('city')->nullable();
			$table->string('state')->nullable();
			$table->string('zip')->nullable();
			$table->string('email')->nullable();
			$table->string('website')->nullable();
			$table->string('avatar')->nullable();
			$table->string('pay_period_type')->nullable();
			$table->date('pay_period_start')->nullable();
			$table->string('pay_period_start_1')->nullable();
			$table->string('pay_period_start_2')->nullable();
			$table->string('job_defaults')->nullable();
			$table->text('bid_header')->nullable();
			$table->text('contract_language')->nullable();
			$table->integer('next_job_number')->nullable();
			$table->integer('next_invoice_number')->nullable();
			$table->string('custom_drop_1')->nullable();
			$table->string('custom_drop_2')->nullable();
			$table->string('custom_drop_3')->nullable();
			$table->string('custom_text_1')->nullable();
			$table->string('custom_text_2')->nullable();
			$table->string('custom_text_3')->nullable();
			$table->string('status')->nullable();
			$table->integer('created_by')->nullable();
			$table->timestamps();
		});


	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable('company_settings')) {
			Schema::drop('company_settings');
		}
	}

}
