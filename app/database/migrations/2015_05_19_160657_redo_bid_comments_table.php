<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RedoBidCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (Schema::hasTable('bid_comments')) {
			Schema::drop('bid_comments');
		}			
		Schema::create('bid_comments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('job_id')->nullable();
			$table->integer('bid_id')->nullable();
			$table->string('comment')->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bid_comments');
	}

}