<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Companies
		Schema::create('profile', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name')->nullable();
			$table->string('street1')->nullable();
			$table->string('street2')->nullable();
			$table->string('city')->nullable();
			$table->string('state')->nullable();
			$table->string('zip')->nullable();
			$table->string('email')->nullable();
			$table->string('website')->nullable();
			$table->string('fax')->nullable();
			$table->string('phone')->nullable();
			$table->string('mobile')->nullable();
			$table->string('url')->nullable();
			$table->integer('wage_rate')->nullable();
			$table->string('avatar')->nullable();
			$table->string('status')->nullable();
			$table->integer('created_by')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('profile');
	}

}
