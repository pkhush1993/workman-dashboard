<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTimesheetsToForemanPermissions extends Migration {

	/**
	 * @var array
     */
	protected $timesheetApprovalRoles = [
			'executive',
			'accounting',
			'project_manager',
			'estimator',
			'time_entry',
			'admin',
			'salesman'
	];

	/**
	 * @var array
     */
	protected $timesheetAccessRoles = ['foreman'];

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		foreach ( $this->timesheetApprovalRoles as $slug ) {

			$role = Sentinel::findRoleBySlug($slug);
			$role->addPermission('timesheet.approval')->save();
		}

		foreach ( $this->timesheetAccessRoles as $slug ) {

			$role = Sentinel::findRoleBySlug($slug);
			$role->addPermission('navigation.timekeeping')->save();
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		foreach ( $this->timesheetApprovalRoles as $slug ) {
			$role = \Sentinel::findRoleBySlug($slug);
			$role->removePermission('timesheet.approval')->save();
		}

		foreach ( $this->timesheetAccessRoles as $slug ) {
			$role = \Sentinel::findRoleBySlug($slug);
			$role->removePermission('navigation.timekeeping')->save();
		}
	}

}
