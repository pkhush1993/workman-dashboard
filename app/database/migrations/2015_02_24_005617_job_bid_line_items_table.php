<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class JobBidLineItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		// Job Bid Line Items
		Schema::create('job_bid_line_items', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('bid_id')->unsigned();
			$table->foreign('bid_id')->references('id')->on('job_bids')->onDelete('cascade');
			$table->integer('category_id')->nullable();
			$table->string('category')->nullable();
			$table->integer('product_id')->nullable();
			$table->string('product')->nullable();
			$table->integer('quantity')->nullable();
			$table->decimal('rate', 18, 2)->nullable();
			$table->string('unit')->nullable();
			$table->boolean('taxable')->nullable();
			$table->decimal('amount', 18, 2)->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('sort_order')->nullable();
			$table->timestamps();
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('job_bid_line_items');
	}

}
