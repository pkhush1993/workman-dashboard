<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DdUniqueIndexToJobNumber extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		\Schema::table('jobs', function(Blueprint $table) {
			$table->unique('job_number');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\Schema::table('jobs', function(Blueprint $table) {
			$table->dropUnique('job_number');
		});
	}

}
