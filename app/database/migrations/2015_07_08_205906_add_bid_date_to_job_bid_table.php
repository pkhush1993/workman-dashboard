<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBidDateToJobBidTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('job_bids', function(Blueprint $table)
		{
			$table->date('bid_date')->after('sales_tax_total')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('job_bids', function(Blueprint $table)
		{
			$table->dropColumn('bid_date');
		});
	}

}
