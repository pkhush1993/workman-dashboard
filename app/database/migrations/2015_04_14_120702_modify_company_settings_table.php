<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyCompanySettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('company_settings', function(Blueprint $table)
		{
			// $table->dropColumn('pay_period_end');
			// $table->string('pay_period_start_1')->after('pay_period_start')->nullable();
			// $table->string('pay_period_start_2')->after('pay_period_start_1')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('company_settings', function(Blueprint $table)
		{
			// $table->date('pay_period_end')->nullable();
			// $table->dropColumn('pay_period_start_1');
			// $table->dropColumn('pay_period_start_2');
		});
	}

}
