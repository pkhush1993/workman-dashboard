<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSoftDeleteToAccountsAndContactsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('accounts', function(Blueprint $table)
		{
			$table->softDeletes();
		});

		Schema::table('contacts', function(Blueprint $table)
		{
			$table->softDeletes();
		});		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('accounts', function(Blueprint $table)
		{
			Schema::drop('deleted_at');
		});
		
		Schema::table('contacts', function(Blueprint $table)
		{
			Schema::drop('deleted_at');
		});		
	}

}
