<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RedoTimesheetDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		if (Schema::hasTable('timesheet_details')) {
			Schema::drop('timesheet_details');
		}
		Schema::create('timesheet_details', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('timesheets_id')->unsigned();
			$table->foreign('timesheets_id')->references('id')->on('timesheets')->onDelete('cascade');
			$table->integer('job_id')->nullable();
			$table->date('date_in')->nullable();
			$table->date('date_out')->nullable();
			$table->time('time_in')->nullable();
			$table->time('time_out')->nullable();
			$table->integer('break')->nullable();
			$table->decimal('total', 18, 2)->nullable();
			$table->string('task')->nullable();
			$table->integer('created_by')->nullable();
			$table->timestamps();	
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('timesheet_details', function(Blueprint $table)
		{
			Schema::drop('timesheet_details');
		});
	}

}



