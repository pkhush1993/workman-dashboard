<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTimeEntryGracePeriod extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('company_settings', function(Blueprint $table)
		{
			$table->integer('time_entry_grace_period')->after('pay_period_start_2')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('company_settings', function(Blueprint $table)
		{
			$table->dropColumn('time_entry_grace_period');
		});
	}

}
