<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RedoAccountsContactsTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		Schema::drop('accounts');
		Schema::drop('contacts');

		Schema::create('accounts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('company_id')->nullable();
			$table->string('name')->nullable();
			$table->string('address_1')->nullable();
			$table->string('address_2')->nullable();
			$table->string('city')->nullable();
			$table->string('state')->nullable();
			$table->integer('zip')->nullable();
			$table->string('phone')->nullable();
			$table->string('fax')->nullable();
			$table->string('mobile')->nullable();
			$table->string('email')->nullable();
			$table->string('status')->nullable();
			$table->integer('created_by')->nullable();			
			$table->timestamps();
		});

		Schema::create('contacts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('company_id')->nullable();
			$table->integer('account_id')->nullable();
			$table->string('first_name')->nullable();
			$table->string('last_name')->nullable();
			$table->string('title')->nullable();
			$table->string('address_1')->nullable();
			$table->string('address_2')->nullable();
			$table->string('city')->nullable();
			$table->string('state')->nullable();
			$table->integer('zip')->nullable();
			$table->string('phone')->nullable();
			$table->string('fax')->nullable();
			$table->string('mobile')->nullable();
			$table->string('email')->nullable();
			$table->string('status')->nullable();
			$table->integer('created_by')->nullable();			
			$table->timestamps();
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('accounts');
	}

}
