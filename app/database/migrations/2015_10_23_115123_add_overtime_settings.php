<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOvertimeSettings extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('company_settings', function(Blueprint $table)
		{
			$table->decimal('overtime_pay', 18, 2)->after('pay_period_start_2')->nullable();
			$table->decimal('weekly_hours', 18, 2)->after('pay_period_start_2')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('company_settings', function(Blueprint $table)
		{
			Schema::drop('overtime_pay');
			Schema::drop('weekly_hours');
		});
	}

}
