<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePriceColumnOnBidFieldItems extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('bid_field_items', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE bid_field_items MODIFY COLUMN price decimal(18,2)');
		
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('bid_field_items', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE bid_field_items MODIFY COLUMN price int(11)');
		});
	}

}
