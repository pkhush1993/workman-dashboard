<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaxRatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tax_rates', function(Blueprint $table)
		{
			$table->increments('id');
			$table->decimal('tax_rate', 18, 2)->nullable();
			$table->string('tax_description')->nullable();			
			$table->timestamps();			
		});
	}

	/**
	 * Reverse the migrations.å
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tax_rates');
	}

}
