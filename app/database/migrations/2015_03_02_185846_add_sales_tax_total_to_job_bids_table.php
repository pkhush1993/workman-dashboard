<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSalesTaxTotalToJobBidsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('job_bids', function(Blueprint $table)
		{
			$table->decimal('sales_tax_total', 18, 2)->after('tax_rate')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('job_bids', function(Blueprint $table)
		{
			$table->dropColumn('sales_tax_total');
		});
	}

}
