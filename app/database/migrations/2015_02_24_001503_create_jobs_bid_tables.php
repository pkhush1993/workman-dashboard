<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsBidTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		// Job Bids
		Schema::create('job_bids', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('job_id');
			$table->string('comment')->nullable();
			$table->string('status')->nullable();
			$table->integer('created_by')->nullable();
			$table->decimal('subtotal', 18, 2)->nullable();
			$table->decimal('grand_total', 18, 2)->nullable();
			$table->string('tax_description')->nullable();
			$table->decimal('tax_rate', 18, 2)->nullable();
			$table->timestamps();
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('job_bids');
	}

}
