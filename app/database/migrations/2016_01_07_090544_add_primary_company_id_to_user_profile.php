<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPrimaryCompanyIdToUserProfile extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		\Schema::table('profiles', function(Blueprint $table) {
			$table->integer('company_id')
					->unsigned()
					->after('wage_rate')
					->default(1);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\Schema::table('profiles', function(Blueprint $table) {
			$table->dropColumn('company_id');
		});
	}

}
