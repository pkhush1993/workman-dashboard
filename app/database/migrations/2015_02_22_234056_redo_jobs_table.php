<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RedoJobsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		Schema::drop('jobs');

		// Jobs Table
		Schema::create('jobs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name')->nullable();
			$table->string('status')->nullable();
			$table->integer('job_number')->nullable();
			$table->integer('company')->nullable();
			$table->integer('account')->nullable();
			$table->integer('primary_contact')->nullable();
			$table->string('contacts')->nullable();
			$table->string('prime_contract')->nullable();
			$table->string('salesman')->nullable();
			$table->string('estimator')->nullable();
			$table->string('project_manager')->nullable();
			$table->string('foreman')->nullable();
			$table->integer('lead_source_id')->nullable();
			$table->string('lead_source')->nullable();
			$table->integer('invoice_number')->nullable();
			$table->string('custom_drop_1')->nullable();
			$table->string('custom_drop_2')->nullable();
			$table->string('custom_drop_3')->nullable();
			$table->string('custom_text_1')->nullable();
			$table->string('custom_text_2')->nullable();
			$table->string('custom_text_3')->nullable();
			$table->date('bid_date')->nullable();
			$table->dateTime('completed_date')->nullable();
			$table->string('site_address_1')->nullable();
			$table->string('site_address_2')->nullable();
			$table->string('site_city')->nullable();
			$table->string('site_state')->nullable();
			$table->integer('site_zip')->nullable();
			$table->string('billing_address_1')->nullable();
			$table->string('billing_address_2')->nullable();
			$table->string('billing_city')->nullable();
			$table->string('billing_state')->nullable();
			$table->integer('billing_zip')->nullable();
			$table->string('estimator_priority')->nullable();
			$table->string('estimator_deadline')->nullable();
			$table->string('completion_priority')->nullable();
			$table->string('completion_deadline')->nullable();
			$table->string('tax_description')->nullable();
			$table->integer('tax_rate')->nullable();
			$table->integer('created_by')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('jobs');

		// Old Jobs Table
		Schema::create('jobs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name')->nullable();
			$table->string('status')->nullable();
			$table->integer('job_number')->nullable();
			$table->integer('company')->nullable();
			$table->integer('account')->nullable();
			$table->integer('primary_contact')->nullable();
			$table->integer('contact')->nullable();
			$table->string('prime_contract')->nullable();
			$table->string('salesman')->nullable();
			$table->string('estimator')->nullable();
			$table->string('manager')->nullable();
			$table->string('foreman')->nullable();
			$table->integer('lead_source_id')->nullable();
			$table->string('lead_source')->nullable();
			$table->integer('invoice_number')->nullable();
			$table->string('custom_1')->nullable();
			$table->string('custom_2')->nullable();
			$table->string('custom_3')->nullable();
			$table->string('custom_4')->nullable();
			$table->string('custom_5')->nullable();
			$table->date('bid_date')->nullable();
			$table->dateTime('completed_date')->nullable();
			$table->string('site_street')->nullable();
			$table->string('site_city')->nullable();
			$table->string('site_state')->nullable();
			$table->integer('site_zip')->nullable();
			$table->string('bill_street')->nullable();
			$table->string('bill_city')->nullable();
			$table->string('bill_state')->nullable();
			$table->integer('bill_zip')->nullable();
			$table->string('estimator_priority')->nullable();
			$table->string('estimator_deadline')->nullable();
			$table->string('completion_priority')->nullable();
			$table->string('completion_deadline')->nullable();
			$table->string('tax_description')->nullable();
			$table->integer('tax_rate')->nullable();
			$table->integer('created_by')->nullable();
			$table->timestamps();
		});

	}

}
