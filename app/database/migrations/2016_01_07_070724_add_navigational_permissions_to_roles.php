<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNavigationalPermissionsToRoles extends Migration {


	protected $roleIds = [43, 44, 45, 46, 48, 49, 53];

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		foreach ( $this->roleIds as $roleId ) {

			$role = \Sentinel::findRoleById($roleId);

			$role->permissions = [
					'navigation.jobs' => true,
					'navigation.accounts' => true,
					'navigation.timekeeping' => true,
					'navigation.reports' => true,
			];

			$role->save();

		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		foreach ( $this->roleIds as $roleId ) {

			$role = \Sentinel::findRoleById($roleId);

			$role->permissions = [];

			$role->save();

		}
	}

}
