<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RedoJobNotesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		Schema::create('job_notes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('job_id')->nullable();
			$table->string('activity')->nullable();
			$table->string('type')->nullable();
			$table->text('note')->nullable();
			$table->integer('job_contacts_id')->nullable();
			$table->integer('assigned')->nullable();
			$table->string('follow_up')->nullable();
			$table->string('view_salesman')->nullable();
			$table->string('view_estimator')->nullable();
			$table->string('view_manager')->nullable();
			$table->string('view_foreman')->nullable();
			$table->integer('created_by')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('job_notes');
	}

}
