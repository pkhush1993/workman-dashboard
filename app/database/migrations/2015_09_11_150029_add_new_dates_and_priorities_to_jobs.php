<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewDatesAndPrioritiesToJobs extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('jobs', function(Blueprint $table)
		{

			// Drop Old Columns if they exist
			if (Schema::hasColumn('jobs','estimator_deadline')){
				$table->dropColumn('estimator_deadline');
			}
			if (Schema::hasColumn('jobs','estimator_priority')) {
				$table->dropColumn('estimator_priority');
			}
			if (Schema::hasColumn('jobs','production_deadline')) {
				$table->dropColumn('production_deadline');
			}
			if (Schema::hasColumn('jobs','production_priority')) {
				$table->dropColumn('production_priority');
			}

			// Add New Columns
			$table->date('estimator_deadline')->after('billing_zip')->nullable();
			$table->string('estimator_priority')->after('billing_zip')->nullable();
			$table->date('production_deadline')->after('billing_zip')->nullable();
			$table->string('production_priority')->after('billing_zip')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('jobs', function(Blueprint $table)
		{
			$table->dropColumn('estimator_deadline');
			$table->dropColumn('estimator_priority');
			$table->dropColumn('production_deadline');
			$table->dropColumn('production_priority');
		});
	}

}
