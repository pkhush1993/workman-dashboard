<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Companies
		Schema::create('company', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name')->nullable();
			$table->integer('phone')->nullable();
			$table->string('street1')->nullable();
			$table->string('street2')->nullable();
			$table->string('city')->nullable();
			$table->string('state')->nullable();
			$table->string('zip')->nullable();
			$table->string('email')->nullable();
			$table->string('website')->nullable();
			$table->string('url')->nullable();			
			$table->string('bid_header')->nullable();
			$table->string('contract_language')->nullable();
			$table->string('next_job_number')->nullable();
			$table->string('next_invoice_number')->nullable();
			$table->string('status')->nullable();
			$table->integer('created_by')->nullable();
			$table->timestamps();
		});
		// Company Notes
		Schema::create('company_notes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('company_id')->nullable();
			$table->string('note')->nullable();
			$table->string('status')->nullable();
			$table->integer('parent')->nullable();
			$table->integer('created_by')->nullable();
			$table->timestamps();
		});
		// Jobs Table
		Schema::create('jobs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name')->nullable();
			$table->string('status')->nullable();
			$table->integer('job_number')->nullable();
			$table->integer('company')->nullable();
			$table->integer('account')->nullable();
			$table->integer('primary_contact')->nullable();
			$table->integer('contact')->nullable();
			$table->string('prime_contract')->nullable();
			$table->string('salesman')->nullable();
			$table->string('estimator')->nullable();
			$table->string('manager')->nullable();
			$table->string('foreman')->nullable();
			$table->integer('lead_source_id')->nullable();
			$table->string('lead_source')->nullable();
			$table->integer('invoice_number')->nullable();
			$table->string('custom_1')->nullable();
			$table->string('custom_2')->nullable();
			$table->string('custom_3')->nullable();
			$table->string('custom_4')->nullable();
			$table->string('custom_5')->nullable();
			$table->date('bid_date')->nullable();
			$table->dateTime('completed_date')->nullable();
			$table->string('site_street')->nullable();
			$table->string('site_city')->nullable();
			$table->string('site_state')->nullable();
			$table->integer('site_zip')->nullable();
			$table->string('bill_street')->nullable();
			$table->string('bill_city')->nullable();
			$table->string('bill_state')->nullable();
			$table->integer('bill_zip')->nullable();
			$table->string('estimator_priority')->nullable();
			$table->string('estimator_deadline')->nullable();
			$table->string('completion_priority')->nullable();
			$table->string('completion_deadline')->nullable();
			$table->string('tax_description')->nullable();
			$table->integer('tax_rate')->nullable();
			$table->integer('created_by')->nullable();
			$table->timestamps();
		});
		// Job Notes
		Schema::create('job_notes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('job_id')->nullable();
			$table->text('note')->nullable();
			$table->string('status')->nullable();
			$table->string('view_salesman')->nullable();
			$table->string('view_estimator')->nullable();
			$table->string('view_manager')->nullable();
			$table->string('view_foreman')->nullable();
			$table->integer('created_by')->nullable();
			$table->timestamps();
		});
		// Job Contacts
		Schema::create('job_contacts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('job_id')->nullable();
			$table->integer('contact_id')->nullable();
			$table->integer('created_by')->nullable();
			$table->string('status')->nullable();
			$table->timestamps();
		});
		// Job Rework
		Schema::create('rework', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('job_id')->nullable();
			$table->dateTime('date_opened')->nullable();
			$table->dateTime('deadline')->nullable();
			$table->string('description')->nullable();
			$table->string('status')->nullable();
			$table->integer('created_by')->nullable();
			$table->timestamps();
		});
		// Job Statuses 
		Schema::create('job_statuses', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('company_id')->nullable();
			$table->string('name')->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
		});		
		// Bids
		Schema::create('bids', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('job_id')->nullable();
			$table->string('comment')->nullable();
			$table->string('status')->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('subtotal')->nullable();
			$table->integer('grand_total')->nullable();
			$table->string('tax_description')->nullable();
			$table->integer('tax_rate')->nullable();
			$table->timestamps();
		});
		// Bid Line Items
		Schema::create('bid_line_items', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('bid_id')->nullable();
			$table->integer('product_id')->nullable();
			$table->string('product')->nullable();
			$table->integer('description_id')->nullable();
			$table->string('description')->nullable();
			$table->integer('rate')->nullable();
			$table->boolean('taxable')->nullable();
			$table->string('unit')->nullable();
			$table->integer('total')->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('sort_order')->nullable();
			$table->timestamps();
		});
		// Bid Comments
		Schema::create('bid_comments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('company_id')->nullable();
			$table->string('comment')->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
		});		
		// Invoices
		Schema::create('invoices', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('bid_id')->nullable();
			$table->integer('job_id')->nullable();
			$table->string('comment')->nullable();
			$table->string('status')->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('subtotal')->nullable();
			$table->integer('tax_rate')->nullable();
			$table->string('tax_description')->nullable();
			$table->integer('grand_total')->nullable();
			$table->timestamps();
		});
		// Invoice Line Items
		Schema::create('invoice_line_items', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('invoice_id')->nullable();
			$table->integer('product_id')->nullable();
			$table->string('product')->nullable();
			$table->integer('description_id')->nullable();
			$table->string('description')->nullable();
			$table->integer('rate')->nullable();
			$table->boolean('taxable')->nullable();
			$table->string('unit')->nullable();
			$table->integer('total')->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('sort_order')->nullable();
			$table->timestamps();
		});
		// Accounts
		Schema::create('accounts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('company_id')->nullable();
			$table->string('name')->nullable();
			$table->integer('phone')->nullable();
			$table->string('street1')->nullable();
			$table->string('street2')->nullable();
			$table->string('city')->nullable();
			$table->string('state')->nullable();
			$table->string('zip')->nullable();
			$table->string('status')->nullable();
			$table->integer('created_by')->nullable();
			$table->timestamps();
		});				
		// Account Notes
		Schema::create('account_notes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('account_id')->nullable();
			$table->string('note')->nullable();
			$table->string('status')->nullable();
			$table->integer('parent')->nullable();
			$table->integer('created_by')->nullable();
			$table->timestamps();
		});
		// Contacts
		Schema::create('contacts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('company_id')->nullable();
			$table->integer('account_id')->nullable();
			$table->string('first_name')->nullable();
			$table->string('last_name')->nullable();
			$table->string('title')->nullable();
			$table->integer('phone')->nullable();
			$table->integer('fax')->nullable();
			$table->integer('mobile')->nullable();
			$table->string('email')->nullable();
			$table->integer('created_by')->nullable();
			$table->string('status')->nullable();
			$table->timestamps();
		});				
		// Contact Notes
		Schema::create('contact_notes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('contact_id')->nullable();
			$table->string('note')->nullable();
			$table->string('status')->nullable();
			$table->integer('parent')->nullable();
			$table->integer('created_by')->nullable();
			$table->timestamps();
		});
		// Products and Services
		Schema::create('product_types', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('company_id')->nullable();
			$table->string('name')->nullable();
			$table->string('status')->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
		});		
		// Products
		Schema::create('products', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('type_id')->nullable();
			$table->string('description')->nullable();
			$table->string('status')->nullable();
			$table->integer('default_rate')->nullable();
			$table->integer('default_unit')->nullable();
			$table->integer('sort_order')->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
		});
		// Timesheets
		Schema::create('timesheets', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->nullable();
			$table->date('period_end')->nullable();
			$table->boolean('approval')->nullable(); 
			$table->date('approval_date')->nullable();
			$table->integer('created_by')->nullable();
			$table->timestamps();
		});
		// Timesheet Details
		Schema::create('timesheet_details', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('timesheet_id')->nullable();
			$table->integer('job_id')->nullable();
			$table->date('date')->nullable();
			$table->dateTime('time_in')->nullable();
			$table->dateTime('time_out')->nullable();
			$table->dateTime('break')->nullable();
			$table->integer('created_by')->nullable();
			$table->timestamps();
		});
		// Lead Sources
		Schema::create('lead_sources', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('company_id')->nullable();
			$table->string('name')->nullable();
			$table->string('status')->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
		});
		// Follow Ups
		Schema::create('follow_ups', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('description')->nullable();
			$table->dateTime('original_deadline')->nullable();
			$table->dateTime('deadline')->nullable();
			$table->integer('job_id')->nullable();
			$table->integer('user_id')->nullable();
			$table->integer('bid_id')->nullable();
			$table->string('account_id')->nullable();
			$table->string('contact_id')->nullable();
			$table->integer('company_id')->nullable();
			$table->string('status')->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
		});
		// Follow Up Notes
		Schema::create('follow_up_notes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('follow_up_id')->nullable();
			$table->string('note')->nullable();
			$table->integer('created_by')->nullable();
			$table->dateTime('new_deadline')->nullable();
			$table->timestamps();
		});			
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('company');
		Schema::drop('company_notes');
		Schema::drop('jobs');
		Schema::drop('job_notes');
		Schema::drop('job_contacts');
		Schema::drop('rework');
		Schema::drop('job_statuses');
		Schema::drop('bids');
		Schema::drop('bid_line_items');
		Schema::drop('bid_comments');
		Schema::drop('invoices');
		Schema::drop('invoice_line_items');
		Schema::drop('accounts');
		Schema::drop('account_notes');
		Schema::drop('contacts');
		Schema::drop('contact_notes');
		Schema::drop('product_types');
		Schema::drop('products');
		Schema::drop('timesheets');
		Schema::drop('timesheet_details');
		Schema::drop('lead_sources');
		Schema::drop('follow_ups');
		Schema::drop('follow_up_notes');

	}

}
