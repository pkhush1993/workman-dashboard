<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterJobCustomDropDowns extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		\DB::statement('ALTER TABLE `jobs` MODIFY `custom_drop_1` VARCHAR(255);');
		\DB::statement('ALTER TABLE `jobs` MODIFY `custom_drop_2` VARCHAR(255);');
		\DB::statement('ALTER TABLE `jobs` MODIFY `custom_drop_3` VARCHAR(255);');

		$this->convertRelationshipToString();
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\DB::statement('ALTER TABLE `jobs` MODIFY `custom_drop_1` INTEGER(11);');
		\DB::statement('ALTER TABLE `jobs` MODIFY `custom_drop_2` INTEGER(11);');
		\DB::statement('ALTER TABLE `jobs` MODIFY `custom_drop_3` INTEGER(11);');
	}

	/**
	 * Converts Integer Foreign Keys to Textual Relationship
     */
	private function convertRelationshipToString()
	{

		// I'm hard-coding since multiple companies isn't current active.
		$company = \App\Models\Company::find(1);

		$custom_options_1 = is_array(unserialize($company->getAttribute('custom_drop_1'))) ?
			current(unserialize($company->getAttribute('custom_drop_1'))) : [];

		$custom_options_2 = is_array(unserialize($company->getAttribute('custom_drop_2'))) ?
			current(unserialize($company->getAttribute('custom_drop_2'))) : [];

		$custom_options_3 = is_array(unserialize($company->getAttribute('custom_drop_3'))) ?
			current(unserialize($company->getAttribute('custom_drop_3'))) : [];

		foreach ( \App\Models\Job::all() as $job ) {
			if ( $custom_1 = $job->getAttribute('custom_drop_1') ) {
				$job->setAttribute('custom_drop_1',
						array_key_exists($custom_1, $custom_options_1) ?
								$custom_options_1[$custom_1] : $custom_1);
			}

			if ( $custom_2 = $job->getAttribute('custom_drop_2') ) {
				$job->setAttribute('custom_drop_2',
						array_key_exists($custom_2, $custom_options_2) ?
								$custom_options_1[$custom_2] : $custom_2);
			}

			if ( $custom_3 = $job->getAttribute('custom_drop_3') ) {
				$job->setAttribute('custom_drop_3',
						array_key_exists($custom_3, $custom_options_3) ?
								$custom_options_1[$custom_3] : $custom_3);
			}

			try {
				$job->save();
			} catch ( \Exception $e ) {

			}
		}
	}
}
