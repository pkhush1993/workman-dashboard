<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RedoInvoicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		if (Schema::hasTable('invoices')) {
			Schema::drop('invoices');
		}		

		Schema::create('invoices', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('job_id')->nullable();
			$table->integer('bid_id')->nullable();
			$table->string('invoice_type')->nullable();
			$table->string('down_payment')->nullable();
			$table->string('status')->nullable();
			$table->decimal('subtotal', 18, 2)->nullable();
			$table->integer('tax_rate')->nullable();
			$table->string('tax_description')->nullable();
			$table->decimal('grand_total', 18, 2)->nullable();
			$table->integer('created_by')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('invoices');
	}

}


