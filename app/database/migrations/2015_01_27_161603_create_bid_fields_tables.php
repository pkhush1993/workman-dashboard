<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBidFieldsTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bid_fields', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('name');
			$table->integer('sort_order')->nullable();
			$table->timestamps();
		});
		Schema::create('bid_field_items', function(Blueprint $table)
		{
			$table->integer('bid_field_id')->unsigned()->index();
			$table->foreign('bid_field_id')->references('id')->on('bid_fields')->onDelete('cascade');
			$table->text('description');
			$table->string('units')->nullable();
			$table->integer('price')->nullable();
			$table->integer('sort_order')->nullable();
			$table->timestamps();

		});		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bid_fields');
		Schema::drop('bid_field_items');
	}

}
