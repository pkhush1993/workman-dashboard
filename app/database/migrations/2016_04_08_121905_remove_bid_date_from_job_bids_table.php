<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveBidDateFromJobBidsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		foreach ( \App\Models\JobBid::all() as $bid ) {
			$bid->created_at = $bid->getAttribute('bid_date');
			$bid->save();
		}
		\Schema::table('job_bids', function(Blueprint $table) {
			$table->dropColumn('bid_date');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\Schema::table('job_bids', function(Blueprint $table) {
			$table->timestamp('bid_date')->after('sales_tax_total');
		});

		foreach ( \App\Models\JobBid::all() as $bid ) {
			$bid->bid_date = $bid->getAttribute('created_at');
			$bid->save();
		}
	}

}
