<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdditionalDecimalsToInvoiceTotals extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		\DB::statement(\DB::raw('ALTER TABLE `invoices` CHANGE COLUMN `subtotal` `subtotal` decimal(18,4) NOT NULL;'));
		\DB::statement(\DB::raw('ALTER TABLE `invoices` CHANGE COLUMN `tax_rate` `tax_rate` decimal(18,4) NOT NULL;'));
		\DB::statement(\DB::raw('ALTER TABLE `invoices` CHANGE COLUMN `sales_tax_total` `sales_tax_total` decimal(18,4) NOT NULL;'));
		\DB::statement(\DB::raw('ALTER TABLE `invoices` CHANGE COLUMN `grand_total` `grand_total` decimal(18,4) NOT NULL;'));
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\DB::statement(\DB::raw('ALTER TABLE `invoices` CHANGE COLUMN `subtotal` `subtotal` decimal(18,2) NOT NULL;'));
		\DB::statement(\DB::raw('ALTER TABLE `invoices` CHANGE COLUMN `tax_rate` `tax_rate` decimal(18,2) NOT NULL;'));
		\DB::statement(\DB::raw('ALTER TABLE `invoices` CHANGE COLUMN `sales_tax_total` `sales_tax_total` decimal(18,2) NOT NULL;'));
		\DB::statement(\DB::raw('ALTER TABLE `invoices` CHANGE COLUMN `grand_total` `grand_total` decimal(18,2) NOT NULL;'));
	}

}
