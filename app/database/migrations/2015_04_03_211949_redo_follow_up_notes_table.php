<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RedoFollowUpNotesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (Schema::hasTable('follow_up_notes')) {
			Schema::drop('follow_up_notes');
		}			
		Schema::create('follow_up_notes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('job_id')->references('id')->on('jobs')->onDelete('cascade')->nullable();
			$table->integer('job_note_id')->references('id')->on('job_notes')->onDelete('cascade');
			$table->string('note')->nullable();
			$table->dateTime('new_deadline')->nullable();
			$table->integer('created_by')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('follow_up_notes');
	}

}
