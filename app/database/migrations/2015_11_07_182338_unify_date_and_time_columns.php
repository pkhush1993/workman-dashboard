<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UnifyDateAndTimeColumns extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('timesheet_details', function(Blueprint $table)
		{
			$table->dropColumn('time_in');
			$table->dropColumn('time_out');
			DB::statement('ALTER TABLE timesheet_details MODIFY COLUMN date_in datetime');
			DB::statement('ALTER TABLE timesheet_details MODIFY COLUMN date_out datetime');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('timesheet_details', function(Blueprint $table)
		{
			$table->dateTime('time_in')->nullable();
			$table->dateTime('time_out')->nullable();
			DB::statement('ALTER TABLE timesheet_details MODIFY COLUMN date_in date');
			DB::statement('ALTER TABLE timesheet_details MODIFY COLUMN date_out date');
		});
	}

}
