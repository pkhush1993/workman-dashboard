<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CleanTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		if (Schema::hasTable('account_notes'))
		{
			Schema::drop('account_notes');
		}
		if (Schema::hasTable('bids'))
		{
			Schema::drop('bids');
		}
		if (Schema::hasTable('company'))
		{
			Schema::drop('company');
		}
		if (Schema::hasTable('company_notes'))
		{
			Schema::drop('company_notes');
		}
		if (Schema::hasTable('contact_notes'))
		{
			Schema::drop('contact_notes');
		}
		if (Schema::hasTable('follow_up'))
		{
			Schema::drop('follow_up');
		}
		if (Schema::hasTable('job_statuses'))
		{
			Schema::drop('job_statuses');
		}
		if (Schema::hasTable('lead_sources'))
		{
			Schema::drop('lead_sources');
		}
		if (Schema::hasTable('product_types'))
		{
			Schema::drop('product_types');
		}
		if (Schema::hasTable('products'))
		{
			Schema::drop('products');
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
