<?php

class JobBidRecalculationSeeder extends Seeder {

	public function run()
	{
		foreach ( \App\Models\JobBid::all() as $bid ) {
			$bid->recalculateTotals();
		}
	}

}