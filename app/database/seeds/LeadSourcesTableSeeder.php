<?php

class LeadSourcesTableSeeder extends \Illuminate\Database\Seeder {

	/**
	 * @var array
     */
	protected $defaultLeads = [
		'Outbound Sales',
		'Referral',
		'Employee',
		'Website',
		'Social Media',
		'Blogging',
		'Email',
		'Print Ad',
		'Mail',
		'TV',
		'Radio',
		'Conference',
		'Sponsorships',
	];

	/**
	 * Run the database table seeder
     */
	public function run()
	{
		foreach ( $this->defaultLeads as $lead )
		{
			\App\Models\Lead::create([
				'lead'	=>	$lead
			]);
		}
	}

}