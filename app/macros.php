<?php 

//Add Date/Time Inputs
Form::macro('date', function($name, $value = null, $options = array()) {
	$attributes = HTML::attributes($options);
	$input =  '<input type="date" name="' . $name . '" value="' . $value . '"'. $attributes.'>';
	return $input;
});