<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class PurgeUserPasswordsCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'passwords:purge';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Purges all user passwords and sets them to the same admin password';

	/**
	 * @var string
     */
	protected $email;

	/**
	 * @var \App\Models\User
     */
	protected $user;


	/**
	 * PurgeUserPasswordsCommand constructor.
     */
    public function __construct()
	{
		$this->user = new \App\Models\User();

		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{

		if ( $this->canPurge() ) {

			$this->email = $this->ask('What user [email] should we copy the password from?');
			$this->info('Resetting Passwords Now ...');

			foreach ( $this->getUserCollection() as $user ) {

				$this->updateUserPassword($user);
				$this->info($user->getAttribute('email').' Purged');

			}
		}
	}

	/**
	 * Get a collection of users to be purged
	 *
	 * @return mixed
     */
	protected function getUserCollection()
	{
		return $this->user->where('email', '!=', $this->email)->get();
	}

	/**
	 * Get the primary account whose password will be copied
	 *
	 * @return mixed
	 * @throws Exception
     */
	protected function getPrimaryUserAccount()
	{
		$user = $this->user->where('email', $this->email)->first();

		if ( $user ) {
			return $user;
		}

		throw new \Exception($this->email.' Not Found');
	}

	/**
	 * Resets the given users password
	 *
	 * @param \App\Models\User $user
	 *
	 * @throws Exception
     */
    protected function updateUserPassword(\App\Models\User $user)
	{
		$user::unguard();
		$user->fill([
				'password' => $this->getPrimaryUserAccount()
									->getAttribute('password')
		])->save();
		$user::reguard();
	}

	/**
	 * Verifies that we're not in production mode
	 *
	 * @return bool
	 * @throws Exception
     */
	protected function canPurge()
	{
		if ( !app()->isLocal() ) {
			throw new \Exception('This command cannot be ran while in production mode.');
		}

		return $this->confirm('Purging Passwords will reset all user passwords. Do you wish to continue? [yes|no]');
	}
}
