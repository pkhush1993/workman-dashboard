<?php namespace App\Models;

use Eloquent;

class TaxRate extends Eloquent {

	protected $guarded = array('id');
	protected $table = 'tax_rates';

	public static function getTaxRateSelect()
	{
		// Get all users that are estimators
		$_tax_rates = TaxRate::all();
			
		// Build Estimators Select Options
		$tax_rates = array('' => 'Select Tax');
		foreach ($_tax_rates as $tax_rate){
			 $tax_rates[$tax_rate->id] = $tax_rate->tax_description;
		}
		$tax_rates['custom'] = 'Enter Your Own';

		return $tax_rates;

	}



	public function getRate()
	{
		return $this->tax_rate;
	}
	
}
