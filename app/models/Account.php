<?php namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Account extends Eloquent {
	
	use SoftDeletingTrait;

	/**
	 * @var bool
     */
	public static $unguarded = true;


	/**
	 * @return mixed
     */
	public function getCompanyId()
	{
		return $this->company_id;
	}

	/**
	 * @return mixed
     */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @return mixed
     */
	public function getAddress1()
	{
		return $this->address_1;
	}

	/**
	 * @return mixed
     */
	public function getAddress2()
	{
		return $this->address_2;
	}

	/**
	 * @return mixed
     */
	public function getCity()
	{
		return $this->city;
	}

	/**
	 * @return mixed
     */
	public function getState()
	{
		return $this->state;
	}

	/**
	 * @return mixed
     */
	public function getZip()
	{
		return $this->zip;
	}

	/**
	 * @return mixed
     */
	public function getPhone()
	{
		return $this->phone;
	}

	/**
	 * @return mixed
     */
	public function getFax()
	{
		return $this->fax;
	}

	/**
	 * @return mixed
     */
	public function getMobile()
	{
		return $this->mobile;
	}

	/**
	 * @return mixed
     */
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * @return mixed
     */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @return mixed
     */
	public function getPrimaryContact()
	{
		return $this->hasOne('App\Models\Contact','primary_contact','id')->get();
	}

	/**
	 * @return mixed
     */
	public function getContacts()
	{
		return $this->hasMany('App\Models\Contact','account_id','id')->get();
	}

	public function getContactCount()
	{
		return $this->getContacts()->count();
	}

	/**
	 * @param $account_id
	 *
	 * @return string
     */
    public static function deleteAccount($account_id)
	{
		if ($account_id) {

			// Get Account
			$account = Account::find($account_id);

			// --- First Soft Delete the Contacts Attached to the Account ---

			// Get Contacts on Account
			$contacts = $account->getContacts();

			//Soft Delete Contacts
			foreach ($contacts as $contact) {
				$contact->delete();
			}

			return $account->delete();

		}
		
		return 'Account ID not Supplied';
	}

	/**
	 * @return array
     */
	public static function getAccountsInputOptions()
	{
		// Get Accounts
		$_accounts = Account::orderBy('name', 'ASC')->get();


		$accounts = array( '' => 'Select Accounts');
		foreach ($_accounts as $account){
			 $accounts[$account->id] = $account->name;
		}

		return $accounts;

	}

}












