<?php namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\SoftDeletingTrait;
use App\Models\Profile;
use App\Models\Contact;

class JobNote extends Eloquent {

	use SoftDeletingTrait;

	/**
	 * @var array
     */
	protected $guarded = array('id');

	/**
	 * @return \App\Models\Job
     */
	public function job()
	{
	 	return $this->belongsTo('App\Models\Job','job_id')->first();
	}

	public function jobData()
	{
		return $this->belongsTo('App\Models\Job','job_id');
	}

	/**
	 * @return \App\Models\FollowUpNote
     */
	public function notes()
	{
		return $this->hasMany('App\Models\FollowUpNote','job_note_id','id')->get();
	}

	public function notesData()
	{
		return $this->hasMany('App\Models\FollowUpNote','job_note_id','id');
	}

	/**
	 * @return \App\Models\Contact
     */
	public function contact_profile()
	{
		return $this->hasOne('App\Models\Contact', 'id', 'job_contacts_id')->withTrashed()->first();
	}

	/**
	 * @return \App\Models\Profile
     */
	public function user_profile()
	{
		return $this->belongsTo('App\Models\Profile', 'assigned')->first();
	}

	/**
	 * @return \App\Models\Contact
	 */
	public function contact_profile_data()
	{
		return $this->hasOne('App\Models\Contact', 'id', 'job_contacts_id')->withTrashed();
	}

	/**
	 * @return \App\Models\Profile
	 */
	public function user_profile_data()
	{
		return $this->belongsTo('App\Models\Profile', 'assigned');
	}

	/**
	 * @return \App\Models\Profile
     */
	public function created_profile()
	{
		return $this->belongsTo('App\Models\Profile', 'created_by')->first();
	}

	public function created_profile_data()
	{
		return $this->belongsTo('App\Models\Profile', 'created_by');
	}

}
