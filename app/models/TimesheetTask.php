<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TimesheetTask extends Model {

	protected $table = 'timesheet_tasks';

	/**
	 * The attributes that aren't mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = array('id');

	public static function getTaskSelect()
	{
		// Get all users that are estimators
		$tasks = TimesheetTask::lists('task', 'task');

		return $tasks;
	}

}