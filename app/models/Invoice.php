<?php namespace App\Models;
use Eloquent;

class Invoice extends Eloquent {
	
	protected $table = 'invoices';
	protected $guarded = array('id');
	

	public function getCreatedByProfile()
	{
		return $this->hasOne('App\Models\Profile','id','created_by')->first();
	}

	public function job()
	{
		return $this->hasOne('App\Models\Job','id','job_id')->first();
	}		


}