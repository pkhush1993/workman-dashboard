<?php namespace App\Models;
use Eloquent;


class BidFieldItem extends Eloquent {

	// protected $primaryKey = 'customer_id'
	// protected $table = '';
	public $incrementing = false;
	public static $unguarded = true;


	/**
	 * @return mixed
     */
	public function bidField()
    {
        return $this->hasOne('BidField');
    }

	/**
	 * @return mixed
     */
	public function getBidFieldId()
	{
		return $this->bid_field_id;
	}

	/**
	 * @return mixed
     */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @return mixed
     */
	public function getUnits()
	{
		return $this->units;
	}

	/**
	 * @return mixed
     */
	public function getPrice()
	{
		return $this->price;
	}

	/**
	 * @return mixed
     */
	public function getSortOrder()
	{
		return $this->sort_order;
	}


	/**
	 * @param $bid_field_id
	 *
	 * @return array
     */
    public static function getBidFieldItemsInputOptions($bid_field_id)
	{

		// If the Bid Field is Not Custom Grab the Items with that Category
		if (BidField::find($bid_field_id)) {
			// Get Bid Field Items
			$_bid_field_items = BidField::find($bid_field_id)->getBidFields();

			//Setup Bid Field Items Dropdown
			$bid_field_items = array( '' => 'Select Product/Service');
			$bid_field_items['custom'] = 'Enter Your Own';
			foreach ($_bid_field_items as $bid_field_item){
				 $bid_field_items[$bid_field_item->id] = $bid_field_item->description;
			}
		}
		// If it Is Custom Setup a Blank Array
		else{
			$bid_field_items = array( '' => 'Select Product/Service');
			$bid_field_items['custom'] = 'Enter Your Own';
		}

		return  $bid_field_items;

	}


}
