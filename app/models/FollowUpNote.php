<?php namespace App\Models;

use Eloquent;
use App\Models\Profile;

class FollowUpNote extends Eloquent {
	// protected $primaryKey = 'customer_id'
	// protected $table = '';
	protected $guarded = array('id');

	public function user_profile()
	{
		return $this->belongsTo('App\Models\Profile', 'created_by')->first();
	}

}


