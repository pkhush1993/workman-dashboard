<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class JobBid
 *
 * @package App\Models
 */
class JobBid extends Model {

	/**
	 * The attributes that are not mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = array('id');

	/**
	 * Find a model by its primary key.
	 *
	 * @param  mixed  $id
	 * @param  array  $columns
	 * @return \Illuminate\Support\Collection|static|null
	 */
	public static function find($id, $columns = array('*'))
	{
		$instance = parent::find($id, $columns);

		return $instance->recalculateTotals();
	}


	/**
	 * Get the job associated with bid
	 *
	 * @return \Illuminate\Database\Eloquent\Collection
     */
	public function job()
    {
        return $this->belongsTo('App\Models\Job')->withTrashed()->first();
    }

	/**
	 * Get the associated bid line items
	 *
	 * @return \Illuminate\Database\Eloquent\Collection
     */
	public function getLineItems()
	{
		return $this->hasMany('App\Models\JobBidLineItems','bid_id','id')->get();
	}


	/**
	 * Get the associated comments for a bid
	 *
	 * @return mixed
     */
	public function getBidComments()
	{
		return $this->hasMany('App\Models\BidComment','bid_id','id')->get();
	}

	/**
	 * Recalculate a bids amounts
	 *
	 * @return void
     */
	public function recalculateTotals()
	{
		if ( $this->getLineItems() ) {

			$total = 0;
			$taxAmount = 0;

			foreach ( $this->getLineItems() as $lineItem ) {
				
				$lineTotal = (float)$lineItem->getAttribute('quantity') * (float)$lineItem->getAttribute('rate');
				$total += $lineTotal;

				if ( $lineItem->getAttribute('taxable') == 1 ) {
					$taxAmount += $lineTotal * $this->getAttribute('tax_rate');
				}
			}

			$taxAmount *= .01;

			/** Self Update */
			$this->fill([
				'grand_total' 		=> $total + $taxAmount,
				'subtotal'    		=> $total,
				'sales_tax_total'	=> $taxAmount
			])->save();
		}

		return $this;
	}


}
