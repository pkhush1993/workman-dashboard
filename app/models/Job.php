<?php namespace App\Models;

use Sentinel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Job extends Model {

	use SoftDeletingTrait;

	/**
	 * @var bool
     */
	protected $softDelete = true;

	/**
	 * @var array
     */
	protected $guarded = array('id');

	/**
	 * @var Job
	 */
	protected static $instance;

	/**
	 * Job constructor.
	 * @param array $attributes
	 */
	public function __construct(array $attributes = array())
	{
		parent::__construct($attributes);

		self::$instance = $this;
	}


	/**
	 * @return mixed
     */
	public function account()
	{
	 return $this->hasOne('App\Models\Account','id','account')->withTrashed()->first();
	}

	/**
	 * @return mixed
     */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @return mixed
     */
	public function getEstimator()
	{
		return $this->hasOne('App\Models\Profile','id','estimator')->first();
	}

	/**
	 * @return mixed
     */
	public function getProjectManager()
	{
		return $this->hasOne('App\Models\Profile','id','project_manager')->first();
	}

	/**
	 * @return mixed
     */
	public function getForeman()
	{
		return $this->hasOne('App\Models\Profile','id','foreman')->first();
	}

	/**
	 * @return mixed
     */
	public function getAccountant()
	{
		return $this->hasOne('App\Models\Profile','id','accountant')->first();
	}


	/**
	 * @return mixed
     */
	public function getJobNumber()
	{
		return $this->job_number;
	}

	/**
	 * @return array
     */
	public static function getJobsSelect()
	{
		// Get all users that are estimators
		$_jobs = Job::all();

		// Check If There are Jobs
		if (!$_jobs->isEmpty()) {

			// Build Jobs Select Options
			$jobs = array('' => 'Select Job');
			foreach ($_jobs as $job){
				 $jobs[$job->id] = '#'.$job->job_number .' - '. $job->name;
			}

			return $jobs;
		}
	}

	/**
	 * @param null $all
	 *
	 * @return array
     */
    public static function getEstimatorsSelect($all = NULL)
	{
		// Get all users that are estimators
		$_estimators = User::getUsersByRoleSlug('estimator');

		// Check If There are Users Set as Estimators
		if (!$_estimators->isEmpty()) {

			// Build Estimators Select Options
			$estimators = array('' => 'Select Estimator');

			// If option for All is true prepend an "ALL" option to the array
			if ($all) {
				$estimators['all'] = 'All';
			}

			foreach ($_estimators as $estimator){
				 $estimators[$estimator->id] = $estimator->getFullName();
			}

			return $estimators;
		}
	}

	/**
	 * @param null $all
	 *
	 * @return array
     */
    public static function getProjectManagersSelect($all = NULL)
	{
		// Get all users that are Project Managers
		$_project_managers = User::getUsersByRoleSlug('project_manager');

		// Check If There are Users Set as Project Managers
		if (!$_project_managers->isEmpty()) {

			// Build Project Managers Select Options
			$project_managers = array( '' => 'Select Project Manager');

			// If option for All is true prepend an "ALL" option to the array
			if ($all) {
				$project_managers['all'] = 'All';
			}

			foreach ($_project_managers as $project_manager){

				 $project_managers[$project_manager->id] = $project_manager->getFullName();
			}

			return $project_managers;
		}
	}

	/**
	 * @param null $all
	 *
	 * @return array
     */
    public static function getForemenSelect($all = NULL)
	{
		// Get all users that are Foreman

		$_foremen = User::getUsersByRoleSlug('foreman');

		// Check If There are Users Set as Foreman
		if (!$_foremen->isEmpty()) {

			// Build Foreman Select Options
			$foremen = array('' => 'Select Foreman');

			// If option for All is true prepend an "ALL" option to the array
			if ($all) {
				$foremen['all'] = 'All';
			}

			foreach ($_foremen as $foreman){
				 $foremen[$foreman->id] = $foreman->getFullName();
			}
			return $foremen;
		}

	}

	/**
	 * @param null $all
	 *
	 * @return array
     */
    public static function getAccountingSelect($all = NULL)
	{
		// Get all users that are Salesman

		$_accountants = User::getUsersByRoleSlug('accounting');

		// Check If There are Users Set as Salesman
		if (!$_accountants->isEmpty()) {

			// Build Salesman Select Options
			$accountants = array('' => 'Select Accountant');

			// If option for All is true prepend an "ALL" option to the array
			if ($all) {
				$accountants['all'] = 'All';
			}

			foreach ($_accountants as $accounting){
				 $accountants[$accounting->id] = $accounting->getFullName();

			}

			return $accountants;
		}

	}

	/**
	 * @return array
     */
	public static function getSalesmanSelect()
	{
		// Get all users that are Salesman

		$_salesmen = User::getUsersByRoleSlug('salesman');

		// Check If There are Users Set as Salesman
		if (!$_salesmen->isEmpty()) {

			// Build Salesman Select Options
			$salesmen = array('' => 'Select Foreman');
			foreach ($_salesmen as $salesman){
				 $salesmen[$salesman->id] = $salesman->getFullName();

			}

			return $salesmen;
		}

	}

	/**
	 * @param $completed_date
	 *
	 * @return string
     */
    public static function getDaysSinceJobCompletion($completed_date)
	{
		if ( date('Y-m-d', strtotime($completed_date)) <= date('Y-m-d') ){
			$today = new \DateTime();
			$completed_date = new \DateTime( date('Y-m-d', strtotime($completed_date)) );
        	$past_due = $today->diff($completed_date)->format("%a");
        	return '<span style="color:red">'. $past_due. ' Days Since Job Completion </span>';
		}
		else{
			return 'Due on '.date('Y-m-d', strtotime($completed_date));
		}
	}


	/**
	 * @param int $company_id
	 *
	 * @return array
     */
    public static function getCustomDropOptions($company_id = 1)
	{
		//Get Company
		$company = Company::find($company_id);

		//Set Default Option
		$default = ['' => 'Select Field'];

		//Setup Array
		$custom_drops = [];

		//Custom Drop 1
		if ($company->getCustomDrop1()) {
			$options = $company->getCustomDrop1()[key($company->getCustomDrop1())];
			$current = self::$instance ? self::$instance->getAttribute('custom_drop_1') : '';
			if ( !array_key_exists($current, $options)) {
				$options[$current] = $current;
			}

			$custom_drop_1 =  $default + array_combine($options, $options);
			$custom_drops['custom_drop_1'] = $custom_drop_1;
		}

		//Custom Drop 2
		if ($company->getCustomDrop2()) {
			$options = $company->getCustomDrop2()[key($company->getCustomDrop2())];
			$current = self::$instance ? self::$instance->getAttribute('custom_drop_2') : '';
			if ( !array_key_exists($current, $options)) {
				$options[$current] = $current;
			}

			$custom_drop_2 =  $default + array_combine($options, $options);
			$custom_drops['custom_drop_2'] = $custom_drop_2;
		}

		//Custom Drop 3
		if ($company->getCustomDrop3()) {
			$options = $company->getCustomDrop3()[key($company->getCustomDrop3())];
			$current = self::$instance ? self::$instance->getAttribute('custom_drop_3') : '';
			if ( !array_key_exists($current, $options)) {
				$options[$current] = $current;
			}

			$custom_drop_3 =  $default + array_combine($options, $options);
			$custom_drops['custom_drop_3'] = $custom_drop_3;
		}


		return $custom_drops;
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany|JobContact
     */
	public function getContacts()
	{
		return $this->hasMany(JobContact::class, 'job_id', 'id');
	}

	/**
	 * @return array|bool
     */
	public function getActiveJobContactsSelect()
	{
		// Get all users that are Salesman

		$_job_contacts = $this->belongsToMany('App\Models\Contact', 'job_contacts', 'job_id', 'contact_id')->withPivot('id')->whereNull('job_contacts.deleted_at')->get();


		// Check If There are Users Set as Salesman
		if (!$_job_contacts->isEmpty()) {

			// Build Salesman Select Options
			$job_contacts = array('0' => 'Select Contact');
			foreach ($_job_contacts as $job_contact){
				 $job_contacts[$job_contact->id] = $job_contact->first_name.' '.$job_contact->last_name;

			}

			return $job_contacts;
		}
		else{
			return false;
		}

	}

	/**
	 * @param null $column
	 * @param null $failed
	 *
	 * @return null
     */
    public function getBid($column = NULL, $failed = NULL)
	{

		if ($this->hasOne('App\Models\JobBid','job_id','id')->first()) {
			if ($column) {
				return $this->hasOne('App\Models\JobBid','job_id','id')->first()->$column;
			}
			else{
				return $this->hasOne('App\Models\JobBid','job_id','id')->first();
			}
		}

		return $failed;

	}

	/**
	 * Return the job bid instance, if it exists
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne|JobBid
     */
	public function bid()
	{
		return $this->hasOne(JobBid::class, 'job_id', 'id');
	}

	/**
	 * @return mixed
     */
	public function getBids()
	{
		return $this->hasMany('App\Models\JobBid','job_id','id')->get();
	}

	/**
	 * @return array
     */
	public function labor()
	{
		$labor = 0;
		$hours = 0;
		// Check for Time Entries and Calculate
		if ($entries = $this->hasMany('App\Models\TimesheetDetail','job_id','id')->get()) {
			foreach ($entries as $entry){
				if ( User::find($entry->user_id) ) {
					$wage_rate = User::find($entry->user_id)->profile()->wage_rate ? User::find($entry->user_id)->profile()->wage_rate : 1;
					$labor += $entry->total * $wage_rate;
					$hours += $entry->total;
				}
			}
			return ['cost'=> $labor, 'hours' => $hours];
		}
		else{
			return ['cost'=> $labor, 'hours' => $hours];
		}
	}

	/**
	 * @return mixed
     */
	public function timeEntries()
	{
		return $this->hasMany('App\Models\TimesheetDetail','job_id','id')->get();
	}


	// Get Bid Totals for Jobs Depending on Status
	/**
	 * @param $status
	 *
	 * @return array
     */
    public static function getJobsBidTotalByStatus($status)
	{
		$grand_total = 0;

		foreach (Job::where('status',$status)->get() as $job)
		{
			if ($bid = $job->getBid()) {
		    	$grand_total += $bid->grand_total;
			}
		}
		return array('grand_total'=>$grand_total,'job_count'=>count(Job::where('status',$status)->get()));
	}

	/**
	 * @return mixed
     */
	public function getAccount()
	{
		return $this->hasOne('App\Models\Account','id','account')->first();
	}

	/**
	 * @return mixed
     */
	public function getPrimaryContact()
	{
		return $this->hasOne('App\Models\Contact','id','primary_contact')->first();
	}


	/**
	 * @return mixed
     */
	public function getActiveJobContacts()
	{
		return $this->belongsToMany('App\Models\Contact', 'job_contacts', 'job_id', 'contact_id')->withPivot('id')->whereNull('job_contacts.deleted_at')->get();
	}

	/**
	 * @return mixed
     */
	public function getInactiveJobContacts()
	{
		return $this->belongsToMany('App\Models\Contact', 'job_contacts', 'job_id', 'contact_id')->withPivot('id')->whereNotNull('job_contacts.deleted_at')->get();
	}

	/**
	 * @return mixed
     */
	public function getJobContacts()
	{
		return $this->belongsToMany('App\Models\Contact', 'job_contacts', 'job_id', 'contact_id')->withPivot('id')->get();
	}

	/**
	 * @return mixed
     */
	public function getInvoices()
	{
		return $this->hasMany('App\Models\Invoice','job_id','id')->get();
	}

	/**
	 * @return mixed
     */
	public function getRework()
	{
		return $this->hasMany('App\Models\JobRework','job_id','id')->get();
	}

	/**
	 * @return mixed
     */
	public function getNotes()
	{
		return $this->hasMany('App\Models\JobNote','job_id','id')->get();
	}

	/**
	 * @return mixed
     */
	public function getAccountingNotes()
	{
		return $this->hasMany('App\Models\JobNote','job_id','id')->where('type','accounting')->get();
	}

	/**
	 * @return mixed
     */
	public function getEstimatingNotes()
	{
		return $this->hasMany('App\Models\JobNote','job_id','id')->where('type','estimating')->get();
	}

	/**
	 * @return mixed
     */
	public function getProductionNotes()
	{
		return $this->hasMany('App\Models\JobNote','job_id','id')->where('type','production')->get();
	}

	/**
	 * @return mixed
     */
	public function getHiddenNotes()
	{
		return $this->hasMany('App\Models\JobNote','job_id','id')->onlyTrashed()->get();
	}

	/**
	 * Create a job bid
	 *
	 * @return $this
	 */
	public function attachNewBid()
	{
		if ( $jobBid = $this->getBid() ) {
			return $jobBid;
		}

		return JobBid::create([
				'job_id'          => $this->getKey(),
				'status'          => 'open',
				'subtotal'        => 0,
				'grand_total'     => 0,
				'tax_rate'        => 0,
				'tax_description' => 0,
				'sales_tax_total' => 0,
		]);
	}

}
