<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model {

	/**
	 * @var string
     */
	protected $table = 'company_settings';

	/**
	 * @return mixed
     */
	public function getName()
	{
		return $this->getAttribute('name');
	}

	/**
	 * @return mixed
     */
	public function getAddress1()
	{
		return $this->getAttribute('address_1');
	}

	/**
	 * @return mixed
     */
	public function getAddress2()
	{
		return $this->getAttribute('address_2');
	}

	/**
	 * @return mixed
     */
	public function getCity()
	{
		return $this->getAttribute('city');
	}

	/**
	 * @return mixed
     */
	public function getState()
	{
		return $this->getAttribute('state');
	}

	/**
	 * @return mixed
     */
	public function getZip()
	{
		return $this->getAttribute('zip');
	}

	/**
	 * @return mixed
     */
	public function getEmail()
	{
		return $this->getAttribute('email');
	}

	/**
	 * @return mixed
     */
	public function getWebsite()
	{
		return $this->getAttribute('website');
	}

	/**
	 * @return mixed
     */
	public function getFax()
	{
		return $this->getAttribute('fax');
	}

	/**
	 * @return mixed
     */
	public function getPhone()
	{
		return $this->getAttribute('phone');
	}

	/**
	 * @return mixed
     */
	public function getMobile()
	{
		return $this->getAttribute('mobile');
	}

	/**
	 * @return mixed
     */
	public function getWageRate()
	{
		return $this->getAttribute('wage_rate');
	}

	/**
	 * @return mixed
     */
	public function getAvatar()
	{
		return $this->getAttribute('avatar');
	}

	/**
	 * @return mixed
     */
	public function getBidHeader()
	{
		return $this->getAttribute('bid_header');
	}

	/**
	 * @return mixed
     */
	public function getContractLanguage()
	{
		return $this->getAttribute('contract_language');
	}

	/**
	 * @return mixed
     */
	public function getNextJob()
	{
		return $this->getAttribute('next_job_number');
	}

	/**
	 * @return mixed
     */
	public function getNextInvoice()
	{
		return $this->getAttribute('next_invoice_number');
	}

	/**
	 * @return mixed
     */
	public function getPayPeriod()
	{
		return $this->getAttribute('pay_period_type');
	}

	/**
	 * @return mixed
     */
	public function getTimeEntryGracePeriod()
	{
		return $this->getAttribute('time_entry_grace_period');
	}

	/**
	 * @return mixed
     */
	public function getPayPeriodStart()
	{
		return $this->getAttribute('pay_period_start');
	}

	/**
	 * @return mixed
     */
	public function getPayPeriodStart1()
	{
		return $this->getAttribute('pay_period_start_1');
	}

	/**
	 * @return mixed
     */
	public function getPayPeriodStart2()
	{
		return $this->getAttribute('pay_period_start_2');
	}

	/**
	 * @return array
     */
	public function getCurrentPayPeriod()
	{

		// Get Company
		$company = Company::find(1);

		// Set Date Variables
		$month = date('m');
		$day   = date('d');
		$year  = date('Y');

		// Bi-Monthly Pay Period Dates
		if ( $company->getPayPeriod() == 'bi-monthly') {

			// If Today is Greater Than the First Pay Period Use the 2nd Pay Period
			if($day > $company->getPayPeriodStart1()){

				$next_pay_period_start = date('Y-m-d',strtotime($year.$month.$company->getPayPeriodStart2()));

				$next_pay_period_end = date('Y-m-d',strtotime($year.$month++.$company->getPayPeriodStart1().'- 1 day + 1 month'));
			}
			else{
				$next_pay_period_start = date('Y-m-d',strtotime($year.$month++.$company->getPayPeriodStart1()));
			}
			return array('start_date' => $next_pay_period_start,'end_date' => $next_pay_period_end );

		}
		// Two Weeks
		else{
			if ($company->getPayPeriodStart() == strtotime(date('Y-m-d'), 0)) {

				// Add Week Difference to Original Date to Get the Next Pay Period End Date
				$end_date =  date('Y-m-d',strtotime($company->getPayPeriodStart().'+ 13 days'));

				// Subtract 2 Weeks from End Date to Get Start Date
				$start_date = date('Y-m-d',strtotime($company->getPayPeriodStart()));
			}
			else{
				// Get the Difference Between the Start Pay Period Date and The Current Date
				$diff = strtotime(date('Y-m-d').'+1 day', 0) - strtotime($company->getPayPeriodStart(), 0);
				$week_difference = ceil($diff / 604800);

				//If the number of weeks is Odd Round Up to the Nearest Even Week
				if ($week_difference % 2 != 0) {
					$week_difference++;
				}

				// Add Week Difference to Original Date to Get the Next Pay Period End Date
				$end_date =  date('Y-m-d',strtotime($company->getPayPeriodStart().'+'.$week_difference.'weeks - 1 day' ));

				// Subtract 2 Weeks from End Date to Get Start Date
				$start_date = date('Y-m-d',strtotime($end_date.'- 13 days'));
			}

			return array('period_start' => $start_date,'period_end' => $end_date );

		}
	}

	/**
	 * @return array
     */
	public function getLastPayPeriod()
	{

		// Get Company
		$company = Company::find(1);

		// Set Date Variables
		$month = date('m');
		$day   = date('d');
		$year  = date('Y');

		// Bi-Monthly Pay Period Dates
		if ( $company->getPayPeriod() == 'bi-monthly') {

			// If Today is Greater Than the First Pay Period Use the 1st Pay Period
			if($day > $company->getPayPeriodStart1()){
				$next_pay_period_start = date('Y-m-d',strtotime($year.$month++.$company->getPayPeriodStart1()));
				$next_pay_period_end = date('Y-m-d',strtotime($year.$month++.$company->getPayPeriodStart2().'- 1 day'));
			}
			// If Today is less than the 2nd Pay Period Use the Second
			else{
				$next_pay_period_start = date('Y-m-d',strtotime($year.$month.$company->getPayPeriodStart2()));
				$next_pay_period_end = date('Y-m-d',strtotime($year.$month++.$company->getPayPeriodStart1().'- 1 day + 1 month'));
			}
			return array('start_date' => $next_pay_period_start,'end_date' => $next_pay_period_end );

		}
		// Two Weeks
		else{
			if (strtotime($company->getPayPeriodStart()) == strtotime(date('Y-m-d'), 0)) {

				// Add Week Difference to Original Date to Get the Next Pay Period End Date
				$end_date =  date('Y-m-d',strtotime($company->getPayPeriodStart().'- 1 days'));

				// Subtract 2 Weeks from End Date to Get Start Date
				$start_date = date('Y-m-d',strtotime($company->getPayPeriodStart().'- 13 days'));
			}
			else{
				// Get the Difference Between the Start Pay Period Date and The Current Date
				$diff = strtotime(date('Y-m-d').'+1 day', 0) - strtotime($company->getPayPeriodStart(), 0);
				$week_difference = ceil($diff / 604800);

				//If the number of weeks is Odd Round Up to the Nearest Even Week
				if ($week_difference % 2 != 0) {
					$week_difference++;
				}

				// Add Week Difference to Original Date to Get the Next Pay Period End Date
				$end_date =  date('Y-m-d',strtotime($company->getPayPeriodStart().'+'.$week_difference.'weeks - 1 day - 2 weeks' ));

				// Subtract 2 Weeks from End Date to Get Start Date
				$start_date = date('Y-m-d',strtotime($end_date.'- 13 days'));
			}

			return array('period_start' => $start_date,'period_end' => $end_date );
		}
	}


	/**
	 * @return mixed
     */
	public function getCustomDrop1()
	{
		return unserialize($this->getAttribute('custom_drop_1'));
	}

	/**
	 * @return mixed
     */
	public function getCustomDrop2()
	{
		return unserialize($this->getAttribute('custom_drop_2'));
	}

	/**
	 * @return mixed
     */
	public function getCustomDrop3()
	{
		return unserialize($this->getAttribute('custom_drop_3'));
	}

	/**
	 * @return mixed
     */
	public function getCustomText1()
	{
		return $this->getAttribute('custom_text_1');
	}

	/**
	 * @return mixed
     */
	public function getCustomText2()
	{
		return $this->getAttribute('custom_text_2');
	}

	/**
	 * @return mixed
     */
	public function getCustomText3()
	{
		return $this->getAttribute('custom_text_3');
	}

	/**
	 * @return array
     */
	public function getJobDefaults()
	{
		return explode(',', $this->getAttribute('job_defaults'));
	}

	/**
	 * @return mixed
     */
	public function getStatus()
	{
		return $this->getAttribute('status');
	}

	/**
	 * @return mixed
     */
	public function getType()
	{
		return $this->getAttribute('type');
	}

	/**
	 * @param null $column
	 * @return mixed
     */
	public function getCompanyInfo($column = NULL)
	{
		if ( is_null($column) ) {
			return $this->getAttributes();
		}
		return $this->getAttribute($column);
	}
}
