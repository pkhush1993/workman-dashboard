<?php namespace App\Models;

use Eloquent;

class Activity extends Eloquent {

	protected $guarded = array('id');
	protected $table = 'activities';

	public static function getActivitiesSelect()
	{
		// Get all users that are Salesman
		
		$_activities = Activity::get(['activity']);

		// Check If There are Default Activities Set
		if (!$_activities->isEmpty()) {

			// Build Activities Select Options
			$activities = array('' => 'Select Activity');
			foreach ($_activities as $activity){
				 $activities[$activity->activity] = $activity->activity;
				
			}

			$activities['custom'] = 'Enter Your Own';
			
			return $activities;
		}
		else{
			return false;
		}

	}	

}
