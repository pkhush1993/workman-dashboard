<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Lead extends Model
{

    use SoftDeletingTrait;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'lead_sources';

    /**
     * Indicates if all mass assignment is enabled.
     *
     * @var bool
     */
    public static $unguarded = true;
}