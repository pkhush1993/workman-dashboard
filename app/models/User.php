<?php
namespace App\Models;
use Eloquent,Sentinel,DB;
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Illuminate\Database\Eloquent\Model;

class User extends Model implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');


	public function profile()
	{
		return $this->belongsTo('App\Models\Profile', 'id')->firstOrFail();
	}

	public function roles()
	{

		return $this->belongsToMany('Cartalyst\Sentinel\Roles\EloquentRole', 'role_users', 'user_id', 'role_id')->get();
	}

	public function getFullName()
	{
		// Fallbacks for Name Function
		if ( ($this->profile()->first_name) AND ($this->profile()->last_name) ) {
			return $this->profile()->first_name.' '.$this->profile()->last_name;
		}
		if ( ($this->profile()->first_name) AND (!$this->profile()->last_name) ) {
			return $this->profile()->first_name;
		}
		elseif ( ($this->profile()->last_name) AND (!$this->profile()->first_name) ) {
			return $this->profile()->last_name;
		}
		else{
			return 'No Name';
		}
	}


	public static function getUsersSelect()
	{
		// Get all active users
		$_users = User::whereNotNull('last_login')->get();

		// Check If There are Users
		if (!$_users->isEmpty()) {

			// Build Salesman Select Options
			$users = array('' => 'Select User');
			foreach ($_users as $user){

				// Remove Time Entry Users
				if (!Sentinel::findById($user->id)->inRole('time_entry')) {
					$users[$user->id] = $user->profile()->first_name.' '.$user->profile()->last_name;
				}
			}

			return $users;
		}

	}

	public static function getEmployeeSelect($role = 'all')
	{
		// Get all  users
		if ($role == 'all') {
			$_users = User::all();
		}
		elseif ($role == 'project_manager') {
			$_users = User::getUsersByRoleSlug('project_manager');
		}
		elseif ($role == 'estimator') {
			$_users = User::getUsersByRoleSlug('estimator');
		}
		elseif ($role == 'foreman') {
			$_users = User::getUsersByRoleSlug('foreman');
		}
		elseif ($role == 'accounting') {
			$_users = User::getUsersByRoleSlug('accounting');
		}

		// Check If There are Users
		if (!$_users->isEmpty()) {

			// Build Salesman Select Options
			$users = array('all' => 'All Employees');
			foreach ($_users as $user){

				// Remove Time Entry Users
				if (!Sentinel::findById($user->id)->inRole('time_entry')) {
					$users[$user->id] = $user->profile()->first_name.' '.$user->profile()->last_name;
				}
			}

			return $users;
		}

	}

	public static function checkForUserRole($user_id,$role)
	{
		// Make Sure the User Exist
		if ( ($user = Sentinel::findById($user_id)) ) {
			if ($user->inRole($role)) {
				return true;
			}
			return false;
		}
		else{
			return false;
		}
	}


	public function getUserJobsByRole($role,$status = NULL,$start_date = NULL,$end_date = NULL)
	{
		// Get Jobs By Status and Date Range
		if ($status AND $start_date AND $end_date) {

			return $this->hasMany('App\Models\Job',$role,'id')->whereIn('status',$status)->where('completed_date','>=',date('Y-m-d', strtotime($start_date)))->where('completed_date','<=',date('Y-m-d', strtotime($end_date)))->orderBy('completed_date', 'asc')->get();
		}
		// If Date Range is NOT Defined Get Jobs By Status
		elseif ($status){
			return $this->hasMany('App\Models\Job',$role,'id')->whereIn('status',$status)->get();
		}
		// If Just Role is Defined
		else{
			return $this->hasMany('App\Models\Job',$role,'id')->get();
		}
	}

	/**
	 * Through Sentinel Get Users by Role and then Return the Users using the User Model
	 *
	 * @param string $slug
	 * @return User
	 */
	public static function getUsersByRoleSlug($slug)
	{

		$users = Sentinel::findRoleBySlug($slug)->users()->get();

		$user_ids = [];

		foreach ($users as $user) {
			$user_ids[] = $user->id;
		}

		return User::find($user_ids);

	}

}
