<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BidComment extends Model {

    /**
     * @var array
     */
    protected $guarded = array('id');

    /**
     * @return mixed
     */
    public function bid()
    {
        return $this->belongsTo('JobBid');
    }


}