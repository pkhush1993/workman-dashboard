<?php namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class JobContact extends Eloquent {

	use SoftDeletingTrait;
	protected $guarded = array('id');
	// protected $primaryKey = 'customer_id'
	// protected $table = '';

	// Contact model
	public function contacts()
	{
	 return $this->belongsToMany('App\Models\Contact');
	}

	// Contact model
	public function contact()
	{
	 return $this->belongsTo('App\Models\Contact');
	}

}
