<?php namespace App\Models;

use Eloquent;
use App\Models\Account;
use App\Models\DB;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Contact extends Eloquent {

	use SoftDeletingTrait;

	public static $unguarded = true;
	// protected $primaryKey = 'customer_id'
	// protected $table = '';


	// Contact model
	public function jobContact()
	{
	 return $this->belongsTo('App\Models\JobContact');
	}

	// Contact model
	public function account()
	{
	 return $this->belongsTo('App\Models\Account')->first();
	}


	public function getFullName()
	{
		// Fallbacks for Name Function
		if ( ($this->first_name) AND ($this->last_name) ) {
			return $this->first_name.' '.$this->last_name;
		}
		if ( ($this->first_name) AND (!$this->last_name) ) {
			return $this->first_name;
		}
		elseif ( ($this->last_name) AND (!$this->first_name) ) {
			return $this->last_name;
		}
		else{
			return 'No Name';
		}
	}

	public function getCompanyId()
	{
		return $this->company_id;
	}
	public function getAccountId()
	{
		return $this->account_id;
	}

	public function getFirstName()
	{
		return $this->first_name;
	}
	public function getLastName()
	{
		return $this->last_name;
	}

	public function getNotes()
	{
		return $this->notes;
	}
	public function getTitle()
	{
		return $this->title;
	}

	public function getAddress1()
	{
		return $this->address_1;
	}

	public function getAddress2()
	{
		return $this->address_2;
	}

	public function getCity()
	{
		return $this->city;
	}

	public function getState()
	{
		return $this->state;
	}

	public function getZip()
	{
		return $this->zip;
	}

	public function getPhone()
	{
		return $this->phone;
	}

	public function getFax()
	{
		return $this->fax;
	}

	public function getAccount()
	{
		return $this->account_id;
	}

	public function getMobile()
	{
		return $this->mobile;
	}

	public function getEmail()
	{
		return $this->email;
	}

	public function getStatus()
	{
		return $this->status;
	}

	public static function getContactsInputOptions($account_id = null,$contact_ids  = null)
	{
		// All Contacts For A Specific Account
		if ($account_id) {
			$_contacts = Contact::where('account_id', '=', $account_id)->get();
		}
		// If Account ID not Specified Get All Contacts
		else{
			$_contacts = Contact::get(['id', 'first_name', 'last_name']);
		}
		if (!$_contacts->isEmpty()) {
			$contacts = array( '' => 'Select Contact');
			foreach ($_contacts as $contact){
				 $contacts[$contact->id] = $contact->first_name.' '.$contact->last_name;
			}

			// Check for Additional Contacts to Add on the Fly
			if ($contact_ids) {
				foreach ($contact_ids as $contact_id) {
					// Make Sure Contact is Not Already in The Array and that it Exists
					if ( (!array_key_exists($contact_id, $contacts)) AND ($contact_info = Contact::where('id', '=', $contact_id)->first())  ) {
						$contacts[$contact_id] = $contact_info->first_name.' '.$contact_info->last_name;
					}
				}
			}

			return $contacts;
		}

		return $contacts = array( '' => 'No Contacts Created');

	}

	public static function getContactsInAccount($account_id)
	{
		$contacts = Contact::where('account_id', '=', $account_id)->get();

		// Attach Account Name to Contacts
		foreach ($contacts as $contact){

			$contact->account_name =  Account::find($contact->account_id)->getName();

		}

		if ($contacts) {
			return $contacts;
		}
		else{
			return 'No contacts attached to this account';
		}

	}


	// Get the Contacts that are not already attached to a job
	public static function getAvailibleContactsInAccount($account_id,$job_id)
	{

		$availible_contacts = Contact::where('account_id', '=', $account_id)->whereNotIn('id', function ($query) use($job_id)
	    {
	        $query->select('contact_id')
	        	->whereNull('deleted_at')
	        	->where('job_id',$job_id)
	         	->from('job_contacts');
	    })->get();


		// Attach Account Name to Contacts
		foreach ($availible_contacts as $availible_contact){

			$availible_contact->account_name =  Account::find($availible_contact->account_id)->getName();

		}

		if ($availible_contacts) {
			return $availible_contacts;
		}
		else{
			return 'No Available Contacts';
		}

	}

	//Get the Archive Contacts
	public static function getArchivedContactsInAccount($account_id)
	{
		$contacts = Contact::withTrashed()->where('account_id', $account_id)->get();

		// Attach Account Name to Contacts
		foreach ($contacts as $contact){

			$contact->account_name = Account::withTrashed()->where('id', $contact->account_id)->first()->getName();

		}

		if ($contacts) {
			return $contacts;
		}
		else{
			return 'No contacts attached to this account';
		}

	}



}
