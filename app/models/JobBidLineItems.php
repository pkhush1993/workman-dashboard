<?php namespace App\Models;

use Eloquent;

class JobBidLineItems extends Eloquent {

	protected $guarded = array('id');

	// protected $primaryKey = 'customer_id'
	// protected $table = '';
	// public function getName()
	// {
	// 	return $this->name;
	// }

    public function bid()
    {
        return $this->hasOne('JobBid');
    }	
}
