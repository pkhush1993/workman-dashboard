<?php namespace App\Models;
use Eloquent;

class BidField extends Eloquent {
	public static $unguarded = true;

	// protected $primaryKey = 'customer_id'
	// protected $table = '';

	public function getName()
	{
		return $this->name;
	}

	public function getSortOrder()
	{
		return $this->sort_order;
	}


	public function getBidFields()
	{
		return $this->hasMany('App\Models\BidFieldItem','bid_field_id','id')->get();
	}

	public static function getBidFieldsAndItems()
	{

		// Get Bid Fields
		$bid_fields = BidField::all();

		//Setup Array
		$_bid_field = [];

		//Bid Fields
		foreach ($bid_fields as $bid_field) {

			//Get Each Bid Fields Items (children)
			$bid_field_items = BidField::find($bid_field->id)->getBidFields();

			//Skip Empty Bid Fields
			if( $bid_field_items->count() ){

				//Build Array
				$_bid_field[$bid_field->id] = ['items' => null, 'name' => $bid_field->name ];

				//Store Items in Bid Field Array
				foreach ($bid_field_items as $bid_field_item){
		    		$_bid_field[$bid_field->id]['items'][] = array(
		    			'bid_field_id'	=>	$bid_field_item->bid_field_id,
		    			'bid_field_item_id'	=>	$bid_field_item->id,
						'description'	=>	$bid_field_item->description,
						'units' 	 	=>	$bid_field_item->units,
						'price'   	 	=>	$bid_field_item->price,
						'sort_order' 	=>	$bid_field_item->sort_order
		    		);
				}

			}

		}

		return $_bid_field;
	}


	public static function getBidFieldsInputOptions()
	{
		// Get all users that are estimators
		$_bid_fields = BidField::all();
			$bid_fields = array('' => 'Select Bid Line Item');
			$bid_fields['custom'] = 'Enter Your Own';
		// Check If There are Users Set as Estimators
		if (!$_bid_fields->isEmpty()) {

			// Build Estimators Select Options

			foreach ($_bid_fields as $bid_field){
				 $bid_fields[$bid_field->id] = $bid_field->name;
			}
		}
		return $bid_fields;
	}



}