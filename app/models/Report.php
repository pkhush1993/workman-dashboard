<?php namespace App\Models;

use Eloquent;

class Report extends Eloquent {

	// protected $primaryKey = 'customer_id'
	// protected $table = '';
	//
	public static function getGroupedLineItems($bid)
	{
		$grouparr = [];
		if ( $bid && $bid->getLineItems() ) {
			foreach ($bid->getLineItems()->toArray() as $key => $val) {
				$grouparr[$val['category']][]=$val;
			}
		}
		return $grouparr;
	}

	public static function getTrailingTwelveMonthBidDollars($date)
	{

		// Get The Start Date for 12 months ago
		$twelve_months_ago =  date( 'Y-m', strtotime( '-12 months' . $date ) );

		// Get the Sum of All The Bids Grand Totals within the 12 month range
		$twelve_months_bids = JobBid::where('bid_date','>=',date('Y-m-01', strtotime($twelve_months_ago)))->where('bid_date','<=',date('Y-m-t', strtotime($date)))->orderBy('bid_date', 'asc')->sum('grand_total');

		// Divide by 12 to get the Trailing 12 Month Value
		//$trailing_twelve_month = round($twelve_months_bids / 12, 2);

		// Get the 12 month Trailing without the divison of 12
		$trailing_twelve_month = round($twelve_months_bids, 2);

		return $trailing_twelve_month;

	}

	public static function getTrailingTwelveMonthBidDollarsByJobStatus($date,$status)
	{

		// Get The Start Date for 12 months ago
		$twelve_months_ago =  date( 'Y-m', strtotime( '-12 months' . $date ) );

		// Get the Sum of All The Bids Grand Totals within the 12 month range
		$twelve_months_bids = JobBid::where('bid_date','>=',date('Y-m-01', strtotime($twelve_months_ago)))->where('bid_date','<=',date('Y-m-t', strtotime($date)))->orderBy('bid_date', 'asc')->get();

		// If there are Bids in the Date Range Add up the Grand Totals
		if ( count($twelve_months_bids) ) {
			foreach ($twelve_months_bids as $bid){
				// Filter To Get Jobs That Have the Desired Status
				if ( in_array($bid->job()->status,$status) ) {
					$twelve_months_bids_total[] = $bid->subtotal;
				}
			}
		}
		else{
			$twelve_months_bids_total[] = "0";
		}

		// Divide by 12 to get the Trailing 12 Month Value
		// $trailing_twelve_month = round(array_sum($twelve_months_bids_total) / 12, 2);

		// Get the 12 month Trailing without the divison of 12
		$trailing_twelve_month = round(array_sum($twelve_months_bids_total), 2);


		return $trailing_twelve_month;

	}

	public static function getTrailingTwelveMonthJobsBid($date)
	{

		// Get The Start Date for 12 months ago
		$twelve_months_ago =  date( 'Y-m', strtotime( '-12 months' . $date ) );

		// Get the Sum of All The Bids Grand Totals within the 12 month range
		$twelve_months_bids = JobBid::where('bid_date','>=',date('Y-m-01', strtotime($twelve_months_ago)))->where('bid_date','<=',date('Y-m-t', strtotime($date)))->orderBy('bid_date', 'asc')->count();

		// Divide by 12 to get the Trailing 12 Month Value
		//$trailing_twelve_month = round($twelve_months_bids / 12, 2);

		// Get the 12 month Trailing without the divison of 12
		$trailing_twelve_month = round($twelve_months_bids, 2);

		return $trailing_twelve_month;

	}

	public static function getTrailingTwelveMonthJobsBidByJobStatus($date,$status)
	{

		// Get The Start Date for 12 months ago
		$twelve_months_ago =  date( 'Y-m', strtotime( '-12 months' . $date ) );

		// Get the Sum of All The Bids Grand Totals within the 12 month range
		$twelve_months_bids = JobBid::where('bid_date','>=',date('Y-m-01', strtotime($twelve_months_ago)))->where('bid_date','<=',date('Y-m-t', strtotime($date)))->orderBy('bid_date', 'asc')->get();

		// If there are Bids in the Date Range Add up the Grand Totals
		if ( count($twelve_months_bids) ) {
			foreach ($twelve_months_bids as $bid){
				// Filter To Get Jobs That Have the Desired Status
				if ( in_array($bid->job()->status,$status) ) {
					$twelve_months_bids_total[] = 1;
				}
			}
		}
		else{
			$twelve_months_bids_total[] = "0";
		}

		// Divide by 12 to get the Trailing 12 Month Value
		// $trailing_twelve_month = round(array_sum($twelve_months_bids_total) / 12, 2);

		// Get the 12 month Trailing without the divison of 12
		$trailing_twelve_month = round(array_sum($twelve_months_bids_total), 2);


		return $trailing_twelve_month;

	}


}
