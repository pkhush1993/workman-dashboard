<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TimesheetDetail extends Model
{

	/**
	 * The attributes that aren't mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = array('id');


	/**
	 * The "booting" method of the model.
	 *
	 * @return void
	 */
	public static function boot()
	{
		parent::boot();

		// Force a user id from the parent, if none is set.
		static::saving(function($detail) {
			if ( !$detail->getAttribute('user_id') || $detail->getAttribute('user_id') == 0 ) {
				$detail->setAttribute('user_id', $detail->timesheet()->getAttribute('user_id'));
			}
		});

		static::saved(function($detail) {
			if ($detail->getAttribute('updated_at') > $detail->timesheet()->getAttribute('updated_at')) {
				$detail->timesheet()->fill([
					'updated_at' => $detail->getAttribute('updated_at')
				])->save();
			}
		});
	}

	/**
	 * Relationship to a single job
	 *
	 * @return mixed
     */
	public function job()
	{
	 	return $this->hasOne('App\Models\Job','id','job_id')->first();
	}

	/**
	 * Relationship to a single timesheet
	 *
	 * @return mixed
     */
	public function timesheet()
	{
		return $this->hasOne('App\Models\Timesheet', 'id', 'timesheets_id')->first();
	}
}
