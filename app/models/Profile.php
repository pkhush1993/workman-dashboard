<?php namespace App\Models;

use Eloquent;

class Profile extends Eloquent
{
	/**
	 * @var bool
     */
	public $incrementing = false;

	/**
	 * @return mixed
     */
	public function company()
	{
		return $this->hasOne('\App\Models\Company', 'id', 'company_id')->firstOrFail();
	}

	/**
	 * @return mixed
     */
	public function getFirstName()
	{
		return $this->first_name;
	}

	/**
	 * @return mixed
     */
	public function getLastName()
	{
		return $this->last_name;
	}

	/**
	 * @return string
     */
	public function getFullName()
	{
		return $this->first_name.' '.$this->last_name;
	}

	/**
	 * @return mixed
     */
	public function getAddress1()
	{
		return $this->address_1;
	}

	/**
	 * @return mixed
     */
	public function getAddress2()
	{
		return $this->address_2;
	}

	/**
	 * @return mixed
     */
	public function getCity()
	{
		return $this->city;
	}

	/**
	 * @return mixed
     */
	public function getState()
	{
		return $this->state;
	}

	/**
	 * @return mixed
     */
	public function getZip()
	{
		return $this->zip;
	}

	/**
	 * @return mixed
     */
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * @return mixed
     */
	public function getWebsite()
	{
		return $this->website;
	}

	/**
	 * @return mixed
     */
	public function getFax()
	{
		return $this->fax;
	}

	/**
	 * @return mixed
     */
	public function getPhone()
	{
		return $this->phone;
	}

	/**
	 * @return mixed
     */
	public function getMobile()
	{
		return $this->mobile;
	}

	/**
	 * @return mixed
     */
	public function getWageRate()
	{
		return max(0.00, $this->getAttribute('wage_rate'));
	}

	/**
	 * @return mixed
     */
	public function getAvatar()
	{
		return $this->avatar;
	}

	/**
	 * @return mixed
     */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @return mixed
     */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @param null $column
	 *
	 * @return mixed
     */
    public function getProfileInfo($column = NULL)
	{
		return $this->$column;
	}

	/**
	 * @param $user_id
	 *
	 * @return mixed
     */
    public static function getCurrentTimesheet($user_id)
	{
		return Timesheet::where('user_id',$user_id)->where('period_start','<=',date('Y-m-d'))->where('period_end','>=',date('Y-m-d'))->first();
	}


}
