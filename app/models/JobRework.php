<?php namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\SoftDeletingTrait;
use App\Models\Profile;

class JobRework extends Eloquent {

	use SoftDeletingTrait;

	protected $guarded = array('id');
	// protected $primaryKey = 'customer_id'
	// protected $table = '';


	// Get Attached Job
	public function job()
	{
	 return $this->belongsTo('App\Models\Job','job_id')->firstOrFail();
	}


	public function profile()
	{
		return $this->belongsTo('App\Models\Profile', 'created_by')->firstOrFail();
	}

	public static function getNextReworkNumber($job_id)
	{
		$job_number = Job::find($job_id)->getJobNumber();

		$next_rework_number = count(JobRework::withTrashed()->where('job_id',$job_id)->get())+1;


		return $job_number.'.'.$next_rework_number;
	}



}
