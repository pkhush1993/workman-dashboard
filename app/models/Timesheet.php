<?php namespace App\Models;

use Carbon\Carbon;
use Eloquent,Sentinel;
use Illuminate\Support\Collection;

class Timesheet extends Eloquent {

	protected $guarded = array('id');

	/**
	 * @var int
     */
	protected $standardTime = 0;

	/**
	 * @var int
     */
	protected $overTime = 0;

	/**
	 * @param null $type
	 * @return mixed
     */
	public static function getUserTimesheets($type = NULL)
	{

		$user_id  = Sentinel::getUser()->id;

		if ($type == 'current') {
			return Timesheet::where('user_id',$user_id)->where('approval','0')->get();
		}
		elseif ($type == 'pending') {
			return Timesheet::where('user_id',$user_id)->where('approval','1')->whereNull('approval_date')->get();
		}
		elseif ($type == 'approved') {
			return Timesheet::where('user_id',$user_id)->whereNotNull('approval_date')->get();
		}
		else{
			return Timesheet::where('user_id',$user_id)->get();
		}
	}

	/**
	 * @return mixed
     */
	public function details()
	{
		return $this->hasMany('App\Models\TimesheetDetail','timesheets_id','id')->get();
	}

	/**
	 * @return mixed
     */
	public function profile()
	{
	 return $this->hasOne('App\Models\Profile','id','user_id')->first();
	}

	// Check to see if we are still within the Grace Period to Edit a Past Timesheet
	/**
	 * @return bool|string
     */
	public static function checkEntryGracePeriod()
	{
		$company = Company::find(1);
		$past_period_end  = $company->getLastPayPeriod()['period_end'];
		$entry_grace_period = date('m/d/Y',strtotime($past_period_end.'+'.$company->time_entry_grace_period.'days'));

		// If Today is Less than or Equal to the Grace Period End Date We are still within the Grace Period
		if ( date('m/d/Y') <= date('m/d/Y',strtotime($entry_grace_period)) ){
			return $entry_grace_period;
		}

		return false;
	}

	// Get Last Timesheet Entered Defined by User
	/**
	 * @param $user_id
	 * @return bool
     */
	public static function getPastTimeSheet($user_id)
	{
		// Get Last Timesheet
		if ($last_timesheet = Timesheet::where('user_id',$user_id)->orderBy('id', 'desc')->skip(1)->take(1)->first()) {
			return $last_timesheet ;
		}

		return false;
	}


	/**
	 * Get the standard amount of time worked for a given time range
	 *
	 * @param Carbon $from
	 * @param Carbon $to
	 * @return int
     */
	public function getStandardTimeForRange(Carbon $from, Carbon $to)
	{
		$total = 0;

		$timesheets = self::where('period_start', '>', $from->toDateString() )
						->where('period_end', '<', $to->toDateString() )
						->where('approval', 1)
						->where('user_id', $this->getAttribute('user_id'))
						->get();

		foreach ( $timesheets as $timesheet )
		{
			$total += $timesheet->details()->sum('total');
		}

		return $total;
	}


	/**
	 * Get the amount of overtime hours worked for a given range
	 *
	 * @param Carbon $from
	 * @param Carbon $to
	 * @param int $max
	 * @return mixed
     */
	public function getOverTimeForRange(Carbon $from, Carbon $to, $max = null)
	{
		if ( is_null($max) ) {
			$max = $this->getHoursInWeek();
		}
		return max(0, $this->getStandardTimeForRange($from, $to)-$max);
	}

	/**
	 * Get the amount of standard hours worked for a given week
	 *
	 * @param Carbon $date
	 * @param $week
	 * @param int $max
	 * @return mixed
     */
	public function getStandardTimeForWeek(Carbon $date, $week, $max = null)
	{
		if ( is_null($max) ) {
			$max = $this->getHoursInWeek();
		}

		$to = clone $date;
		return $this->standardTime += min($max, $this->getStandardTimeForRange($date, $to->addWeeks($week)));
	}

	/**
	 * Get the amount of overtime hours worked for a given week
	 *
	 * @param Carbon $date
	 * @param $week
	 * @return mixed
     */
	public function getOverTimeForWeek(Carbon $date, $week)
	{
		$to = clone $date;
		return $this->overTime += $this->getOverTimeForRange($date, $to->addWeeks($week));
	}

	/**
	 * @return array
     */
	public function report()
	{
		$details = $this->details();
		$hours_in_work_week = $this->profile()->company()->weekly_hours;
		$overtime_payrate = $this->profile()->company()->overtime_pay;
		$wage_rate = Profile::find($this->user_id)->wage_rate;


		$total_time = 0;

		// Calculate Total Time
		foreach ($details as $detail) {
			$total_time = $total_time + $detail->total;
		}

		// Check for Overtime Hours
		if ($total_time > $hours_in_work_week) {
			$overtime_hours = $total_time - $hours_in_work_week;
			$regular_hours = $hours_in_work_week;
		} else {
			$regular_hours = $total_time;
			$overtime_hours = 0;
		}

		// Calculate Pay Rates
		if ($overtime_hours) {
			$regular_pay = $regular_hours * $wage_rate;
			$overtime_pay = $overtime_hours * ($wage_rate * $overtime_payrate);
		}
		else {
			$regular_pay = $regular_hours * $wage_rate;
			$overtime_pay = 0;
		}

		// Calculate Total Pay
		$total_pay = $regular_pay + $overtime_pay;

		return [
			'total_time'     => number_format($total_time,2),
			'regular_hours'  => number_format($regular_hours,2),
			'overtime_hours' => number_format($overtime_hours,2),
			'regular_pay'    => number_format($regular_pay,2),
			'overtime_pay'   => number_format($overtime_pay,2),
			'total_pay'      => number_format($total_pay,2),
			'wage_rate'      => number_format($wage_rate,2)
		];
	}

	/**
	 * @return mixed
     */
	public function getHoursInWeek()
	{
		return $this->profile()->company()->getAttribute('weekly_hours');
	}

	/**
	 * Gets the total amount of standard time hours
	 *
	 * @return int
     */
	public function getTotalStandardTime()
	{
		return $this->standardTime;
	}

	/**
	 * Gets the total amount of overtime hours
	 *
	 * @return int
     */
	public function getTotalOverTime()
	{
		return $this->overTime;
	}

	/**
	 * Get the total cost of an employee's standard hours worked
	 *
	 * @return int
     */
	public function getStandardTimeCost()
	{
		return str_money($this->getTotalStandardTime() * $this->getStandardTimeWageRate());
	}

	/**
	 * Get the total cost for an employee's overtime
	 *
	 * @return mixed
     */
	public function getOverTimeCost()
	{
		return str_money($this->getOverTimeRate() * $this->getTotalOverTime());
	}

	/**
	 * Get an employee's standard wage rate
	 *
	 * @return mixed
     */
	public function getStandardTimeWageRate()
	{
		return $this->profile()->getWageRate();
	}

	/**
	 * Get the wage rate for overtime on a given company
	 *
	 * @return mixed
     */
	public function getOverTimeRate()
	{
		return $this->profile()->company()->getAttribute('overtime_pay');
	}
}
