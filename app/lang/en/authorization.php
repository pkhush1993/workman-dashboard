<?php
return array(
	/*
	|--------------------------------------------------------------------------
	| Authorization Language File
	|--------------------------------------------------------------------------
    |
	| This file contains language data relevant to the authorization stages of
	| the web application, such as login, password resets and registration.
	|
	*/
	'reset_request_success' 			=> 'A reset code as been sent to your email address',
	'reset_code_not_provided'			=> 'Your reset code is invalid or has expired. Please resend your reset request',
	'login_success'     				=> 'Welcome back, :email',
	'email_reset_instructions_subject'	=> 'Password reset request from :url',
	'passwords_must_match'				=> 'Passwords must match',
	'password_min_length'				=> 'Password must be at least :min characters',
	'reset_success'						=> 'Password reset successfully. You can now login with your new password',
	'reset_failed'						=> 'Your reset request failed. Please resend your reset request or contact an admin if the problem persists',
	'logout_success'					=> 'You have been logged out successfully',
	'missing_activation_code'			=> 'Your activation request failed. You may need to register again or contact an admin if the problem persists',
	'invalid_email'						=> 'Please enter a valid email address',
	'not_empty'							=> 'This field is required',
	'email_activate_account_subject'	=> 'Attention Required: Please verify your email address for :url',
	'account_activation_required'		=> 'Almost there. Check your email for instructions on activating your account',
	'registration_complete'				=> 'New User Created! Please login',
	'user_exists'						=> 'User is already registered. Try using Forgot Password to retrieve account',
	"no_user" 							=> "We can't find a user with that e-mail address.",
	"wrong_token"  						=> "This password reset token is invalid.",
	"forgot_sent" 						=> "Password Reset Sent!",
	'roles_updated' 					=> 'Users Roles Have Been Updated',
	'no_permission' 					=> 'You do not have the permissions to access this page'


);