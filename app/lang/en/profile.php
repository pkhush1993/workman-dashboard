<?php
return array(
	/*
	|--------------------------------------------------------------------------
	| Authorization Language File
	|--------------------------------------------------------------------------
    |
	| This file contains language data relevant to the profile updates
	|
	*/
	'profile_update' 					=> 'Your profile has been updated!',
	'profile_delete' 					=> 'Your profile has been deleted',
	'no_profile' 						=> 'Profile not found'
);
