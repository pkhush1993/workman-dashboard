<?php
return array(
	/*
	|--------------------------------------------------------------------------
	| General Language File
	|--------------------------------------------------------------------------
    |
	| This file contains general language data relevant to the company
	|
	*/

	// Company Updates
	'company_update' 					=> 'Company Info has been updated!',
	'company_delete' 					=> 'Company has been deleted',
	'company_invite' 					=> 'User(s) have been Invited!',
	'activity_update' 					=> 'Default Activity Options have been Updated',
	'admin_removed' 					=> 'User is no longer an Administrator',
	'bid_fields_updated' 				=> 'Bid Fields Have Been Updated',
	'bid_fields_created' 				=> 'New Bid Field(s) Have Been Created',
	'contact_update' 					=> 'Contact has been updated!',
	'contact_created' 					=> 'Contact has been created!',
	'contact_deleted' 					=> 'Contact has been deleted!',
	'contact_restored' 					=> 'Contacts have been restored!',
	'no_archived_contacts' 				=> 'There are currently No Archived Contacts',
	'no_contact' 						=> 'Contact does not exist, if you think this is by mistake contact the site administrator',
	'account_update' 					=> 'Account has been updated',
	'account_created' 					=> 'Account has been created!',
	'account_deleted' 					=> 'Account has been deleted',
	'account_restored' 					=> 'Accounts have been restored!',
	'account_name_exists' 				=> 'The Account Name :name Is Already Being Used by Another Account',
	'no_archived_accounts' 				=> 'There are currently No Archived Accounts',
	'no_account' 						=> 'Account does not exist, if you think this is by mistake contact the site administrator',
	'job_create' 						=> 'Job has been Created',
	'job_update' 						=> 'Job has been Updated',
	'job_deleted' 						=> 'The :job_name Job has been Deleted',
	'no_job' 							=> 'Job does not exist, if you think this is by mistake contact the site administrator',
	'no_note' 							=> 'Note does not exist, if you think this is by mistake contact the site administrator',
	'job_bid_saved' 					=> 'The Bid for this Job has been saved!',
	'job_bid_comments_updated' 			=> 'Bid Comments have been Updated',
	'job_contacts_added' 				=> 'Contacts Have Been Added to This Job',
	'job_contacts_removed' 				=> 'Contacts Have Been Removed From This Job',
	'job_duplicate_number'				=> 'Job number :job_number already exists in the system',
	'job_rework_created' 				=> 'New Job Rework Added',
	'job_rework_updated' 				=> 'Job Rework Updated',
	'job_rework_deleted' 				=> 'Job Rework Deleted',
	'job_rework_closed' 				=> 'Job Rework Closed',
	'job_rework_opened' 				=> 'Job Rework Re-Opened',
	'job_note_created' 					=> 'New Job Note/Activity Added',
	'job_note_updated' 					=> 'Job Note Updated',
	'job_note_deleted' 					=> 'Job Note Hidden',
	'job_note_restored' 				=> 'Job Note Restored',
	'follow_up_resolved' 				=> 'Follow Up has been Resolved',
	'no_job_rework' 					=> 'Job Rework does not exist, if you think this is by mistake contact the site administrator',
	'no_job_contacts' 					=> 'Job Contacts Not Selected',
	'tax_rate_update' 					=> 'Tax Rates have been Updated',
	'timesheet_submit' 					=> 'Timesheet has been Submitted for Approval',
	'timesheet_saved' 					=> 'Timesheet has been Saved',
	'timesheet_approved'				=> 'Timesheet Has Been Approved!',
	'timesheet_no_edit'					=> 'The Selected Timesheet Is Past the Grace Period of Editing, Contact your Administrator to Update',
	'no_report_results'				    => 'No Results for the Queried Report',
	'revoke_invite_success'				=> 'User invitation has been revoked',
	'job_update_exist_error'			=> 'The job name you entered is already in use'

);
