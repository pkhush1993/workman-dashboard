<?php

Event::listen('eloquent.created:*', function($model)
{
	try {

		$model->created_by = Sentinel::getUser()->id;
		$model->save();

	} catch (\Exception $e) {
		Log::info('Error from Event Observer: '.$e->getMessage());
	}

});

if ( ! function_exists('view'))
{
	/**
	 * Generate a URL to a controller action.
	 *
	 * @param  string  $name
	 * @param  array   $parameters
	 * @return string
	 */
	function view($name, $parameters = array())
	{
		return app('view')->make($name, $parameters);
	}
}

if ( ! function_exists('str_money'))
{
	/**
	 * Generate a money string from integer
	 *
	 * @param integer $number
	 * @param string $symbol
	 * @param integer $decimals
	 * @return string
	 */
	function str_money($number, $symbol = '$', $decimals = 2)
	{
		return $symbol.number_format($number, $decimals);
	}
}

if (! function_exists('git'))
{
	function git()
	{
		return new \App\Services\GitHelper();
	}
}

if ( ! function_exists('current_user') )
{
	function current_user()
	{
		if ( $sentinel = \Sentinel::getUser() ) {
			return \App\Models\User::find($sentinel->getAttribute('id'));
		}
	}
}

if ( ! function_exists('tab_route') )
{
	/**
	 * Creates a URL that matches jQuery Address Path
	 *
	 * @param string $route
	 * @param string $tab
	 *
	 * @return string
     */
    function tab_redirect($route, $tab = 'personal')
	{
		return route($route, ['company_id' => 1]).'#/'.$tab;
	}
}

if ( ! function_exists('array_median') )
{

	/**
	 * Get the median value from an array
	 *
	 * @param array $array
	 *
	 * @return float
     */
    function array_median(array $array = [])
	{
		sort($array);

		$mid = floor((count($array)-1)/2);

		if ( count($array) % 2) {
			return $array[$mid];
		}

		$low = $array[$mid];
		$high = $array[$mid+1];

		return (($low+$high)/2);
	}
}