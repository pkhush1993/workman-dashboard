<?php
use App\Controllers\TimeSheetSearchController;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
|
*/
// Install
Route::get('install', array('as' => 'installer', 'uses' => 'App\Controllers\InstallController@getInstall'));
Route::get('install/roles', array('as' => 'installer.roles', 'uses' => 'App\Controllers\InstallController@getRoles'));

// Logout
Route::get('auth/logout', array('as' => 'auth.logout', 'uses' => 'App\Controllers\AuthController@getLogout'));

//Register (REMOVED for security reasons)
//Route::get('auth/register', array('as' => 'auth.register', 'uses' => 'App\Controllers\AuthController@getRegister'));
//Route::post('auth/register', array('as' => 'auth.register', 'uses' => 'App\Controllers\AuthController@postRegister'));

/*
|==========================================================================
| PostAuth - make sure user is NOT logged in before running routes
|==========================================================================*/
// Run PostAuth Filter Before using the following routes
Route::group(array('before' => 'postauth'), function() {

	// Home
	Route::get('/', function(){return View::make('auth.login');});

	// Login
	Route::get('auth/login/{decoy?}/{returnUrl?}', array('as' => 'auth.login', 'uses' => 'App\Controllers\AuthController@getLogin'));
	Route::post('auth/login/{decoy?}/{returnUrl?}', array('as' => 'auth.login', 'uses' => 'App\Controllers\AuthController@postLogin'));
	Route::post('auth/login-ajax/', array('as' => 'auth.ajax.login', 'uses' => 'App\Controllers\AuthController@ajaxPostLogin'));


	// Forgot Password
	Route::get('auth/forgot', array('as' => 'auth.forgot', 'uses' => 'App\Controllers\AuthController@getForgot'));
	Route::post('auth/forgot', array('as' => 'auth.forgot', 'uses' => 'App\Controllers\AuthController@postForgot'));

	// Activate/Reset Account
	Route::get('auth/reset/{resetCode}/{email?}', array('as' => 'auth.reset', 'uses' => 'App\Controllers\AuthController@getReset'));
	Route::post('auth/reset/{resetCode}/{email?}', array('as' => 'auth.reset', 'uses' => 'App\Controllers\AuthController@postReset'));
	Route::get('auth/activate/{activationCode}/{email?}', array('as' => 'auth.activate', 'uses' => 'App\Controllers\AuthController@getActivate'));

	// Account Setup
	Route::get('auth/setup/{activationCode}/{email}', array('as' => 'auth.setup_account', 'uses' => 'App\Controllers\AuthController@getSetup'));
	Route::post('auth/setup/', array('as' => 'auth.post.setup.account', 'uses' => 'App\Controllers\AuthController@postSetup'));
});

/*
|==========================================================================
| Authorize - make sure user is logged in before running routes
|==========================================================================*/
Route::group(array('before' => 'auth'), function() {
	// Check user login status
	Route::get('users/login-status/', array('as' => 'user.ajax.status', 'uses' => 'App\Controllers\AuthController@ajaxLoginStatus'));
	// Dashboard
	Route::get('dashboard', array('as' => 'dashboard', 'uses' => 'App\Controllers\DashboardController@viewDashboard'));

	/* ==== Executive Restricted Views - make sure user is has the correct Role ====*/
	Route::group(array('before' => 'executive'), function() {
		Route::get('dashboard/executive', array('as' => 'dashboard.executive', 'uses' => 'App\Controllers\DashboardController@viewExecutiveDashboard'));
		Route::get('admin/settings/company/{company_id}', array('as' => 'company.profile', 'uses' => 'App\Controllers\CompanySettingsController@viewCompany'));
		Route::get('view/users', array('as' => 'admin.users', 'uses' => 'App\Controllers\UserController@viewUsers'));
		Route::get('user/edit/{user_id}', array('as' => 'edit.user', 'uses' => 'App\Controllers\UserController@editUser'));
		Route::post('update/user/{user_id}', array('as' => 'update.user', 'uses' => 'App\Controllers\UserController@updateUser'));
		Route::post('update/user/roles/{profile_id}', array('as' => 'update.user.roles', 'uses' => 'App\Controllers\UserController@updateUserRoles'));
		Route::post('update/company/{company_id}/{action}', array('as' => 'update.company', 'uses' => 'App\Controllers\CompanySettingsController@updateCompany'));
		

	});

	/* ==== Foreman Restricted Views - make sure user is has the correct Role ====*/
	Route::group(array('before' => 'foreman'), function() {
		Route::get('dashboard/foreman/{user_id?}', array('as' => 'dashboard.foreman', 'uses' => 'App\Controllers\DashboardController@viewForemanDashboard'));
	});

	/* ==== Project Manager Restricted Views - make sure user is has the correct Role ====*/
	Route::group(array('before' => 'project_manager'), function() {
		Route::get('dashboard/project-manager/{user_id?}', array('as' => 'dashboard.project-manager', 'uses' => 'App\Controllers\DashboardController@viewProjectManagerDashboard'));
		Route::get('timesheets/pending-approvals', array('as' => 'view.approve.timesheets', 'uses' => 'App\Controllers\TimeSheetController@viewPendingApprovalTimesheets'));
		Route::get('timesheets/ajax/pending-approvals', array('as' => 'view.approve.timesheets.ajax', 'uses' => 'App\Controllers\TimeSheetController@viewPendingApprovalTimesheetsAjax'));
		Route::get('timesheet/approve/{timesheet_id}', array('as' => 'view.approve.timesheet', 'uses' => 'App\Controllers\TimeSheetController@viewApproveTimesheet'));
		Route::post('timesheet/approval/{timesheet_id}/{approval?}', array('as' => 'approve.timesheet', 'uses' => 'App\Controllers\TimeSheetController@approveTimesheet'));
	});

	/* ==== Estimator Restricted Views - make sure user is has the correct Role ====*/
	Route::group(array('before' => 'estimator'), function() {
		Route::get('dashboard/estimator/{user_id?}', array('as' => 'dashboard.estimator', 'uses' => 'App\Controllers\DashboardController@viewEstimatorDashboard'));
	});

	/* ==== Accounting Restricted Views - make sure user is has the correct Role ====*/
	Route::group(array('before' => 'accounting'), function() {
		Route::get('dashboard/accounting/{user_id?}', array('as' => 'dashboard.accounting', 'uses' => 'App\Controllers\DashboardController@viewAccountingDashboard'));
	});


	/* ==== Routes That Are Restricted From the Time Entry User Roles ====*/
	Route::group(array('before' => 'restrict_time_entry'), function() {

		// Jobs
		Route::get('jobs', array('as' => 'view.jobs', 'uses' => 'App\Controllers\JobController@viewJobs'));
		Route::get('jobs-ajax', array('as' => 'view.jobs.ajax', 'uses' => 'App\Controllers\JobController@viewJobsAJAX'));

		Route::get('job', array('as' => 'add.job', 'uses' => 'App\Controllers\JobController@viewAddJob'));
		Route::post('job/create', array('as' => 'create.job', 'uses' => 'App\Controllers\JobController@createJob'));
		Route::post('job', array('as' => 'go.to.job', 'uses' => 'App\Controllers\JobController@goToJob'));
		Route::post('job/update/{job_id}', array('as' => 'update.job', 'uses' => 'App\Controllers\JobController@updateJob'));
		Route::get('job/delete/{job_id}', array('as' => 'delete.job', 'uses' => 'App\Controllers\JobController@deleteJob'));

		// Job Accounts/Contacts
		Route::get('job/edit/contact/{contact_id}', array('as' => 'job.contact', 'uses' => 'App\Controllers\AccountContactsController@viewJobContact'));
		Route::post('job/add/contacts/{job_id}/{account_id}', array('as' => 'job.add.contacts', 'uses' => 'App\Controllers\JobController@attachContactsToJob'));
		Route::post('job/remove/contacts/', array('as' => 'job.remove.contacts', 'uses' => 'App\Controllers\JobController@removeContactsOnJob'));
		Route::post('job/restore/contacts/', array('as' => 'job.restore.contacts', 'uses' => 'App\Controllers\JobController@restoreContactsOnJob'));
		Route::get('account/contacts/{account_id}/{job_id?}', array('as' => 'job.contacts', 'uses' => 'App\Controllers\AccountContactsController@viewAccountContacts'));
		Route::get('job/account/contacts/{account_id}/{job_id?}', array('as' => 'job.account.contacts', 'uses' => 'App\Controllers\AccountContactsController@viewJobAccountContacts'));

		// Job Rework
		Route::post('job/rework/delete', array('as' => 'job.delete.rework', 'uses' => 'App\Controllers\JobReworkController@deleteJobRework'));
		Route::post('job/rework/create/{job_id}', array('as' => 'job.create.rework', 'uses' => 'App\Controllers\JobReworkController@createJobRework'));
		Route::post('job/rework/complete', array('as' => 'job.complete.rework', 'uses' => 'App\Controllers\JobReworkController@completeJobRework'));
		Route::post('job/rework/open', array('as' => 'job.open.rework', 'uses' => 'App\Controllers\JobReworkController@openJobRework'));

		//Notes
		Route::post('job/notes/create/{job_id?}', array('as' => 'job.create.note', 'uses' => 'App\Controllers\NotesController@createJobNote'));
		Route::post('follow-up/note/create/', array('as' => 'follow-up.create.note', 'uses' => 'App\Controllers\NotesController@createFollowUpNote'));
		Route::post('follow-up/resolve', array('as' => 'resolve.follow-up', 'uses' => 'App\Controllers\NotesController@resolveFollowUp'));
		Route::get('job/note/edit/{job_id}/{note_id}', array('as' => 'job.edit.note', 'uses' => 'App\Controllers\NotesController@editJobNote'));
		Route::post('job/note/update/{note_id}', array('as' => 'job.update.note', 'uses' => 'App\Controllers\NotesController@updateNote'));
		Route::post('job/note/remove', array('as' => 'job.remove.note', 'uses' => 'App\Controllers\NotesController@removeNote'));
		Route::post('job/note/restore', array('as' => 'job.restore.notes', 'uses' => 'App\Controllers\NotesController@restoreNote'));

		// Copy Job
		Route::post('job/copy{job_id}', array('as' => 'job.copy.job', 'uses' => 'App\Controllers\JobCopyController@createCopy'));

		// BidFields and Bid Items
		Route::post('job/bid/update/{job_id}', array('as' => 'job.bid', 'uses' => 'App\Controllers\JobController@updateJobBid'));
		Route::post('update/bidfields/{company_id}', array('as' => 'update.bid.fields', 'uses' => 'App\Controllers\BidFieldController@updateBidFields'));
		Route::post('create/bidfields/{company_id}', array('as' => 'create.bid.fields', 'uses' => 'App\Controllers\BidFieldController@createBidFields'));
		Route::get('bid/items/drop/{bid_field_id}', array('as' => 'job.bid.items.drop', 'uses' => 'App\Controllers\BidFieldController@getBidItemsDrop'));
		Route::get('bid/item/{bid_item_id}', array('as' => 'job.bid.item.info', 'uses' => 'App\Controllers\BidFieldController@getBidItemInfo'));

		// BidFields and Bid Items
		Route::post('job/bid/comments/update/{job_id}/{bid_id}', array('as' => 'job.bid.comments', 'uses' => 'App\Controllers\BidCommentController@updateBidComments'));

		//Contacts
		Route::get('contacts', array('as' => 'company.contacts', 'uses' => 'App\Controllers\AccountContactsController@viewContacts'));
		Route::get('contacts/archived/', array('as' => 'archived.contacts', 'uses' => 'App\Controllers\AccountContactsController@viewArchivedContacts'));
		Route::post('contacts/unarchive/', array('as' => 'unarchive.contact', 'uses' => 'App\Controllers\AccountContactsController@restoreContacts'));
		Route::get('contact/edit/{contact_id}', array('as' => 'company.contact', 'uses' => 'App\Controllers\AccountContactsController@viewContact'));
		Route::post('contact/update', array('as' => 'update.contact', 'uses' => 'App\Controllers\AccountContactsController@updateContact'));
		Route::get('contact/add', array('as' => 'add.contact', 'uses' => 'App\Controllers\AccountContactsController@viewAddContact'));
		Route::post('contact/create/{job_id?}', array('as' => 'create.contact', 'uses' => 'App\Controllers\AccountContactsController@createContact'));
		Route::get('contact/remove/{contact_id}', array('as' => 'remove.contact', 'uses' => 'App\Controllers\AccountContactsController@removeContact'));
		Route::post('contacts/remove/', array('as' => 'remove.contacts', 'uses' => 'App\Controllers\AccountContactsController@removeContacts'));

		//Accounts
		Route::get('accounts', array('as' => 'company.accounts', 'uses' => 'App\Controllers\AccountContactsController@viewAccounts'));
		Route::get('accounts/archived/', array('as' => 'archived.accounts', 'uses' => 'App\Controllers\AccountContactsController@viewArchivedAccounts'));
		Route::post('accounts/unarchive/', array('as' => 'unarchive.account', 'uses' => 'App\Controllers\AccountContactsController@restoreAccounts'));
		Route::get('account/edit/{account_id}', array('as' => 'company.account', 'uses' => 'App\Controllers\AccountContactsController@viewAccount'));
		Route::get('account/contacts/{account_id}', array('as' => 'company.account.contacts', 'uses' => 'App\Controllers\AccountContactsController@viewAccountContacts'));
		Route::post('account/update', array('as' => 'update.account', 'uses' => 'App\Controllers\AccountContactsController@updateAccount'));
		Route::get('account/add', array('as' => 'add.account', 'uses' => 'App\Controllers\AccountContactsController@viewAddAccount'));
		Route::post('account/create', array('as' => 'create.account', 'uses' => 'App\Controllers\AccountContactsController@createAccount'));
		Route::get('account/remove/{account_id}', array('as' => 'remove.account', 'uses' => 'App\Controllers\AccountContactsController@removeAccount'));
		Route::get('account/{account_id}', array('as' => 'account.address', 'uses' => 'App\Controllers\AccountContactsController@getAccountAJAX'));
		Route::post('accounts/remove/', array('as' => 'remove.accounts', 'uses' => 'App\Controllers\AccountContactsController@removeAccounts'));

		Route::get('accounts/ajax', array('as' => 'view.accounts.ajax', 'uses' => 'App\Controllers\AccountContactsController@viewAccountsAjax'));
		Route::get('contacts/ajax/{account_id?}', array('as' => 'view.contacts.ajax', 'uses' => 'App\Controllers\AccountContactsController@viewContactsAjax'));

		//Invoices
		Route::post('job/invoice/create/{job_id}', array('as' => 'job.invoice.create', 'uses' => 'App\Controllers\InvoiceController@createInvoice'));

		//Tax Rates
		Route::get('job/tax-rates/get/{tax_rate_id}', array('as' => 'job.tax.rate', 'uses' => 'App\Controllers\JobController@getTaxRate'));

		// ===== Reports =====
		Route::get('reports', array('as' => 'reports', 'uses' => 'App\Controllers\ReportController@viewReports'));
		Route::post('reports', array('as' => 'run.report', 'uses' => 'App\Controllers\ReportController@runReport'));

		// Invoices
		Route::get('job/invoice/{invoice_type}/{job_id}/{down_payment?}/{invoice_id?}/{view_type?}', array('as' => 'job.invoice', 'uses' => 'App\Controllers\ReportController@viewInvoice'));
		Route::get('job/invoices/{start_date}/{end_date}', array('as' => 'invoices.report', 'uses' => 'App\Controllers\ReportController@viewInvoices'));
		Route::get('job/work-order/{job_id}/', array('as' => 'job.work.order', 'uses' => 'App\Controllers\ReportController@viewJobWorkOrder'));

		// Revenue Reports
		Route::get('/report/employee-revenue/{report_view}/{start_date}/{end_date}/{user_id?}', array('as' => 'user.revenue.report', 'uses' => 'App\Controllers\ReportController@viewUserRevenue'));
		// Removed Was A Dupe with the New UI
		// Route::get('/report/revenue/{report_view}/{start_date}/{end_date}', array('as' => 'revenue.report', 'uses' => 'App\Controllers\ReportController@viewRevenue'));

		// Account history performance
		Route::get('/report/account-hist-perfm/{start_date}/{end_date}',  array('as' => 'account.history.performance', 'uses' => 'App\Controllers\ReportController@viewAccountHistoryPerformance'));
		
		// Bids
		Route::get('/report/bid-history/{report_view}/{start_date}/{end_date}/{user_id?}', array('as' => 'bid.history.report', 'uses' => 'App\Controllers\ReportController@viewBidHistory'));
		Route::get('/report/bid-requests/{user_id?}', array('as' => 'bid.requests.report', 'uses' => 'App\Controllers\ReportController@viewBidRequests'));
		Route::get('/report/bid-dollars/{report_view}/{start_date}/{end_date}/{project_manager?}/{estimator?}', array('as' => 'bid.dollars.chart', 'uses' => 'App\Controllers\ReportController@viewBidDollars'));
		Route::post('/report/bid-dollars/', array('as' => 'post.bid.dollars.chart', 'uses' => 'App\Controllers\ReportController@postBidDollars'));

		// Jobs
		Route::get('/report/booked-work/{user_id?}/{job_status?}', array('as' => 'booked.work.report', 'uses' => 'App\Controllers\ReportController@viewBookedWork'));
		Route::get('/report/production-pipeline/{user_id?}', array('as' => 'production.pipeline.report', 'uses' => 'App\Controllers\ReportController@viewProductionPipeline'));
		Route::get('/report/jobs-completed/{start_date}/{end_date}/{user_id?}', array('as' => 'jobs.completed.report', 'uses' => 'App\Controllers\ReportController@viewJobsCompleted'));
		Route::get('/report/jobs-margin/{start_date}/{end_date}', array('as' => 'jobs.margin.report', 'uses' => 'App\Controllers\ReportController@viewJobsMargin'));
		Route::get('/report/uninvoiced-jobs-labor/{start_date}/{end_date}', array('as' => 'uninvoiced.jobs.labor.report', 'uses' => 'App\Controllers\ReportController@viewUninvoicedJobsLabor'));
		Route::get('/report/employee-hours/{start_date}/{end_date}', array('as' => 'employee.hours.report', 'uses' => 'App\Controllers\ReportController@viewEmployeeHours'));
		// Job Follow Ups
		Route::get('/report/follow-ups/{start_date}/{end_date}/{user_id?}', array('as' => 'follow.ups.report', 'uses' => 'App\Controllers\ReportController@viewFollowUps'));

		// Job Notes
		Route::get('/report/follow-ups/{start_date}/{end_date}/{user_id?}', array('as' => 'follow.ups.report', 'uses' => 'App\Controllers\ReportController@viewFollowUps'));

		// Labor Costs
		Route::get('/report/labor-cost/{start_date}/{end_date}',  array('as' => 'labor.cost.report', 'uses' => 'App\Controllers\ReportController@viewLaborCosts'));

		//Search
		Route::get('search', array('as' => 'search', 'uses' => 'App\Controllers\SearchController@viewSearch'));

		// Revenue
		Route::post('/report/revenue/', array('as' => 'post.revenue', 'uses' => 'App\Controllers\ReportController@postRevenueByMonth'));
		Route::get('/report/revenue/{report_view}/{start_date}/{end_date}/{project_manager?}/{estimator?}', array('as' => 'revenue', 'uses' => 'App\Controllers\ReportController@viewRevenueByMonth'));

		//Gross Revenue
		Route::post('/report/gross-revenue/', array('as' => 'post.gross.revenue', 'uses' => 'App\Controllers\ReportController@postGrossRevenue'));
		Route::get('/report/gross-revenue/{start_date}/{end_date}', array('as' => 'gross.revenue', 'uses' => 'App\Controllers\ReportController@viewGrossRevenue'));

		// Dollars Bid and Performed
		Route::post('/report/dollars-bid-and-performed/', array('as' => 'post.dollars.bid.and.performed', 'uses' => 'App\Controllers\ReportController@postDollarsBidPerformed'));
		Route::get('/report/dollars-bid-and-performed/{report_view}/{start_date}/{end_date}', array('as' => 'dollars.bid.and.performed', 'uses' => 'App\Controllers\ReportController@viewDollarsBidPerformed'));

		// Jobs Bid and Performed
		Route::post('/report/jobs-bid-and-performed/', array('as' => 'post.jobs.bid.and.performed', 'uses' => 'App\Controllers\ReportController@postJobsBidPerformed'));
		Route::get('/report/jobs-bid-and-performed/{report_view}/{start_date}/{end_date}', array('as' => 'jobs.bid.and.performed', 'uses' => 'App\Controllers\ReportController@viewJobsBidPerformed'));

		// Bid and Completion Trend by Dollars
		Route::post('/report/bid-completion-by-dollars/', array('as' => 'post.bid.completion.by.dollars', 'uses' => 'App\Controllers\ReportController@postBidCompletionByDollars'));
		Route::get('/report/bid-completion-by-dollars/{report_view}/{start_date}/{end_date}', array('as' => 'bid.completion.by.dollars', 'uses' => 'App\Controllers\ReportController@viewBidCompletionByDollars'));

		// Bid and Completion Trend by Jobs
		Route::post('/report/bid-completion-by-jobs/', array('as' => 'post.bid.completion.by.jobs', 'uses' => 'App\Controllers\ReportController@postBidCompletionByJobs'));
		Route::get('/report/bid-completion-by-jobs/{report_view}/{start_date}/{end_date}', array('as' => 'bid.completion.by.jobs', 'uses' => 'App\Controllers\ReportController@viewBidCompletionByJobs'));

		// Bid Completion Histogram
		Route::post('/report/bid-completion-histogram/', array('as' => 'post.bid.completion.histogram', 'uses' => 'App\Controllers\ReportController@postBidCompletionHistogram'));
		Route::get('/report/bid-completion-histogram/{start_date}/{end_date}/{project_manager?}/{estimator?}', array('as' => 'bid.completion.histogram', 'uses' => 'App\Controllers\ReportController@viewBidCompletionHistogram'));

		// Importers
		Route::any('/job/{job_id}/bid/import', array('as' => 'import.bid.csv', 'uses' => 'App\Controllers\ImportController@bidCsvImport'));
		Route::get('job/{job_id}/{type?}', array('as' => 'view.job', 'uses' => 'App\Controllers\JobController@viewJob'));
		
		Route::post('job/bid/reload', array('as' => 'job.bid.reload', 'uses' => 'App\Controllers\Ajax\JobBidController@reload'));

		// New time reporting - View and make edits to timesheets

		Route::get('time/search', array('as' => 'search-time', 'uses' => 'App\Controllers\TimeSheetSearchController@viewReports'));
		Route::post('time/search', array('as' => 'run.search-time', 'uses' => 'App\Controllers\TimeSheetSearchController@showReports'));

		// get query
		Route::get('time/search/{start_date}/{end_date}/{job_id?}/{user_id?}', array('as' => 'show.timesheet.search-results', 'uses' => 'App\Controllers\TimeSheetSearchController@viewTimeSearchResults'));
		Route::post('update/time-sheets/', array('as' => 'update.time-sheets', 'uses' => 'App\Controllers\TimeSheetSearchController@updateTimeSheets'));

		// excel export
		Route::get('time/export/{start_date}/{end_date}/{job_id?}/{user_id?}', array('as' => 'export.timesheet.search-results', 'uses' => 'App\Controllers\TimeSheetSearchController@exportTimeSheetstoCSV'));


	});

	// Profiles
	Route::post('update/profile/{profile_id}', array('as' => 'update.profile', 'uses' => 'App\Controllers\ProfileController@updateProfile'));
	Route::get('admin/settings/profile/{profile_id}', array('as' => 'user.profile', 'uses' => 'App\Controllers\ProfileController@viewProfile'));
	Route::post('update/profile/roles/{profile_id}', array('as' => 'user.profile.roles', 'uses' => 'App\Controllers\ProfileController@updateProfileRoles'));

	//Timesheet
	Route::get('timesheet/add', array('as' => 'add.timesheet', 'uses' => 'App\Controllers\TimeSheetController@addTimesheet'));
	Route::get('timesheets', array('as' => 'view.timesheets', 'uses' => 'App\Controllers\TimeSheetController@viewTimesheets'));
	Route::post('timesheet/create/{approval?}', array('as' => 'create.timesheet', 'uses' => 'App\Controllers\TimeSheetController@createTimesheet'));
	Route::get('timesheet/view/{timesheet_id}', array('as' => 'view.timesheet', 'uses' => 'App\Controllers\TimeSheetController@viewTimesheet'));
	Route::get('timesheet/past/view/{user_id}', array('as' => 'view.past.timesheet', 'uses' => 'App\Controllers\TimeSheetController@viewPastTimesheet'));
	Route::post('timesheet/update/{timesheet_id}/{approval?}', array('as' => 'update.timesheet', 'uses' => 'App\Controllers\TimeSheetController@updateTimesheet'));
	Route::get('timesheet/approved/{timesheet_id}', array('as' => 'view.approved.timesheet', 'uses' => 'App\Controllers\TimeSheetController@viewApprovedTimesheet'));
	
	// Global Ajax Calls for Inputs
	Route::get('jobs-input-ajax/{q?}', array('as' => 'jobs.input.ajax', 'uses' => 'App\Controllers\JobController@viewJobsInputAJAX'));

	Route::get('users-input-ajax/{q?}', array('as' => 'users.input.ajax', 'uses' => 'App\Controllers\UserController@viewUsersInputAJAX'));

	Route::get('accounts-input-ajax/{q?}', array('as' => 'accounts.input.ajax', 'uses' => 'App\Controllers\AccountContactsController@viewAccountsInputAJAX'));

	Route::get('accounts-input-ajax/get/{q?}', array('as' => 'accounts.input.ajax', 'uses' => 'App\Controllers\AccountContactsController@viewAccountsInputAJAXsecond'));

	Route::get('accounts-details-ajax/post-key/', array('uses' => 'App\Controllers\AccountContactsController@ReturnSelectedAccountId'));
	Route::get('accounts-details-return/get/', array('uses' => 'App\Controllers\AccountContactsController@viewAccountsInputAJAXsecond'));


	Route::post('job/account/address/ajax/{account_id}', [
		'as'	=>	'job.account.address.ajax',
		'uses'  =>  'App\Controllers\Ajax\AccountController@address'
	]);
});