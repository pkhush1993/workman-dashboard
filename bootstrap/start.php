<?php

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| The first thing we will do is create a new Laravel application instance
| which serves as the "glue" for all the components of Laravel, and is
| the IoC container for the system binding all of the various parts.
|
*/

$app = new Illuminate\Foundation\Application;

/*
|--------------------------------------------------------------------------
| Detect The Application Environment
|--------------------------------------------------------------------------
|
| Laravel takes a dead simple approach to your application environments
| so you can just specify a machine name for the host that matches a
| given environment, then we will automatically detect it for you.
|
*/
$env = $app->detectEnvironment(function() use ($app) {

    // A bit of an explanation as to why this was altered and
    // what it does. Previously, we were using the default
    // \Illuminate\Foundation\EnviornmentDetector
    // class to determine which application .env to load. This has
    // been altered slightly to check against the HTTP hosts, mainly to
    // facilitate the multiple websites and single codebase served by the
    // demo server.

    // Please don't change this functionality, just add
    // to the $environment array, if you need to add a website.
    $environments = [
        'local'             =>      'panviso.app',
        'staging'           =>      'staging.workmansdashboard.com',
        'demo'              =>      'demo.workmansdashboard.com',
        'rainierrightofway' =>      'rainierrightofway.workmansdashboard.com',
        'rcscountertops'    =>      'rcscountertops.workmansdashboard.com',
        'diamondasphalt'    =>      'diamondasphalt.workmansdashboard.com',
        'accumark'          =>      'accumark.workmansdashboard.com',
        'trivalley'         =>      'trivalley.workmansdashboard.com',
        '7'                 =>      '7.workmansdashboard.com',
        '8'                 =>      '8.workmansdashboard.com',
        '9'                 =>      '9.workmansdashboard.com',
        '10'                =>      '10.workmansdashboard.com',
    ];
    $host = $app['request']->server('HTTP_HOST');

    // Search the above array for the environment config
    if ( in_array($host, $environments) ) {
        return array_search($host, $environments);
    }
         // Try to generate a key based on the host subdomain
     if ($subdomain = current(explode('.', $host))) {
         return $subdomain;
     }

     return 'production';

    // Fallback to the generic setup
    return $app->detectEnvironment($environments);
});

/*
|--------------------------------------------------------------------------
| Bind Paths
|--------------------------------------------------------------------------
|
| Here we are binding the paths configured in paths.php to the app. You
| should not be changing these here. If you need to change these you
| may do so within the paths.php file and they will be bound here.
|
*/

$app->bindInstallPaths(require __DIR__.'/paths.php');

/*
|--------------------------------------------------------------------------
| Load The Application
|--------------------------------------------------------------------------
|
| Here we will load this Illuminate application. We will keep this in a
| separate location so we can isolate the creation of an application
| from the actual running of the application with a given request.
|
*/

$framework = $app['path.base'].
                 '/vendor/laravel/framework/src';

require $framework.'/Illuminate/Foundation/start.php';

/*
|--------------------------------------------------------------------------
| Return The Application
|--------------------------------------------------------------------------
|
| This script returns the application instance. The instance is given to
| the calling script so we can separate the building of the instances
| from the actual running of the application and sending responses.
|
*/

return $app;
